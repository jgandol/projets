package control;

import modele.Personne;
import service.FacadePersonne;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "PersonneCtrl")
@SessionScoped
public class ControlerPersonne {
    @EJB
    private FacadePersonne fp;

    private String nom;
    private String prenom;

    private List<Personne> liste = new ArrayList<>();

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public List<Personne> getListe() {
        liste = fp.findAll();
        return liste;
    }
    public void setListe(List<Personne> liste) {
        this.liste = liste;
    }
}
