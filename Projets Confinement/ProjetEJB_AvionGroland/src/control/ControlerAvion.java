package control;

import modele.Avion;
import modele.Personne;
import modele.TypeAvion;
import service.FacadeAvion;
import service.FacadePersonne;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "avionCtrl")
@SessionScoped
public class ControlerAvion {
    @EJB
    private FacadeAvion fa;

    private String nom;
    private int capacite;

    private List<TypeAvion> listeType = new ArrayList<>();

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public int getCapacite() {
        return capacite;
    }
    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }
    public List<TypeAvion> getListeType() {
        listeType = fa.findAllType();
        return listeType;
    }
    public void setListeType(List<TypeAvion> listeType) {
        this.listeType = listeType;
    }

    public String doCreateTypeAvion(){
        fa.createTypeAvion(new TypeAvion(nom,capacite));
        this.nom = "";
        this.capacite = 0;
        return "avion";
    }

    public String doDeleteTypeAvion(TypeAvion ta){
        fa.deleteTypeAvion(ta);
        return null;
    }

}
