package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Ville")
public class Ville {
    private int idVille;
    private String nom;
    private Aeroport aeroport;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ville")
    public int getIdVille() {
        return idVille;
    }
    public void setIdVille(int idVille) {
        this.idVille = idVille;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }


    @ManyToOne
    @JoinColumn(name = "id_aeroport", referencedColumnName = "id_aeroport")
    public Aeroport getAeroport() {
        return aeroport;
    }
    public void setAeroport(Aeroport aeroport) {
        this.aeroport = aeroport;
    }
}
