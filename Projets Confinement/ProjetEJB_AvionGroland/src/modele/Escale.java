package modele;

import enumerated.TypeEscale;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "Escale")
public class Escale {
    private int idEscale;
    private TypeEscale typeEscale;
    private Time hArrivee;
    private Time hDepart;
    private Aeroport aeroportByIdAeroport;
    private Vol vol;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_escale")
    public int getIdEscale() {
        return idEscale;
    }
    public void setIdEscale(int idEscale) {
        this.idEscale = idEscale;
    }

    @Basic
    @Column(name = "type_escale")
    public TypeEscale getTypeEscale() {
        return typeEscale;
    }
    public void setTypeEscale(TypeEscale typeEscale) {
        this.typeEscale = typeEscale;
    }

    @Basic
    @Column(name = "h_arrivee")
    public Time gethArrivee() {
        return hArrivee;
    }
    public void sethArrivee(Time hArrivee) {
        this.hArrivee = hArrivee;
    }

    @Basic
    @Column(name = "h_depart")
    public Time gethDepart() {
        return hDepart;
    }
    public void sethDepart(Time hDepart) {
        this.hDepart = hDepart;
    }


    @ManyToOne
    @JoinColumn(name = "id_aeroport", referencedColumnName = "id_aeroport")
    public Aeroport getAeroportByIdAeroport() {
        return aeroportByIdAeroport;
    }
    public void setAeroportByIdAeroport(Aeroport aeroportByIdAeroport) {
        this.aeroportByIdAeroport = aeroportByIdAeroport;
    }

    @ManyToOne
    @JoinColumn(name = "id_vol", referencedColumnName = "id_vol")
    public Vol getVol() {
        return vol;
    }
    public void setVol(Vol vol) {
        this.vol = vol;
    }
}
