package modele;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "Vol")
public class Vol {
    private int idVol;
    private String resOuvert;
    private Timestamp dateDepart;
    private Timestamp dateArrive;
    private int numero;
    private String nom;
    private AffectationVol affectationVol;
    private Set<Escale> escales;
    private List<Reservation> reservations;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_vol")
    public int getIdVol() {
        return idVol;
    }
    public void setIdVol(int idVol) {
        this.idVol = idVol;
    }

    @Basic
    @Column(name = "res_ouvert")
    public String getResOuvert() {
        return resOuvert;
    }
    public void setResOuvert(String resOuvert) {
        this.resOuvert = resOuvert;
    }

    @Basic
    @Column(name = "dateDepart")
    public Timestamp getDateDepart() {
        return dateDepart;
    }
    public void setDateDepart(Timestamp dateDepart) {
        this.dateDepart = dateDepart;
    }

    @Basic
    @Column(name = "dateArrive")
    public Timestamp getDateArrive() {
        return dateArrive;
    }
    public void setDateArrive(Timestamp dateArrive) {
        this.dateArrive = dateArrive;
    }

    @Basic
    @Column(name = "numero")
    public int getNumero() {
        return numero;
    }
    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vol)) return false;
        Vol vol = (Vol) o;
        return numero == vol.numero &&
                nom.equals(vol.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numero, nom);
    }

    @OneToOne(mappedBy = "vol")
    public AffectationVol getAffectationVol() {
        return affectationVol;
    }
    public void setAffectationVol(AffectationVol affectationVol) {
        this.affectationVol = affectationVol;
    }

    @OneToMany(mappedBy = "vol")
    public Set<Escale> getEscales() {
        return escales;
    }
    public void setEscales(Set<Escale> escales) {
        this.escales = escales;
    }

    @OneToMany(mappedBy = "vol")
    public List<Reservation> getReservations() {
        return reservations;
    }
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
