package modele;

import enumerated.Sexe;
import enumerated.TypeClient;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Client")
public class Client {
    private int idClient;
    private String nom;
    private String adresse;
    private TypeClient typeClient;
    private Double chiffreAffaire;
    private String prenom;
    private Sexe sexe;
    private List<Reservation> reservations;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_client")
    public int getIdClient() {
        return idClient;
    }
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "adresse")
    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "type_client")
    public TypeClient getTypeClient() {
        return typeClient;
    }
    public void setTypeClient(TypeClient typeClient) {
        this.typeClient = typeClient;
    }

    @Basic
    @Column(name = "chiffreAffaire")
    public Double getChiffreAffaire() {
        return chiffreAffaire;
    }
    public void setChiffreAffaire(Double chiffreAffaire) {
        this.chiffreAffaire = chiffreAffaire;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "sexe")
    public Sexe getSexe() {
        return sexe;
    }
    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    @OneToMany(mappedBy = "client")
    public List<Reservation> getReservations() {
        return reservations;
    }
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
