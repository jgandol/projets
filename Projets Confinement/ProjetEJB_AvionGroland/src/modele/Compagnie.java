package modele;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Compagnie")
public class Compagnie {
    private int idCompagnie;
    private String libelle;
    private List<Avion> avions;
    private List<RolePilote> rolePilotes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_compagnie")
    public int getIdCompagnie() {
        return idCompagnie;
    }
    public void setIdCompagnie(int idCompagnie) {
        this.idCompagnie = idCompagnie;
    }

    @Basic
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @OneToMany(mappedBy = "compagnie")
    public List<Avion> getAvions() {
        return avions;
    }
    public void setAvions(List<Avion> avions) {
        this.avions = avions;
    }

    @OneToMany(mappedBy = "compagnie")
    public List<RolePilote> getRolePilotes() {
        return rolePilotes;
    }
    public void setRolePilotes(List<RolePilote> rolePilotes) {
        this.rolePilotes = rolePilotes;
    }
}
