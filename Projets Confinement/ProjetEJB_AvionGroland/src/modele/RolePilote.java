package modele;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "RolePilote")
public class RolePilote {
    private int idRolePilote;
    private Date dateDebut;
    private Date dateFin;
    private Double salaire;
    private List<AffectationVol> affectationVols;
    private RolePersonne rolePersonne;
    private Compagnie compagnie;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rolePilote")
    public int getIdRolePilote() {
        return idRolePilote;
    }
    public void setIdRolePilote(int idRolePilote) {
        this.idRolePilote = idRolePilote;
    }

    @Basic
    @Column(name = "date_debut")
    public Date getDateDebut() {
        return dateDebut;
    }
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    @Basic
    @Column(name = "date_fin")
    public Date getDateFin() {
        return dateFin;
    }
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    @Basic
    @Column(name = "salaire")
    public Double getSalaire() {
        return salaire;
    }
    public void setSalaire(Double salaire) {
        this.salaire = salaire;
    }


    @OneToMany(mappedBy = "rolePilote")
    public List<AffectationVol> getAffectationVols() {
        return affectationVols;
    }
    public void setAffectationVols(List<AffectationVol> affectationVols) {
        this.affectationVols = affectationVols;
    }

    @ManyToOne
    @JoinColumn(name = "id_rolePersonne", referencedColumnName = "id_rolePersonne")
    public RolePersonne getRolePersonne() {
        return rolePersonne;
    }
    public void setRolePersonne(RolePersonne rolePersonne) {
        this.rolePersonne = rolePersonne;
    }

    @ManyToOne
    @JoinColumn(name = "id_compagnie", referencedColumnName = "id_compagnie")
    public Compagnie getCompagnie() {
        return compagnie;
    }
    public void setCompagnie(Compagnie compagnie) {
        this.compagnie = compagnie;
    }
}
