package modele;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Avion")
public class Avion {
    private int idAvion;
    private Timestamp miseEnFonction;
    private List<AffectationVol> affectationVols;
    private TypeAvion typeAvion;
    private Compagnie compagnie;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_avion")
    public int getIdAvion() {
        return idAvion;
    }
    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    @Basic
    @Column(name = "miseEnFonction")
    public Timestamp getMiseEnFonction() {
        return miseEnFonction;
    }
    public void setMiseEnFonction(Timestamp miseEnFonction) {
        this.miseEnFonction = miseEnFonction;
    }


    @OneToMany(mappedBy = "avion")
    public List<AffectationVol> getAffectationVols() {
        return affectationVols;
    }
    public void setAffectationVols(List<AffectationVol> affectationVols) {
        this.affectationVols = affectationVols;
    }

    @ManyToOne
    @JoinColumn(name = "id_type_avion", referencedColumnName = "id_type_avion")
    public TypeAvion getTypeAvion() {
        return typeAvion;
    }
    public void setTypeAvion(TypeAvion typeAvion) {
        this.typeAvion = typeAvion;
    }

    @ManyToOne
    @JoinColumn(name = "id_compagnie", referencedColumnName = "id_compagnie")
    public Compagnie getCompagnie() {
        return compagnie;
    }
    public void setCompagnie(Compagnie compagnie) {
        this.compagnie = compagnie;
    }
}
