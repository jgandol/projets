package modele;

import enumerated.TypeRole;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "RolePersonne")
public class RolePersonne {
    private int idRolePersonne;
    private String archiver;
    private TypeRole typeRole;
    private List<RolePassager> rolePassagers;
    private Personne personne;
    private List<RolePilote> rolePilotes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rolePersonne")
    public int getIdRolePersonne() {
        return idRolePersonne;
    }
    public void setIdRolePersonne(int idRolePersonne) {
        this.idRolePersonne = idRolePersonne;
    }

    @Basic
    @Column(name = "archiver")
    public String getArchiver() {
        return archiver;
    }
    public void setArchiver(String archiver) {
        this.archiver = archiver;
    }

    @Basic
    @Column(name = "type_role")
    public TypeRole getTypeRole() {
        return typeRole;
    }
    public void setTypeRole(TypeRole typeRole) {
        this.typeRole = typeRole;
    }


    @OneToMany(mappedBy = "rolePersonne")
    public List<RolePassager> getRolePassagers() {
        return rolePassagers;
    }
    public void setRolePassagers(List<RolePassager> rolePassagers) {
        this.rolePassagers = rolePassagers;
    }

    @ManyToOne
    @JoinColumn(name = "id_pers", referencedColumnName = "id_pers", nullable = false)
    public Personne getPersonne() {
        return personne;
    }
    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @OneToMany(mappedBy = "rolePersonne")
    public List<RolePilote> getRolePilotes() {
        return rolePilotes;
    }
    public void setRolePilotes(List<RolePilote> rolePilotes) {
        this.rolePilotes = rolePilotes;
    }
}
