package modele;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Aeroport")
public class Aeroport {
    private int idAeroport;
    private String nom;
    private List<Escale> escales;
    private List<Ville> villes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_aeroport")
    public int getIdAeroport() {
        return idAeroport;
    }
    public void setIdAeroport(int idAeroport) {
        this.idAeroport = idAeroport;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @OneToMany(mappedBy = "aeroportByIdAeroport")
    public List<Escale> getEscales() {
        return escales;
    }
    public void setEscales(List<Escale> escales) {
        this.escales = escales;
    }

    @OneToMany(mappedBy = "aeroport")
    public List<Ville> getVilles() {
        return villes;
    }
    public void setVilles(List<Ville> villes) {
        this.villes = villes;
    }
}
