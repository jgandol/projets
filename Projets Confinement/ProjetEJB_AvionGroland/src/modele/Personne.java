package modele;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Personne")
@NamedQueries({
        @NamedQuery(name = "Personne.findAll", query = "select p from Personne p order by p.nom")
})
public class Personne {
    private int idPers;
    private String nom;
    private String prenom;
    private List<RolePersonne> rolePersonnes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pers")
    public int getIdPers() {
        return idPers;
    }
    public void setIdPers(int idPers) {
        this.idPers = idPers;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    @OneToMany(mappedBy = "personne")
    public List<RolePersonne> getRolePersonnes() {
        return rolePersonnes;
    }
    public void setRolePersonnes(List<RolePersonne> rolePersonnes) {
        this.rolePersonnes = rolePersonnes;
    }
}
