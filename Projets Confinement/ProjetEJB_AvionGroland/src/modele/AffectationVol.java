package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "AffectationVol")
public class AffectationVol {
    private int idAffectationVol;
    private Avion avion;
    private RolePilote rolePilote;
    private Vol vol;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_affectation_vol")
    public int getIdAffectationVol() {
        return idAffectationVol;
    }
    public void setIdAffectationVol(int idAffectationVol) {
        this.idAffectationVol = idAffectationVol;
    }

    @ManyToOne
    @JoinColumn(name = "id_avion", referencedColumnName = "id_avion")
    public Avion getAvion() {
        return avion;
    }
    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    @ManyToOne
    @JoinColumn(name = "id_rolePilote", referencedColumnName = "id_rolePilote")
    public RolePilote getRolePilote() {
        return rolePilote;
    }
    public void setRolePilote(RolePilote rolePilote) {
        this.rolePilote = rolePilote;
    }

    @OneToOne
    @JoinColumn(name = "id_vol", referencedColumnName = "id_vol")
    public Vol getVol() {
        return vol;
    }
    public void setVol(Vol vol) {
        this.vol = vol;
    }
}
