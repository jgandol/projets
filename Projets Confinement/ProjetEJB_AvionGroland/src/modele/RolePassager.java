package modele;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "RolePassager")
public class RolePassager {
    private int idRolePassager;
    private List<Reservation> reservations;
    private RolePersonne rolePersonne;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rolePassager")
    public int getIdRolePassager() {
        return idRolePassager;
    }
    public void setIdRolePassager(int idRolePassager) {
        this.idRolePassager = idRolePassager;
    }

    @OneToMany(mappedBy = "rolePassager")
    public List<Reservation> getReservations() {
        return reservations;
    }
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    @ManyToOne
    @JoinColumn(name = "id_rolePersonne", referencedColumnName = "id_rolePersonne")
    public RolePersonne getRolePersonne() {
        return rolePersonne;
    }
    public void setRolePersonne(RolePersonne rolePersonne) {
        this.rolePersonne = rolePersonne;
    }
}
