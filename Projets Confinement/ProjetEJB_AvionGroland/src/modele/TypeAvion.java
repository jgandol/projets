package modele;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Type_Avion")
@NamedQueries({
        @NamedQuery(name = "TypeAvion.findAll", query = "Select ta from TypeAvion ta")
})
public class TypeAvion {
    private int idTypeAvion;
    private String nom;
    private int capacite;
    private List<Avion> avions;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_type_avion")
    public int getIdTypeAvion() {
        return idTypeAvion;
    }
    public void setIdTypeAvion(int idTypeAvion) {
        this.idTypeAvion = idTypeAvion;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "capacite")
    public int getCapacite() {
        return capacite;
    }
    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public TypeAvion(String nom, int capacite) {
        this.nom = nom;
        this.capacite = capacite;
    }

    public TypeAvion() {
    }

    @OneToMany(mappedBy = "typeAvion")
    public List<Avion> getAvions() {
        return avions;
    }
    public void setAvions(List<Avion> avions) {
        this.avions = avions;
    }
}
