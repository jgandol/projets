package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Reservation")
public class Reservation {
    private int idRes;
    private String confirmee;
    private String annulee;
    private Double prix;
    private RolePassager rolePassager;
    private Client client;
    private Vol vol;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_res")
    public int getIdRes() {
        return idRes;
    }
    public void setIdRes(int idRes) {
        this.idRes = idRes;
    }

    @Basic
    @Column(name = "confirmee")
    public String getConfirmee() {
        return confirmee;
    }
    public void setConfirmee(String confirmee) {
        this.confirmee = confirmee;
    }

    @Basic
    @Column(name = "annulee")
    public String getAnnulee() {
        return annulee;
    }
    public void setAnnulee(String annulee) {
        this.annulee = annulee;
    }

    @Basic
    @Column(name = "prix")
    public Double getPrix() {
        return prix;
    }
    public void setPrix(Double prix) {
        this.prix = prix;
    }


    @ManyToOne
    @JoinColumn(name = "id_rolePassager", referencedColumnName = "id_rolePassager")
    public RolePassager getRolePassager() {
        return rolePassager;
    }
    public void setRolePassager(RolePassager rolePassager) {
        this.rolePassager = rolePassager;
    }

    @ManyToOne
    @JoinColumn(name = "id_client", referencedColumnName = "id_client")
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }

    @ManyToOne
    @JoinColumn(name = "id_vol", referencedColumnName = "id_vol")
    public Vol getVol() {
        return vol;
    }
    public void setVol(Vol vol) {
        this.vol = vol;
    }
}
