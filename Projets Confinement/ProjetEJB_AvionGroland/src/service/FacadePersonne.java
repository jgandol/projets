package service;

import modele.Personne;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateful
@LocalBean
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class FacadePersonne {
    @PersistenceContext(unitName = "GROLAND_PU")
    private EntityManager em;

    public List<Personne> findAll() {
        return em.createNamedQuery("Personne.findAll", Personne.class).getResultList();
    }
}
