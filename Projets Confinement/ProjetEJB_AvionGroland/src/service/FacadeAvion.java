package service;

import modele.Avion;
import modele.Personne;
import modele.TypeAvion;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transaction;
import java.lang.reflect.Type;
import java.util.List;

@Stateful
@LocalBean
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class FacadeAvion {
    @PersistenceContext(unitName = "GROLAND_PU")
    private EntityManager em;

    public List<TypeAvion> findAllType(){
        return em.createNamedQuery("TypeAvion.findAll", TypeAvion.class).getResultList();
    }

    public TypeAvion findTypeById(int id){
        return em.find(TypeAvion.class,id);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void createTypeAvion(TypeAvion type){
        em.persist(type);
        em.flush();
    }
    public void updateTypeAvion(TypeAvion type){
        type = em.merge(type);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deleteTypeAvion(TypeAvion type){
        em.remove(em.merge(type));
    }




}
