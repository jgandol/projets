drop table if exists Reservation;
drop table if exists AffectationVol;
drop table if exists Escale;
drop table if exists Avion;
drop table if exists RolePilote;
drop table if exists RolePassager;
drop table if exists RolePersonne;
drop table if exists Personne;
drop table if exists Type_Avion;
drop table if exists Ville;
drop table if exists Aeroport;
drop table if exists Compagnie;
drop table if exists Client;
drop table if exists Vol;

create table Compagnie(
	id_compagnie int primary key not null auto_increment,
	libelle varchar(50) not null
);

create table Personne(
	id_pers int primary key not null auto_increment,
	nom varchar(30),
	prenom varchar(30)
);

create table RolePersonne(
	id_rolePersonne int primary key not null auto_increment,
	archiver varchar(4) not null default 'FAUX',
	id_pers int not null,
	constraint FK_RolePersonne_Personne foreign key (id_pers) references Personne(id_pers)
);

create table RolePassager(
	id_rolePassager int primary key not null auto_increment,
	id_rolePersonne int not null,
	constraint FK_RolePassager_RolePersonne foreign key (id_rolePersonne) references RolePersonne(id_rolePersonne)
);

create table RolePilote(
	id_rolePilote int primary key not null auto_increment,
	id_rolePersonne int not null,
	salaire double,
	id_compagnie int not null,
	constraint FK_RolePilote_RolePersonne foreign key (id_rolePersonne) references RolePersonne(id_rolePersonne),
	constraint FK_RolePilote_Compagnie foreign key (id_compagnie) references Compagnie(id_compagnie)
);

create table Type_Avion(
	id_type_avion int primary key not null auto_increment,
	nom varchar(50) not null,
	capacite int not null
);

create table Avion(
	id_avion int primary key not null auto_increment,
	id_type_avion int not null,
	id_compagnie int not null,
	miseEnFonction timestamp default current_timestamp,
	constraint FK_Avion_TypeAvion foreign key (id_type_avion) references Type_Avion (id_type_avion),
	constraint FK_Avion_Compagnie foreign key (id_compagnie) references Compagnie (id_compagnie)
);

create table Client(
	id_client int primary key not null auto_increment,
	nom varchar(50) not null,
	adresse varchar(50) not null,
	type_client ENUM('moral','physique') not null,
	chiffreAffaire double,
	prenom varchar(30),
	sexe ENUM('homme','femme','autre')
);

create table Vol(
	id_vol int primary key not null auto_increment,
	res_ouvert varchar(4) default 'vrai',
	dateDepart timestamp not null default current_timestamp,
	dateArrive timestamp not null default current_timestamp,
	numero int not null,
	nom varchar(50)
);

create table Aeroport(
	id_aeroport int primary key not null auto_increment,
	nom varchar(50) not null
);

create table Ville(
	id_ville int primary key not null auto_increment,
	nom varchar(50) not null,
	id_aeroport int not null,
	constraint FK_Ville_Aeroport foreign key (id_aeroport) references Aeroport(id_aeroport)
);

create table Escale(
	id_escale int primary key not null auto_increment,
	type_escale ENUM('depart','arrivee','escale') not null,
	h_arrivee time not null,
	h_depart time not null,
	id_aeroport int not null,
	id_vol int not null,
	constraint FK_Escale_Aeroport foreign key (id_aeroport) references Aeroport(id_aeroport),
	constraint FK_Escale_Vol foreign key (id_vol) references Vol(id_vol)
);

create table AffectationVol(
	id_affectation_vol int primary key not null,
	id_avion int not null,
	id_rolePilote int not null,
	id_vol int not null,
	constraint FK_Affect_Avion foreign key (id_avion ) references Avion(id_avion),
	constraint FK_Affect_Pilote foreign key (id_rolePilote) references RolePilote(id_rolePilote),
	constraint FK_Affect_vol foreign key (id_vol) references Vol(id_vol)
);

create table Reservation(
	id_res int primary key not null auto_increment,
	confirmee varchar(4) default 'faux',
	annulee varchar(4) default 'faux',
	prix double,
	id_rolePassager int not null,
	id_client int not null,
	id_vol int not null,
	constraint FK_Reservation_Passager foreign key (id_rolePassager) references RolePassager(id_rolePassager),
	constraint FK_Reservation_Client foreign key (id_client) references Client(id_client),
	constraint FK_Reservation_Vol foreign key (id_vol) references Vol(id_vol)
)






