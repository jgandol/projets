package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Personne")
@NamedQueries({
        @NamedQuery(name = "Personne.findAll", query = "select p from Personne p")
})
public class Personne {
    private int idPers;
    private String nom;
    private String prenom;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pers")
    public int getIdPers() {
        return idPers;
    }
    public void setIdPers(int idPers) {
        this.idPers = idPers;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}
