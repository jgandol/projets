package modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Participant")
public class Participant {
    private int idPart;
    private String nom;
    private String prenom;
    private int nSemaine;
    private int nJour;
    private Double depenses;
    private List<ActiviteParticipant> activiteParticipants;
    private Restauration restauration;
    private Reservation reservation;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_part")
    public int getIdPart() {
        return idPart;
    }
    public void setIdPart(int idPart) {
        this.idPart = idPart;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "nSemaine")
    public int getnSemaine() {
        return nSemaine;
    }
    public void setnSemaine(int nSemaine) {
        this.nSemaine = nSemaine;
    }

    @Basic
    @Column(name = "nJour")
    public int getnJour() {
        return nJour;
    }
    public void setnJour(int nJour) {
        this.nJour = nJour;
    }

    @Basic
    @Column(name = "depenses")
    public Double getDepenses() {
        return depenses;
    }
    public void setDepenses(Double depenses) {
        this.depenses = depenses;
    }


    @OneToMany(mappedBy = "participant")
    public List<ActiviteParticipant> getActiviteParticipants() {
        return activiteParticipants;
    }
    public void setActiviteParticipants(List<ActiviteParticipant> activiteParticipants) {
        this.activiteParticipants = activiteParticipants;
    }

    @ManyToOne
    @JoinColumn(name = "id_rest", referencedColumnName = "id_rest", nullable = false)
    public Restauration getRestauration() {
        return restauration;
    }
    public void setRestauration(Restauration restauration) {
        this.restauration = restauration;
    }

    @ManyToOne
    @JoinColumn(name = "id_res", referencedColumnName = "id_res", nullable = false)
    public Reservation getReservation() {
        return reservation;
    }
    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }
}
