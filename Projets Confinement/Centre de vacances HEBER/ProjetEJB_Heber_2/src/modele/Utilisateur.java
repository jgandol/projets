package modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Utilisateur")
@NamedQueries({
        @NamedQuery(name = "Utilisateur.findAll", query = "select u from Utilisateur u group by u.nom,u.prenom"),
        @NamedQuery(name = "Utilisateur.findByNomPrenom", query = "select u from Utilisateur u where u.nom = :nom and u.prenom = :prenom")
})
public class Utilisateur {
    private int idUtil;
    private String nom;
    private String prenom;
    private String fonction;
    private List<Reservation> reservations;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_util")
    public int getIdUtil() {
        return idUtil;
    }
    public void setIdUtil(int idUtil) {
        this.idUtil = idUtil;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "fonction")
    public String getFonction() {
        return fonction;
    }
    public void setFonction(String fonction) {
        this.fonction = fonction;
    }


    @OneToMany(mappedBy = "utilisateur")
    public List<Reservation> getReservations() {
        return reservations;
    }
    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

}
