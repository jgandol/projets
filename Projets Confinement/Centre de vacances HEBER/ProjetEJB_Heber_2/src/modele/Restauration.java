package modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Restauration")
public class Restauration {
    private int idRest;
    private String type;
    private Double prix;
    private List<Participant> participants;

    @Id
    @Column(name = "id_rest")
    public int getIdRest() {
        return idRest;
    }
    public void setIdRest(int idRest) {
        this.idRest = idRest;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "prix")
    public Double getPrix() {
        return prix;
    }
    public void setPrix(Double prix) {
        this.prix = prix;
    }



    @OneToMany(mappedBy = "restauration")
    public List<Participant> getParticipants() {
        return participants;
    }
    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }
}
