package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Activite_Participant")
public class ActiviteParticipant {
    private int idActivPart;
    private Participant participant;
    private Activite activite;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_activ_part")
    public int getIdActivPart() {
        return idActivPart;
    }
    public void setIdActivPart(int idActivPart) {
        this.idActivPart = idActivPart;
    }

    @ManyToOne
    @JoinColumn(name = "id_part", referencedColumnName = "id_part", nullable = false)
    public Participant getParticipant() {
        return participant;
    }
    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    @OneToOne
    @JoinColumn(name = "id_activ", referencedColumnName = "id_activ", nullable = false)
    public Activite getActivite() {
        return activite;
    }
    public void setActivite(Activite activite) {
        this.activite = activite;
    }
}
