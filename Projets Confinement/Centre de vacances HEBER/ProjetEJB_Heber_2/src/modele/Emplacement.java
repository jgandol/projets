package modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Emplacement")
public class Emplacement {
    private int idEmp;
    private String type;
    private int nbPersType;
    private int numLocalisation;
    private String adresse;
    private Double prix;
    private List<ReservationEmplacement> reservationEmplacements;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_emp")
    public int getIdEmp() {
        return idEmp;
    }
    public void setIdEmp(int idEmp) {
        this.idEmp = idEmp;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "nb_pers_type")
    public int getNbPersType() {
        return nbPersType;
    }
    public void setNbPersType(int nbPersType) {
        this.nbPersType = nbPersType;
    }

    @Basic
    @Column(name = "num_localisation")
    public int getNumLocalisation() {
        return numLocalisation;
    }
    public void setNumLocalisation(int numLocalisation) {
        this.numLocalisation = numLocalisation;
    }

    @Basic
    @Column(name = "adresse")
    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "prix")
    public Double getPrix() {
        return prix;
    }
    public void setPrix(Double prix) {
        this.prix = prix;
    }


    @OneToMany(mappedBy = "emplacement")
    public List<ReservationEmplacement> getReservationEmplacements() {
        return reservationEmplacements;
    }
    public void setReservationEmplacements(List<ReservationEmplacement> reservationEmplacements) {
        this.reservationEmplacements = reservationEmplacements;
    }
}
