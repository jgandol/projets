package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Reservation_Emplacement")
public class ReservationEmplacement {
    private int idResEmp;
    private Reservation reservation;
    private Emplacement emplacement;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_res_emp")
    public int getIdResEmp() {
        return idResEmp;
    }
    public void setIdResEmp(int idResEmp) {
        this.idResEmp = idResEmp;
    }


    @ManyToOne
    @JoinColumn(name = "id_res", referencedColumnName = "id_res", nullable = false)
    public Reservation getReservation() {
        return reservation;
    }
    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    @ManyToOne
    @JoinColumn(name = "id_emp", referencedColumnName = "id_emp", nullable = false)
    public Emplacement getEmplacement() {
        return emplacement;
    }
    public void setEmplacement(Emplacement emplacement) {
        this.emplacement = emplacement;
    }
}
