package control;


import com.sun.org.apache.xml.internal.security.c14n.implementations.UtfHelpper;
import modele.Utilisateur;
import service.HeberFacade;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named("loginCtrl")
@SessionScoped
public class ConnexionControl implements Serializable {

    @EJB
    private HeberFacade facade;

    public List<Utilisateur> getListeUser(){
        System.out.println(facade.findAllUser());
        return facade.findAllUser();
    }

    public String doGetUser(){
        String prenom = facade.findUserByNomPrenom("Jeremie","Gandolfo").getPrenom();
        String nom = facade.findUserByNomPrenom("Jeremie","Gandolfo").getNom();
        System.out.println(prenom + " + " + nom);
        return prenom + " " + nom + " Jgandol";
    }


}