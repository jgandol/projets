package popschool.reactor_book_server.controller;

import org.springframework.web.bind.annotation.*;
import popschool.reactor_book_server.model.Book;
import popschool.reactor_book_server.service.BookService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class BookController {

    private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(value = "/books")
    public Flux<Book> getAllBooks() {
        return bookService.findAll();
    }

    @GetMapping(value = "/book/{id}")
    public Mono<Book> getBookById(@PathVariable("id") String id) {
        return bookService.findById(id);
    }

    @PostMapping(value = "/book")
    public Mono<Book> createBook(@RequestBody() Book book) {
        return bookService.save(book);
    }

    @PutMapping(value = "/book")
    public Mono<Book> updateBook(@RequestBody() Book book) {
        return bookService.save(book);
    }

    @DeleteMapping(value = "/book/{id}")
    public Mono<Void> updateBook(@PathVariable("id") String id) {
        return bookService.deleteById(id);
    }

    @GetMapping(value = "/books/author/{author}")
    public Flux<Book> getBooksByAuthor(@PathVariable("author") String author){
        return bookService.findByAuthor(author);
    }


}
