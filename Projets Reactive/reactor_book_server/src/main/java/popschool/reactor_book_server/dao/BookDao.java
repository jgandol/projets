package popschool.reactor_book_server.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import popschool.reactor_book_server.model.Book;
import reactor.core.publisher.Flux;

import java.util.List;

public interface BookDao extends ReactiveMongoRepository<Book,String> {
    Flux<Book> findByAuthorLike(String author);
}
