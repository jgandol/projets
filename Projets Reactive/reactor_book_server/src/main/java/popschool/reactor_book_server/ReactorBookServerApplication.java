package popschool.reactor_book_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactorBookServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactorBookServerApplication.class, args);
    }

}
