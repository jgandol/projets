package popschool.reactor_book_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.reactor_book_server.dao.BookDao;
import popschool.reactor_book_server.model.Book;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookDao bookDao;

    @Override
    public Mono<Book> findById(String id) {
        return bookDao.findById(id);
    }

    @Override
    public Flux<Book> findAll() {
        return bookDao.findAll();
    }

    @Override
    public Mono<Book> save(Book book) {
        return bookDao.save(book);
    }

    @Override
    public Mono<Void> deleteById(String id) {
        return bookDao.deleteById(id);
    }

    @Override
    public Flux<Book> findByAuthor(String author) {
        return bookDao.findByAuthorLike("*" + author + "*");
    }
}
