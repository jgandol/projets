package popschool.reactor_book_server.service;

import popschool.reactor_book_server.model.Book;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BookService {

    Mono<Book> findById(String id);
    Flux<Book> findAll();
    Mono<Book> save(Book book);
    Mono<Void> deleteById(String id);
    Flux<Book> findByAuthor(String author);
}
