package popschool.reactor_book_client.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import popschool.reactor_book_client.model.Book;
import popschool.reactor_book_client.service.Service;
import reactor.core.publisher.Flux;

import javax.websocket.server.PathParam;
import java.util.List;

@Controller
public class BookControl {
    @Autowired
    private Service service;

    @GetMapping(path = "/listBooks")
    public String getAllBooks(Model model){
        model.addAttribute("books", service.findAll());
        return "ensBooks";
    }

    @GetMapping(path = "/createBooks")
    public String redirectCreateBook(Model model){
        Book b = new Book();
        model.addAttribute("boukin", b);
        return "createBook";
    }

    @PostMapping(path = "/createBooks")
    public String createBook(Model model, @RequestParam("title") String title, @RequestParam("author") String author){
        Book book = new Book(title,author);
        System.out.println(book);
        service.create(book);
        model.addAttribute("books", service.findAll());
        return "ensBooks";
    }

    @GetMapping(path = "/listBooksOfAuthor")
    public String findByAuthor(Model model, @RequestParam("findauthor") String author){
//        System.out.println(author);
        model.addAttribute("books",service.findByAuthor(author));
        return "ensBooksOfAuthor";
    }

    @GetMapping(path = "/deleteBook")
    public String deleteById(Model model, @PathParam("id") String id){
        System.out.println(id);
        service.deleteById(id);
        model.addAttribute("books",service.findAll());
        return "ensBooks";
    }

    @GetMapping(path = "/updateBook")
    public String redirectUpdateById(Model model, @PathParam("id") String id){
//        System.out.println(id);
        model.addAttribute("boukin",service.findById(id).get());
        return "updateBook";
    }

    @PostMapping(path = "/updateBook")
    public String updateById(Model model, @RequestParam("title") String title, @RequestParam("author") String author, @PathParam("id") String id){
        Book b = service.findById(id).get();
        b.setId(id);b.setAuthor(author);b.setTitle(title);
        service.updateById(b);
        model.addAttribute("books",service.findAll());
        return "ensBooks";
    }
}
