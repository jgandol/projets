package popschool.reactor_book_client.service;

import ch.qos.logback.core.net.server.Client;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import popschool.reactor_book_client.model.Book;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class Service {
    WebClient client = WebClient.create("http://localhost:8080");

    public List<Book> findAll() {
        Flux<Book> response = client.get().uri("/books").accept(MediaType.APPLICATION_JSON).retrieve()
                .bodyToFlux(Book.class);
        List<Book> books = response.collectList().block();
        return books;
    }

    public Optional<Book> findById(String id) {
        Mono<Book> response = client.get().uri("/book/{id}", id).accept(MediaType.APPLICATION_JSON).retrieve()
                .bodyToMono(Book.class);
        return response.blockOptional();
    }

    public Book create(Book newBook) {
        Mono<Book> bookMono = Mono.just(newBook);
        Mono<Book> response = client.post().uri("/book").accept(MediaType.APPLICATION_JSON).body(bookMono, Book.class).retrieve()
                .bodyToMono(Book.class);
        return response.block();
    }

    public void deleteById(String id) {
        Mono<ClientResponse> clientRes = client.delete().uri("/book/{id}", id)
                .accept(MediaType.APPLICATION_JSON).exchange();
        ClientResponse cl = clientRes.block();
    }

    public List<Book> findByAuthor(String author) {
        Flux<Book> response = client.get().uri("/books/author/{author}", author).accept(MediaType.APPLICATION_JSON).retrieve()
                .bodyToFlux(Book.class);
        List<Book> books = response.collectList().block();
        return books;
    }

    public Book updateById(Book book) {
        Mono<Book> newBookMono = Mono.just(book);

        Mono<Book> response2 = client.put().uri("/book", book).accept(MediaType.APPLICATION_JSON)
                .body(newBookMono, Book.class).retrieve().bodyToMono(Book.class);
        System.out.println(response2.block());
        return response2.block();
    }
}
