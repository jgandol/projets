package popschool.reactor_book_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import popschool.reactor_book_client.model.Book;
import reactor.core.publisher.Flux;

import java.util.List;

@SpringBootApplication
public class ReactorBookClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactorBookClientApplication.class, args);


    }

}
