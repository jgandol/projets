package popschool.reactor_book_client;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import popschool.reactor_book_client.model.Book;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class ReactorBookClientApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void httpGetTest() {
        WebClient client = WebClient.create("http://localhost:8080");
        Flux<Book> response = client.get().uri("/books").accept(MediaType.APPLICATION_JSON)
                .retrieve().bodyToFlux(Book.class);
        List<Book> books = response.collectList().block();
        assertTrue(books.size() > 0);

        for (Book book : books) {
            System.out.println(book);
        }
    }

    @Test
    public void httpPostTest() {
        Book newBook = new Book("Fistbook", "Mark Zucchero");

        WebClient client = WebClient.create("http://localhost:8080");
        Mono<Book> newBookMono = Mono.just(newBook);

        Mono<Book> response = client.post().uri("/book").accept(MediaType.APPLICATION_JSON)
                .body(newBookMono, Book.class).retrieve().bodyToMono(Book.class);
        Book createdBook = response.block();
        System.out.println(createdBook);
        assert createdBook != null;
        assertEquals(newBook.getAuthor(), createdBook.getAuthor());
        assertEquals(newBook.getTitle(), createdBook.getTitle());

    }

    @Test
    public void httpPutTest() {
        // Création du livre
        Book newBook = new Book("Spring, c'est pas qu'un verbe en anglais", "Jean-Michel Programmer");
        WebClient client = WebClient.create("http://localhost:8080");

        Mono<Book> newBookMono =  Mono.just(newBook);

        Mono<Book> response = client.post().uri("/book")
                .accept(MediaType.APPLICATION_JSON)
                .body(newBookMono, Book.class)
                .retrieve().bodyToMono(Book.class);

        Book createdBook = response.block();
        System.out.println(createdBook);

        //Update du livre

        createdBook.setAuthor("Jean-Louis Programmer");

        Mono<Book> createdBookMono =  Mono.just(createdBook);

        Mono<Book> response2 = client.put().uri("/book", createdBook).accept(MediaType.APPLICATION_JSON)
                .body(createdBookMono, Book.class).retrieve().bodyToMono(Book.class);

        Book updatedBook = response2.block();

        System.out.println(updatedBook);
        /*assert createdBook != null;
        assertEquals(newBook.getAuthor(), createdBook.getAuthor());
        assertEquals(newBook.getTitle(), createdBook.getTitle());*/

    }

    @Test
    public void httpDeleteTest() {
        // Création du livre
        WebClient client = WebClient.create("http://localhost:8080");
        Book mstSpringBook = new Book("Spring, c'est pas qu'un verbe en anglais", "Jean-Michel Programmer");
        Mono<Book> mstSpringMono =  Mono.just(mstSpringBook);

        Mono<Book> response = client.post().uri("/book")
                .accept(MediaType.APPLICATION_JSON)
                .body(mstSpringMono, Book.class)
                .retrieve().bodyToMono(Book.class);

        mstSpringBook = response.block();
        assertTrue(mstSpringBook.getId().length()>0);
        System.out.println(mstSpringBook);

        //Suppression du livre
        Mono<ClientResponse> clientRes = client.delete().uri("/book/{id}",mstSpringBook.getId())
                .accept(MediaType.APPLICATION_JSON).exchange();

        assertEquals(HttpStatus.OK,clientRes.block().statusCode());
    }


    @Test
    public void httpFindByIdTest() {
        // Création du livre
        WebClient client = WebClient.create("http://localhost:8080");
        Book mstSpringBook = new Book("Spring, c'est pas qu'un verbe en anglais", "Jean-Michel Programmer");
        Mono<Book> mstSpringMono =  Mono.just(mstSpringBook);

        Mono<Book> response = client.get().uri("/book/{id}","1")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve().bodyToMono(Book.class);

        Book book = response.block();
        assertTrue(book != null);

        System.out.println(book);

    }

}
