package popschool.reactor_book_serveur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactorBookServeurApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactorBookServeurApplication.class, args);
    }

}
