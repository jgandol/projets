package dto;

import java.io.Serializable;

public class ForgeDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id_forge;
    private String nom;
    private String adresse;

    public int getId_forge() {
        return id_forge;
    }
    public String getNom() {
        return nom;
    }
    public String getAdresse() {
        return adresse;
    }

    public ForgeDTO(String nom, String adresse) {
        this.nom = nom;
        this.adresse = adresse;
    }
}
