package dto;

import java.io.Serializable;

public class FormationDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id_form;
    private String nom;

    public int getId_form() {
        return id_form;
    }
    public String getNom() {
        return nom;
    }

    public FormationDTO(String nom) {
        this.nom = nom;
    }
}
