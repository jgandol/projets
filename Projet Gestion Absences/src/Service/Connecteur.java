package Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connecteur {

    // URL DE CONNEXION
    private final static String URL = "jdbc:mysql://localhost:3306/Gestion_Absences";
    // NOM DE L'USER
    private final static String USER = "jeremie";
    // PASSWORD
    private final static String PW = "admin";

    //SINGLETON
    private static Connection INSTANCE;

    //CONSTRUCTEUR PRIVE
    private Connecteur() {
        try {
            INSTANCE = DriverManager.getConnection(URL, USER, PW);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getINSTANCE(){
        if(INSTANCE == null){
            new Connecteur();
        }
        return INSTANCE;
    }
}
