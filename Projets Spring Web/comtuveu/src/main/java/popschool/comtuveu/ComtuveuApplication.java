package popschool.comtuveu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComtuveuApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComtuveuApplication.class, args);
    }

}
