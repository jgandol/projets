package popschool.comtuveu.form;

import lombok.Data;

@Data
public class EmployeeForm {
    private int id;
    private String name;
    private String role;

    public EmployeeForm() {

    }

    public EmployeeForm(String name, String role) {
        this.name = name;
        this.role = role;
    }

}