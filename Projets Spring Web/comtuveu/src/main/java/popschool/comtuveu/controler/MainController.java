package popschool.comtuveu.controler;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import popschool.comtuveu.form.EmployeeForm;
import popschool.comtuveu.modele.Employee;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    private static List<Employee> employees = new ArrayList<Employee>();

    static {
        employees.add(new Employee("Bill", "hero"));
        employees.add(new Employee("Marc", "Jardinier"));
    }

    // Injectez (inject) via application.properties.
    @Value("${welcome.message}")
    private String message;
    @Value("${error.message}")
    private String errorMessage;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        return "index";
    }

    @RequestMapping(value = {"/employeeList"}, method = RequestMethod.GET)
    public String employeeList(Model model) {
        model.addAttribute("employees", employees);
        return "employeeList";
    }

    @RequestMapping(value = {"/addEmployee"}, method = RequestMethod.GET)
    public String showAddEmployeePage(Model model) {
        EmployeeForm employeeForm = new EmployeeForm();
        model.addAttribute("employeeForm", employeeForm);
        return "addEmployee";
    }

    @RequestMapping(value = {"/addEmployee"}, method = RequestMethod.POST)
    public String saveEmployee(Model model, //
                               @ModelAttribute("employeeForm") EmployeeForm employeeForm) {

        String name = employeeForm.getName();
        String role = employeeForm.getRole();

        if (name != null && name.length() > 0 && role != null && role.length() > 0) {
            Employee newEmployee = new Employee(name, role);
            employees.add(newEmployee);
            return "redirect:/employeeList";
        }
        model.addAttribute("errorMessage", errorMessage);
        return "addEmployee";
    }

}
