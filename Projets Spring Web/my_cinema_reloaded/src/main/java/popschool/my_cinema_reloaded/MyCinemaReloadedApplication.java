package popschool.my_cinema_reloaded;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyCinemaReloadedApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyCinemaReloadedApplication.class, args);
    }


    /*
    TODO:
        Index :
            - Consulter les acteurs
            - Consulter les films
            - Emprunter un film
        Consulter les acteurs :
            - List des acteurs (nom, prenom)
            - choix -> list des films de cet acteurs
        Consulter les films :
            - Liste des genres
            - choix -> liste des films de ce genre
        Emprunter un film :
            - Choisir un client et un film
            - Declencher l'emprunt
     */

}
