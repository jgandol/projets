package popschool.my_cinema_reloaded.control;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.my_cinema_reloaded.dao.EmpruntRepository;
import popschool.my_cinema_reloaded.dao.FilmRepository;
import popschool.my_cinema_reloaded.modele.Acteur;
import popschool.my_cinema_reloaded.modele.Emprunt;
import popschool.my_cinema_reloaded.service.VideoService;


import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private VideoService videoService;
    @Autowired
    private FilmRepository filmRepository;
    @Autowired
    private EmpruntRepository empruntRepository;
//    private static List<Acteur> acteurs = new ArrayList<Acteur>();


    //    // Injectez (inject) via application.properties.
    @Value("${welcome.message}")
    private String message;
    @Value("${error.message}")
    private String errorMessage;


    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        return "index";
    }

    @RequestMapping(value = {"/acteursList"}, method = RequestMethod.GET)
    public String acteursList(Model model) {
        model.addAttribute("listActeur", videoService.ensActeur());
        return "acteursList";
    }

    @RequestMapping(value = {"/clientsList"}, method = RequestMethod.GET)
    public String clientsList(Model model) {
        model.addAttribute("listClients", videoService.ensClients());
        return "clientsList";
    }


    @RequestMapping(value = {"/filmsList"}, method = RequestMethod.GET)
    public String filmsList(Model model) {
        model.addAttribute("listFilms", videoService.ensFilm());
        return "filmsList";
    }

    @RequestMapping(value = {"/pageListEmprunt"}, method = RequestMethod.GET)
    public String pageListEmprunt(Model model) {
        model.addAttribute("listEmprunts", videoService.ensEmprunts());
        return "pageListEmprunt";
    }


    @RequestMapping(value = "/pageEmprunt", method = RequestMethod.GET)
    public String pageEmprunt(Model model) {
        model.addAttribute("listFilms", videoService.ensFilmsEmpruntables());
        model.addAttribute("listClients", videoService.ensClients());
        return "emprunt";
    }

    @RequestMapping(value = {"/emprunt"}, method = RequestMethod.POST)
    public String emprunt(@RequestParam("filmchoisi") String titre,
                          @RequestParam("clientchoisi") String nom, Model model) {
//        System.out.println(titre + " : " + nom);
        videoService.emprunter(nom, titre);
        model.addAttribute("listEmprunts", videoService.ensEmprunts());
        return "pageListEmprunt";
    }

    @RequestMapping(value = {"/selectionGenre"}, method = RequestMethod.GET)
    public String selectionGenre(Model model) {
        model.addAttribute("listegenre", videoService.ensGenres());
        return "selectionGenre";
    }

    @PostMapping("/voirfilm")
    public String films(@RequestParam("genrechoisi") String nature, Model model) {
        model.addAttribute("ensfilmchoisi", videoService.ensFilmDuGenre(nature));
        return "filmsList";
    }

    @GetMapping("/filmsListActeur/{id}")
    public String filmsByActeur(@PathVariable("id") Long id, Model model) {
        model.addAttribute("ensFilmActeur", videoService.findFilmsByActeur(id));
        return "filmsListActeur";
    }

    @GetMapping("/rendreEmprunt/{id}")
    public String rendreEmprunt(@PathVariable("id") Long id, Model model) {
        Emprunt e = empruntRepository.findByNemprunt(id);
//        System.out.println(e);
        videoService.retourEmprunt(e.getClient().getNom(),e.getFilm().getTitre());
        model.addAttribute("listEmprunts", videoService.ensEmprunts());
        return "pageListEmprunt";
    }

//    @GetMapping("/delete/{id}")
//    public String deleteUser(@PathVariable("id")long id, Model model){
//        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(("Invalid user Id:" + id));
//        userRepository.delete(user);
//        model.addAttribute("users",userRepository.findAll()));
//        return "index";
//
//    }


//
//    @RequestMapping(value = {"/employeeList"}, method = RequestMethod.GET)
//    public String employeeList(Model model) {
//        model.addAttribute("employees", employees);
//        return "employeeList";
//    }
//
//    @RequestMapping(value = {"/addEmployee"}, method = RequestMethod.GET)
//    public String showAddEmployeePage(Model model) {
//        EmployeeForm employeeForm = new EmployeeForm();
//        model.addAttribute("employeeForm", employeeForm);
//        return "addEmployee";
//    }
//
//    @RequestMapping(value = {"/addEmployee"}, method = RequestMethod.POST)
//    public String saveEmployee(Model model, //
//                               @ModelAttribute("employeeForm") EmployeeForm employeeForm) {
//
//        String name = employeeForm.getName();
//        String role = employeeForm.getRole();
//
//        if (name != null && name.length() > 0 && role != null && role.length() > 0) {
//            Employee newEmployee = new Employee(name, role);
//            employees.add(newEmployee);
//            return "redirect:/employeeList";
//        }
//        model.addAttribute("errorMessage", errorMessage);
//        return "addEmployee";
//    }

}
