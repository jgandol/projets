package popschool.my_cinema_reloaded.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.my_cinema_reloaded.modele.Genre;


import java.util.List;

public interface GenreRepository extends CrudRepository<Genre, Integer> {

    List<Genre> findAll();

}
