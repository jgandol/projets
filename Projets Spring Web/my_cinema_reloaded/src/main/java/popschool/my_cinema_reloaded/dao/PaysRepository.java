package popschool.my_cinema_reloaded.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.my_cinema_reloaded.modele.Pays;

public interface PaysRepository extends CrudRepository<Pays, Long> {


}
