package popschool.my_cinema_reloaded.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.my_cinema_reloaded.MyCinemaReloadedApplication;
import popschool.my_cinema_reloaded.modele.Acteur;
import popschool.my_cinema_reloaded.service.VideoService;


@Component
public class EmpruntTest implements CommandLineRunner {


    private void information(Acteur a){
        log.info(a.toString());
    }

    private final static Logger log = LoggerFactory.getLogger(MyCinemaReloadedApplication.class);
//    @Autowired
//    EmpruntRepository empRep;
//
//    @Autowired
//    ClientRepository clientRep;

    @Autowired
    VideoService videoService;

    @Override
    public void run(String... args) throws Exception {
//        Client c = new Client("Gandolfo","Jeremie","Onnaing",50);
//        clientRep.save(c);
//        clientRep.save(new Client("Gandolfo","Jeremie","Onnaing",50));

//        videoService.emprunter("Gandolfo","Le Parrain III");
//        videoService.ensClient().forEach(System.out::println);
//        videoService.ensFilm().forEach(System.out::println);
//        videoService.ensFilmDuGenre("Fantastique").forEach(System.out::println);
//        videoService.ensGenres().forEach(System.out::println);

//        System.out.println("nb de film du genre : " + videoService.nombreFilmDuGenre("Fantastique"));

//        videoService.ensClient().forEach(System.out::println);
//        videoService.ensFilmsEmpruntables().forEach(System.out::println);
//        videoService.ensFilmsEmpruntes().forEach(System.out::println);
//        videoService.retourEmprunt("Gandolfo","West side story");
//        videoService.infoRealisateurActeur("West side story").forEach(e -> {
//            System.out.println(e.get(0) + " " + e.get(1));
//        });
//        videoService.findFilmsByActeur(15).forEach(System.out::println);
    }
}
