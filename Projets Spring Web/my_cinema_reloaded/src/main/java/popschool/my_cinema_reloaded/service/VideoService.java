package popschool.my_cinema_reloaded.service;

import org.springframework.data.jpa.repository.Query;
import popschool.my_cinema_reloaded.modele.*;


import javax.persistence.Tuple;
import java.util.List;

public interface VideoService {

    public void emprunter(String nom, String titre);

    public List<Client> ensClient();

    public List<Film> ensFilm();

    @Query("select e.film from Emprunt e where e.retour = \"NON\" order by e.film.titre")
    List<Client> findAllByOrderByNom();

    List<Genre> ensGenres();
    List<Acteur> ensActeur();
    List<Film> ensFilmDuGenre(String nature);
    List<Film> findFilmsByActeur(long id);
    List<Tuple>infoRealisateurActeur(String titre);
    int nombreFilmDuGenre(String nature);
    List<Client> ensClients();
    List<Film> ensFilmsEmpruntables();
    List<Film> ensFilmsEmpruntes();
    int emprunter2(String nomClient, String titreFilm);
    void retourEmprunt(String nomClient, String titreFilm);

    List<Emprunt> ensEmprunts();
}
