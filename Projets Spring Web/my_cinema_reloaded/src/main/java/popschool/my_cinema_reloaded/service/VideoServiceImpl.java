package popschool.my_cinema_reloaded.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.my_cinema_reloaded.dao.*;
import popschool.my_cinema_reloaded.modele.*;

import javax.persistence.Tuple;
import javax.transaction.Transactional;

import java.sql.Date;
import java.util.Optional;
import java.util.List;

@Service
@Transactional
public class VideoServiceImpl implements VideoService {

    @Autowired
    ClientRepository clientDAO;
    @Autowired
    FilmRepository filmDAO;
    @Autowired
    EmpruntRepository empruntDAO;
    @Autowired
    ActeurRepository acteurDAO;
    @Autowired
    PaysRepository paysDAO;
    @Autowired
    GenreRepository genreDAO;

    public void emprunter(String nom, String titre){
        Optional<Client> opCl = clientDAO.findByNom(nom);
        Optional<Film> filmCl = filmDAO.findByTitre(titre);
        java.util.Date utilDate=new java.util.Date();
        empruntDAO.save(new Emprunt(opCl.get(),filmCl.get(),"NON",new Date(utilDate.getTime())));
    }

    public List<Client> ensClient(){return clientDAO.findAll();}
    public List<Film> ensFilm(){return filmDAO.findAll();}
    public List<Acteur> ensActeur(){return acteurDAO.findAll();}

    public List<Film> findFilmsByActeur(long id){
        return filmDAO.findFilmByActeur(acteurDAO.findById(id).get().getNom());}

    @Override
    public List<Client> findAllByOrderByNom() {
        return clientDAO.findAllByOrderByNom();
    }

    @Override
    public List<Genre> ensGenres() {
        return genreDAO.findAll();
    }

    @Override
    public List<Film> ensFilmDuGenre(String nature) {
        return filmDAO.findEnsFilmByNature(nature);
    }

    @Override
    public List<Tuple> infoRealisateurActeur(String titre) {
        return filmDAO.findTupleFilm(titre);
    }

    @Override
    public int nombreFilmDuGenre(String nature) {
        return filmDAO.getCountFilmDuGenre(nature);
    }

    @Override
    public List<Client> ensClients() {
        return clientDAO.findAll();
    }

    @Override
    public List<Film> ensFilmsEmpruntables() {
        return empruntDAO.findAllFilmsEmpruntables();
    }

    @Override
    public List<Film> ensFilmsEmpruntes() {
        return empruntDAO.findAllFilmsEmpruntes();
    }

    @Override
    public int emprunter2(String nomClient, String titreFilm) {
        return 0;
    }

    @Override
    public void retourEmprunt(String nomClient, String titreFilm) {

        Film f = filmDAO.findByTitre(titreFilm).get();
        Client c = clientDAO.findByNom(nomClient).get();
        System.out.println(f + " " + c);
        Emprunt e = empruntDAO.findByFilmAndClient(f,c).get();
        System.out.println(e);
        e.setRetour("OUI");
        System.out.println(e);
        empruntDAO.save(e);


    }

    @Override
    public List<Emprunt> ensEmprunts() {
        return empruntDAO.findAllEmpruntsNonRetournés();
    }


}
