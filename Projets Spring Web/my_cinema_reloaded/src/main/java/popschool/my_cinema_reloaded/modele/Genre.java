package popschool.my_cinema_reloaded.modele;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "GENRE")
public class Genre {
    private Long ngenre;
    private String nature;
    private List<Film> films;

    public Genre() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ngenre")
    public Long getNgenre() {
        return ngenre;
    }
    public void setNgenre(Long ngenre) {
        this.ngenre = ngenre;
    }

    @Basic
    @Column(name = "nature")
    public String getNature() {
        return nature;
    }
    public void setNature(String nature) {
        this.nature = nature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return ngenre == genre.ngenre &&
                Objects.equals(nature, genre.nature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ngenre, nature);
    }

    @OneToMany(mappedBy = "genre")
    public List<Film> getFilms() {
        return films;
    }
    public void setFilms(List<Film> films) {
        this.films = films;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "ngenre=" + ngenre +
                ", nature='" + nature + '\'' +
                '}';
    }
}
