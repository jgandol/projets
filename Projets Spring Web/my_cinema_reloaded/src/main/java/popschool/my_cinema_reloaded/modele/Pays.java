package popschool.my_cinema_reloaded.modele;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "PAYS")
public class Pays {
    private Long npays;
    private String nom;
    private List<Acteur> acteurs;
    private List<Film> films;

    public Pays() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "npays")
    public Long getNpays() {
        return npays;
    }
    public void setNpays(Long npays) {
        this.npays = npays;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @OneToMany(mappedBy = "nationalite")
    public List<Acteur> getActeurs() {
        return acteurs;
    }
    public void setActeurs(List<Acteur> acteurs) {
        this.acteurs = acteurs;
    }

    @OneToMany(mappedBy = "pays")
    public List<Film> getFilms() {
        return films;
    }
    public void setFilms(List<Film> films) {
        this.films = films;
    }

    @Override
    public String toString() {
        return "Pays{" +
                "npays=" + npays +
                ", nom='" + nom + '\'' +
                '}';
    }
}
