package popschool.my_cinema_reloaded.modele;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "ACTEUR")
public class Acteur {
    private Long nacteur;
    private String nom;
    private String prenom;
    private Date naissance;
    private Integer nbrefilms;
    private Pays nationalite;
    private List<Film> films;

    public Acteur() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "nacteur")
    public Long getNacteur() {
        return nacteur;
    }
    public void setNacteur(Long nacteur) {
        this.nacteur = nacteur;
    }


    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "naissance")
    public Date getNaissance() {
        return naissance;
    }
    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }


    @Basic
    @Column(name = "nbreFilms")
    public Integer getNbrefilms() {
        return nbrefilms;
    }
    public void setNbrefilms(Integer nbrefilms) {
        this.nbrefilms = nbrefilms;
    }

    @ManyToOne
    @JoinColumn(name = "nationalite")
    public Pays getNationalite() {
        return nationalite;
    }
    public void setNationalite(Pays nationalite) {
        this.nationalite = nationalite;
    }

    @OneToMany(mappedBy = "acteur")
    public List<Film> getFilms() {
        return films;
    }
    public void setFilms(List<Film> films) {
        this.films = films;
    }

    @Override
    public String toString() {
        return "Acteur{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", nbrefilms=" + nbrefilms +
                ", nationalite=" + nationalite +
                '}';
    }
}
