package popschool.picsou;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import popschool.picsou.dao.*;
import popschool.picsou.service.PicsouService;

@SpringBootApplication
public class PicsouApplication {

    public static void main(String[] args) {
        SpringApplication.run(PicsouApplication.class, args);
    }

}
