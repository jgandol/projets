package popschool.picsou.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.picsou.dao.CompteRepository;
import popschool.picsou.service.PicsouService;

import javax.websocket.server.PathParam;

@Controller
public class ClientController {

    @Autowired
    private PicsouService picsouService;
    @Autowired
    private CompteRepository compteRepository;


    //    Injectez (inject) via application.properties.
    @Value("${welcome.message}")
    private String message;
    @Value("${error.message}")
    private String errorMessage;


    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        return "index";
    }

    @GetMapping(value = {"/clientsList"})
    public String clientsList(Model model) {
        model.addAttribute("listClient", picsouService.findAllClients());
        return "clientsList";
    }

    @GetMapping("/comptesClient/{id}")
    public String comptesClient(@PathVariable("id") int id, Model model) {
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
        model.addAttribute("id",id);
        return "comptesClient";
    }

    @GetMapping(value = {"/comptesClient/{id}/credit"})
    public String creditByID(@PathVariable("id") int id,Model model) {
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));

        return "credit";
    }

    @PostMapping(value = {"credit"})
    public String credit(@RequestParam("comptechoisi") int id,@RequestParam("montant") double montant,Model model) {
        picsouService.crediter(id,montant);
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
//        model.addAttribute("id",id);
        id = picsouService.findCompteByClient(id).get(1).getClient().getIdClient();
        return "redirect:/comptesClient/{" + id + "}/credit";
    }

    @GetMapping(value = {"/comptesClient/{id}/debit"})
    public String debitById(@PathVariable("id") int id,Model model) {
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
        return "debit";
    }

    @PostMapping(value = {"/debit"})
    public String debit(@RequestParam("comptechoisideb") int id,@RequestParam("montant") double montant,Model model) {
        picsouService.debiter(id,montant);
//        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
        return "index";
    }

    @GetMapping(value = {"/comptesClient/{id}/transfert"})
    public String transfertById(@PathVariable("id") int id,Model model) {
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
        model.addAttribute("ensComptes", compteRepository.findAll());
        return "transfert";
    }

    @PostMapping(value = {"/transfert"})
    public String transfert(@RequestParam("compteorigin") Integer id_origin, @RequestParam("comptedest") Integer id_dest,
            @RequestParam("montant") Double montant,Model model) {
        System.out.println(id_origin + " | " + id_dest + " | " + montant);
        picsouService.transfer(id_origin,id_dest,montant);
        model.addAttribute("ensComptes", compteRepository.findAll());
        return "index";
    }

//    @RequestMapping(value = {"/acteursList"}, method = RequestMethod.GET)
//    public String acteursList(Model model) {
//        model.addAttribute("listActeur", videoService.ensActeur());
//        return "acteursList";
//    }
//
//    @RequestMapping(value = {"/clientsList"}, method = RequestMethod.GET)
//    public String clientsList(Model model) {
//        model.addAttribute("listClients", videoService.ensClients());
//        return "clientsList";
//    }
//
//
//    @RequestMapping(value = {"/filmsList"}, method = RequestMethod.GET)
//    public String filmsList(Model model) {
//        model.addAttribute("listFilms", videoService.ensFilm());
//        return "filmsList";
//    }
//
//    @RequestMapping(value = {"/pageListEmprunt"}, method = RequestMethod.GET)
//    public String pageListEmprunt(Model model) {
//        model.addAttribute("listEmprunts", videoService.ensEmprunts());
//        return "pageListEmprunt";
//    }
//
//
//    @RequestMapping(value = "/pageEmprunt", method = RequestMethod.GET)
//    public String pageEmprunt(Model model) {
//        model.addAttribute("listFilms", videoService.ensFilmsEmpruntables());
//        model.addAttribute("listClients", videoService.ensClients());
//        return "emprunt";
//    }
//
//    @RequestMapping(value = {"/emprunt"}, method = RequestMethod.POST)
//    public String emprunt(@RequestParam("filmchoisi") String titre,
//                          @RequestParam("clientchoisi") String nom, Model model) {
////        System.out.println(titre + " : " + nom);
//        videoService.emprunter(nom, titre);
//        model.addAttribute("listEmprunts", videoService.ensEmprunts());
//        return "pageListEmprunt";
//    }
//
//    @RequestMapping(value = {"/selectionGenre"}, method = RequestMethod.GET)
//    public String selectionGenre(Model model) {
//        model.addAttribute("listegenre", videoService.ensGenres());
//        return "selectionGenre";
//    }
//
//    @PostMapping("/voirfilm")
//    public String films(@RequestParam("genrechoisi") String nature, Model model) {
//        model.addAttribute("ensfilmchoisi", videoService.ensFilmDuGenre(nature));
//        return "filmsList";
//    }
//
//    @GetMapping("/filmsListActeur/{id}")
//    public String filmsByActeur(@PathVariable("id") Long id, Model model) {
//        model.addAttribute("ensFilmActeur", videoService.findFilmsByActeur(id));
//        return "filmsListActeur";
//    }
//
//    @GetMapping("/rendreEmprunt/{id}")
//    public String rendreEmprunt(@PathVariable("id") Long id, Model model) {
//        Emprunt e = empruntRepository.findByNemprunt(id);
////        System.out.println(e);
//        videoService.retourEmprunt(e.getClient().getNom(), e.getFilm().getTitre());
//        model.addAttribute("listEmprunts", videoService.ensEmprunts());
//        return "pageListEmprunt";
//    }
     /*

    user s'identifie
    il arrive sur le catalogue
    pn peut consulter le panier et supprimer des articles du panier
    valider la commande
     */
}
