package popschool.picsou.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import popschool.picsou.modele.Client;
import popschool.picsou.modele.Compte;
import popschool.picsou.modele.Mouvement;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


public interface PicsouService{

    List<Client> findAllClients();
    List<Compte> findCompteByClient(int idclient);
    Client findClientById(int id);
    Compte findCompteById(int id);

    void ajoutClient(String nom, String prenom, String adresse);
    void ajoutCompte(String num, Client client);
    void ajoutCarte(String num, Date date_expiration, String code,Compte c);

    void updateCompte(Compte compte);
    void updateMouvement(Mouvement mouvement);
    void crediter(int id, double montant);
    void debiter(int id, double montant);
    void transfer(int idorigine, int iddestination, double montant);

}


