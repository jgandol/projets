package popschool.picsou.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.picsou.dao.*;
import popschool.picsou.modele.Carte;
import popschool.picsou.modele.Client;
import popschool.picsou.modele.Compte;
import popschool.picsou.modele.Mouvement;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PicsouServiceImplementation implements PicsouService{
    @Autowired
    CarteRepository carteDAO;
    @Autowired
    ClientRepository clientDAO;
    @Autowired
    CompteRepository compteDAO;
    @Autowired
    MouvementRepository mouvDAO;
    @Autowired
    RetraitRepository retraitDAO;


    @Override
    public List<Client> findAllClients() {
        return clientDAO.findAll();
    }

    @Override
    public List<Compte> findCompteByClient(int idclient) {
        return clientDAO.findComptesByClient(idclient);
    }

    @Override
    public Client findClientById(int id) {
        return clientDAO.findByIdClient(id);
    }

    @Override
    public Compte findCompteById(int id) {
        return compteDAO.findByIdCompte(id);
    }

    @Override
    public void ajoutClient(String nom, String prenom, String adresse) {
        if(nom!=null && prenom!=null){
            Client c = new Client(nom,prenom,adresse);
            clientDAO.save(c);
        }

    }

    @Override
    public void ajoutCompte(String num, Client client) {
        if(num!=null && client!=null){
            Compte c = new Compte(num);
            c.setClient(client);
            compteDAO.save(c);
        } return;
    }

    @Override
    public void ajoutCarte(String num, Date date_expiration, String code, Compte c) {
        if(num != null && date_expiration != null && code !=null && clientDAO !=null){
            Carte carte = new Carte(num, (java.sql.Date) date_expiration,code);
            carte.setCompte(c);
            carteDAO.save(carte);
        } return;
    }

    @Override
    public void updateCompte(Compte compte) {
        compteDAO.save(compte);
    }

    @Override
    public void updateMouvement(Mouvement mouvement) {
        mouvDAO.save(mouvement);
    }

    @Override
    public void crediter(int id, double montant) {
        Optional<Compte> compte = compteDAO.findById(id);
        if(compte.isPresent()){
            compte.get().setSolde(compte.get().getSolde()+montant);
            Mouvement m = new Mouvement();
            m.setCompte(compte.get());
            m.setMontant(montant);
            m.setNature("CR");
            m.setDateOperation(new Timestamp(System.currentTimeMillis()));
            mouvDAO.save(m);
        } else return;
    }

    @Override
    public void debiter(int id, double montant) {
        Compte compte = compteDAO.findByIdCompte(id);
        if(compte != null){
            if(montant < (compte.getSolde()+compte.getPlafond())){
                compte.setSolde(compte.getSolde()-montant);
                compteDAO.save(compte);
            } else return;
        }

        Mouvement mvt = new Mouvement();
        mvt.setCompte(compte);
        mvt.setMontant(montant);
        mvt.setNature("DB");
        mvt.setDateOperation(new Timestamp(System.currentTimeMillis()));
        mouvDAO.save(mvt);

    }

    @Override
    public void transfer(int idorigine, int iddestination, double montant) {
        Optional<Compte> compteOrigine = compteDAO.findById(idorigine);
        Optional<Compte> compte = compteDAO.findById(iddestination);

        if(!compte.isPresent() || !compteOrigine.isPresent() || compte==compteOrigine){
           return;
        }
        if(compteOrigine.get().getBloque().equals("oui")) return;

        Compte co = compteOrigine.get();
        Compte c = compte.get();

        co.setSolde(co.getSolde()-montant);
        c.setSolde(c.getSolde()+montant);

        Mouvement mvt2 = new Mouvement();
        mvt2.setCompte(co);
        mvt2.setMontant(montant);
        mvt2.setNature("DB");
        mvt2.setDateOperation(new Timestamp(System.currentTimeMillis()));
        mouvDAO.save(mvt2);

        Mouvement mvt = new Mouvement();
        mvt.setCompte(c);
        mvt.setMontant(montant);
        mvt.setNature("CR");
        mvt.setDateOperation(new Timestamp(System.currentTimeMillis()));
        mouvDAO.save(mvt);
        compteDAO.save(co);
        compteDAO.save(c);
    }
}
