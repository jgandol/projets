package popschool.picsou.modele;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Retrait {
    private int idRetrait;
    private Timestamp dateRetrait;
    private String adresseGab;
    private double montant;
    private String etat;
    private Carte carte;
    private Mouvement mouvement;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_retrait")
    public int getIdRetrait() {
        return idRetrait;
    }

    public void setIdRetrait(int idRetrait) {
        this.idRetrait = idRetrait;
    }

    @Basic
    @Column(name = "date_retrait")
    public Timestamp getDateRetrait() {
        return dateRetrait;
    }

    public void setDateRetrait(Timestamp dateRetrait) {
        this.dateRetrait = dateRetrait;
    }

    @Basic
    @Column(name = "adresseGAB")
    public String getAdresseGab() {
        return adresseGab;
    }

    public void setAdresseGab(String adresseGab) {
        this.adresseGab = adresseGab;
    }

    @Basic
    @Column(name = "montant")
    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "etat")
    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }


    @ManyToOne
    @JoinColumn(name = "id_carte", referencedColumnName = "id_carte", nullable = false)
    public Carte getCarte() {
        return carte;
    }

    public void setCarte(Carte carte) {
        this.carte = carte;
    }

    @ManyToOne
    @JoinColumn(name = "id_mouv", referencedColumnName = "id_mouv")
    public Mouvement getMouvement() {
        return mouvement;
    }

    public void setMouvement(Mouvement mouvement) {
        this.mouvement = mouvement;
    }
}
