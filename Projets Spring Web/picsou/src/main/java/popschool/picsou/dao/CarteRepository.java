package popschool.picsou.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.picsou.modele.Carte;

import java.util.List;

public interface CarteRepository extends CrudRepository<Carte,Integer> {

    List<Carte> findAll();
    Carte findByIdCarte(int id);
}
