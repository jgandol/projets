package popschool.picsou.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.picsou.modele.Client;
import popschool.picsou.modele.Compte;

import java.awt.*;
import java.util.List;

public interface CompteRepository extends CrudRepository<Compte,Integer> {

    List<Compte> findAll();
    Compte findByIdCompte(int id);
}
