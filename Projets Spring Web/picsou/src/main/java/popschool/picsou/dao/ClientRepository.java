package popschool.picsou.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.picsou.modele.Client;
import popschool.picsou.modele.Compte;

import java.util.List;

public interface ClientRepository extends CrudRepository<Client,Integer> {

    List<Client> findAll();
    Client findByIdClient(int id);
    Client findByNom(String nom);

    @Query("select c.comptes from Client c where c.idClient = :id")
    List<Compte> findComptesByClient(int id);


}
