package popschool.picsou.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.picsou.modele.Client;
import popschool.picsou.modele.Retrait;

import java.util.List;

public interface RetraitRepository extends CrudRepository<Retrait,Integer> {

    List<Retrait> findAll();
    Retrait findByIdRetrait(int id);

}
