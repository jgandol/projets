package popschool.picsou.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.picsou.modele.Mouvement;

import java.util.List;

public interface MouvementRepository extends CrudRepository<Mouvement,Integer> {

    List<Mouvement> findAll();
    Mouvement findByIdMouv(int id);

}
