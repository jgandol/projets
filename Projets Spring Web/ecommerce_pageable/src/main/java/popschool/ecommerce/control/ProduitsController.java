package popschool.ecommerce.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import popschool.ecommerce.dao.ProduitRepository;
import popschool.ecommerce.dao.ProduitRepositoryJpa;
import popschool.ecommerce.model.Produit;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class ProduitsController {

    @Autowired
    ProduitRepositoryJpa produitDAO;


//    @GetMapping("/voirProduits")
//    public String goToProduits(Model model){
//        return "produits_pagination";
//    }


    @RequestMapping(value = "/listeProduits")
    public String listeProd(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Pageable triParNom =
                PageRequest.of(currentPage - 1, pageSize, Sort.by("NPRODUIT"));

        Page<Produit> produitPage = produitDAO.findAll(PageRequest.of(currentPage - 1, pageSize));

        model.addAttribute("produitPage", produitPage);
        model.addAttribute("listeProduits", produitPage.getContent());

        int totalPages = produitPage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for (int i = 1; i <= totalPages; i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "produits_pagination";
    }

    //
//    @RequestMapping(value = "/listeProduits")
//    public String listeProd(
//            Model model,
//            @RequestParam("page") Optional<Integer> page,
//            @RequestParam("size") Optional<Integer> size)
//    {
//        int currentPage = page.orElse(1);
//        int pageSize = size.orElse(5);
//
//        Pageable triParNom =
//                PageRequest.of(currentPage-1,pageSize, Sort.by("NPRODUIT"));
//
//        Page<Produit> produitPage = produitDAO.findAll(PageRequest.of(currentPage-1,pageSize));
//
//        model.addAttribute("produitPage", produitPage);
//        model.addAttribute("listeProduits", produitPage.getContent());
//
//        int totalPages = produitPage.getTotalPages();
//        if (totalPages > 0) {
///*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
//                    .boxed()
//                    .collect(Collectors.toList());*/
//            List<Integer> pageNumbers = new ArrayList<>();
//            for(int i=1; i<=totalPages;i++)
//                pageNumbers.add(i);
//            model.addAttribute("pageNumbers", pageNumbers);
//        }
//
//        return "produits_pagination";
//    }
    @PostMapping("/ajoutnb")
    String addPanier(Model model,@RequestParam("qte")int qte, @RequestParam("id")int id) {
        System.out.println(qte + " " + id);

        return "panier";
    }


    @RequestMapping("/ajoutPanierV2")
    String addPanier(Model model, @RequestParam("id") int id) {

        return "panier";
    }

}
