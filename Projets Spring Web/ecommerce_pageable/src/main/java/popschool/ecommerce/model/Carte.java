package popschool.ecommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "CARTE")
public class Carte {
    private int ncarte;
    private Integer numero;
    private Integer codesecret;
    private Date dateperemption;
    private Client client;
//@CreditCardNumber(ignoreDigitCharacters = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NCARTE")
    public int getNcarte() {
        return ncarte;
    }

    public void setNcarte(int ncarte) {
        this.ncarte = ncarte;
    }

    @Basic
    @Column(name = "NUMERO")
    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "CODESECRET")
    public Integer getCodesecret() {
        return codesecret;
    }

    public void setCodesecret(Integer codesecret) {
        this.codesecret = codesecret;
    }

    @Basic
    @Column(name = "DATEPEREMPTION")
    public Date getDateperemption() {
        return dateperemption;
    }

    public void setDateperemption(Date dateperemption) {
        this.dateperemption = dateperemption;
    }

    @OneToOne
    @JoinColumn(name = "TITULAIRE", referencedColumnName = "NOM")
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
