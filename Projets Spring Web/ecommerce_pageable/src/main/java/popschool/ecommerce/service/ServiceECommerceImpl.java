package popschool.ecommerce.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.ecommerce.dao.ClientRepository;
import popschool.ecommerce.dao.CommandeRepository;
import popschool.ecommerce.dao.DetailcdeRepository;
import popschool.ecommerce.dao.ProduitRepository;
import popschool.ecommerce.model.Client;
import popschool.ecommerce.model.Commande;
import popschool.ecommerce.model.Detailcde;
import popschool.ecommerce.model.Produit;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ServiceECommerceImpl implements ServiceECommerce{
    @Autowired
    ProduitRepository produitRepository;
    @Autowired
    CommandeRepository commandeRepository;
    @Autowired
    DetailcdeRepository detailcdeRepository;
    @Autowired
    ClientRepository clientRepository;

    @Override
    public List<Client> findAllClient() {
        return clientRepository.findAll();
    }
    @Override
    public List<Commande> findAllCommande() {
        return commandeRepository.findAll();
    }
    @Override
    public List<Detailcde> findAllDetailCde() {
        return detailcdeRepository.findAll();
    }
    @Override
    public List<Produit> findAllProduit() {
        return produitRepository.findAll();
    }

    @Override
    public Boolean UserExist(String nom, String mdp) {
        Optional<Client> c = clientRepository.findByNom(nom);
        if(c.isPresent() && c.get().getMotdepasse().equals(mdp))
            return true;
        else return false;
    }

    @Override
    public void validerCommande(Commande c) {

        commandeRepository.save(c);
    }


}
