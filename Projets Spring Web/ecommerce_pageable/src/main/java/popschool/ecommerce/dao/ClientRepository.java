package popschool.ecommerce.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.ecommerce.model.Client;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client,String> {
    List<Client> findAll();
    Optional<Client> findByNom(String nom);
}
