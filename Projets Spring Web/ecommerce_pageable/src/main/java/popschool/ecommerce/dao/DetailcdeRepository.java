package popschool.ecommerce.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.ecommerce.model.Detailcde;

import java.util.List;
import java.util.Optional;

public interface DetailcdeRepository extends CrudRepository<Detailcde,Integer> {
    List<Detailcde> findAll();
    Optional<Detailcde> findByNdetail(int id);
}

