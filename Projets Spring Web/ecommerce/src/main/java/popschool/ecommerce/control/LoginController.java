package popschool.ecommerce.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.SessionScope;
import popschool.ecommerce.dao.ClientRepository;
import popschool.ecommerce.dao.ProduitRepository;
import popschool.ecommerce.model.Client;
import popschool.ecommerce.model.Commande;
import popschool.ecommerce.model.Detailcde;
import popschool.ecommerce.model.Produit;
import popschool.ecommerce.service.ServiceECommerce;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes(value = "panierCommande", types = Commande.class)
public class LoginController {
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    ServiceECommerce serviceECommerce;
    @Autowired
    ProduitRepository produitRepository;
//    @Value("${error.message}")
//    private String errorMessage;

    @ModelAttribute("panierCommande")
    public Commande addMyBean1ToSessionScope(@PathParam("login") String login) {
        return new Commande();
    }

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String closeSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        return "index";
    }

//    @PostMapping("/")
//    public String prout (
//            @ModelAttribute("myBean1") MyOtherBean bean,
//    )

    @PostMapping("/catalogue")
    public String catalogue(@ModelAttribute("panierCommande")Commande c,@RequestParam("login") String login, @RequestParam("mdp") String mdp, Model model) {
//        System.out.println(login);
        if (serviceECommerce.UserExist(login, mdp)) {
            c.setClient(clientRepository.findByNom(login).get());
            model.addAttribute("ensProduits", serviceECommerce.findAllProduit());
            return "catalogue";
        } else return "index";
    }

    @PostMapping("/catalogue2")
    public String catalogue2(Model model){
        model.addAttribute("ensProduits",serviceECommerce.findAllProduit());
        return "catalogue";
    }

    @PostMapping("/panier")
    public String catalogue(@ModelAttribute("panierCommande")Commande c,@RequestParam(name = "addpanier",required = false)int[] idp, Model model) {
        for(int i = 0; i<idp.length;i++){
//            System.out.println(idp[i]);
            Produit p = produitRepository.findByNproduit(idp[i]).get();
            Detailcde d = new Detailcde(1,c,p);
            p.getDetailcdes().add(d);
            c.getDetailcdes().add(d);
        }
        model.addAttribute("panierTempo",c.getDetailcdes());
        return "panier";
    }

    @GetMapping("/voirPanier")
    public String voirPanier(@ModelAttribute("panierCommande")Commande c,Model model){
        model.addAttribute("panierTempo",c.getDetailcdes());
        return "panier";
    }

    @GetMapping("/panier/delete/{desc}")
    public String catalogueDelete(@ModelAttribute("panierCommande")Commande c,@PathParam("desc")String desc, Model model) {
        int i = 0;
        for(Detailcde d : c.getDetailcdes()){

//            if(d.getProduit().getDescriptif().equals(desc)){
//            if(c.getDetailcdes().get(i).getProduit().getDescriptif().equals(desc)){
                c.getDetailcdes().remove(i);
                System.out.println(i);
//                break;
//            }
            i++;
        }
        model.addAttribute("panierTempo",c.getDetailcdes());
        model.addAttribute("ensProduits",serviceECommerce.findAllProduit());
        return "catalogue";
    }

    @PostMapping("/validerPanier")
    public String validerPanier(@ModelAttribute("panierCommande")Commande c, Model model){
        serviceECommerce.validerCommande(c);
        model.addAttribute("ensProduits",serviceECommerce.findAllProduit());
        c.getDetailcdes().clear();
        return "catalogue";

    }

//    @PostMapping("/voirfilmbyacteur" )
//    public String voirfilmbyacteur(@RequestParam("acteurchoisi")String nomacteur, Model model){
//        model.addAttribute("ensfilmchoisi", filmRepository.findByActeur_Nom(nomacteur));
//        return "listfilms";
//    }


}
