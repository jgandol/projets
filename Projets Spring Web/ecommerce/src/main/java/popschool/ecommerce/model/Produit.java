package popschool.ecommerce.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "PRODUIT")
public class Produit {
    private Integer nproduit;
    private String descriptif;
    private double prix;
    private String disponible;
    private int qteenstock;
    private List<Detailcde> detailcdes = new ArrayList<>();

    public Produit() {
    }

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NPRODUIT")
    public int getNproduit() {
        return nproduit;
    }
    public void setNproduit(int nproduit) {
        this.nproduit = nproduit;
    }

    @Basic
    @Column(name = "DESCRIPTIF")
    public String getDescriptif() {
        return descriptif;
    }
    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    @Basic
    @Column(name = "PRIX")
    public double getPrix() {
        return prix;
    }
    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Basic
    @Column(name = "DISPONIBLE")
    public String getDisponible() {
        return disponible;
    }
    public void setDisponible(String disponible) {
        this.disponible = disponible;
    }

    @Basic
    @Column(name = "QTEENSTOCK")
    public int getQteenstock() {
        return qteenstock;
    }
    public void setQteenstock(int qteenstock) {
        this.qteenstock = qteenstock;
    }

    @OneToMany(mappedBy = "produit")
    public List<Detailcde> getDetailcdes() {
        return detailcdes;
    }
    public void setDetailcdes(List<Detailcde> detailcdes) {
        this.detailcdes = detailcdes;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "nproduit=" + nproduit +
                ", descriptif='" + descriptif + '\'' +
                ", prix=" + prix +
                ", disponible='" + disponible + '\'' +
                ", qteenstock=" + qteenstock +
                '}';
    }
}
