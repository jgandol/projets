package popschool.ecommerce.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.ecommerce.model.Commande;

import java.util.List;
import java.util.Optional;

public interface CommandeRepository extends CrudRepository<Commande,Integer> {
    List<Commande> findAll();
    Optional<Commande> findByNcommande(int id);
}

