package popschool.ecommerce.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.ecommerce.model.Produit;

import java.util.List;
import java.util.Optional;

public interface ProduitRepository extends CrudRepository<Produit,Integer> {
    List<Produit> findAll();
    Optional<Produit> findByNproduit(int id);
}
