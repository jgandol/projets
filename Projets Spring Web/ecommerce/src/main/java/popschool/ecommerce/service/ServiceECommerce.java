package popschool.ecommerce.service;

import popschool.ecommerce.model.Client;
import popschool.ecommerce.model.Commande;
import popschool.ecommerce.model.Detailcde;
import popschool.ecommerce.model.Produit;

import java.util.List;

public interface ServiceECommerce {
    List<Client> findAllClient();
    List<Commande> findAllCommande();
    List<Detailcde> findAllDetailCde();
    List<Produit> findAllProduit();
    Boolean UserExist(String nom, String mdp);
    void validerCommande(Commande c);
}
