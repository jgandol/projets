package popschool.frais_kilometrique.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Adresse {
    private int idAdresse;
    private String rue;
    private String ville;
    private Integer cp;
    private String pays;
    private String coordonnes;
    private List<Trajet> trajets_retour;
    private List<Trajet> trajets_allee;

    @Id
    @Column(name = "id_adresse")
    public int getIdAdresse() {
        return idAdresse;
    }

    public void setIdAdresse(int idAdresse) {
        this.idAdresse = idAdresse;
    }

    @Basic
    @Column(name = "rue")
    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    @Basic
    @Column(name = "ville")
    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @Basic
    @Column(name = "cp")
    public Integer getCp() {
        return cp;
    }

    public void setCp(Integer cp) {
        this.cp = cp;
    }

    @Basic
    @Column(name = "pays")
    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    @Basic
    @Column(name = "coordonnes")
    public String getCoordonnes() {
        return coordonnes;
    }

    public void setCoordonnes(String coordonnes) {
        this.coordonnes = coordonnes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Adresse adresse = (Adresse) o;
        return idAdresse == adresse.idAdresse &&
                Objects.equals(rue, adresse.rue) &&
                Objects.equals(ville, adresse.ville) &&
                Objects.equals(cp, adresse.cp) &&
                Objects.equals(pays, adresse.pays) &&
                Objects.equals(coordonnes, adresse.coordonnes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAdresse, rue, ville, cp, pays, coordonnes);
    }

    @OneToMany(mappedBy = "adresseDepart")
    public List<Trajet> getTrajets_retour() {
        return trajets_retour;
    }

    public void setTrajets_retour(List<Trajet> trajets_retour) {
        this.trajets_retour = trajets_retour;
    }

    @OneToMany(mappedBy = "adresseArrivee")
    public List<Trajet> getTrajets_allee() {
        return trajets_allee;
    }

    public void setTrajets_allee(List<Trajet> trajets_allee) {
        this.trajets_allee = trajets_allee;
    }
}
