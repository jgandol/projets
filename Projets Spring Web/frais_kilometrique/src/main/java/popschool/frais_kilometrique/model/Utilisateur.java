package popschool.frais_kilometrique.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Utilisateur {
    private int idUser;
    private String login;
    private String nom;
    private String prenom;
    private String mdp;
    private String typeUser;
    private List<Document> documents;
    private List<Droits> droits;
    private List<Trajet> trajets;

    @Id
    @Column(name = "id_user")
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "mdp")
    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    @Basic
    @Column(name = "type_user")
    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Utilisateur that = (Utilisateur) o;
        return idUser == that.idUser &&
                Objects.equals(login, that.login) &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(mdp, that.mdp) &&
                Objects.equals(typeUser, that.typeUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUser, login, nom, prenom, mdp, typeUser);
    }

    @OneToMany(mappedBy = "utilisateur")
    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    @OneToMany(mappedBy = "utilisateur")
    public List<Droits> getDroits() {
        return droits;
    }

    public void setDroits(List<Droits> droits) {
        this.droits = droits;
    }

    @OneToMany(mappedBy = "utilisateur")
    public List<Trajet> getTrajets() {
        return trajets;
    }

    public void setTrajets(List<Trajet> trajets) {
        this.trajets = trajets;
    }
}
