package popschool.frais_kilometrique.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Document {
    private int idDocument;
    private String chemin;
    private Utilisateur utilisateur;
    private Vehicule vehicule;

    @Id
    @Column(name = "id_document")
    public int getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(int idDocument) {
        this.idDocument = idDocument;
    }

    @Basic
    @Column(name = "chemin")
    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return idDocument == document.idDocument &&
                Objects.equals(chemin, document.chemin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDocument, chemin);
    }

    @ManyToOne
    @JoinColumn(name = "permis", referencedColumnName = "id_user")
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @ManyToOne
    @JoinColumn(name = "carte_grise", referencedColumnName = "id_vehicule")
    public Vehicule getVehicule() {
        return vehicule;
    }

    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }
}
