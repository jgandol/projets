package popschool.frais_kilometrique.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Droits {
    private int idDroit;
    private Byte modifierUser;
    private Byte supprimerUser;
    private Byte gererVoyage;
    private Byte gererVehicule;
    private Byte validerUser;
    private Byte consultation;
    private Utilisateur utilisateur;

    @Id
    @Column(name = "id_droit")
    public int getIdDroit() {
        return idDroit;
    }

    public void setIdDroit(int idDroit) {
        this.idDroit = idDroit;
    }

    @Basic
    @Column(name = "modifier_user")
    public Byte getModifierUser() {
        return modifierUser;
    }

    public void setModifierUser(Byte modifierUser) {
        this.modifierUser = modifierUser;
    }

    @Basic
    @Column(name = "supprimer_user")
    public Byte getSupprimerUser() {
        return supprimerUser;
    }

    public void setSupprimerUser(Byte supprimerUser) {
        this.supprimerUser = supprimerUser;
    }

    @Basic
    @Column(name = "gerer_voyage")
    public Byte getGererVoyage() {
        return gererVoyage;
    }

    public void setGererVoyage(Byte gererVoyage) {
        this.gererVoyage = gererVoyage;
    }

    @Basic
    @Column(name = "gerer_vehicule")
    public Byte getGererVehicule() {
        return gererVehicule;
    }

    public void setGererVehicule(Byte gererVehicule) {
        this.gererVehicule = gererVehicule;
    }

    @Basic
    @Column(name = "valider_user")
    public Byte getValiderUser() {
        return validerUser;
    }

    public void setValiderUser(Byte validerUser) {
        this.validerUser = validerUser;
    }

    @Basic
    @Column(name = "consultation")
    public Byte getConsultation() {
        return consultation;
    }

    public void setConsultation(Byte consultation) {
        this.consultation = consultation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Droits droits = (Droits) o;
        return idDroit == droits.idDroit &&
                Objects.equals(modifierUser, droits.modifierUser) &&
                Objects.equals(supprimerUser, droits.supprimerUser) &&
                Objects.equals(gererVoyage, droits.gererVoyage) &&
                Objects.equals(gererVehicule, droits.gererVehicule) &&
                Objects.equals(validerUser, droits.validerUser) &&
                Objects.equals(consultation, droits.consultation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDroit, modifierUser, supprimerUser, gererVoyage, gererVehicule, validerUser, consultation);
    }

    @ManyToOne
    @JoinColumn(name = "utilisateur", referencedColumnName = "id_user", nullable = false)
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
}
