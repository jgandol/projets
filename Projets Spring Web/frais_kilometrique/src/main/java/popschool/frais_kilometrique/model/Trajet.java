package popschool.frais_kilometrique.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Trajet {
    private int idTrajet;
    private Double kilometres;
    private Date dateTrajet;
    private String typeTrajet;
    private Utilisateur utilisateur;
    private Adresse adresseDepart;
    private Adresse adresseArrivee;
    private Vehicule vehicule;

    @Id
    @Column(name = "id_trajet")
    public int getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(int idTrajet) {
        this.idTrajet = idTrajet;
    }

    @Basic
    @Column(name = "kilometres")
    public Double getKilometres() {
        return kilometres;
    }

    public void setKilometres(Double kilometres) {
        this.kilometres = kilometres;
    }

    @Basic
    @Column(name = "date_trajet")
    public Date getDateTrajet() {
        return dateTrajet;
    }

    public void setDateTrajet(Date dateTrajet) {
        this.dateTrajet = dateTrajet;
    }

    @Basic
    @Column(name = "type_trajet")
    public String getTypeTrajet() {
        return typeTrajet;
    }

    public void setTypeTrajet(String typeTrajet) {
        this.typeTrajet = typeTrajet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trajet trajet = (Trajet) o;
        return idTrajet == trajet.idTrajet &&
                Objects.equals(kilometres, trajet.kilometres) &&
                Objects.equals(dateTrajet, trajet.dateTrajet) &&
                Objects.equals(typeTrajet, trajet.typeTrajet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTrajet, kilometres, dateTrajet, typeTrajet);
    }

    @ManyToOne
    @JoinColumn(name = "utilisateur", referencedColumnName = "id_user", nullable = false)
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @ManyToOne
    @JoinColumn(name = "adresseDepart", referencedColumnName = "id_adresse")
    public Adresse getAdresseDepart() {
        return adresseDepart;
    }

    public void setAdresseDepart(Adresse adresseDepart) {
        this.adresseDepart = adresseDepart;
    }

    @ManyToOne
    @JoinColumn(name = "adresseArrivee", referencedColumnName = "id_adresse")
    public Adresse getAdresseArrivee() {
        return adresseArrivee;
    }

    public void setAdresseArrivee(Adresse adresseArrivee) {
        this.adresseArrivee = adresseArrivee;
    }

    @ManyToOne
    @JoinColumn(name = "vehicule", referencedColumnName = "id_vehicule", nullable = false)
    public Vehicule getVehicule() {
        return vehicule;
    }

    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }
}
