package popschool.frais_kilometrique.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Vehicule {
    private int idVehicule;
    private String marque;
    private String modele;
    private String plaque;
    private String couleur;
    private int chevaux;
    private String typeVehicule;
    private List<Document> documents;
    private List<Trajet> trajets;
    private Utilisateur utilisateur;

    @Id
    @Column(name = "id_vehicule")
    public int getIdVehicule() {
        return idVehicule;
    }

    public void setIdVehicule(int idVehicule) {
        this.idVehicule = idVehicule;
    }

    @Basic
    @Column(name = "marque")
    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    @Basic
    @Column(name = "modele")
    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    @Basic
    @Column(name = "plaque")
    public String getPlaque() {
        return plaque;
    }

    public void setPlaque(String plaque) {
        this.plaque = plaque;
    }

    @Basic
    @Column(name = "couleur")
    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    @Basic
    @Column(name = "chevaux")
    public int getChevaux() {
        return chevaux;
    }

    public void setChevaux(int chevaux) {
        this.chevaux = chevaux;
    }

    @Basic
    @Column(name = "type_vehicule")
    public String getTypeVehicule() {
        return typeVehicule;
    }

    public void setTypeVehicule(String typeVehicule) {
        this.typeVehicule = typeVehicule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicule vehicule = (Vehicule) o;
        return idVehicule == vehicule.idVehicule &&
                chevaux == vehicule.chevaux &&
                Objects.equals(marque, vehicule.marque) &&
                Objects.equals(modele, vehicule.modele) &&
                Objects.equals(plaque, vehicule.plaque) &&
                Objects.equals(couleur, vehicule.couleur) &&
                Objects.equals(typeVehicule, vehicule.typeVehicule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idVehicule, marque, modele, plaque, couleur, chevaux, typeVehicule);
    }

    @OneToMany(mappedBy = "vehicule")
    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    @OneToMany(mappedBy = "vehicule")
    public List<Trajet> getTrajets() {
        return trajets;
    }

    public void setTrajets(List<Trajet> trajets) {
        this.trajets = trajets;
    }

    @ManyToOne
    @JoinColumn(name = "utilisateur", referencedColumnName = "id_user", nullable = false)
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
}
