package popschool.frais_kilometrique;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FraisKilometriqueApplication {

    public static void main(String[] args) {
        SpringApplication.run(FraisKilometriqueApplication.class, args);
    }

}
