package popschool.spring_pagination.model;

import lombok.Data;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "album")
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String titre;
    private Integer annee;

    @OneToOne
    @JoinColumn(name = "suivant")
    private Album suivant;

    @OneToMany(mappedBy = "album")
    private List<Apparait> apparaits = new ArrayList<>();

    public Album(String titre, Integer annee, Album suivant) {
        this.titre = titre;
        this.annee = annee;
        this.suivant = suivant;
    }

    public Album() {
    }

    @Override
    public String toString() {
        return "Album{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", annee=" + annee +
                (suivant != null?", suivant= " + suivant.getTitre(): "") +
                '}';
    }
}
