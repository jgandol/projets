package popschool.spring_pagination.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import popschool.spring_pagination.model.Personnage;
import popschool.spring_pagination.dao.PersonnageDao;

import javax.websocket.server.PathParam;

@Controller
@SessionAttributes(value = "seks", types = String.class)
public class PersonnageController {
    @Autowired
    private PersonnageDao personnageDao;

    @ModelAttribute("seks")
    public String addMyBean1ToSessionScope(String sexe) {
        return sexe;
    }

    @GetMapping(value = "/listePersonnages")
    public String listePers(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParNom =
                PageRequest.of(currentPage-1,pageSize, Sort.by("nom"));

        Page<Personnage> personnagePage = personnageDao.findAll(PageRequest.of(currentPage-1,pageSize));

        model.addAttribute("personnagePage", personnagePage);
        model.addAttribute("listePerso", personnagePage.getContent());

        int totalPages = personnagePage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "list";
    }

    @GetMapping(value = "/listePersonnagesByNom")
    public String listePersNom(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParNom =
                PageRequest.of(currentPage-1,pageSize, Sort.by("nom").ascending());

        Page<Personnage> personnagePage = personnageDao.findAll(triParNom);

        model.addAttribute("personnagePage", personnagePage);
        model.addAttribute("listePerso", personnagePage.getContent());

        int totalPages = personnagePage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "list_nom";
    }

    @GetMapping(value = "/listePersonnagesBySexe")
    public String listePersSexe(Model model, @ModelAttribute("seks")String sexe,
                                @RequestParam("page") Optional<Integer> page,
                                @RequestParam("size") Optional<Integer> size)

    {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
//        System.out.println(sexe);
        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParNom =
                PageRequest.of(currentPage-1,pageSize, Sort.by("nom").ascending());

        Page<Personnage> personnagePage = personnageDao.findBySexe(sexe,triParNom);

        model.addAttribute("personnagePage", personnagePage);
        model.addAttribute("listePerso", personnagePage.getContent());

        int totalPages = personnagePage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "list_sexe";
    }


    @PostMapping(value = "/listePersonnagesBySexe")
    public String listePersSexe(@PathParam("sexe") String sexe,
            Model model, @ModelAttribute("seks") String s,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)

    {
        s = sexe;
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

//        model.addAttribute("sexes",sexe);

        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParNom =
                PageRequest.of(currentPage-1,pageSize, Sort.by("nom").ascending());

        Page<Personnage> personnagePage = personnageDao.findBySexe(sexe,triParNom);

        model.addAttribute("personnagePage", personnagePage);
        model.addAttribute("listePerso", personnagePage.getContent());

        int totalPages = personnagePage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "list_sexe";
    }

    @GetMapping("/list")
    public String listeRedirect(Model model){
        return "list";
    }
}
