package popschool.spring_pagination.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import popschool.spring_pagination.dao.AlbumDao;
import popschool.spring_pagination.model.Album;
import popschool.spring_pagination.model.Personnage;

import javax.websocket.server.PathParam;

@Controller
@SessionAttributes(value = {"an_debut","an_fin"}, types = {Integer.class,Integer.class})
public class AlbumController {
    @Autowired
    private AlbumDao albumDao;

    private int an_debut;
    private int an_fin;
    @ModelAttribute("an_debut")
    public int getAn_debut() {
        return an_debut;
    }

    public void setAn_debut(int an_debut) {
        this.an_debut = an_debut;
    }

    @ModelAttribute("an_fin")
    public int getAn_fin() {
        return an_fin;
    }

    public void setAn_fin(int an_fin) {
        this.an_fin = an_fin;
    }

    @GetMapping(value = "/listeAlbumsByTitre")
    public String listeAlbNom(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParNom = PageRequest.of(currentPage-1,pageSize, Sort.by("titre").ascending());

        Page<Album> albumsPage = albumDao.findAll(triParNom);

        model.addAttribute("albumsPage", albumsPage);
        model.addAttribute("listeAlbums", albumsPage.getContent());

        int totalPages = albumsPage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "list_album";
    }

    @PostMapping(value = "/listeAlbumsByAnnee")
    public String listeAlbAnPost(
            @RequestParam("d1")int dd1, @RequestParam("d2")int dd2,
            @ModelAttribute("an_debut")Integer dd,@ModelAttribute("an_fin")Integer df,
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int d1 = dd1;
        int d2 = dd2;
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
        if(d1>d2){
            Integer d3 = d2;
            d2 = d1; d1 = d3;
        }
        dd = d1; df = d2;
//        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParTitre = PageRequest.of(currentPage-1,pageSize, Sort.by("titre").ascending());

        Page<Album> albumsPage = albumDao.findByAnneeBetween(dd,df,triParTitre);

        model.addAttribute("albumsPage", albumsPage);
        model.addAttribute("listeAlbums", albumsPage.getContent());

        int totalPages = albumsPage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "list_album_an";
    }

    @GetMapping(value = "/listeAlbumsByAnnee")
    public String listeAnGet(Model model,
            @ModelAttribute("an_debut")int dd,@ModelAttribute("an_fin")int df,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size)
    {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);
//        Pageable sansTri  = PageRequest.of(currentPage-1,pageSize);
        Pageable triParTitre = PageRequest.of(currentPage-1,pageSize, Sort.by("titre").ascending());

        System.out.println("dd: " + dd + " " + df);
        Page<Album> albumsPage = albumDao.findByAnneeBetween(dd,df,triParTitre);

        model.addAttribute("albumsPage", albumsPage);
        model.addAttribute("listeAlbums", albumsPage.getContent());

        int totalPages = albumsPage.getTotalPages();
        if (totalPages > 0) {
/*            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());*/
            List<Integer> pageNumbers = new ArrayList<>();
            for(int i=1; i<=totalPages;i++)
                pageNumbers.add(i);
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "list_album_an";
    }
}
