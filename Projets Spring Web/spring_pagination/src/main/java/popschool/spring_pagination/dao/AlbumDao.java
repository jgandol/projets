package popschool.spring_pagination.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import popschool.spring_pagination.model.Album;
import popschool.spring_pagination.model.Personnage;

public interface AlbumDao extends JpaRepository<Album, Integer> {
    Page<Album> findByAnneeBetween(int an1, int an2, Pageable request);
}
