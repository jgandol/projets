package popschool.spring_pagination.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import popschool.spring_pagination.model.Personnage;

public interface PersonnageDao extends JpaRepository<Personnage, Integer> {
    Page<Personnage> findBySexe(String sexe, Pageable request);
}
