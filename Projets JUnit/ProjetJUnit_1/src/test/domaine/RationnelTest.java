package domaine;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.junit.Assert.*;

class RationnelTest {
    private Rationnel r1,r2,r3;

    @AfterTest
    void getNumerateur() {
    }

    @BeforeEach
    void setUp() {
        r1 = new Rationnel(2,-4);
        r2 = new Rationnel(1,4);
        r3 = new Rationnel(4);
    }

    @AfterEach
    void tearDown() {
        r1 = null;
        r2 = null;
        r3 = null;
    }

    @Test
    void testGetNumerateur() {
    }

    @Test
    void setNumerateur() {
    }

    @Test
    void getDenominateur() {
    }

    @Test
    void setDenominateur() {
    }

    @Test
    void add() {
    }

    @Test
    void testAdd() {
        assertEquals(0,r1.add(r2));
    }

    @Test
    void sub() {
    }

    @Test
    void testSub() {
    }

}