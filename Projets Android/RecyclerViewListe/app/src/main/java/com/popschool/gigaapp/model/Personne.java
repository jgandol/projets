package com.popschool.gigaapp.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Personne implements Serializable {
    private String firstName;
    private String lastName;
    private String email;
    private String age;

    public Personne(String firstName, String lastName, String email, String age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
    }

    public Personne() {
    }

    @Override
    public String toString() {
        return "Personne{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}
