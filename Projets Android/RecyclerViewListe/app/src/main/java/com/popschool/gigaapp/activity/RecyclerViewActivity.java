package com.popschool.gigaapp.activity;

import android.app.Activity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.popschool.gigaapp.R;
import com.popschool.gigaapp.model.Personne;

import java.util.ArrayList;


public class RecyclerViewActivity extends Activity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<Personne> contactList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        contactList.add(new Personne("Sellam", "Brahim", "b.sellam@popschool.com", "18"));
        contactList.add(new Personne("Olivier", "Batri", "o.tuning@mail.com", "25"));
        contactList.add(new Personne("Thierry", "Pelouse", "t.pelouse@gmail.com", "210"));
        contactList.add(new Personne("Anatole", "Talent", "atalent@bulldo.fr", "18"));


        setContentView(R.layout.activity_single_selection);
        recyclerView = findViewById(R.id.recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
//        mAdapter = new PeopleAdapter(RecyclerViewActivity.this, contactList);
//        mAdapter = new MyAdapter(contactList);
        recyclerView.setAdapter(mAdapter);
    }
    // ...
}
