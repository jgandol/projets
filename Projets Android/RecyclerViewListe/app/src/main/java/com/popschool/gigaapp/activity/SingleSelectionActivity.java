package com.popschool.gigaapp.activity;
import android.os.Bundle;

import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.popschool.gigaapp.R;
import com.popschool.gigaapp.adapters.SingleAdapter;
import com.popschool.gigaapp.model.Personne;


import java.util.ArrayList;

public class SingleSelectionActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Personne> personnes = new ArrayList<>();
    private SingleAdapter adapter;
    private AppCompatButton btnGetSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_selection);
        this.btnGetSelected = (AppCompatButton) findViewById(R.id.btnGetSelected);
        this.recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        getSupportActionBar().setTitle("Single Selection");

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        adapter = new SingleAdapter(this, personnes);
        recyclerView.setAdapter(adapter);

        createList();

        btnGetSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapter.getSelected() != null) {
                    showToast(adapter.getSelected().getFirstName());
                } else {
                    showToast("No Selection");
                }
            }
        });
    }

    private void createList() {
        personnes = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            Personne employee = new Personne();
            employee.setFirstName("Personne " + (i + 1));
            personnes.add(employee);
        }
        adapter.setPersonnes(personnes);
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
