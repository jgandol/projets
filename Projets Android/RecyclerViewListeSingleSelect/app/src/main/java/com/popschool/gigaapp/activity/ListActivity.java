package com.popschool.gigaapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.textservice.TextInfo;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.popschool.gigaapp.R;
import com.popschool.gigaapp.adapters.MyAdapter;
import com.popschool.gigaapp.adapters.PeopleAdapter;
import com.popschool.gigaapp.model.Personne;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {
    ArrayList<Personne> contactList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.people_list);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        contactList.add(new Personne("Sellam", "Brahim", "b.sellam@popschool.com", "18"));
        contactList.add(new Personne("Olivier", "Batri", "o.tuning@mail.com", "25"));
        contactList.add(new Personne("Thierry", "Pelouse", "t.pelouse@gmail.com", "210"));
        contactList.add(new Personne("Anatole", "Talent", "atalent@bulldo.fr", "18"));
        MyAdapter adapter = new MyAdapter(contactList);
        recyclerView.setAdapter(adapter);



        /*recyclerView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Personne p = (Personne) adapterView.getAdapter().getItem(i);
                System.out.println(p);
//                setContentView(R.layout.infos_element);
                Intent intent = new Intent(getApplicationContext(),ListActivitySecond.class);
                intent.putExtra("person",p);
                startActivity(intent);
            }


        });*/
    }

}

