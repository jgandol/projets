package com.popschool.gigaapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import com.popschool.gigaapp.R;
import com.popschool.gigaapp.model.Personne;

public class MainActivity extends AppCompatActivity {



    /*
    DOCS RETROFIT
    SWAPI : swapidev/people

    -> Recuperer la liste des personnes API Starwars
    -> Afficher dans la RecyclerView
    -> Utiliser Retrofit pour appels API (Gradle pour import librairies)
    -> DYNAMISATION PAR API
    -> Instance Retrofit => Interface(URL) => Calls


     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form);

       final EditText editTextPrenom = findViewById(R.id.prenomet);
       final EditText editTextNom = findViewById(R.id.nomet);
       final EditText editTextEmail = findViewById(R.id.emailet);
       final EditText editTextAge = findViewById(R.id.ageet);


       Button b = findViewById(R.id.buttonValiderInfos);
//
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferencesName();
//        SharedPreferences.Editor editor = SharedPreferences.edit();

        b.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                final String name = editTextPrenom.getText().toString();
                final String firstname = editTextNom.getText().toString();
                final String email = editTextEmail.getText().toString();
                final String age = editTextAge.getText().toString();

                Personne myPerson = new Personne(name,firstname,email,age);
//                System.out.println(myPerson);
                Log.d("joyPad","J'ai cliqué sur le bouton");
                Intent intent = new Intent(getApplicationContext(),ActivityForm.class);
                intent.putExtra("person",myPerson);
                startActivity(intent);
            }
        });     
    }


//    private void prefillFields(Context context){
//        SharedPreferences sharredPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//
//    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("joyPad","onStart.MainActivity");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("joyPad","onResume.MainActivity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("joyPad","onPause.MainActivity");

    }
}
