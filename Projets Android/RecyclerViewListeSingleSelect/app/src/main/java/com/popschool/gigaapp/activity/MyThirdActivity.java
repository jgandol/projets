package com.popschool.gigaapp.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.popschool.gigaapp.R;

public class MyThirdActivity extends AppCompatActivity {
    public MyThirdActivity() {
        super();
    }

    int cpt = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_layout);

        Button incrementBut = findViewById(R.id.cptButton);
        final TextView counterLabel = findViewById(R.id.cptText);
        final TextView bonjour = findViewById(R.id.bonjourText);
        final TextView cptmax = findViewById(R.id.cpt20max);

        counterLabel.setText("Clique 10 fois sur le bouton");
//        counterLabel.setText(R.string.never_clicked);
//        counterLabel.setText(getText(R.string.never_clicked));
        bonjour.setText(getString(R.string.helloargs,getIntent().getStringExtra("CLEF")));
        incrementBut.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                cpt ++;
//                String j = "Jeremie";
                Log.d("joyPad","tu as cliqué " + cpt + " fois");
//                System.out.println("Bonjour " + getIntent().getStringExtra("CLEF"));

                counterLabel.setText(getString(R.string.counter_label,cpt,getString(R.string.time)));
//                Intent intent = new Intent(getApplicationContext(),MyThirdActivity.class);
//                intent.putExtra(j, "Ouais");
//                startActivity(intent);
                if(cpt==10){
                    cptmax.setText(getString(R.string.cpt10));
                    counterLabel.setText("");
                }
                if(cpt>10){
                    counterLabel.setText("Ah bah nan t'es con t'es allé jusqu'a " + cpt);
                    cptmax.setText("");
                }
                if(cpt>=20){
                    counterLabel.setText("Doucement t'as dépassé de " + (cpt-10));
                }
                if(cpt>=30){
                    counterLabel.setText("Ah ouais tu m'ecoutes pas enfaite.. ");
                }
                if(cpt>=40){
                counterLabel.setText("C'est bon bg on a compris stop mtnt ");
                }
                if(cpt>=50){
                    counterLabel.setText("T'as gagné jte tej");
                }
                if(cpt==54){
                    setContentView(R.layout.accueil);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("joyPad","onResume.MainActivity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("joyPad", "onPause.MainActivity");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("joyPad","onStart.MainActivity");
    }
}
