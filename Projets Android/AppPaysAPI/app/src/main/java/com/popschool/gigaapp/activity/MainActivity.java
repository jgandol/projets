package com.popschool.gigaapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.popschool.gigaapp.R;

public class MainActivity extends AppCompatActivity {



    /*
    DOCS RETROFIT
    SWAPI : swapidev/people

    -> Recuperer la liste des personnes API Starwars
    -> Afficher dans la RecyclerView
    -> Utiliser Retrofit pour appels API (Gradle pour import librairies)
    -> DYNAMISATION PAR API
    -> Instance Retrofit => Interface(URL) => Calls


     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        RecyclerView r = findViewById(R.layout.my_recycler_country);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("joyPad","onStart.MainActivity");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("joyPad","onResume.MainActivity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("joyPad","onPause.MainActivity");

    }
}
