package com.popschool.gigaapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.popschool.gigaapp.R;
import com.popschool.gigaapp.activity.CountryActivity;
import com.popschool.gigaapp.bordel.SvgDecoder;
import com.popschool.gigaapp.bordel.SvgDrawableTranscoder;
import com.popschool.gigaapp.bordel.SvgSoftwareLayerSetter;
import com.popschool.gigaapp.model.Country;
import com.popschool.gigaapp.retrofit.CountryService;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PaysAdapter extends RecyclerView.Adapter<PaysAdapter.MyViewHolder> {
    private List<Country> countries;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View view;
        private Context context;
        private ImageView flag;
        private TextView name, capital;
        private LinearLayout container;


        public MyViewHolder(Context context, View v) {
            super(v);
            this.context = context;
            name = v.findViewById(R.id.name);
            capital = v.findViewById(R.id.capital);
            flag = v.findViewById(R.id.flag);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PaysAdapter(List<Country> countries) {
        this.countries = countries;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_pays, parent, false);
        return new MyViewHolder(parent.getContext(),v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element



        holder.name.setText(countries.get(position).getName());
        holder.capital.setText("Capitale:  "+ countries.get(position).getCapital());
//        ((ImageView)holder.view.findViewById(R.id.flag)).setImageURI(Uri.parse(countries.get(position).getFlag()));

        GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder = Glide.with(holder.context)
                .using(Glide.buildStreamModelLoader(Uri.class, holder.context), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .listener(new SvgSoftwareLayerSetter<Uri>());

        requestBuilder.diskCacheStrategy(DiskCacheStrategy.NONE)
                .load(Uri.parse(countries.get(position).getFlag()))
                .into(holder.flag);
//        Sharp.loadString(countries.get(position).getFlag()).into(holder.view);


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return countries.size();
    }



}

