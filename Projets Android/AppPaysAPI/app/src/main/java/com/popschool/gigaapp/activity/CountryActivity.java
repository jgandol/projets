package com.popschool.gigaapp.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.popschool.gigaapp.R;
import com.popschool.gigaapp.adapters.PaysAdapter;
import com.popschool.gigaapp.model.Country;
import com.popschool.gigaapp.retrofit.CountryService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.ArrayList;
import java.util.List;

public class CountryActivity extends AppCompatActivity {
    private String API_BASE_URL = "https://restcountries.eu/rest/v2/";
    private ArrayList<Country> countries = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_recycler_countries);
        recyclerView = findViewById(R.id.my_recycler_countries);

        ImageView flagIV = findViewById(R.id.flag);
//        Uri uri = Uri.parse("Url de l'image")
//        GlideToVectorYou.justLoadImage(this, uri, flagIV));
//        callWithAsyncTask();
        callWithRetrofit();

    }

    private void callWithRetrofit() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        CountryService service = retrofit.create(CountryService.class);
        Call<List<Country>> countries = service.getCountries();
        countries.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                CountryActivity.this.countries.addAll(response.body());
                Log.d("joyPAD", "on a retrouvé " + CountryActivity.this.countries.size() + " pays");
                callRecycler(response.body());
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.d("joyPAD", String.valueOf(t));
            }
        });
    }

    private void callRecycler(List<Country> countries) {
        if(recyclerView != null) Log.d("prout","Initialisé");

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
//        mAdapter = new PeopleAdapter(RecyclerViewActivity.this, contactList);
        mAdapter = new PaysAdapter(countries);
        recyclerView.setAdapter(mAdapter);
//        setContentView(R.layout.list_recycler_view);
    }
}
