package com.popschool.gigaapp.retrofit;

import com.popschool.gigaapp.model.Country;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface CountryService {

    @GET("all")
    Call<List<Country>> getCountries();
}
