package com.popschool.gigaapp.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.popschool.gigaapp.R;
import com.popschool.gigaapp.model.Personne;

public class ActivityForm extends AppCompatActivity {
    public ActivityForm() {
        super();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.returninfos);

        final TextView infos = findViewById(R.id.showInfos);

//        String prenom = getIntent().getExtras().getString("prenom");
//        String nom = getIntent().getExtras().getString("nom");
//        String email = getIntent().getExtras().getString("email");
//        String age = getIntent().getExtras().getString("age");
        Personne p = (Personne) getIntent().getSerializableExtra("person");


//        System.out.println(p);
        infos.setText("Vous êtes "+ p.getFirstName() + " " + p.getLastName() +
                " et vous avez " + p.getAge() + " ans : " + p.getEmail());

//        Button button = findViewById(R.id.buttonValiderInfos);
//        button.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d("joyPad","onResume.MainActivity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("joyPad", "onPause.MainActivity");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("joyPad","onStart.MainActivity");
    }
}
