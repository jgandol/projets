package com.popschool.gigaapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.textservice.TextInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.popschool.gigaapp.R;
import com.popschool.gigaapp.adapters.PeopleAdapter;
import com.popschool.gigaapp.model.Personne;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {
    ArrayList<Personne> contactList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);

        ListView lv = findViewById(R.id.people_list);
        contactList.add(new Personne("Sellam", "Brahim", "b.sellam@popschool.com", "18"));
        contactList.add(new Personne("Olivier", "Batri", "o.tuning@mail.com", "25"));
        contactList.add(new Personne("Thierry", "Pelouse", "t.pelouse@gmail.com", "210"));
        contactList.add(new Personne("Anatole", "Talent", "atalent@bulldo.fr", "18"));
        PeopleAdapter adapter = new PeopleAdapter(ListActivity.this, contactList);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Personne p = (Personne) adapterView.getAdapter().getItem(i);
                System.out.println(p);
//                setContentView(R.layout.infos_element);
                Intent intent = new Intent(getApplicationContext(),ListActivitySecond.class);
                intent.putExtra("person",p);
                startActivity(intent);
            }
        });
    }

//        lv.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view) {
//                final String name = editTextPrenom.getText().toString();
//                final String firstname = editTextNom.getText().toString();
//                final String email = editTextEmail.getText().toString();
//                final String age = editTextAge.getText().toString();
//
//                Personne myPerson = new Personne(name,firstname,email,age);
//                System.out.println(myPerson);
//                Log.d("joyPad","J'ai cliqué sur le bouton");
//
//                Intent intent = new Intent(getApplicationContext(),ActivityForm.class);
//                intent.putExtra("person",myPerson);
//                startActivity(intent);
//            }
//        });

}

