package com.popschool.gigaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.popschool.gigaapp.R;
import com.popschool.gigaapp.model.Personne;

import java.util.ArrayList;
import java.util.List;

public class PeopleAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Personne> contactsList;

    @Override
    public int getCount() {
        return contactsList.size();
    }

    @Override
    public Object getItem(int i) {
        return contactsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = LayoutInflater.from(context).inflate(R.layout.list_item, viewGroup, false);
        }
        Personne p = (Personne) getItem(i);
        TextView fullnameET=view.findViewById(R.id.fullname);
        TextView emailET=view.findViewById(R.id.email);
//        TextView ageET=view.findViewById(R.id.age);
        String fullname = p.getFirstName() + " " + p.getLastName();
        fullnameET.setText(fullname);
        emailET.setText(p.getEmail());
//        ageET.setText(String.valueOf(p.getAge()));
        return view;
    }

    public PeopleAdapter(Context context, ArrayList<Personne> contactsList) {
        this.context = context;
        this.contactsList = contactsList;
    }
}
