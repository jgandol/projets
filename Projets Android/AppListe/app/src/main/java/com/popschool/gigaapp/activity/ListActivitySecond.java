package com.popschool.gigaapp.activity;

import android.os.Bundle;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.popschool.gigaapp.R;
import com.popschool.gigaapp.model.Personne;

public class ListActivitySecond extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infos_element);
        Personne p = (Personne) getIntent().getSerializableExtra("person");
        final TextView nom = findViewById(R.id.fullnameEl);
        final TextView age = findViewById(R.id.ageEl);
        final TextView mail = findViewById(R.id.emailEl);
        System.out.println(p.getFirstName() + " " + p.getLastName());
        nom.setText(p.getFirstName() + " " + p.getLastName());
        age.setText(p.getAge());
        mail.setText(p.getEmail());

    }


}
