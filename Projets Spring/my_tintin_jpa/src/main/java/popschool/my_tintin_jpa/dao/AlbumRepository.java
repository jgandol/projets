package popschool.my_tintin_jpa.dao;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.w3c.dom.ls.LSOutput;
import popschool.my_tintin_jpa.model.Album;

import java.sql.SQLOutput;
import java.util.List;

public interface AlbumRepository extends CrudRepository<Album,Integer> {

@Query("select a from Album a")

    List<Album> findAll();
//    List<Album> findAllOrderByTitreAsc();
//    @Query("select a from Album a join fetch a.apparaits ap")
//    @Query("select p from Personnage p join fetch p.apparaits ap")
//    List<Personnage> findAllpersonnagesEtAlbums();

//    PersonnageRepository.findPersonnageEtAlbums().forEach(p->{
//    System.out.println(p);
//    p.getAlbums().foreach(a->{System.out.println("--" + a.getAlbum().toString());})});


//    Nested: les albums suites d’un album caractérisé par son titre
    @Query("select b from Album a inner join Album b on a.suivant.id = b.id where a.titre LIKE :titre")
    List<Album> findSuiteOfAlbum(String titre);

//    Less than: les albums parus avant l’année proposée
    List<Album> findByAnneeLessThan(int an);

//    Between: les albums parus entre 2 années proposées
    List<Album> findByAnneeBetween(int debut,int fin);

    List<Album> findByTitreAndSuivantIsNotNull(String titre);

    

}
