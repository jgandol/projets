package popschool.my_tintin_jpa.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.my_tintin_jpa.model.Album;
import popschool.my_tintin_jpa.model.Personnage;

import java.util.List;

public interface PersonnageRepository extends CrudRepository<Personnage,Integer> {

//    Equalité: le personnage défini par son nom
    List<Personnage> findByNomOrPrenomLike(String nom,String prenom);

//    IgnoreCase: le personnage défini par son prénom en ignorant la casse descaractères
    List<Personnage> findByPrenomIgnoreCase(String prenom);

//    Not Equal: les personnages qui ne sont pas du genre proposéLike/Contains/Start
    List<Personnage> findByGenreNotContains(String genre);

//    with: les personnages dont le prénom correspondent au pattern/ contiennent les lettres proposées /  commencent par la suite de lettres proposées
    List<Personnage> findByPrenomContainingIgnoreCase(String pattern);

//    Logical And: le personnage dont le nom et le prénom sont proposés
    List<Personnage> findByPrenomAndNomLike(String prenom, String nom);

//    Logical Or: les personnages qui sont soit du genre soit du sexe proposé
    List<Personnage> findByGenreOrSexeLikeOrderByPrenom(String genre, String sexe);

    @Query("Select p from Personnage p where p.prenom IS NULL")
    List<Personnage> findPrenomNull();

//    Find all albums et personnage
    @Query("select distinct p from Personnage p join fetch p.apparaits ap")
    List<Personnage> findAllPersonnagesEtAlbums();

//    Personnage d'un album donné
    List<Personnage> findByApparaits_Album_TitreLike(String titreApproche);

}
