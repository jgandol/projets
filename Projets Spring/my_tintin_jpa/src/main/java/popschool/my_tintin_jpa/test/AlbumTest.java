package popschool.my_tintin_jpa.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import popschool.my_tintin_jpa.dao.AlbumRepository;
import popschool.my_tintin_jpa.dao.PersonnageRepository;
import popschool.my_tintin_jpa.model.Album;

@Component
public class AlbumTest implements CommandLineRunner {

    private final static Logger log = LoggerFactory.getLogger(AlbumTest.class);

    private void information(Album a){
        log.info(a.toString());
    }

    @Autowired
    AlbumRepository albumRep;

    @Autowired
    PersonnageRepository persoRep;

    @Override
    public void run(String... args) throws Exception {

//        albumRep.findAll().forEach(System.out::println);

//        persoRep.findByNomOrPrenomLike("Dupont","").forEach(System.out::println);

//        persoRep.findByPrenomIgnoreCase("ARCHIBALD").forEach(System.out::println);

//        persoRep.findByGenreNotContains("GENTIL").forEach(System.out::println);

//        persoRep.findByPrenomContainingIgnoreCase("ch").forEach(System.out::println);

//        persoRep.findByPrenomAndNomLike("Tryphon","TOURNESOl").forEach(System.out::println);

//        persoRep.findByGenreOrSexeLikeOrderByPrenom("MECHANT","F").forEach(System.out::println);

//        albumRep.findSuiteOfAlbum("Les cigares du pharaon").forEach(System.out::println);

//        albumRep.findByAnneeLessThan(1950).forEach(System.out::println);

//        albumRep.findByAnneeBetween(1935,1950).forEach(System.out::println);

//        persoRep.findAllPersonnagesEtAlbums().forEach(p->{
//            System.out.println("\n --------||| " + p);
//            p.getApparaits().forEach(
//                    a->{System.out.println("--> " + a.getAlbum().toString());
//                    });
//        });


//        persoRep.findByApparaits_Album_TitreLike("Le Lotus Bleu").forEach(System.out::println);

albumRep.findByTitreAndSuivantIsNotNull("Le temple du Soleil");
    }


}
