package popschool.my_tintin_jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyTintinJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTintinJpaApplication.class, args);
    }

}
