package popschool.my_tintin_jpa.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "apparait")
public class Apparait {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    @JoinColumn(name="album_id")
    private Album album;
    @ManyToOne
    @JoinColumn(name = "personnage_id")
    private Personnage personnage;

    public Apparait() {
    }

    @Override
    public String toString() {
        return "Apparait{" +
                "id=" + id +
                ", album=" + album.getTitre() +
                ", personnage=" + personnage.getNom() + " " +personnage.getPrenom() +
                '}';
    }
}
