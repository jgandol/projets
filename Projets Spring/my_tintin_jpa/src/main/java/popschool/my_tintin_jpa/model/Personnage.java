package popschool.my_tintin_jpa.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "personnage")
public class Personnage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String nom;
    private String prenom;
    private String profession;
    private String sexe;
    private String genre;

    @OneToMany(mappedBy = "personnage")
    private List<Apparait> apparaits = new ArrayList<>();

    public Personnage(Integer id, String prenom, String nom, String profession, String sexe, String genre) {
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.profession = profession;
        this.sexe = sexe;
        this.genre = genre;
    }

    public Personnage() {
    }

    @Override
    public String toString() {
        return "Personnage{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", profession='" + profession + '\'' +
                ", sexe='" + sexe + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}
