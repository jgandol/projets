package popschool.activites_sportives;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivitesSportivesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActivitesSportivesApplication.class, args);
    }

}
