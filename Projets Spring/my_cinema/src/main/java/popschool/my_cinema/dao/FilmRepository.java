package popschool.my_cinema.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.my_cinema.model.Film;
import popschool.my_cinema.model.Genre;


import javax.persistence.Tuple;
import java.util.List;

public interface FilmRepository extends CrudRepository<Film,Long> {

//Liste des films d'un acteur caractérisé par son nom
    List<Film> findByNationalite_Nom(String pays);

//Liste des films d'un genre caractérisé par sa nature
    @Query("select f from Film f join Genre g on g.ngenre = f.genre.ngenre where g.nature = :nature")
    List<Film> findByGenre(String nature);

//nombre de films par genre(group by)
    @Query("select f.genre.nature as nature,count(f) as nb from Film f group by f.genre.nature")
    List<Tuple> CountByGenres();

//nom de films par acteur (group by)
    @Query("select a.filmsbyacteur from Film f join Acteur a on a.nacteur = f.nacteurprincipal.nacteur where a.nom = :nomActeur")
    List<Film> findFilmByActeur(String nomActeur);

//        Film
//                Liste des films empruntable
//                Liste des films empruntés
//                Liste des films en retard


}
