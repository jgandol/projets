package popschool.my_cinema.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.my_cinema.model.Client;
import popschool.my_cinema.model.Emprunt;

public interface ClientRepository extends CrudRepository<Client,Integer> {

//        Gestion des Clients
//                Creation,Modification
//                Suppression
//                Finder ... divers

    @Query("delete from Emprunt e where e.client.nom = :nom")
    @Modifying
    int supprimerEmpruntsClient(String nom);



}
