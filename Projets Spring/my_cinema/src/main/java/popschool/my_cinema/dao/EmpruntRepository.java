package popschool.my_cinema.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.my_cinema.model.Emprunt;

public interface EmpruntRepository extends CrudRepository<Emprunt,Integer> {

//        Gestion des Emprunts
//                Ajout
//                Restitution
//                Finder ...

    int deleteByClient_Nom(String nom);
}
