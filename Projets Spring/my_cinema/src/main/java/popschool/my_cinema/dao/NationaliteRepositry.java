package popschool.my_cinema.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.my_cinema.model.Nationalite;


import java.util.List;

public interface NationaliteRepositry extends CrudRepository<Nationalite, Long> {


}
