package popschool.my_cinema.model;


import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "FILM")
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "nfilm")
    Long nfilm;

    @Basic
    @Column(name = "titre")
    String titre;

    @Column(name = "sortie")
    Date sortie;

    @ManyToOne
    @JoinColumn(name = "nacteurPrincipal", referencedColumnName = "nacteur")
    Acteur nacteurprincipal;

    @Column(name = "realisateur")
    String realistaeur;

    @Column(name = "entrees")
    Integer entrees;

    @Column(name = "oscar")
    String oscar;

    @ManyToOne
    @JoinColumn(name = "ngenre",referencedColumnName = "ngenre")
    Genre genre;

    @ManyToOne
    @JoinColumn(name = "npays", referencedColumnName = "npays")
    Nationalite nationalite;

    @OneToMany(mappedBy = "film")
    List<Emprunt> emprunts = new ArrayList<>();

}
