package popschool.my_cinema.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ACTEUR")
@Data
public class Acteur {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "nacteur")
    Long nacteur;

    @Basic
    @Column(name = "nom")
    String nom;

    @Basic
    @Column(name = "prenom")
    String prenom;

    @Basic
    @Column(name = "naissance")
    Date naissance;

    @Basic
    @Column(name = "nbreFilms")
    Integer nbrefilms;


    @ManyToOne
    @JoinColumn(name = "nationalite")
    Nationalite nationalite;

    @OneToMany(mappedBy = "nacteurPrincipal")
    List<Film> filmsbyacteur = new ArrayList<>();

    public Acteur() {
    }

    @Override
    public String toString() {
        return "Acteur{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", nbreFilms=" + nbrefilms +
                ", nationalite=" + nationalite.getNom() +
                '}';
    }

    public Long getNacteur() {
        return nacteur;
    }

    public void setNacteur(Long nacteur) {
        this.nacteur = nacteur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }

    public Integer getNbrefilms() {
        return nbrefilms;
    }

    public void setNbrefilms(Integer nbrefilms) {
        this.nbrefilms = nbrefilms;
    }


}
