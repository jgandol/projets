package popschool.my_cinema.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PAYS")
@Data
public class Nationalite {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "npays")
    Long npays;
    String nom;

    @OneToMany(mappedBy = "nationalite")
    List<Acteur> acteurs ;

    @OneToMany
    @JoinColumn(referencedColumnName = "npays",name = "npays")
    List<Film> filmsbynationalite;

}
