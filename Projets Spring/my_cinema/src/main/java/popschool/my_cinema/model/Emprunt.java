package popschool.my_cinema.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Data
@Table(name = "EMPRUNT")
public class Emprunt {
    private Integer nemprunt;
    private String retour;
    private Date dateEmprunt;
    private Client client;
    private Film film;

    @Id
    @Column(name = "nemprunt")
    public int getNemprunt() {
        return nemprunt;
    }
    public void setNemprunt(int nemprunt) {
        this.nemprunt = nemprunt;
    }

    @Basic
    @Column(name = "retour")
    public String getRetour() {
        return retour;
    }
    public void setRetour(String retour) {
        this.retour = retour;
    }

    @Basic
    @Column(name = "dateEmprunt")
    public Date getDateEmprunt() {
        return dateEmprunt;
    }
    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }


    @ManyToOne
    @JoinColumn(name = "nclient")
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }

    @ManyToOne
    @JoinColumn(name = "nfilm")
    public Film getFilm() {
        return film;
    }
    public void setFilm(Film film) {
        this.film = film;
    }

    public Emprunt() {
    }

    public Emprunt(String retour, Date dateEmprunt, Client client, Film film) {
        this.retour = retour;
        this.dateEmprunt = dateEmprunt;
        this.client = client;
        this.film = film;
    }
}
