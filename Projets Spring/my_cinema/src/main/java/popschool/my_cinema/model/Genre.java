package popschool.my_cinema.model;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "GENRE")
@Data
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ngenre")
    Long ngenre;
    String nature;

    @OneToMany
    @JoinColumn(name = "ngenre")
    List<Film> filmsbygenre;
}
