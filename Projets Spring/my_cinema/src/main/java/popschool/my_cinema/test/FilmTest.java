package popschool.my_cinema.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.my_cinema.MyCinemaApplication;
import popschool.my_cinema.dao.FilmRepository;
import popschool.my_cinema.model.Film;


@Component
public class FilmTest implements CommandLineRunner {

    private final static Logger log = LoggerFactory.getLogger(MyCinemaApplication.class);

    @Autowired
    private FilmRepository repository;


    private void information(Film f){
        log.info(f.toString());
    }


    @Override
    public void run(String... args) throws Exception {

//        repository.findByNationalite_Nom("USA").forEach(this::information);

//        System.out.println("NB DE FILMS PAR GENRE");
//        repository.CountByGenres().forEach(e->{
//            log.info(e.get("nature",String.class) + " " + e.get("nb",Long.class));
//        });
//
//        System.out.println("FILMS PAR GENRE");
//        repository.findByGenre("Drame").forEach(System.out::println);
//
//        System.out.println("FILMS PAR ACTEUR");
//        repository.findFilmByActeur("De Niro").forEach(System.out::println);







    }
}
