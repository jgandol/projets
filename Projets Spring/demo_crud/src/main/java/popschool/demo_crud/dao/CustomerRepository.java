package popschool.demo_crud.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.demo_crud.modele.Customer;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer,Long> {
    List<Customer> findByLastName(String nom);
}
