package popschool.demo_crud.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.demo_crud.DemoCrudApplication;
import popschool.demo_crud.dao.CustomerRepository;
import popschool.demo_crud.modele.Customer;

import java.util.Optional;

@Component
public class CustomerTest implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(CustomerTest.class);

    @Autowired
    private CustomerRepository rep;

    private void information(Customer c){
        log.info(c.toString());
    }

    @Override
    public void run(String... args) throws Exception {
        //Save some customers
        rep.save(new Customer("Patrick","Balkany"));
        rep.save(new Customer("Thierry","Pelouse"));
        rep.save(new Customer("Diamond","Joe"));
        rep.save(new Customer("Chirac","Nicolas"));
        rep.save(new Customer("Zarkozy","Jacques"));

        //fetch all customers
        log.info("Customer found findAll");
        log.info("-----------------------");
        for(Customer customer : rep.findAll())
            log.info(customer.toString());
        log.info("");

        //fecth an individual customer by ID
        Optional<Customer> customer = rep.findById(1L);
        log.info("Customer found with findByID(1L):");
        log.info("------------------------");
        if(customer.isPresent()){
            Customer cust = customer.get();
            log.info(customer.get().toString());
            log.info("");
            rep.delete(cust);
            rep.findAll().forEach(this::information);
            log.info("");
        }

        log.info("Customer found with findByLastName : \n");
        rep.findByLastName("Joe").forEach(name -> {log.info(name.toString());});
        log.info("");


    }


}
