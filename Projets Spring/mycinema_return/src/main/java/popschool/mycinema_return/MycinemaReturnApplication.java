package popschool.mycinema_return;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MycinemaReturnApplication {

    public static void main(String[] args) {
        SpringApplication.run(MycinemaReturnApplication.class, args);
    }

}
