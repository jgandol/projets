package popschool.mycinema_return.dao;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.mycinema_return.modele.Client;
import popschool.mycinema_return.modele.Emprunt;
import popschool.mycinema_return.modele.Film;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface EmpruntRepository extends CrudRepository<Emprunt,Long> {

//        Gestion des Emprunts
//                Ajout
//                Restitution
//                Finder ...
//
//    int deleteByClient_Nom(String nom);

    @Query(value = "select * from Emprunt e where e.dateemprunt < current_date() -15 and e.retour = 'NON' ",nativeQuery = true)
    Collection<Emprunt> findAllRetardNative();

//    Liste des films emprtunalbe ce soir
//    liste des films emprunté
//    liste des films en raterd


//    Liste des films emprunté du client
    @Query("delete from Emprunt e where e.client=:client")
    @Modifying
    @Transactional
    int supprimerEmpruntsClient(Client client);

//    Ensemble des films empruntables
    @Query("Select f from Film f left outer join Emprunt e on f.nfilm = e.film.nfilm where f.emprunts.size <= 0")
    List<Film> findAllFilmsEmpruntables();

//    Ensemble des films empruntés
    @Query("Select f from Film f inner join Emprunt e on f.nfilm = e.film.nfilm")
    List<Film> findAllFilmsEmpruntes();

////    Retour sur Emprunt
//    @Query("update Emprunt e set e.retour = 'OUI' where e.film.titre = :titre and e.client.nom = :nom")
//    @Modifying
//    @Transactional
//    void retourEmprunt(int id);

    Optional<Emprunt> findByFilmAndClient(Film f, Client c);


}
