package popschool.mycinema_return.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.mycinema_return.modele.Pays;

public interface PaysRepository extends CrudRepository<Pays, Long> {


}
