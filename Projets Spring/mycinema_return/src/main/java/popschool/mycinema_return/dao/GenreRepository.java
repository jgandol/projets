package popschool.mycinema_return.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.mycinema_return.modele.Genre;

import java.util.List;

public interface GenreRepository extends CrudRepository<Genre, Integer> {

    List<Genre> findAll();

}
