package popschool.mycinema_return.modele;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "FILM")
public class Film {
    private Long nfilm;
    private String titre;
    private Date sortie;
    private String realisateur;
    private Integer entrees;
    private String oscar;
    private List<Emprunt> emprunts;
    private Genre genre;
    private Pays pays;
    private Acteur acteur;

    public Film() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "nfilm")
    public Long getNfilm() {
        return nfilm;
    }
    public void setNfilm(Long nfilm) {
        this.nfilm = nfilm;
    }


    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "sortie")
    public Date getSortie() {
        return sortie;
    }
    public void setSortie(Date sortie) {
        this.sortie = sortie;
    }

    @Basic
    @Column(name = "realisateur")
    public String getRealisateur() {
        return realisateur;
    }
    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }


    @Basic
    @Column(name = "entrees")
    public Integer getEntrees() {
        return entrees;
    }
    public void setEntrees(Integer entrees) {
        this.entrees = entrees;
    }

    @Basic
    @Column(name = "oscar")
    public String getOscar() {
        return oscar;
    }
    public void setOscar(String oscar) {
        this.oscar = oscar;
    }



    @OneToMany(mappedBy = "film")
    public List<Emprunt> getEmprunts() {
        return emprunts;
    }
    public void setEmprunts(List<Emprunt> emprunts) {
        this.emprunts = emprunts;
    }

    @ManyToOne
    @JoinColumn(name = "ngenre", referencedColumnName = "ngenre", nullable = false)
    public Genre getGenre() {
        return genre;
    }
    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @ManyToOne
    @JoinColumn(name = "npays", referencedColumnName = "npays", nullable = false)
    public Pays getPays() {
        return pays;
    }
    public void setPays(Pays pays) {
        this.pays = pays;
    }

    @ManyToOne
    @JoinColumn(name = "nacteurPrincipal", referencedColumnName = "nacteur", nullable = false)
    public Acteur getActeur() {
        return acteur;
    }
    public void setActeur(Acteur acteur) {
        this.acteur = acteur;
    }

    @Override
    public String toString() {
        return "Film{" +
                "nfilm=" + nfilm +
                ", titre='" + titre + '\'' +
                ", sortie=" + sortie +
                ", realisateur='" + realisateur + '\'' +
                ", entrees=" + entrees +
                ", oscar='" + oscar + '\'' +
                ", genre=" + genre +
                ", pays=" + pays +
                ", acteur=" + acteur +
                '}';
    }
}
