package popschool.mycinema_return.modele;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "EMPRUNT")
public class Emprunt {
    private Long nemprunt;
    private String retour;
    private Date dateemprunt;
    private Client client;
    private Film film;

    public Emprunt() {
    }

    public Emprunt(Client client, Film film,String retour, Date dateemprunt) {
        this.retour = retour;
        this.dateemprunt =dateemprunt;
        this.client = client;
        this.film = film;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "nemprunt")
    public Long getNemprunt() {
        return nemprunt;
    }
    public void setNemprunt(Long nemprunt) {
        this.nemprunt = nemprunt;
    }

    @Basic
    @Column(name = "retour")
    public String getRetour() {
        return retour;
    }
    public void setRetour(String retour) {
        this.retour = retour;
    }

    @Basic
    @Column(name = "dateEmprunt")
    public Date getDateemprunt() {
        return dateemprunt;
    }
    public void setDateemprunt(Date dateemprunt) {
        this.dateemprunt = dateemprunt;
    }


    @ManyToOne
    @JoinColumn(name = "nclient", referencedColumnName = "nclient", nullable = false)
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }

    @ManyToOne
    @JoinColumn(name = "nfilm", referencedColumnName = "nfilm", nullable = false)
    public Film getFilm() {
        return film;
    }
    public void setFilm(Film film) {
        this.film = film;
    }

    @Override
    public String toString() {
        return "Emprunt{" +
                "nemprunt=" + nemprunt +
                ", retour='" + retour + '\'' +
                ", dateemprunt=" + dateemprunt +
//                ", client=" + client.getNom() +
//                ", film=" + film.getTitre() +
                '}';
    }
}
