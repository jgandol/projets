package popschool.mycinema_return.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinema_return.MycinemaReturnApplication;
import popschool.mycinema_return.dao.ActeurRepository;
import popschool.mycinema_return.modele.Acteur;

import java.sql.Date;
import java.util.Optional;


@Component
public class ActeurTest implements CommandLineRunner {

    private final static Logger log = LoggerFactory.getLogger(MycinemaReturnApplication.class);

    @Autowired
    private ActeurRepository repository;


    private void information(Acteur a){
        log.info(a.toString());
    }


    @Override
    public void run(String... args) throws Exception {

//        repository.findAll().forEach(this::information);
//        Optional<Acteur> opt = repository.findById(4L);
//        if(opt.isPresent()){
//            Acteur acteur = opt.get();
//            information(acteur);
//        }
//
//        Optional<Acteur> opt2 = repository.findActeurByNom("Barr");
//        if(opt2.isPresent()){
//            Acteur acteur = opt2.get();
//            information(acteur);
//        }
//
//        Optional<Acteur> opt3 = repository.findActeurByNom("THISDEONSTEXIST");
//        if(opt3.isPresent()){
//            Acteur acteur = opt3.get();
//            information(acteur);
//        }
//
//        repository.findActeurByNomContaining("be").forEach(this::information);
//
//        repository.findByPrenomIsLike("jack").forEach(this::information);
//
//        Date date = Date.valueOf("1940-01-01");
//
//        repository.findActeurByNaissanceAfter(date).forEach(this::information);
//        repository.findActeurByNaissanceBefore(new Date(1940-01-01)).forEach(this::information);
//
//        repository.findActeurByMaxFilms().forEach(this::information);
//
//        repository.findActeurByNationalite_Nom("USA").forEach(this::information);
//
//        repository.findActeurByNationalite_Npays(4L).forEach(this::information);
////
//        repository.findActeurByNationalite_NomContaining("e").forEach(this::information);

    }
}
