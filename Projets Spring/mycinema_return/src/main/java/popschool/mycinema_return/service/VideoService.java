package popschool.mycinema_return.service;

import org.springframework.data.jpa.repository.Query;
import popschool.mycinema_return.modele.Client;
import popschool.mycinema_return.modele.Film;
import popschool.mycinema_return.modele.Genre;

import javax.persistence.Tuple;
import java.util.List;

public interface VideoService {


    public void emprunter(String nom, String titre);

    public List<Client> ensClient();

    public List<Film> ensFilm();

    @Query("select e.film from Emprunt e where e.retour = \"NON\" order by e.film.titre")
    List<Client> findAllByOrderByNom();

    List<Genre> ensGenres();
    List<Film> ensFilmDuGenre(String nature);
    List<Tuple>infoRealisateurActeur(String titre);
    int nombreFilmDuGenre(String nature);
    List<Client> ensClients();
    List<Film> ensFilmsEmpruntables();
    List<Film> ensFilmsEmpruntes();
    int emprunter2(String nomClient, String titreFilm);
    void retourEmprunt(String nomClient, String titreFilm);
}
