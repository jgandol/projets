DROP table if exists BOOKS;
create table BOOKS(
    id SERIAL,
    name VARCHAR(255),
    price NUMERIC(15,2)
)