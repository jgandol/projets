package popschool.librairie.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import popschool.librairie.DAO.BookRepository;
import popschool.librairie.modele.Book;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class JdbcBookRepository implements BookRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int count() {
        int nb = jdbcTemplate.queryForObject("select count(*) from BOOKS b",Integer.class);
        return nb;
    }

    @Override
    public int save(Book book) {
        return jdbcTemplate.update("insert into BOOKS(name,price) values( ?, ?)",book.getName(),book.getPrice());
    }

    @Override
    public int update(Book book) {
        return jdbcTemplate.update(
                "update BOOKS set price = ? where id = ?",book.getPrice(), book.getId());
    }

    @Override
    public int deleteById(Long id) {
        return jdbcTemplate.update(
                "delete from BOOKS where id = ?", id);
    }


    @Override
    public List<Book> findAll() {
        return jdbcTemplate.query("select * from books",this::mapBook);
    }

    protected Book mapBook(ResultSet rs,int row) throws SQLException {
        return new Book(rs.getLong("id"),rs.getString("name"),rs.getBigDecimal("price"));
    }

//    VARIANTE
//    @Override
//    public List<Book> findAll() {
//        return jdbcTemplate.query("select * from books",(rs, num)
//                -> {return new Book(rs.getLong("id"),rs.getString("name"),rs.getBigDecimal("price"));});
//    }

//    @Override
//    public int delete(Book book){
//        return jdbcTemplate.update("delete from BOOKS where id = ?", book.getId());
//    }

    @Override
    public List<Book> findByNameAndPrice(String name, BigDecimal price) {
        return jdbcTemplate.query("select * from books where name like ? AND price <= ?",
                this::mapBook,"%"+name+"%",price);
    }

    @Override
    public Optional<Book> findById(Long id) {
        return jdbcTemplate.query("select * from books where id = ?",
                this::resultSetExtractorOptionalBook,id);
    }

    protected Optional<Book> resultSetExtractorOptionalBook(ResultSet rs) throws SQLException {
        if(rs.next()){
            return Optional.of(new Book(rs.getLong("id"),rs.getString("name"),
                    rs.getBigDecimal("price")));}return Optional.ofNullable(null);
    }

    @Override
    public String getNameById(Long id) {
        return jdbcTemplate.queryForObject("select name from BOOKS where id = ?",String.class,id);
    }
}
