package popschool.activsportives.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "PLAYER")
public class Player {
    private int playerId;
    private String name;
    private String position;
    private double salary;
    private List<Affectation> affectations;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "playerid")
    public int getPlayerid() {
        return playerId;
    }
    public void setPlayerid(int playerId) {
        this.playerId = playerId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "position")
    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }

    @Basic
    @Column(name = "salary")
    public double getSalary() {
        return salary;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }

    @OneToMany(mappedBy = "player")
    public List<Affectation> getAffectations() {
        return affectations;
    }
    public void setAffectations(List<Affectation> affectations) {
        this.affectations = affectations;
    }

    public Player(String name, String position, double salary) {
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    public Player() {
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerId=" + playerId +
                ", name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                '}';
    }
}
