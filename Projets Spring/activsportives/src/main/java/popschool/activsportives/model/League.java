package popschool.activsportives.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "LEAGUE")
public class League {
    private int leagueid;
    private String name;
    private String sport;
    private List<Team> teams;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "leagueid")
    public Integer getLeagueid() {
        return leagueid;
    }
    public void setLeagueid(Integer leagueid) {
        this.leagueid = leagueid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "sport")
    public String getSport() {
        return sport;
    }
    public void setSport(String sport) {
        this.sport = sport;
    }

    @OneToMany(mappedBy = "leagueid")
    public List<Team> getTeams() {
        return teams;
    }
    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public void addTeam(Team team){
        if(team==null) return;
        if(team.getLeagueid() != null)
            team.getLeagueid().getTeams().remove(team);
        team.setLeagueid(this);
        this.getTeams().add(team);
    }

    public League() {
    }

    public League(String name, String sport) {
        this.name = name;
        this.sport = sport;
    }


    public League(int id, String nom, String sport) {
//        this.leagueid = id;
        this.name = nom;
        this.sport = sport;
    }



    @Override
    public String toString() {
        return "League{" +
                "leagueid=" + leagueid +
                ", name='" + name + '\'' +
                ", sport='" + sport + '\'' +
                '}';
    }
}
