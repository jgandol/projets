package popschool.activsportives.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "AFFECTATION")
public class Affectation {
    private int id;
    private Player player;
    private Team team;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "playerid",foreignKey = @ForeignKey(name= "fk_affectation_player_id"))
    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }

    @ManyToOne
    @JoinColumn(name = "teamid",foreignKey = @ForeignKey(name= "fk_affectation_team_id"))
    public Team getTeam() {
        return team;
    }
    public void setTeam(Team team) {
        this.team = team;
    }

    public Affectation() {
    }

    public Affectation(Player p, Team t) {
        this.player = p;
        this.team = t;
    }

    @Override
    public String toString() {
        return "Affectation{" +
                "id=" + id +
                ", player=" + player.getName() +
                ", team=" + team.getName() +
                '}';
    }
}
