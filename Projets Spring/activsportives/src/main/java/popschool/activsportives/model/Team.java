package popschool.activsportives.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "TEAM")
public class Team {
    private int teamid;
    private String name;
    private String city;
    private List<Affectation> affectations;
    private League leagueid;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getTeamid() {
        return teamid;
    }
    public void setTeamid(int teamid) {
        this.teamid = teamid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    @OneToMany(mappedBy = "team")
    public List<Affectation> getAffectations() {
        return affectations;
    }
    public void setAffectations(List<Affectation> affectations) {
        this.affectations = affectations;
    }

    @ManyToOne
    @JoinColumn(name = "leagueid", foreignKey = @ForeignKey(name = "fk_team_leagueid"))
    public League getLeagueid() {
        return leagueid;
    }
    public void setLeagueid(League leagueid) {
        this.leagueid = leagueid;
    }

    public Team() {
    }

    public Team(String nom, String ville) {
        this.name = nom;
        this.city = ville;
    }

    public Team(Integer teamid, String name, String city) {
        this.teamid = teamid;
        this.name = name;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Team{" +
                "teamid=" + teamid +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                (leagueid!=null?", leagueid=" + leagueid.getName():" ") +
                '}';
    }
}
