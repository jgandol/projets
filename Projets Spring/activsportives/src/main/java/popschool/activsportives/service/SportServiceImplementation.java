package popschool.activsportives.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.activsportives.dao.AffectationRepository;
import popschool.activsportives.dao.LeagueRepository;
import popschool.activsportives.dao.PlayerRepository;
import popschool.activsportives.dao.TeamRepository;
import popschool.activsportives.model.Affectation;
import popschool.activsportives.model.League;
import popschool.activsportives.model.Player;
import popschool.activsportives.model.Team;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SportServiceImplementation implements SportService {
    @Autowired
    LeagueRepository leagueRepository;
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    TeamRepository teamRepository;
    @Autowired
    AffectationRepository affectationRepository;

    //    a. Rechercher toutes les ligues
    @Override
    public List<League> findAllLeague() {
        return leagueRepository.findAll();
    }

    //    b. Ajout d'une ligue définie pour son id, son nom et son sport
    @Override
    public void ajoutLeague(String nom, String sport) {
        League lol = new League(nom,sport);
        leagueRepository.save(lol);
    }

    //    c. Ajout d'une équipe définie par son id, son nom et sa ville
    @Override
    public void ajoutTeam(String nom, String ville) {
        Team team = new Team(nom,ville);
        teamRepository.save(team);
    }

    //    d. Affectation d'une équipe à une ligue définie par le nom de l'équipe et le nom de la ligue
    @Override
    public void affectTeamLeague(String teamName, String leagueName) {
        Optional<Team> t = teamRepository.findByName(teamName);
        Optional<League> l = leagueRepository.findByName(leagueName);
        Team team = t.get();
        team.setLeagueid(l.get());
        teamRepository.save(team);
    }

    //    e. Rechercher tous les joueurs
    @Override
    public List<Player> findAllPlayer() {
        return playerRepository.findAll();
    }

    //    f. Ajout d'un joueur
    @Override
    public void ajoutPlayer(String nom, String position, double salaire) {
        Player p = new Player(nom,position,salaire);
        playerRepository.save(p);
    }

    //    g. Affectation d'un joeur à une équipe définie par le nom du joueur et le nom de l’équipe
    @Override
    public void affectPlayerTeam(String playerName, String teamName) {
        Player p = playerRepository.findByName(playerName).get();
        Team t = teamRepository.findByName(teamName).get();
        Affectation a = new Affectation(p,t);
        p.getAffectations().add(a);
        t.getAffectations().add(a);
        playerRepository.save(p);
        teamRepository.save(t);
        affectationRepository.save(a);

    }

    //    h. Suppression d’un joueur (et mise à jour des objets dépendants)
    @Override
    public void deletePlayer(String name) {
        Player p = playerRepository.findByName(name).get();
        List<Affectation> list = affectationRepository.findByPlayer(p);
        playerRepository.delete(p);
        for(Affectation a: list){
            affectationRepository.delete(a);
        }
    }

}
