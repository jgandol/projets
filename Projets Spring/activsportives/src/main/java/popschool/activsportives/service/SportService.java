package popschool.activsportives.service;


import popschool.activsportives.model.League;
import popschool.activsportives.model.Player;

import java.util.List;

public interface SportService {

    //    a. Rechercher toutes les ligues
    List<League> findAllLeague();

    //    b. Ajout d'une ligue définie pour son id, son nom et son sport
    void ajoutLeague(String nom, String sport);

    //    c. Ajout d'une équipe définie par son id, son nom et sa ville
    void ajoutTeam( String nom, String ville);

    //    d. Affectation d'une équipe à une ligue définie par le nom de l'équipe et le nom de la ligue
    void affectTeamLeague(String teamName, String leagueName);

    //    e. Rechercher tous les joueurs
    List<Player> findAllPlayer();

    //    f. Ajout d'un joueur
    void ajoutPlayer(String name, String position, double salary);

    //    g. Affectation d'un joueur à une équipe définie par le nom du joueur et le nom de l’équipe
    void affectPlayerTeam(String playerName, String teamName);

    //    h. Suppression d’un joueur (et mise à jour des objets dépendants)
    void deletePlayer(String name);
}
