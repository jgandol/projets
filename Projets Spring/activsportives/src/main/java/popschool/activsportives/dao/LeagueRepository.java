package popschool.activsportives.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.activsportives.model.League;
import popschool.activsportives.model.Team;

import java.util.List;
import java.util.Optional;

public interface LeagueRepository extends CrudRepository<League,Integer> {

//    Ecrivez une méthode pour récupérer toutes les ligues en les ordonnant
//    suivant le nom croissant (test : afficher  leur nom et le sport)

    List<League> findAll();
    Optional<League> findByName(String name);
    List<League> findAllByOrderByName();

    //    Ecrivez une méthode  qui va récupérer les équipes d'une ligue dont on connaît le nom et qui affichera
//    leur nom. Le nom de la ligue devra être un paramètre de la requête. La réponse ne devra pas tenir compte
//    de la casse dans le nom de la ligue. Testez cette méthode pour la ligue "Mountain".

    @Query("select t from Team t join League l on t.leagueid.leagueid = l.leagueid where lower(l.name) LIKE lower(:ligue)")
    List<Team> findNameIgnoreCase(String ligue);

//    List<Team> findByLeague_NameIgnoreCase(String ligue);



}
