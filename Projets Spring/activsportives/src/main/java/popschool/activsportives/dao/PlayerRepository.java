package popschool.activsportives.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.activsportives.model.Player;
import popschool.activsportives.model.Team;

import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends CrudRepository<Player,Integer> {

    //    Ecrivez une méthode qui ne récupère pas les joueurs mais seulement le nom et le salaire
//    des joueurs d'une équipe dont on connaît le nom qui les affiche. (On se servira d'un Tuple ).
//    Testez cette méthode pour l'équipe "Deer".

    @Query("select p.name as name, p.salary as salary from Player p Join fetch Affectation a on p.playerid = a.player.playerid where a.team.name = :nomEquipe")
    List<Tuple> findJouerByNomEquipeTuple(String nomEquipe);

    //Augmenter de 5% le salaire de tlm
    @Query("UPDATE Player Set salary =salary*1.05")
    @Modifying
    @Transactional
    int raiseSalaryOfAll();

//    Ecrivez une méthode qui récupère l'ensemble des joueurs ( de la BD) et affiche le nom et le salaire
//    des joueurs et pour chacun d'entre eux le nom de ses équipes ou le message "aucune équipe"
//    en évitant le problème de "N+1 select".

    @Query("select distinct p from Player p join fetch p.affectations order by p.name")
    List<Player> findEquipesDesJoueurs();

   Optional<Player> findByName(String name);

   List<Player> findAll();

}
