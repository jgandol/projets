package popschool.activsportives.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.activsportives.model.Affectation;
import popschool.activsportives.model.Player;
import popschool.activsportives.model.Team;

import java.util.List;

public interface AffectationRepository extends CrudRepository<Affectation,Integer> {
    List<Affectation> findAll();

    Affectation findByTeamAndPlayer(Team t, Player p);

    List<Affectation> findByPlayer(Player p);

}
