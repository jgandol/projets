package popschool.activsportives.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.activsportives.model.Team;

import java.util.List;
import java.util.Optional;

public interface TeamRepository extends CrudRepository<Team,Integer> {

//    Ecrivez une méthode qui va récupérer toutes les équipes en les ordonnant
//    par le nom de leur ligue (critère majeur) et leur nom (critère mineur)
//    (test :  afficher pour chaque équipe, le nom de la  ligue et le nom de l'équipe)

    List<Team> findAllByOrderByLeagueidAscName();

    Optional<Team> findByName(String name);




}
