package popschool.activsportives.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.activsportives.dao.LeagueRepository;
import popschool.activsportives.dao.PlayerRepository;
import popschool.activsportives.dao.TeamRepository;

@Component
public class TestSport implements CommandLineRunner {
    @Autowired
    LeagueRepository leagueRepository;
    @Autowired
    TeamRepository teamRepository;
    @Autowired
    PlayerRepository playerRepository;

    @Override
    public void run(String... args) throws Exception {

//        leagueRepository.findAllByOrderByName().forEach(System.out::println);
//        teamRepository.findAllByOrderByLeagueidAscName().forEach(System.out::println);
//        teamRepository.findAll().forEach(System.out::println);
//        leagueRepository.findName("mountain").forEach(System.out::println);
//        playerRepository.findJouerByNomEquipeTuple("Deer").forEach(e -> {
//            System.out.println("Joueur : " + e.get("name",String.class) + " gagne " + e.get("salary",Double.class) + "€");
//        });

//        playerRepository.raiseSalaryOfAll();
//        playerRepository.findEquipesDesJoueurs().forEach(p->{
//            System.out.println(p.getName() + " gagne " + p.getSalary());
//            p.getAffectations().forEach(a->{
//                System.out.println(" --> " + a.getTeam().getName());
//            });
//        });

//        System.out.println(teamRepository.findByName("SHAPE"));
//        System.out.println(leagueRepository.findByName("of Legends"));


    }
}
