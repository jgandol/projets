package popschool.activsportives.test;

import net.bytebuddy.asm.Advice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.activsportives.ActivsportivesApplication;
import popschool.activsportives.service.SportService;
import popschool.activsportives.service.SportServiceImplementation;

@Component
public class TestService implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(ActivsportivesApplication.class);

    @Autowired
    SportService sportService;

    @Override
    public void run(String... args) throws Exception {
//        sportService.ajoutLeague("", "");
//        sportService.ajoutTeam("SHAPE","Montpellier");
//        sportService.affectTeamLeague("SHAPE","of Legends");
//        sportService.ajoutPlayer("Faker","MID",1500.00);
        sportService.affectPlayerTeam("Faker","SHAPE");
    }
}
