package popschool.activsportives;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivsportivesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActivsportivesApplication.class, args);
    }

}
