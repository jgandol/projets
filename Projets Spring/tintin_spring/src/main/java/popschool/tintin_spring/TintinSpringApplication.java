package popschool.tintin_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TintinSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(TintinSpringApplication.class, args);
    }


}
