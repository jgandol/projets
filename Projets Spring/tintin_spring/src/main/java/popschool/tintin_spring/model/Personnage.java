package popschool.tintin_spring.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Personnage {
    private int id;
    private String nom;
    private String prenom;
    private String profession;
    private String sexe;
    private String genre;

    public Personnage(int id, String nom, String prenom, String profession, String sexe, String genre) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.profession = profession;
        this.sexe = sexe;
        this.genre = genre;
    }

    public Personnage(String nom, String prenom, String profession, String sexe, String genre) {
        this.nom = nom;
        this.prenom = prenom;
        this.profession = profession;
        this.sexe = sexe;
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Personnage)) return false;
        Personnage that = (Personnage) o;
        return id == that.id &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(profession, that.profession) &&
                Objects.equals(sexe, that.sexe) &&
                Objects.equals(genre, that.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, profession, sexe, genre);
    }
}
