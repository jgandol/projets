package popschool.tintin_spring.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Album {
    private int id;
    private String titre;
    private int annee;
    private Album suivant;

    public Album(int id, String titre, int annee) {
        this.id = id;
        this.titre = titre;
        this.annee = annee;
    }

    public Album(String s, int i) {
        this.titre = s;
        this.annee = i;
        this.suivant = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Album)) return false;
        Album album = (Album) o;
        return id == album.id &&
                annee == album.annee &&
                Objects.equals(titre, album.titre) &&
                Objects.equals(suivant, album.suivant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titre, annee, suivant);
    }
}
