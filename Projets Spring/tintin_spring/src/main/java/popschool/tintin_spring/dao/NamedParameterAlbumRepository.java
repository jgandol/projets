package popschool.tintin_spring.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import popschool.tintin_spring.model.Album;
import popschool.tintin_spring.model.Personnage;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class NamedParameterAlbumRepository extends AlbumRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public List<Album> listAlbums() {
        return jdbcTemplate.query("select * from album",this::mapAlbum);
    }

//      findByID 2 methodes avec lambda expression et optionnal
    @Override
    public Album findById(int id) {
        String sql = " Select id,titre,annee from album where id = :id ";
        MapSqlParameterSource mapSqlParameterSource= new MapSqlParameterSource();
        mapSqlParameterSource.addValue("id",id);
        return namedParameterJdbcTemplate.queryForObject(sql,
                mapSqlParameterSource,
                (rs,rowNum) -> new Album(rs.getInt("id"),rs.getString("titre"),rs.getInt("annee")));
    }

    public Optional<Album> findByIdOpt(int id) {
        return namedParameterJdbcTemplate.query("select * from books where id=:id",
                new MapSqlParameterSource("id",id),this::resultSetExtractorOptionalAlbum);
    }

//    Utilisation du result set pour l'eventualité d'un id null
    protected Optional<Album> resultSetExtractorOptionalAlbum(ResultSet rs) throws SQLException {
        if(rs.next()){
            return Optional.of(new Album(rs.getInt("id"),
                    rs.getString("titre"),rs.getInt("annee")));}return Optional.ofNullable(null);
    }


//    Recherche d'un mot clef dans le titre avec utilisation d'une methode spécialisée
    @Override
    public List<Album> listByTitre(String titre) {
        MapSqlParameterSource mapSqlParameterSource= new MapSqlParameterSource();
        mapSqlParameterSource.addValue("titre","%"+titre+"%");
        return namedParameterJdbcTemplate.query("select * from album where name like :titre",
                mapSqlParameterSource, super::mapAlbum);
    }

//    Recherche de tous les albums paru entre 2 années données. Ici on utilisera 2 parametres nommés
    @Override
    public List<Album> listByParution(int an1, int an2) {
        String sql = "select * from album where annee BETWEEN :a1 AND :a2";
        MapSqlParameterSource mapSqlParameterSource =
                new MapSqlParameterSource().addValue("a1",an1).addValue("a2",an2);
        return namedParameterJdbcTemplate.query(sql,mapSqlParameterSource,this::mapAlbum);
    }

//    Creation d'un nouvel album dans la table, ici on utilisera BeanPropertySqlParameterSource
//     afin de faire correspondre les champs d'album dans la table et dans le DTO
    @Override
    public int ajoutAlbum(Album a) {
        return namedParameterJdbcTemplate.update(
                "insert into album (titre,annee,suivant) values (:titre,:annee,:suivant)",
                new BeanPropertySqlParameterSource(a));
    }
}