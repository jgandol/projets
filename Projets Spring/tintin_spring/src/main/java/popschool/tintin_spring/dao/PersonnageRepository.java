package popschool.tintin_spring.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import popschool.tintin_spring.model.Album;
import popschool.tintin_spring.model.Personnage;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class PersonnageRepository implements PersonnageDAO{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Personnage findById(int id) {
        String sql = " Select * from personnage where id = ? ";
        return jdbcTemplate.queryForObject(sql, this::mapPersonnage, id);
    }

    protected Personnage mapPersonnage(ResultSet rs, int row) throws SQLException {
        return new Personnage(rs.getInt("id"),rs.getString("nom"),
                rs.getString("prenom"),rs.getString("profession")
        ,rs.getString("sexe"),rs.getString("genre"));
    }

    ////////////////////////////////// args null possible (Optional) ////////////////////////////////////
//    @Override
//    public Optional<Personnage> findByNomPrenom(String nom, String prenom) {
//        return jdbcTemplate.query("select * from personnage where nom LIKE ? OR prenom LIKE ?",
//                this::resultSetExtractorOptionalPersonnage,nom,prenom);
//    }

    @Override
    public Optional<Personnage> findByNomPrenom(String nom, String prenom) {
//        return jdbcTemplate.query("select * from personnage where lower(if null(nom,'')) LIKE ? and lower(if null(prenom,'')) LIKE ? ",
        return jdbcTemplate.query("select * from personnage where nom LIKE ? and prenom LIKE ? ",
                this::resultSetExtractorOptionalPersonnage,nom,prenom);
    }

    protected Optional<Personnage> resultSetExtractorOptionalPersonnage(ResultSet rs) throws SQLException {
        if(rs.next()){
            return Optional.of(new Personnage(rs.getInt("id"),rs.getString("nom"),
                    rs.getString("prenom"),rs.getString("profession")
                    ,rs.getString("sexe"),rs.getString("genre")));}return Optional.ofNullable(null);
    }

    //////////////////////////////référence de méthode ///////////////////////////////
    @Override
    public List<Personnage> listPersonnagesBySexe(String sexe) {
        return jdbcTemplate.query("select * from personnage where sexe = ?",this::mapPersonnage,sexe);
    }

    ////////////////////////////// lamba ////////////////////////////
    @Override
    public List<Personnage> listPersonnagesByJob(String job) {
        return jdbcTemplate.query("select * from personnage where profession LIKE ?",
                (rs, rowNum) -> {return new Personnage(rs.getInt("id"),rs.getString("nom"),
                        rs.getString("prenom"),rs.getString("profession")
                        ,rs.getString("sexe"),rs.getString("genre"));},job);
    }

    ////////////////////////////// RowMapper /////////////////////////////
    @Override
//    public List<Personnage> listPersonnagesByGenre(String genre) {
//        return jdbcTemplate.query("select * from personnage where genre = ?",this::mapPersonnage,genre);
//    }


    public List<Personnage> listPersonnagesByGenre(String genre){
        String sql = "select * from personnage where genre = ?";

        PersonnageRowMapper rowMapper = new PersonnageRowMapper();

        List<Personnage> list = jdbcTemplate.query(sql, rowMapper, genre);
        return list;
    }

    private static final class PersonnageRowMapper implements RowMapper<Personnage> {
        @Override
        public Personnage mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Personnage(rs.getInt("id"),rs.getString("nom"),
                    rs.getString("prenom"),rs.getString("profession")
                    ,rs.getString("sexe"),rs.getString("genre"));
        }
    }

    ///////////////////////////// MAP /////////////////////////////


    @Override
    public Map<Personnage, List<Album>> listAlbumsOfPersonnage() {
    String sql = "select a.id, a.titre, a.annee,p.id,p.nom,p.prenom,p.profession,p.sexe,p.genre "
            + " from personnage p join apparait ap on p.id = ap.personnage_id "
            + " join album a on a.id = ap.album_id ORDER BY p.prenom";
            Map<Personnage, List<Album>> map = jdbcTemplate.query(sql,new PersonnageListResultSetExtractor());
                return map;
    }

    @Override
    public int ajoutPersonnage(Personnage p) {
        String sql = "insert into personnage (nom,prenom,profession,sexe,genre) values ('" + p.getNom() + "','"
                + p.getPrenom() + "','" + p.getProfession() + "','" + p.getSexe() + "','" +
                p.getGenre() + "')";
        return jdbcTemplate.update(sql);
    }

    @Override
    public int assocAlbumSuivant(Album a, Album suivant) {

        String sql = "UPDATE albums SET suivant = " + suivant.getId() + " WHERE id = " + a.getId();
        return jdbcTemplate.update(sql);
    }

    @Override
    public int assocAlbumPersonnage(Album a, Personnage p) {

        String sql = "INSERT INTO apparaits(personnage_id, album_id) values(" + p.getId() + "," + a.getId() + ")";
        return jdbcTemplate.update(sql);
    }
}
