package popschool.tintin_spring.dao;

import org.springframework.stereotype.Repository;
import popschool.tintin_spring.model.Album;
import popschool.tintin_spring.model.Personnage;

import java.util.List;
import java.util.Map;


public interface AlbumDAO {


    List<Album> listAlbums();
    Album findById(int id);

    List<Album> listByTitre(String titre);
    List<Album> listByParution(int an1, int an2);
    Map<Album, List<Personnage>> listPersonnagesOfAlbum();
    int ajoutAlbum(Album album);


}
