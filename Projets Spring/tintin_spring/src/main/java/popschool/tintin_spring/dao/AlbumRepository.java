package popschool.tintin_spring.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import popschool.tintin_spring.model.Album;
import popschool.tintin_spring.model.Personnage;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AlbumRepository implements AlbumDAO{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<Album> listAlbums() {
        return jdbcTemplate.query("select * from album",this::mapAlbum);
    }

    protected Album mapAlbum(ResultSet rs, int row) throws SQLException {
        return new Album(rs.getInt("id"),rs.getString("titre"),
                rs.getInt("annee"));
    }
    //select a.id, a.titre, a.annee if null (b.titre,'sans suite') as titre_suivant from album a, left outer join album b on a.suivant = b.id

    @Override
    public Album findById(int id) {
        String sql = " Select * from album where id = ? ";
        return jdbcTemplate.queryForObject(sql, this::mapAlbum, id);
    }

    @Override
    public List<Album> listByTitre(String titre) {
        return jdbcTemplate.query("select * from album where name like ?",this::mapAlbum,"%"+titre+"%");
    }

    @Override
    public List<Album> listByParution(int an1, int an2) {
        String sql = "select * from album where annee BETWEEN " + an1 + " and " + an2;
        return jdbcTemplate.query(sql,this::mapAlbum);
    }

    @Override
    public Map<Album, List<Personnage>> listPersonnagesOfAlbum() {
        String sql = "select a.id, a.titre, a.annee,p.id,p.nom,p.prenom,p.profession,p.sexe,p.genre "
                + " from personnage p join apparait ap on p.id = ap.personnage_id "
                + " join album a on a.id = ap.album_id ORDER BY p.prenom";
        Map<Album, List<Personnage>> map = jdbcTemplate.query(sql,this::extractData);
        return map;
    }

    public Map<Album, List<Personnage>> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Map<Album, List<Personnage>> map = new HashMap<>();
        while(rs.next()){
            Album a = new Album(rs.getInt(1),rs.getString("titre"),rs.getInt("annee"));
            Personnage p = new Personnage(rs.getInt(4),rs.getString("nom"),
                    rs.getString("prenom"),rs.getString("profession")
                    ,rs.getString("sexe"),rs.getString("genre"));

            if(map.containsKey(a)){
                map.get(a).add(p);
            } else {
                List<Personnage> list = new ArrayList<>();
                list.add(p);
                map.put(a, list);
            }
        }
        return map;
    }

    @Override
    public int ajoutAlbum(Album a) {
        String sql = "insert into album (titre,annee,suivant) values ('" + a.getTitre() + "',"
                + a.getAnnee() + "," + a.getSuivant() +")";
        return jdbcTemplate.update(sql);
    }



}
