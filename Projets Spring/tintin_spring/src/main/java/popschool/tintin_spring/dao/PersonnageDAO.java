package popschool.tintin_spring.dao;

import popschool.tintin_spring.model.Album;
import popschool.tintin_spring.model.Personnage;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface PersonnageDAO {

    Personnage findById(int id);
    Optional<Personnage> findByNomPrenom(String nom, String prenom); //args null possible

    List<Personnage> listPersonnagesBySexe(String sexe); //référence de méthode
    List<Personnage> listPersonnagesByJob(String job); //lamba
    List<Personnage> listPersonnagesByGenre(String genre); //RowMapper

    Map<Personnage, List<Album>> listAlbumsOfPersonnage();

    int ajoutPersonnage(Personnage personnage);
    public int assocAlbumSuivant(Album a, Album suivant);
    int assocAlbumPersonnage(Album a, Personnage p);
}
