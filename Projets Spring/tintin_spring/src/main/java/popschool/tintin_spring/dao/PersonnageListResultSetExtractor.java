package popschool.tintin_spring.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import popschool.tintin_spring.model.Album;
import popschool.tintin_spring.model.Personnage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonnageListResultSetExtractor implements ResultSetExtractor<Map<Personnage, List<Album>>> {
    @Override
    public Map<Personnage, List<Album>> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Map<Personnage, List<Album>> map = new HashMap<>();
        while(rs.next()){
            Personnage p = new Personnage(rs.getInt(4),rs.getString("nom"),
                    rs.getString("prenom"),rs.getString("profession")
                    ,rs.getString("sexe"),rs.getString("genre"));
            Album a = new Album(rs.getInt(1),rs.getString("titre"),rs.getInt("annee"));

            if(map.containsKey(p)){
                map.get(p).add(a);
            } else {
                List<Album> list = new ArrayList<>();
                list.add(a);
                map.put(p, list);
            }
        }
        return map;
    }
}

