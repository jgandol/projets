package popschool.tintin_spring.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.tintin_spring.dao.AlbumRepository;
import popschool.tintin_spring.dao.PersonnageListResultSetExtractor;
import popschool.tintin_spring.dao.PersonnageRepository;
import popschool.tintin_spring.model.Album;
import popschool.tintin_spring.model.Personnage;

import java.sql.SQLOutput;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class TestPersonnage implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(TestPersonnage.class);

    @Autowired
//    @Qualifier("NamedParameterAlbumRepository")
    private PersonnageRepository personnageRepository;

    @Autowired
    private AlbumRepository albumRepository;

        @Override
        public void run(String... args) throws Exception {
//            System.out.println("FindById: " + personnageRepository.findById(5));
//
//            Optional<Personnage> aux = personnageRepository.findByNomPrenom("Milou", "");
//            System.out.println("FindByNomPrenom: " + (aux.isPresent() ? aux.get() : "Absent"));
//
//            System.out.println("findBySexe: " + personnageRepository.listPersonnagesBySexe("M"));
//
            System.out.println("findByJob: " + personnageRepository.listPersonnagesByJob("Gangster"));
//
//            System.out.println("findByGenre: " + personnageRepository.listPersonnagesByGenre("MECHANT"));
//            personnageRepository.listPersonnagesByJob("Gangster").forEach(System.out::println);
//
//            System.out.println("\nListe des albums où apparaissent les personnages\n _________________________________________");
//            Map<Personnage, List<Album>> map = personnageRepository.listAlbumsOfPersonnage();
//            map.entrySet().forEach(System.out::println);

            System.out.println("\nListe des albums:");
            albumRepository.listAlbums().forEach(System.out::println);

//            System.out.println("Liste des albums entre 1900 et 2000 :\n");
//            albumRepository.listByParution(1940,1960).forEach(System.out::println);

            System.out.println("\nListe des personnages par album\n_________________________________________________");
            albumRepository.listPersonnagesOfAlbum().entrySet().forEach(System.out::println);

//            Album a = new Album("Tintin et les SJW",2020);
//            System.out.println(albumRepository.ajoutAlbum(a));

//            Personnage p = new Personnage("Balkani","Patrick","Gangster","M","MECHANT");
//            System.out.println(personnageRepository.ajoutPersonnage(p));
        }

}

