package popschool.tintin_spring.test;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.tintin_spring.dao.AlbumDAO;


@Component
public class TestNamedParameter implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(TestNamedParameter.class);

    @Autowired
    @Qualifier("namedParameterAlbumRepository")
    private AlbumDAO albumDAO;


    @Override
    public void run(String... args) throws Exception {

        System.out.println("\nListe des albums:");
        albumDAO.listAlbums().forEach(System.out::println);

            System.out.println("Liste des albums entre 1900 et 2000 :\n");
            albumDAO.listByParution(1940,1960).forEach(System.out::println);
//
//        System.out.println("\nListe des personnages par album\n_________________________________________________");
//        albumRepository.listPersonnagesOfAlbum().entrySet().forEach(System.out::println);

//            Album a = new Album("Tintin et les SJW",2020);
//            System.out.println(albumRepository.ajoutAlbum(a));

//            Personnage p = new Personnage("Balkani","Patrick","Gangster","M","MECHANT");
//            System.out.println(personnageRepository.ajoutPersonnage(p));
    }

}