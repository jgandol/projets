drop table  if exists EMPLOYEE;
drop table if exists DEPT ;

CREATE TABLE DEPT (
                      DEPTNO  INT   NOT NULL,
                      DNAME   VARCHAR(14),
                      LOC     VARCHAR(13),
                      CONSTRAINT PK_DEPT
                          PRIMARY KEY ( DEPTNO ) );

CREATE TABLE EMPLOYEE (
                          EMPNO     NUMERIC(4)    NOT NULL,
                          ENAME     VARCHAR(10),
                          JOB       VARCHAR(9),
                          MGR       NUMERIC(4) ,
                          HIREDATE  DATE,
                          SAL       NUMERIC(7,2),
                          COMM      NUMERIC(7,2),
                          DEPTNO    INT,
                          CONSTRAINT FK_EMP_DEPTNO FOREIGN KEY (DEPTNO) REFERENCES DEPT(DEPTNO),
                          CONSTRAINT PK_EMP
                              PRIMARY KEY ( EMPNO ) ) ;