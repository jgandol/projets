package popschool.rh_2020.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QueryForListReturnListDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;
    public List<String> getDeptNames(){
        String sql ="SELECT d.dname FROM Dept d";
        return jdbcTemplate.queryForList(sql, String.class);
    }
    public List<String> getDeptNames(String searchName){
        String sql ="SELECT d.dname FROM Dept d WHERE d.dname like ?";
        return jdbcTemplate.queryForList(sql, String.class, "%"+searchName+"%");
    }
}