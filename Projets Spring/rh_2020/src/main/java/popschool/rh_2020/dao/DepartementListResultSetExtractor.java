package popschool.rh_2020.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import popschool.rh_2020.modele.Department;
import popschool.rh_2020.modele.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class DepartementListResultSetExtractor implements ResultSetExtractor<Map<Department, List<Employee>>> {
    @Override
    public Map<Department, List<Employee>> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Map<Department, List<Employee>> map = new HashMap<>();
        while(rs.next()){
            Department dept = new Department(rs.getInt("deptno"),rs.getString("dname"),rs.getString("loc"));
            Employee emp = new Employee(rs.getInt("empno"),rs.getString("ename"),rs.getString("job"),
                    rs.getDouble("sal"));
            if(map.containsKey(dept)){
                map.get(dept).add(emp);
            } else {
                map.put(dept,new ArrayList<Employee>());
                map.get(dept).add(emp);
            }
//            employees.add(new Employee(rs.getInt("empno"),rs.getString("ename"),rs.getString("job"),
//                    rs.getDouble("sal")));
        }
        return map;
    }
}
