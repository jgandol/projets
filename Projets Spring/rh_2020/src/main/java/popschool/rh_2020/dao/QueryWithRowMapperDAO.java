package popschool.rh_2020.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import popschool.rh_2020.modele.Department;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class QueryWithRowMapperDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final static String BASE_SQL = "SELECT d.deptno, d.dname, d.loc from Dept d";

    public List<Department> queryDepartmentV2(){
        String sql = BASE_SQL + " Where d.deptno > ?";

        DepartmentRowMapper rowMapper = new DepartmentRowMapper();

        List<Department> list = jdbcTemplate.query(sql, rowMapper, 20);
        return list;
    }

    public List<Department> queryDepartmentV3(){
        String sql = BASE_SQL + " Where d.deptno > ?";

        List<Department> list = jdbcTemplate.query(sql, (rs,rowNum) -> {return new Department(rs.getInt("deptno"),rs.getString("dname"),rs.getString("loc"));},20);
        return list;
    }

    public List<Department> queryDepartmentV4(){
        String sql = BASE_SQL + " Where d.deptno > ?";

        List<Department> list = jdbcTemplate.query(sql, this::mapDepartment, 20);
        return list;
    }

    private Department mapDepartment(ResultSet rs, int row) throws SQLException{
        return new Department(rs.getInt("deptno"),rs.getString("dname"),rs.getString("loc"));
    }

    private static final class DepartmentRowMapper implements RowMapper<Department> {
        @Override
        public Department mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Department(rs.getInt("deptno"),rs.getString("dname"),rs.getString("loc"));
        }
    }




}
