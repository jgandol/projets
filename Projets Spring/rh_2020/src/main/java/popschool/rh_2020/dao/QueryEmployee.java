package popschool.rh_2020.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import popschool.rh_2020.modele.Department;
import popschool.rh_2020.modele.Employee;

import javax.management.Query;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class QueryEmployee {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final static String BASE_SQL = "SELECT e.ename, e.job, e.sal, d.dname from Employee e inner join Dept d on e.deptno = d.deptno";
    private final static String EmployeesByJob = "SELECT e.ename, e.sal, d.dname from Employee e inner join Dept d on e.deptno = d.deptno ";

    public List<Employee> queryEmployees(){
        String sql = BASE_SQL;
        List<Employee> list = jdbcTemplate.query(sql, (rs,rowNum) -> {return new Employee(rs.getString("ename"),rs.getString("job"),rs.getDouble("sal"),rs.getString("dname"));});
        return list;
    }

    public List<Employee> queryEmployeesByJob(String job){
        String sql = EmployeesByJob + " where e.job LIKE ?";

        List<Employee> list = jdbcTemplate.query(sql, (rs,rowNum) -> {return new Employee(rs.getString("ename"),rs.getDouble("sal"),rs.getString("dname"));},job);
        return list;
    }

    public Map<Department,List<Employee>> queryListInfosDept(){
        String sql = "select d.deptno, d.dname, d.loc, e.empno, e.ename, e.job, e.sal "
                + " from employee e inner join dept d on e.deptno = d.deptno ";

//        List<Employee> ListEmployee = jdbcTemplate.query(sql, (rs,rowNum) -> { return new Employee(
//                rs.getInt("empno"), rs.getString("ename"),rs.getString("job"), rs.getDouble("sal"));});
//        Map<Department,Employee> list = new HashMap<Department,Employee>();
//        foreach(Employee e : listEmployee){
//            list.put(e.getDept,e);
//        }
        Map<Department,List<Employee>> map = jdbcTemplate.query(sql,new DepartementListResultSetExtractor());
//        Map<Department,List<Employee>> map = jdbcTemplate.query(sql,this::DepartementListResultSetExtractor);
//        Dans le cas où on a une classe interne
        return map;
    }

}

