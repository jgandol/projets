package popschool.rh_2020.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import popschool.rh_2020.dao.QueryWithRowMapperDAO;
import popschool.rh_2020.modele.Department;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class QueryWithRowMapper_Test implements CommandLineRunner {

    @Autowired
    private QueryWithRowMapperDAO dao;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("\n V2 ");
        List<Department> list = dao.queryDepartmentV2();

        for(Department dept: list){
            System.out.println("DeptNo: " + dept.getDeptno() + " - DeptName: " + dept.getDname());
        }

        System.out.println("------------------------------------------");

        System.out.println(" V3 ");
        list = dao.queryDepartmentV3();

        for(Department dept: list){
            System.out.println("DeptNo: " + dept.getDeptno() + " - DeptName: " + dept.getDname());
        }

        System.out.println("------------------------------------------");

        System.out.println(" V4 ");
        list = dao.queryDepartmentV4();

        for(Department dept: list){
            System.out.println("DeptNo: " + dept.getDeptno() + " - DeptName: " + dept.getDname());
        }
    }
}
