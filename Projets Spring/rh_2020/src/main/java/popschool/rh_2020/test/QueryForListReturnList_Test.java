package popschool.rh_2020.test;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.rh_2020.dao.QueryForListReturnListDAO;

import java.util.List;

@Component
public class QueryForListReturnList_Test implements CommandLineRunner {
    private final QueryForListReturnListDAO dao;

    @Autowired
    public QueryForListReturnList_Test(QueryForListReturnListDAO dao){
        this.dao=dao;
    }

    @Override
    public void run(String... args) throws Exception{
        System.out.println("\n------------------------------------");
        List<String> names = dao.getDeptNames("A");
        System.out.println("Liste des departements qui contiennent 'A' ");
        for (String name : names){
            System.out.println("Dept Name: "+name);
        }

    }


}