package popschool.rh_2020.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Repository;
import popschool.rh_2020.dao.QueryEmployee;
import popschool.rh_2020.modele.Department;
import popschool.rh_2020.modele.Employee;

import java.util.List;
import java.util.Map;

@Repository
public class QueryEmployee_Test implements CommandLineRunner {
    @Autowired
    private QueryEmployee dao;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("\n Q1 ");
        List<Employee> list = dao.queryEmployees();

        for(Employee e: list){
            System.out.println("Ename: " + e.getEname() +" - Job: " + e.getJob() + " - Salaire: " + e.getSal() + " - Departement: " + e.getDname());
        }

        System.out.println("------------------------------------------");
        List<Employee> list_job = dao.queryEmployeesByJob("SALESMAN");
        System.out.println("\n Q2 liste des employés avec le Job: SALESMAN");
        for(Employee e: list_job){
            System.out.println("Ename: " + e.getEname() + " - Salaire: " + e.getSal() + " - Departement: " + e.getDname());
        }
        System.out.println("------------------------------------------");
        System.out.println("Q3");
        Map<Department,List<Employee>> map = dao.queryListInfosDept();
//        Map<Department,List<Employee>> map = jdbcTemplate.queryListInfosDept();
        System.out.println("Departements: ");
//        for (Department dept : map.keySet()) {
//            System.out.println(dept.getDeptno() + " - " + dept.getDname() + " - " + dept.getLoc());
//            for (Employee emp : map.get(dept)) {
//                System.out.print("--> " + emp.getEmpno() + " : " + emp.getEname() + " - " + emp.getJob() + " - " + emp.getSal());
//            }
//        }

        for (Map.Entry<Department, List<Employee>> assoc : map.entrySet()) {
            System.out.println(assoc.getKey());
            for (Employee emp : assoc.getValue()) {
                System.out.print("--> " + emp +"\n");
            }
            System.out.println();
        }

    }
}