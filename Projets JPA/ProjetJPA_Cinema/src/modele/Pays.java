package modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "PAYS", schema = "myCinema")
public class Pays {
    private int npays;
    private String nom;
    private Collection<ActeurEntity> acteurs;
    private Collection<Film> films;

    @Id
    @Column(name = "npays")
    public int getNpays() {
        return npays;
    }

    public void setNpays(int npays) {
        this.npays = npays;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pays that = (Pays) o;
        return npays == that.npays &&
                Objects.equals(nom, that.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(npays, nom);
    }

    @OneToMany(mappedBy = "pays")
    public Collection<ActeurEntity> getActeurs() {
        return acteurs;
    }

    public void setActeurs(Collection<ActeurEntity> acteurs) {
        this.acteurs = acteurs;
    }

    @OneToMany(mappedBy = "paysByNpays")
    public Collection<Film> getFilms() {
        return films;
    }

    public void setFilms(Collection<Film> films) {
        this.films = films;
    }
}
