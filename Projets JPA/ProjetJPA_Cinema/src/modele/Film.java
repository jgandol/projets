package modele;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "FILM", schema = "myCinema")
public class Film {
    private int nfilm;
    private String titre;
    private Date sortie;
    private String realisateur;
    private BigDecimal entrees;
    private String oscar;
    private Genre genre;
    private Acteur acteurPrincipal;

    @Id
    @Column(name = "nfilm")
    public int getNfilm() {
        return nfilm;
    }

    public void setNfilm(int nfilm) {
        this.nfilm = nfilm;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "sortie")
    public Date getSortie() {
        return sortie;
    }

    public void setSortie(Date sortie) {
        this.sortie = sortie;
    }

    @Basic
    @Column(name = "realisateur")
    public String getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    @Basic
    @Column(name = "entrees")
    public BigDecimal getEntrees() {
        return entrees;
    }

    public void setEntrees(BigDecimal entrees) {
        this.entrees = entrees;
    }

    @Basic
    @Column(name = "oscar")
    public String getOscar() {
        return oscar;
    }

    public void setOscar(String oscar) {
        this.oscar = oscar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Film that = (Film) o;
        return nfilm == that.nfilm &&
                Objects.equals(titre, that.titre) &&
                Objects.equals(sortie, that.sortie) &&
                Objects.equals(realisateur, that.realisateur) &&
                Objects.equals(entrees, that.entrees) &&
                Objects.equals(oscar, that.oscar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nfilm, titre, sortie, realisateur, entrees, oscar);
    }

    @ManyToOne
    @JoinColumn(name = "ngenre", referencedColumnName = "ngenre", nullable = false)
    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @ManyToOne
    @JoinColumn(name = "nacteurPrincipal", referencedColumnName = "nacteur", nullable = false)
    public Acteur getActeurPrincipal() {
        return acteurPrincipal;
    }

    public void setActeurPrincipal(Acteur acteurPrincipal) {
        this.acteurPrincipal = acteurPrincipal;
    }
}
