package modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "GENRE", schema = "myCinema")
public class Genre {
    private int ngenre;
    private String nature;
    private Collection<Film> films;

    @Id
    @Column(name = "ngenre")
    public int getNgenre() {
        return ngenre;
    }

    public void setNgenre(int ngenre) {
        this.ngenre = ngenre;
    }

    @Basic
    @Column(name = "nature")
    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre that = (Genre) o;
        return ngenre == that.ngenre &&
                Objects.equals(nature, that.nature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ngenre, nature);
    }

    @OneToMany(mappedBy = "genre")
    public Collection<Film> getFilms() {
        return films;
    }

    public void setFilms(Collection<Film> films) {
        this.films = films;
    }
}
