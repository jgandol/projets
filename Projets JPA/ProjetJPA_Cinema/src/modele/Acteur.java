package modele;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "ACTEUR", schema = "myCinema")
public class Acteur {
    private int nacteur;
    private String nom;
    private String prenom;
    private Date naissance;
    private Integer nbreFilms;
    private Pays pays;
    private Collection<Film> films;

    @Id
    @Column(name = "nacteur")
    public int getNacteur() {
        return nacteur;
    }

    public void setNacteur(int nacteur) {
        this.nacteur = nacteur;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "naissance")
    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }

    @Basic
    @Column(name = "nbreFilms")
    public Integer getNbreFilms() {
        return nbreFilms;
    }

    public void setNbreFilms(Integer nbreFilms) {
        this.nbreFilms = nbreFilms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Acteur that = (Acteur) o;
        return nacteur == that.nacteur &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(naissance, that.naissance) &&
                Objects.equals(nbreFilms, that.nbreFilms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nacteur, nom, prenom, naissance, nbreFilms);
    }

    @ManyToOne
    @JoinColumn(name = "nationalite", referencedColumnName = "npays")
    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    @OneToMany(mappedBy = "acteurPrincipal")
    public Collection<Film> getFilms() {
        return films;
    }

    public void setFilms(Collection<Film> films) {
        this.films = films;
    }
}
