package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "CLIENT", schema = "myCinema")
public class Client {
    private int nclient;
    private String nom;
    private String prenom;
    private String adresse;
    private int anciennete;

    @Id
    @Column(name = "nclient")
    public int getNclient() {
        return nclient;
    }

    public void setNclient(int nclient) {
        this.nclient = nclient;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "adresse")
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "anciennete")
    public int getAnciennete() {
        return anciennete;
    }

    public void setAnciennete(int anciennete) {
        this.anciennete = anciennete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client that = (Client) o;
        return nclient == that.nclient &&
                anciennete == that.anciennete &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(prenom, that.prenom) &&
                Objects.equals(adresse, that.adresse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nclient, nom, prenom, adresse, anciennete);
    }
}
