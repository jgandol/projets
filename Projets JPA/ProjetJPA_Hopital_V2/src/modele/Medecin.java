package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Medecin")//, uniqueConstraints = {@UniqueConstraint(columnNames = {"nom", "prenom"})})
@DiscriminatorValue(value = "med")
@NamedQueries(
        {
                @NamedQuery(
                        name = "Medecin.findMedInfoByNom",
                        query = "SELECT new dto.MedInfo(med.nom, med.salaire)" +
                                " FROM Medecin med WHERE UPPER(med.service.nom) LIKE :nom "),

                @NamedQuery(
                        name = "Medecin.findAll",
                        query = "SELECT med FROM Medecin med ORDER BY med.nom, med.prenom"),
                @NamedQuery(
                        name = "Medecin.findAllAvecEquipe",
                        query = "SELECT med from Medecin med" +
                                " where med.equipes is not empty " +
                                " order by med.nom, med.prenom ")
        }
)
public class Medecin extends Personne implements Serializable {
    private static final long serialVersionUID = 1L;

    private float salaire;

    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Medecin manager;

    @OneToMany(mappedBy = "manager")
    private Collection<Medecin> subs = new HashSet<>();

    @ManyToMany
    private Set<Equipe> equipes = new HashSet<>();

    public Medecin(String nom, String prenom, float salaire) {
        super(nom, prenom);
        this.salaire = salaire;
    }

    protected Medecin() {
        super();
    }

    public float getSalaire() {
        return salaire;
    }

    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Medecin getManager() {
        return manager;
    }

    public void setManager(Medecin manager) {
        if (manager == null) return;
        if (this.getManager() != null) {
            if (this.getManager().equals(manager)) return;
            else this.getManager().getSubs().remove(this);
        }
        //Gestion des 2 extremitées
        manager.subs.add(this);
        this.manager = manager;
    } // Control

    public Collection<Medecin> getSubs() {
        return subs;
    }

    public void setSubs(Collection<Medecin> subs) {
        this.subs = subs;
    }

    public Set<Equipe> getEquipes() {
        return equipes;
    }

    public void setEquipes(Set<Equipe> equipes) {
        this.equipes = equipes;
    }

    public void ajoutParticiaption(Equipe equipe) {
        if (equipe == null) return;
        equipe.getMedecins().add(this);
        this.equipes.add(equipe);
    }

    @Override
    public String toString() {
        return super.toString() +
                " salaire= " + salaire + " " + (service == null ? " sans service " : service.getNom())
                + (manager == null ? ", sans manager " : ", manager: " + manager.getPrenom() + " " + manager.getNom()) +
                //", subs= " + subs +
                '}';
    }
}
