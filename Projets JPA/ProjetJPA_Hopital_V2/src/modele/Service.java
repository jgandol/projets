package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "Service")
public class Service implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, unique = true, length = 30)
    private String nom;
    @Column(nullable = false,length = 40)
    private String situation;

    @OneToMany(mappedBy = "service")
    private Set<Medecin> medecins = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="responsable_id")
    private Medecin responsable;

    // indispensable mais sera inaccessible
    public Service(){
        this.nom = "nouveau service";
        this.situation = "situation";
    }

    public Service(String nom, String situation) {
       setNom(nom);
       setSituation(situation);
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getSituation() {
        return situation;
    }
    public void setSituation(String situation) {
        this.situation = situation;
    }
    public Set<Medecin> getMedecins() {
        return medecins;
    }
    public void setMedecins(Set<Medecin> medecins) {
        this.medecins = medecins;
    }
    public void addMedecin(Personne pers){
        if(pers==null) return;
        if( ! (pers instanceof Medecin)) return;
        Medecin med = (Medecin) pers;

        if(med.getService()!=null){
            if(med.getService().equals(this))return;
            else med.getService().getMedecins().remove(pers);
        }
        medecins.add(med);
        med.setService(this);
    } // Control
    public Medecin getResponsable() {
        return responsable;
    }
    public void setResponsable(Medecin responsable) {
        this.responsable = responsable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Service)) return false;
        Service service = (Service) o;
        return nom.equals(service.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

    @Override
    public String toString() {
        return "Service{" + "id=" + id + ", nom='" + nom + '\'' + ", situation='" + situation + '\'' + (responsable==null?", sans responsable":responsable.getNom()) + '}';
    }
}
