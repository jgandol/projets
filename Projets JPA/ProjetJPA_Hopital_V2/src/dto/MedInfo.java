package dto;

public class MedInfo {
    public String nom;
    public float salaire;

    public MedInfo(String nom, float salaire){
        this.nom = nom;
        this.salaire = salaire;
    }

    @Override
    public String toString() {
        return "Le medecin " + nom + " gagne " + salaire;
    }
}
