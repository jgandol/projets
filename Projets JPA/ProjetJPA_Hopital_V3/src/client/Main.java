package client;

import dto.MedInfo;
import modele.*;

import javax.persistence.*;
import java.util.List;


public class Main {
        private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Hopital_PUV3");

    private void init(){
        String query = "SELECT s FROM Service s ORDER BY s.nom DESC";
        String query2 ="SELECT m FROM Medecin m ORDER BY m.nom";
        String query3 ="SELECT p FROM Personne p ORDER BY p.nom";

        Service serv1 = new Service("Cardiologie","Bat A, 1er étage");
        Service serv2 = new Service("Gériatrie","Bat B rez-de-chaussée");
        Service serv3 = new Service("Urgence","Bat A");

        Medecin m1 = new Medecin("Marc","Jean",1801);
        Medecin m2 = new Medecin("Poivre","Olivier",2000);
        Medecin m3 = new Medecin("Gator","Ali",1900);
        Medecin m4 = new Medecin("Enfayitte","Melusine",2100);
        Medecin m5 = new Medecin("Pilaf","Henri",1900);
        Medecin m6 = new Medecin("Trancenne","Jean",2100);

        serv1.addMedecin(m1);
        serv1.addMedecin(m2);
        serv2.addMedecin(m3);
        serv2.addMedecin(m4);
        serv3.addMedecin(m5);
        serv3.addMedecin(m6);

        m1.setManager(m2);
        m3.setManager(m4);
        m5.setManager(m6);

        serv1.setResponsable(m2);

        Malade mal1 = new Malade("Fogiel","Marc-Olivier", new Adresse(12,"rue du cul","Valenciennes"));
        Malade mal2 = new Malade("drucker","michel");

        Equipe e1= new Equipe("Chine");
        Equipe e2 = new Equipe("Italie");

        m1.ajoutParticipation(e1,"responsable");
        m3.ajoutParticipation(e1,"pipoteur");
        m1.ajoutParticipation(e2,"tresorier");

        //EntityManagerFactory emf = Persistence.createEntityManagerFactory("Hopital_PU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try{
            em.persist(serv1);
            em.persist(serv2);
            em.persist(serv3);
            em.persist(m1);
            em.persist(m2);
            em.persist(m3);
            em.persist(m4);
            em.persist(m5);
            em.persist(m6);
            em.persist(mal1);
            em.persist(mal2);
            em.persist(e1);
            em.persist(e2);

            em.getTransaction().commit();

            //List<Service> liste = em.createQuery(query, Service.class).getResultList();
            List<Medecin> listeMed = em.createQuery(query2, Medecin.class).getResultList();
            List<Personne> listePers = em.createQuery(query3, Personne.class).getResultList();

//            for(Service s : liste){
//                System.out.println(" " + s);
//            }

            for(Medecin m : listeMed ){
                System.out.println(" " + m);
            }

            System.out.println("-------------------------");

            for(Personne p : listePers ){
                System.out.println(" " + p);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    //Select *
    public void test5_1(String nomService){

        String strQuery = "SELECT med FROM Medecin med WHERE UPPER(med.service.nom) LIKE :nom";

        String nom = "%"+nomService+"%"; // % correspond à (0 à N caracteres inconnues), _ pour 1 seul caractere
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<Medecin> query = em.createQuery(strQuery, Medecin.class);
            query.setParameter("nom", nom);
            List<Medecin> liste = query.getResultList();
            for (Medecin med : liste) {
                System.out.println(med);
            }
        } finally {
            em.close();
        }
    }
    //Select nom, salaire
    public void test5_1_2(String nomService){
        String strQuery = "SELECT med.nom, med.salaire FROM Medecin med WHERE UPPER(med.service.nom) LIKE :nom";

        String nom = "%"+nomService+"%";

        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<Object[]> query = em.createQuery(strQuery, Object[].class);
            query.setParameter("nom", nom);
            List<Object[]> liste = query.getResultList();
            for (Object[] tab : liste) {
                System.out.println(tab[0] + " gagne " + tab[1]);
            }
        } finally {
            em.close();
        }

    }
    //Avec le DTO
    public void test5_2_2(String nomService){
        String strQuery = "SELECT new dto.MedInfo(med.nom, med.salaire)" +
                " FROM Medecin med WHERE UPPER(med.service.nom) LIKE :nom ";

        String nom = "%"+nomService+"%";

        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<MedInfo> query = em.createQuery(strQuery,MedInfo.class);
            query.setParameter("nom", nom);
            List<MedInfo> liste = query.getResultList();
            for (MedInfo med: liste) {
                System.out.println(med);
            }
        } finally {
            em.close();
        }

    }
    //Avec les named query
    public void test5_3(String nomService){
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<MedInfo> query = em.createNamedQuery("Medecin.findMedInfoByNom",MedInfo.class);
            query.setParameter("nom", nomService);
            List<MedInfo> liste = query.getResultList();
            for (MedInfo med: liste) {
                System.out.println(med);
            }
        } finally {
            em.close();
        }

    }

    public void test5_4(){
        EntityManager em = emf.createEntityManager();
        try {
            TypedQuery<Medecin> query = em.createNamedQuery("Medecin.findAll",Medecin.class);
            List<Medecin> liste = query.getResultList();
            System.out.println("-- Avant");
            for (Medecin med: liste) {
                System.out.println(med);
            }

            // Debut de modification de la base
            em.getTransaction().begin();
            for (Medecin med: liste) {
                med.setSalaire(med.getSalaire()*1.05f);
            }
            //Envoie dans la base
            em.getTransaction().commit();

            System.out.println("-- Apres ");
            for (Medecin med: liste) {
                System.out.println(med);
            }

        } finally {
            em.close();
        }

    }

    public void test5_5(){
        EntityManager em = emf.createEntityManager();
        try {
        String strQuery = "UPDATE Medecin med" +
                " SET med.salaire = 2200";

        TypedQuery<Medecin> query = em.createNamedQuery("Medecin.findAll",Medecin.class);
        List<Medecin> liste = query.getResultList();

            for (Medecin med: liste) {
                System.out.println(med);
            }

            em.getTransaction().begin();
            em.createQuery(strQuery).executeUpdate();
            em.getTransaction().commit();

            for (Medecin med: liste) {
                em.refresh(med);
                System.out.println(med);
            }

        } finally {
            em.close();
        }


    }

    public void test7(){
        try {
            EntityManager em = emf.createEntityManager();
            TypedQuery<Medecin> query = em.createNamedQuery("Medecin.findMedecinOfEquipe", Medecin.class);
            query.setParameter("nomEquipe", "Chine");
            List<Medecin> liste = query.getResultList();

            for (Medecin med : liste) {
                System.out.println(med);
            }

            TypedQuery<Equipe> query1 = em.createNamedQuery("Equipe.findEquipeOfMedecin", Equipe.class);
            query1.setParameter("nomMedecin", "Gator");
            query1.setParameter("prenomMedecin", "Ali");
            List<Equipe> liste1 = query1.getResultList();

            for (Equipe equipe : liste1) {
                System.out.println(equipe);
            }
        } finally {
            emf.close();
        }
    }


    public static void main(String[] args) {
        Main t = new Main();
//        t.init();
//        t.test5_1("card");
//        t.test5_2_2("cardio");
//        t.test5_3("cardiologie");
//        t.test5_4();
//        t.test5_5();
        t.test7();
    }
}
