package modele;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Malade")//, uniqueConstraints = {@UniqueConstraint(columnNames = {"nom", "prenom"})})
@DiscriminatorValue(value = "mal")
public class Malade extends Personne implements Serializable {

    private static final long serialVersionUID = 1L;

    @Embedded
    private Adresse adresse = new Adresse(10,"rue des Tulipes","Lille");

    public Malade(String nom, String prenom, Adresse adresse) {
        super(nom,prenom);
        this.adresse = adresse;
    }

    public Malade(String nom, String prenom) {
        super(nom,prenom);
    }

    protected Malade() {
    }

    public Adresse getAdresse() {
        return adresse;
    }
    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return  super.toString() + (adresse==null?" ":" adresse : " +adresse) +
                '}';
    }
}
