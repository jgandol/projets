package modele;

import service.JPAUtil;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
//@Table(name = "Organisation")
@NamedQueries({
        @NamedQuery(name="Organisation.findAll",query = "SELECT o FROM Organisation o"),
        @NamedQuery(name="Organisation.findByOrgName",query = "SELECT o FROM Organisation o WHERE upper(o.orgName) = upper(:orgName) ")
})
public class Organisation {
    private String orgName;
    private String description;
    private Set<Gangster> gangsters = new HashSet<>();
    private Gangster boss;

    public Organisation() {}

    public Organisation(String orgName, String description) {
        this.setOrgName(orgName);
        this.setDescription(description);
    }

    @Id
    @Column(name = "ORG_NAME")
    public String getOrgName() {
        return orgName;
    }
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Organisation)) return false;
        Organisation that = (Organisation) o;
        return orgName.equals(that.orgName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orgName);
    }

    @OneToMany(mappedBy = "organisation")
    public Set<Gangster> getGangsters() {
        return gangsters;
    }
    public void setGangsters(Set<Gangster> gangsters) {
        this.gangsters = gangsters;
    }

    @OneToOne
    @JoinColumn(name = "THEBOSS", referencedColumnName = "GANGSTER_ID")
    public Gangster getBoss() {
        return boss;
    }

    public void setBoss(Gangster newBoss) {
        if(newBoss == null) return;
        if(! this.gangsters.contains(newBoss))
        if(this.boss != null){
            if(!this.boss.equals(newBoss)){
                this.boss.setOrganisationGeree(null);
            } else return;
        }
        this.boss = newBoss;
        newBoss.setOrganisationGeree(this);
    }

    @Override
    public String toString() {
        return "Organisation{" +
                "orgName='" + orgName + '\'' +
                ", description='" + description + '\'' +
                '}'+ "\n" ;
    }

    public void addGangster(Gangster gangster){
        if(gangster == null)return;

        if(gangster.getOrganisation() == null){
            gangster.setOrganisation(this);
            this.gangsters.add(gangster);
        } else {
            if(gangster.getOrganisation().equals(this)) return;
            if(gangster.getOrganisation().getBoss().equals(gangster)){
                gangster.getOrganisation().setBoss(null);
            }
            gangster.setOrganisation(this);
            this.gangsters.add(gangster);
        }

    }

    public static void main(String[] args) {
        EntityManager em = JPAUtil.getEmf().createEntityManager();
        List<Organisation> orgs = em.createNamedQuery("Organisation.findAll", Organisation.class).getResultList();
        em.close();
        for (Organisation o :orgs){
            System.out.println(o + " dirigé par :" + o.boss);

            for(Gangster g : o.getGangsters()){
                System.out.println("-- " + g);
                    for(Job j: g.getJobs()){
                        System.out.println("---- " + j) ;
                    }
            }
        }

//        em = JPAUtil.getEmf().createEntityManager();
//        List<Job> jobs = em.createNamedQuery("Job.findAll",Job.class).getResultList();
//        for (Job j :jobs){
//            System.out.println(j);
//        }
    }
}
