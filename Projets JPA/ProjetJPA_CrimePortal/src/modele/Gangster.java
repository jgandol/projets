package modele;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
//@Table(name = "Gangster")
@NamedQueries({
        @NamedQuery(name = "Gangster.findAll", query = "SELECT g FROM Gangster g"),
        @NamedQuery(name = "Gangster.findByGname", query = "SELECT g FROM Gangster g WHERE upper(g.gname) = :gname"),
        @NamedQuery(name = "Gangster.findById", query = "SELECT g FROM Gangster g WHERE g.gangsterId = ?1")
})
public class Gangster {
    private int gangsterId;
    private String gname;
    private String nickname;
    private Integer badness;
    private Set<Job> jobs = new HashSet<>();
    private Organisation organisation;
    private Organisation organisationGeree;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GANGSTER_ID")
    public int getGangsterId() {
        return gangsterId;
    }

    public void setGangsterId(int gangsterId) {
        this.gangsterId = gangsterId;
    }

    @Basic
    @Column(name = "GNAME")
    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    @Basic
    @Column(name = "NICKNAME")
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Basic
    @Column(name = "BADNESS")
    public Integer getBadness() {
        return badness;
    }

    public void setBadness(Integer badness) {
        this.badness = badness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!( o instanceof Gangster)) return false;
        Gangster gangster = (Gangster) o;
        return gname.equals(gangster.gname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gname);
    }

    public Gangster() {
    }

    public Gangster(String gname, String nickname, int badness) {
        this.setNickname(nickname);
        this.setBadness(badness);
        this.setGname(gname);
    }

    public Gangster(String gname, String nickname) {
        this.setNickname(nickname);
        this.setBadness(0);
        this.setGname(gname);
    }

    @ManyToMany
    @JoinTable(name = "AFFECTATION",
            joinColumns = {@JoinColumn(name = "GANGSTER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "JOB_ID")})

    public Set<Job> getJobs() {
        return jobs;
    }

    public void setJobs(Set<Job> jobs) {
        this.jobs = jobs;
    }


    @ManyToOne
    @JoinColumn(name = "ORG_NAME", referencedColumnName = "ORG_NAME")
    public Organisation getOrganisation() {
        return organisation;
    }
    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public void removeOrganisation(){
        if(this.organisation == null) return;
        if(this.organisation.getBoss() != null && this.organisation.getBoss().equals(this)){
            this.getOrganisation().setBoss(null);
            this.organisationGeree = null;
        }

        this.organisation.getGangsters().remove(this);
        this.organisation = null;
    }


    @OneToOne(mappedBy = "boss")
    public Organisation getOrganisationGeree(){ return organisationGeree;}
    public void setOrganisationGeree(Organisation organisationGeree) {
        this.organisationGeree = organisationGeree;
    }

    @Override
    public String toString() {
        return "Gangster{" +
                "gangsterId=" + gangsterId +
                ", gname='" + gname + '\'' +
                ", nickname='" + nickname + '\'' +
                ", badness=" + badness +
                (organisation==null?", Sans organisation ":", organisation = " + organisation.getOrgName()) +
                '}' + "\n";
    }
}
