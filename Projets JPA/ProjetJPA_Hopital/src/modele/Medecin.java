package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

@Entity
@Table(name = "Medecin", uniqueConstraints = {@UniqueConstraint(columnNames = {"nom", "prenom"})})
public class Medecin implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 30)
    private String nom;
    @Column(nullable = false, length = 30)
    private String prenom;

    private float salaire;

    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Medecin manager;

    @OneToMany(mappedBy = "manager")
    private Collection<Medecin> subs = new HashSet<>();

    protected Medecin() {
        this.nom = "Dupont";
        this.prenom = "Thierry";
    }

    public Medecin(String nom, String prenom, float salaire) {
        this.nom = nom;
        this.prenom = prenom;
        this.salaire = salaire;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public float getSalaire() {
        return salaire;
    }
    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }
    public Service getService() {
        return service;
    }
    public void setService(Service service) {
        this.service = service;
    }
    public Medecin getManager() {
        return manager;
    }
    public void setManager(Medecin manager) {
        if (manager == null) return;
        if (this.getManager() != null) {
            if (this.getManager().equals(manager)) return;
            else this.getManager().getSubs().remove(this);
        }
        //Gestion des 2 extremitées
        manager.subs.add(this);
        this.manager = manager;
    } // Control
    public Collection<Medecin> getSubs() {
        return subs;
    }
    public void setSubs(Collection<Medecin> subs) {
        this.subs = subs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Medecin)) return false;
        Medecin medecin = (Medecin) o;
        return nom.equals(medecin.nom) &&
                prenom.equals(medecin.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }

    @Override
    public String toString() {
        return "Medecin{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", salaire=" + salaire +
                ", service=" + service.getNom() +
                (manager==null?"aucun manager":"manager: " + manager.getNom()) +
                ", subs=" + subs +
                '}';
    }
}
