package client;

import modele.Adresse;
import modele.Malade;
import modele.Medecin;
import modele.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        String query = "SELECT s FROM Service s ORDER BY s.nom DESC";
        String query2 ="SELECT m FROM Medecin m ORDER BY m.nom";


        Service serv1 = new Service("Cardiologie","Bat A, 1er étage");
        Service serv2 = new Service("Gériatrie","Bat B rez-de-chaussée");
        Service serv3 = new Service("Urgence","Bat A");

        Medecin m1 = new Medecin("Marc","Jean",1801);
        Medecin m2 = new Medecin("Poivre","Olivier",2000);
        Medecin m3 = new Medecin("Gator","Ali",1900);
        Medecin m4 = new Medecin("Enfayitte","Melusine",2100);
        Medecin m5 = new Medecin("Pilaf","Henri",1900);
        Medecin m6 = new Medecin("Trancenne","Jean",2100);

        serv1.addMedecin(m1);
        serv1.addMedecin(m2);
        serv2.addMedecin(m3);
        serv2.addMedecin(m4);
        serv3.addMedecin(m5);
        serv3.addMedecin(m6);

        m1.setManager(m2);
        m3.setManager(m4);
        m5.setManager(m6);

        serv1.setResponsable(m2);

        Malade mal1 = new Malade("Fogiel","Marc-Olivier", new Adresse(12,"rue du cul","Valenciennes"));

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Hopital_PU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try{
            em.persist(serv1);
            em.persist(serv2);
            em.persist(serv3);
            em.persist(m1);
            em.persist(m2);
            em.persist(m3);
            em.persist(m4);
            em.persist(m5);
            em.persist(m6);
            em.persist(mal1);

            em.getTransaction().commit();

            //List<Service> liste = em.createQuery(query, Service.class).getResultList();
            List<Medecin> listeMed = em.createQuery(query2, Medecin.class).getResultList();

//            for(Service s : liste){
//                System.out.println(" " + s);
//            }

            for(Medecin m : listeMed ){
                System.out.println(" " + m);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }
    }
}
