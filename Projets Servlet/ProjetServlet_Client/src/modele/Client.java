package modele;

import java.io.Serializable;
import java.util.Objects;

public class Client implements Serializable {
    private static final long serialVersionUID =1L;

    private String alias;
    private String password;
    private String nom;
    private String prenom;
    private String email;

    public Client(String alias, String password, String nom, String prenom, String email) {
        this.alias = alias.toUpperCase();
        this.password = password.toUpperCase();
        this.nom = nom.toUpperCase();
        this.prenom = prenom.toUpperCase();
        this.email = email.toUpperCase();
    }

    public String getAlias() {
        return alias;
    }
    public void setAlias(String alias) {
        this.alias = alias;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return email.equals(client.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    @Override
    public String toString() {
        return "Client{" +
                "alias='" + alias + '\'' +
                ", password='" + password + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public static void main(String[] args) {
//        Client cl = new Client("jerm","jerm","jeremie","gandol","jerm@mail.com");
//        System.out.println(cl);
    }
}
