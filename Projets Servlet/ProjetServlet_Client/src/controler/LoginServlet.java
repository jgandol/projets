package controler;

import dao.ClientDAO;
import modele.Client;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/process")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    // Navigation
    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request, response);
    }

    //Controller
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Recuperation de l'action
        try{
            String action = request.getParameter("action");

            //Execution du traitement associe a cette actin
            if(action.equals("authenticate")){
                doAuthenticate(request, response);
            } else if (action.equals("login")){
                doLogin(request,response);
            } else if (action.equals("afficherClient")){
                doAfficherFormEnrClient(request,response);
            } else if(action.equals("enrClient")){
                doEnrClient(request,response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String url = "pages/login.jsp";
        forward(url,request,response);
    }

    private void doAuthenticate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            //Recuperation des données du formulaire
            String alias = request.getParameter("alias");
            String password = request.getParameter("password");

            // Regle metier
            // 1. Consultation de la BDD
            Client client = ClientDAO.getSingleton().find(alias, password);
            if (client != null) {
                request.setAttribute("client", client);
            }

            // 2. Cinematique
            String url = (client == null ? "pages/inconnu.jsp" : "pages/connu.jsp");
            forward(url, request, response);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void doEnrClient(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            String alias = request.getParameter("alias");
            String password = request.getParameter("password");
            String nom = request.getParameter("nom");
            String prenom = request.getParameter("prenom");
            String email = request.getParameter("email");

            Client client = ClientDAO.getSingleton().create(alias,password,nom,prenom,email);
            if(client != null){
                request.setAttribute("client",client);
            }

            // 2. Cinematique
            String url = (client == null ? "pages/inconnu.jsp" : "pages/connu.jsp");
            forward(url, request, response);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }


    }

    private void doAfficherFormEnrClient(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String url = "pages/enregistrement.jsp";
        forward(url,request,response);
    }
}
