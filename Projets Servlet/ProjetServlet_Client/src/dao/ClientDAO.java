package dao;

import modele.Client;

import javax.print.attribute.ResolutionSyntax;
import java.sql.*;

public class ClientDAO {
    private static String URL = "jdbc:mysql://localhost:3306/Client";
    private static String USER = "jeremie";
    private static String PW = "admin";

    private static final String SQLfindClientByAlias =
            " select nom, prenom, email " +
                    " from Client " +
                    " where upper(alias) = ? " +
                    " and password = ? ";
    private static final String SQLinsertCLient =
            " insert into Client values " +
                    "(?, ?, ?, ?, ?) ";

    private Connection connection = null;

    private static ClientDAO clientDao = null;

    public static ClientDAO getSingleton() {
        if (clientDao == null) {
            clientDao = new ClientDAO();
        }
        return clientDao;
    }

    public ClientDAO() {
        try {
            connection = DriverManager.getConnection(URL, USER, PW);
        } catch (Exception e) {
            System.out.println(e);
            ;
        }
    }

    public Client find(String alias, String password) throws Exception {
        // controle de surface
        if (alias == null || password == null) return null;

        Client client = null;
        PreparedStatement selectStatement = null;
        ResultSet rs = null;

        try {
            selectStatement = connection.prepareStatement(SQLfindClientByAlias);
            selectStatement.setString(1, alias.toUpperCase());
            selectStatement.setString(2, password);

            rs = selectStatement.executeQuery();
            if (rs.next()) {
                String nom = rs.getString("nom");
                String prenom = rs.getString("prenom");
                String email = rs.getString("email");
                client = new Client(alias, password, nom, prenom, email);
            }
        } finally {
            rs.close();
            selectStatement.close();
        }
        return client;
    }

    public Client create(String alias, String password, String nom, String prenom, String email) throws Exception {
        // controle de surface
        if (alias == null || password == null || nom == null || prenom == null || email == null) return null;
        Client client = null;
        PreparedStatement insertStatement = null;
        try {
            insertStatement = connection.prepareStatement(SQLinsertCLient);
            insertStatement.setString(1, alias.toUpperCase());
            insertStatement.setString(2, password);
            insertStatement.setString(3, nom.toUpperCase());
            insertStatement.setString(4, prenom.toUpperCase());
            insertStatement.setString(5, email.toUpperCase());
            connection.setAutoCommit(false);

            int nb = insertStatement.executeUpdate();

            if (nb == 1) {
                client = new Client(alias, password, nom, prenom, email);
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return client;
    }

    public static void main(String[] args) throws Exception {
        ClientDAO dao = ClientDAO.getSingleton();
//        Client cl = dao.find("jerm", "jerm");
//        System.out.println(cl);
        Client cl = dao.create("TPELOUZ","1234","Thierry","Pelouse","46000k@mail.com");
        System.out.println(dao.find(cl.getAlias(), cl.getPassword()));
        System.out.println(dao.find("oui","non"));
    }
}
