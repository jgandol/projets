<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Vue : Choix d'un Acteur</title>
</head>
<body>
<form method="post" action="process?action=carriere">
    <fieldset>
        <legend> Choisissez un acteur </legend>
        <select name="acteur">
            <c:forEach var="acteur" items="${ensActeur}">
                <option value="${acteur.nacteur}">
                        ${acteur.nom}
                </option>
            </c:forEach>
        </select>
        <br />
        <br />
        <input type="submit" value="Soumettre">
    </fieldset>
</form>
</body>
</html>
