<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Films</title>
</head>
<body>
Films de l'acteur ${acteur.prenom} ${acteur.nom}
<br /><br />

    <table>
        <thead>
        <tr>
            <th>Titre</th>
            <th>Realisateur</th>
            <th>Genre</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="film" items="${carriere}">
            <tr>

                <td><a href="process?action=infosFilm&nfilm=${film.nfilm}"> ${film.titre} </a></td>
                <td>${film.nom_realisateur}</td>
                <td>${film.genre}</td>

            </tr>
        </c:forEach>
        </tbody>
    </table>

</body>
</html>
