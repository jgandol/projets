package control;

import dao.CinemaDAO;
import dto.ActeurDTO;
import dto.FilmDTO;
import dto.GenreDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.util.List;

@WebServlet("/process")
public class CarriereServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    // Navigation
    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request, response);
    }

    //Controller
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Recuperation de l'action
        try{
            String action = request.getParameter("action");

            //Execution du traitement associe a cette action
            switch (action) {
                case "init":
                    doInit(request, response);
                    break;
                case "carriere":
                    doCarriere(request, response);
                    break;
                case "infosFilm":
                    doInfosFilm(request, response);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        if(request.getSession().getAttribute("ensActeur") == null){
            // Interrogation du modele afin de trouver l'ensemble des acteurs
            List<ActeurDTO> ensActeurs = CinemaDAO.getSingleton().findAllActeurs();

            // sauvegarde de cet ensemble dans le scope SESSION
            request.getSession().setAttribute("ensActeur",ensActeurs);
        }

        String url = "pages/afficherActeur.jsp";
        forward(url,request, response);
    }

    private void doCarriere(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Recuperation du noum du genre choisi
        String choix = request.getParameter("acteur");
        int nacteur = Integer.parseInt(choix);

        //Interrogation du modele afin de recuperer les films de cet acteur ainsi que l'acteur
        CinemaDAO dao = CinemaDAO.getSingleton();
        List<FilmDTO> ensFilms=dao.findFilmsOfActeur(nacteur);
        ActeurDTO acteur = dao.findActeurByID(nacteur);

        //Sauvegarder de l'acteur et de sa carriere dans le scope request
        request.setAttribute("acteur",acteur);
        request.setAttribute("carriere",ensFilms);

        //Cinematique
        String url = "pages/carriere.jsp";
        forward(url,request,response);
    }

    private void doInfosFilm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String choix = request.getParameter("nfilm");
        int nfilm = Integer.parseInt(choix);

        CinemaDAO dao = CinemaDAO.getSingleton();
        FilmDTO f = dao.findFilmByID(nfilm);

        request.setAttribute("film",f);
        //Cinematique
        String url = "pages/afficherInfoFilm.jsp";
        forward(url,request,response);

    }


}
