package dto;

import java.io.Serializable;

public class FilmDTO implements Serializable {
    /////////////////////////// A T T R I B U T E S \\\\\\\\\\\\\\\\\\\\\\\\\\\
    // Class Attributes
    private static final long serialVersionUID = 1L;

    // Instance Attributes
    private int nfilm;
    private String titre;
    private String nom_realisateur;
    private int nacteur;
    private String genre;
    private int entrees;
    private String sortie;
    private String pays;

    /////////////////// G E T T E R S    &    S E T T E R S \\\\\\\\\\\\\\\\\\\\

    public int getNfilm() {
        return nfilm;
    }
    public String getTitre() {
        return titre;
    }
    public String getNom_realisateur() {
        return nom_realisateur;
    }
    public String getGenre() {
        return genre;
    }
    public int getEntrees() {
        return entrees;
    }
    public String getSortie() {
        return sortie;
    }
    public String getPays() {
        return pays;
    }

    public int getNacteur() {
        return nacteur;
    }
    ///////////////////////// C O N S T R U C T O R S \\\\\\\\\\\\\\\\\\\\\\\\\\

    public FilmDTO(int nfilm, String titre, String nom_realisateur, int nacteur, String genre, int entrees, String sortie, String pays) {
        this.nfilm = nfilm;
        this.titre = titre;
        this.nom_realisateur = nom_realisateur;
        this.nacteur = nacteur;
        this.genre = genre;
        this.entrees = entrees;
        this.sortie = sortie;
        this.pays = pays;
    }

    public FilmDTO(int nfilm, String titre, String nom_realisateur, String genre) {
        this.nfilm = nfilm;
        this.titre = titre;
        this.nom_realisateur = nom_realisateur;
        this.genre = genre;
    }

    ////////////////////// B U S I N E S S    R U L E S \\\\\\\\\\\\\\\\\\\\\\\\


    // toString
    @Override
    public String toString() {
        //return "\n" + nfilm + ":" + titre + " réalisé par " + nom_realisateur + " avec " + nom_acteur;
        return titre;
    }
}
