package dto;

import java.io.Serializable;
import java.sql.Date;

public class ActeurDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private int nacteur;
    private String nom;
    private String prenom;

    public int getNacteur() {
        return nacteur;
    }
    public String getNom() {
        return nom;
    }
    public String getPrenom() {
        return prenom;
    }

    public ActeurDTO(int nacteur, String nom, String prenom) {
        this.nacteur = nacteur;
        this.nom = nom;
        this.prenom = prenom;

    }

    @Override
    public String toString() {
        return "ActeurDTO{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                "}\n";
    }
}
