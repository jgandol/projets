package dao;

import dto.ActeurDTO;
import dto.FilmDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CinemaDAO {

    // URL DE CONNEXION
    private final static String URL = "jdbc:mysql://localhost:3306/myCinema";
    // NOM DE L'USER
    private final static String USER = "jeremie";
    // PASSWORD
    private final static String PW = "admin";

    private Connection connection;

    private final static String SQLfindAllActeurs =
            " SELECT nacteur, nom, prenom " +
                    " FROM ACTEUR " +
                    " ORDER BY nom ";

    private final static String SQLfindFilmOfActeur =
            " SELECT nfilm, titre, realisateur, nature " +
                    " FROM FILM " +
                    " JOIN GENRE using(ngenre) " +
                    " WHERE nacteurPrincipal = ? " +
            " ORDER BY titre ";

    private final static String SQLfindActeurByID =
            " SELECT nacteur, nom, prenom " +
                    " FROM ACTEUR " +
                    " WHERE nacteur =  ?" +
                    " ORDER BY nom ";

    private final static String SQLfindFilmByID =
            " SELECT nfilm, titre, realisateur, nacteurPrincipal ,nature, entrees, nom as pays, sortie " +
                    " FROM FILM f " +
                    " JOIN GENRE using(ngenre)" +
                    " JOIN PAYS using(npays) " +
                    " WHERE nfilm = ? ";

    // SINGLETON
    private static CinemaDAO cinemaDAO = new CinemaDAO();
    public static CinemaDAO getSingleton(){
        return cinemaDAO;
    }
    private CinemaDAO(){
        try{
            connection = DriverManager.getConnection(URL,USER,PW);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // R E G L E S    M E T I E R

    public List<ActeurDTO> findAllActeurs(){
        Statement stmt = null;
        ResultSet rs = null;

        List<ActeurDTO> ens = new ArrayList<>();

        try{
            stmt = connection.createStatement();
            rs = stmt.executeQuery(SQLfindAllActeurs);

            while(rs.next()){
                ens.add(new ActeurDTO(rs.getInt("nacteur"),rs.getString("nom"),rs.getString("prenom")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try{
                if(rs != null){
                    rs.close();
                }
                if(stmt != null){
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return ens;
    }

    public List<FilmDTO> findFilmsOfActeur(int nacteur){
        PreparedStatement ps = null;
        ResultSet rs = null;

        List<FilmDTO> ens = new ArrayList<>();
        try{
            ps = connection.prepareStatement(SQLfindFilmOfActeur);
            ps.setInt(1,nacteur);
            rs = ps.executeQuery();

            while(rs.next()){
                ens.add(new FilmDTO(rs.getInt("nfilm"),rs.getString("titre"),
                        rs.getString("realisateur"),rs.getString("nature")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return ens;

    }

    public ActeurDTO findActeurByID(int nacteur){
        PreparedStatement ps = null;
        ResultSet rs = null;

        ActeurDTO a = null;
        try{
            ps = connection.prepareStatement(SQLfindActeurByID);
            ps.setInt(1,nacteur);
            rs = ps.executeQuery();

            if(rs.next()){
                a = new ActeurDTO(rs.getInt("nacteur"),rs.getString("nom"),
                        rs.getString("prenom"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return a;
    }

    public FilmDTO findFilmByID(int nfilm){
        PreparedStatement ps = null;
        ResultSet rs = null;

        FilmDTO f = null;
        try{
            ps = connection.prepareStatement(SQLfindFilmByID);
            ps.setInt(1,nfilm);
            rs = ps.executeQuery();

            if(rs.next()){
                f = new FilmDTO(rs.getInt("nfilm"),rs.getString("titre"),
                        rs.getString("realisateur"),rs.getInt("nacteurPrincipal"),rs.getString("nature"),
                        rs.getInt("entrees"),rs.getString("pays"),rs.getString("sortie"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try{
                if(rs != null){
                    rs.close();
                }
                if(ps != null){
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return f;
    }

    public static void main(String[] args) {
            CinemaDAO dao = new CinemaDAO();
//        System.out.println(dao.listeActeurs());
        System.out.println(dao.findActeurByID(1));
        System.out.println(dao.findFilmsOfActeur(1));
        System.out.println(dao.findFilmByID(12));
    }
}
