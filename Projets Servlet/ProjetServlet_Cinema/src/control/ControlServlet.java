package control;

import dao.CinemaDAO;
import dto.FilmDTO;
import dto.GenreDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/process")
public class ControlServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    // Navigation
    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request, response);
    }

    //Controller
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Recuperation de l'action
        try{
            String action = request.getParameter("action");

            //Execution du traitement associe a cette action
            switch (action) {
                case "init":
                    doInit(request, response);
                    break;
                case "filmsDuGenre":
                    doFilmsDuGenre(request, response);
                    break;
                case "choix":
                    doFilmsSelectionnes(request, response);
                    break;
                case "panier":
                    doPanier(request,response);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //Creation du dao si necessaire
        CinemaDAO dao = CinemaDAO.getInstance();
        //Interrogation du modele afin de trouver l'ensemble des genres
        List<GenreDTO> ensGenres = dao.ensGenres();
        //Sauvagerde de cet ensmbble dans le scope request
        request.setAttribute("ensGenres",ensGenres);

        //alternative : sauvegarder l'ens des genres en session
        //request.getSession().setAttributes("ensGenres", ensGenres);

        String url = "pages/afficheGenres.jsp";
        forward(url,request, response);
    }

    private void doFilmsDuGenre(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //Recuperation du noum du genre choisi
        String choix = request.getParameter("genre");
        int ngenre = Integer.parseInt(choix);

        //Interrogation du modele afin de recuperer les films de cet acteur ainsi que l'acteur
        CinemaDAO dao = CinemaDAO.getInstance();
        List<FilmDTO> ensFilms=dao.ensFilmsDuGenre(ngenre);
        GenreDTO genre = dao.findGenresById(ngenre);

        //Sauvegarder de l'acteur et de sa carriere dans le scope request
        request.setAttribute("genre",genre);
        request.setAttribute("ensFilms",ensFilms);

        //Cinematique
        String url = "pages/afficheFilmsDuGenre.jsp";
        forward(url,request,response);
    }

    private void doFilmsSelectionnes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //Recuperation des films choisis
        String[] choix = request.getParameterValues("choix");

        //creation du panier
        CinemaDAO dao = CinemaDAO.getInstance();
        HttpSession session = request.getSession();
        Map<String, FilmDTO> ensChoisis = (HashMap<String, FilmDTO>) session.getAttribute("ensChoix");

        if(ensChoisis == null ){
            ensChoisis = new HashMap<>();
        }

        for(String cle : choix){
            int nfilm = Integer.parseInt(cle);
            FilmDTO film = dao.filmParId(nfilm);
            ensChoisis.put(cle,film);
        }

        //Sauvegarde du panier
        synchronized (session){
            session.setAttribute("ensChoix",ensChoisis);
        }

        //Cinematique vers la vue
        String url = "pages/afficheFilmsChoisis.jsp";
        forward(url,request,response);
    }

    private void doPanier(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        HttpSession session = request.getSession();
        Map<String, FilmDTO> ensChoisis = (HashMap<String, FilmDTO>) session.getAttribute("ensChoix");

        //Cinematique vers la vue
        String url = "pages/afficheFilmsChoisis.jsp";
        forward(url,request,response);
    }

}
