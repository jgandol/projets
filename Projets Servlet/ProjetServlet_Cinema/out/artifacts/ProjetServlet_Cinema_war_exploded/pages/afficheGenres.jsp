<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Vue : Choix d'un Genre</title>
</head>
<body>
    <form method="post" action="process?action=filmsDuGenre">
        <fieldset>
        <legend> Choix d'un genre </legend>
        <select name="genre">
            <c:forEach var="genre" items="${ensGenres}">
                <option value="${genre.ngenre}">
                        ${genre.nature}
                </option>
            </c:forEach>
        </select>
        <br />
        <br />
        <input type="submit" value="Soumettre">
        </fieldset>
    </form>
<a href="process?action=panier"> <input type="button" value="Voir panier"> </a>
</body>
</html>
