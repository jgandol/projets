package dao;

import dto.*;

import java.sql.*;
import java.util.*;

public enum EcommerceDAO {
    INSTANCE;

    //IDENTIFICATION BASE DE DONNEES
    private final static String URL = "jdbc:mysql://localhost:3306/ECommerce";
    private final static String USER = "jeremie";
    private final static String PW = "admin";

    //SINGLETON : DECLARATION
    private Connection connection = null;

    //REQUETES
    private static final String SQLFindAllItems =
            "SELECT NPRODUIT, DESCRIPTIF, PRIX, DISPONIBLE, QTEENSTOCK FROM PRODUIT";

    private static final String SQLFindClientByNom =
            "SELECT MOTDEPASSE from CLIENT WHERE NOM = ? ";

    private static final String SQLinsertStatementStr =
            "insert into COMMANDE VALUES (?, CURRENT_TIMESTAMP, ?)";

    private static final String SQLdetailInsertStatementStr =
            " insert into DETAIL_CDE VALUES (?, ?, ?) ";

    private static final String SQLmajProduitStatementStr =
            "update PRODUIT SET QTEENSTOCK = QTEENSTOCK-1 " +
                    " WHERE NPRODUIT =? ";


    private EcommerceDAO() {
        try {
            connection = DriverManager.getConnection(URL, USER, PW);

        } catch (Exception e) {
            System.out.println(e);
            ;
        }
    }

    public Catalogue getCatalogue() {
        Map<String, ProduitDTO> items = new HashMap<>();
        try {
            PreparedStatement selectStatement =
                    connection.prepareStatement(SQLFindAllItems);

            ResultSet rs = selectStatement.executeQuery();
            while (rs.next()) {
                String nproduit = (rs.getString("NPRODUIT")).trim();
                String descript = rs.getString("DESCRIPTIF");
                float prix = rs.getFloat("PRIX");
                boolean disponible = (rs.getString("DISPONIBLE")).equals("vrai");
                int stock = rs.getInt("QTEENSTOCK");

                items.put(nproduit, new ProduitDTO(nproduit, descript, prix, disponible, stock));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return new Catalogue(items);
    }

    public ClientDTO findClientsByNom(String nom) throws SQLException {
        String motDePasse = null;

        PreparedStatement selectStatement = connection.prepareStatement(SQLFindClientByNom);
        selectStatement.setString(1, nom);
        ResultSet rs = selectStatement.executeQuery();
        if (rs.next()) {
            motDePasse = rs.getString("MOTDEPASSE");
            return new ClientDTO(nom, motDePasse);
        }
        return null;
    }

    //le cache catalogue doit etre mis a jour donc il doit etre passé en parametre
    //les tables commande, detail_cmde

    public void createPanier(Panier panier, Catalogue catalogue) throws SQLException {
        connection.setAutoCommit(false);
        PreparedStatement insert = connection.prepareStatement(SQLinsertStatementStr);
        PreparedStatement detail = connection.prepareStatement(SQLdetailInsertStatementStr);
        PreparedStatement maj = connection.prepareStatement(SQLmajProduitStatementStr);
        //Enregistrement de l'entete de la commande
        insert.setLong(1, panier.getnCommande());
        // insert.setDate(2, new Date(getcurrenttimemillis))
        insert.setString(2, panier.getClient().getNom());
        int nb = insert.executeUpdate();

        //Enregistrement des dtails ed la commande et maj des stocks
        for (String refItem : panier.getItemIds()) {
            //Creation d'un detail
            detail.setLong(1, panier.getnCommande());
            detail.setInt(2, 1);
            detail.setString(3, refItem);
            nb = detail.executeUpdate();

            //misea a jour des stocks de produits
            maj.setString(1, refItem);
            nb = maj.executeUpdate();

            //mise a jour du catalogue au niveau du cache du serveur
            ProduitDTO p = catalogue.getProduit(refItem);
            p.setStock(p.getStock() - 1);
            catalogue.setProduit(refItem, p);
        }
        connection.commit();
    }

}
