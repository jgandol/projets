package dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.Arrays;

public class Panier implements Serializable {
    private String[] itemIds; //panier tableau de code produit
    private long nCommande;
    private ClientDTO client;
    private boolean processed;

    public Panier(ClientDTO client){
        //creation de la cle primaire
        this.nCommande = System.currentTimeMillis();
        this.client= client;
    }

    public boolean isProcessed(){
        return processed;
    }
    public void setProcessed(boolean processed){
        this.processed = processed;
    }

    //getter setter
    public long getnCommande() {
        return nCommande;
    }

    public void setnCommande(long nCommande) {
        this.nCommande = nCommande;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    //constructeur

    public Panier(String[] itemIds, long nCommande, ClientDTO client) {
        this.itemIds = itemIds;
        this.nCommande = nCommande;
        this.client = client;
    }


    //tostring


    @Override
    public String toString() {
        return "Panier{" +
                "itemIds=" + Arrays.toString(itemIds) +
                ", nCommande=" + nCommande +
                ", client=" + client +
                '}';
    }
}
