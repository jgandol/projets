package control;

import dto.Catalogue;
import dto.ClientDTO;
import dto.Panier;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static dao.EcommerceDAO.*;

@WebServlet("/process")
public class EcommerceServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request, response);
    }

    //navigation
    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request, response);
    }

    //routage
    private void processRquest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //recupertaion de l'action
            String action = request.getParameter("action");

            //execution du traitement
            if (action.equals("init")) {
                doInit(request, response);
            } else if (action.equals("authenticate")) {
                doAuthenticate(request, response);
            } else if (action.equals("addItems")) {
                doAddItems(request, response);
            } else if (action.equals("logout")) {
                doLogout(request, response);
            } else if (action.equals("showCatalog")) {
                doShowCatalog(request, response);
            } else if (action.equals("showDetails")) {
                doShowDetails(request, response);
            } else if (action.equals("validateCommand")) {
                doValidateCommand(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void doValidateCommand(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Panier order = (Panier) request.getSession().getAttribute("anOrder");
        Catalogue catalog = (Catalogue) getServletContext().getAttribute("catalogue");

        System.out.println("Order : " + order);
        try {
            INSTANCE.createPanier(order, catalog);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String url = "pages/receipt.jsp";
        forward(url, request, response);
    }

    private void doShowCatalog(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "pages/showCatalogue.jsp";
        forward(url, request, response);
    }

    private void doLogout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        if (session != null) {
            ClientDTO client = (ClientDTO) session.getAttribute("client");
            if(client !=null){
                session.invalidate();
            }
        }
        session = request.getSession(true);
        response.sendRedirect("index.jsp");
    }

    private void doShowDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "pages/showDetails.jsp";
        forward(url, request, response);
    }

    private void doAddItems(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Il s'agit soit d'une nouvelle commande (VUE : showCatalog)
        //           soit d'une commande modifiée (VUE : showDetails)

        //1. Récupération des produits choisis par le client
        String itemsIds[] = request.getParameterValues("itemId");


        ClientDTO client = (ClientDTO) request.getSession().getAttribute("client");
        String url = "";

        if (itemsIds != null && itemsIds.length != 0) {
            Panier order = new Panier(client);
            order.setItemIds(itemsIds);
            request.getSession().setAttribute("anOrder", order);
            url = "pages/showDetails.jsp";
        } else {
            url = "pages/emptyCart.jsp";
        }
        forward(url, request, response);

    }

    private void doAuthenticate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // 1. Récupération des données du formulaire
            String nom = request.getParameter("nom");
            String motDePasse = request.getParameter("motDePasse");

            // Règle métier
            // 2. recherche du client
            ClientDTO client = INSTANCE.findClientsByNom(nom);
            if (client != null) {
                // 3. Sauvegarde du résultat dans le scope session
                if (client.getPass().equals(motDePasse)) {
                    request.getSession().setAttribute("client", client);
                } else {
                    request.getSession().removeAttribute("client");
                    client = null;
                }
            }

            // cinematique
            String url = (client == null ? "pages/showLogin.jsp" : "pages/showCatalogue.jsp");
            forward(url, request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //gestion du cache application
        if (this.getServletContext().getAttribute("catalogue") == null) {
            // Acces au modele
            Catalogue catalogue = INSTANCE.getCatalogue();
            // Sauvegarde du catalogue en Portée : APPLICATION
            this.getServletContext().setAttribute("catalogue", catalogue);
        }
        String url = "pages/showLogin.jsp";
        forward(url, request, response);

    }
}
