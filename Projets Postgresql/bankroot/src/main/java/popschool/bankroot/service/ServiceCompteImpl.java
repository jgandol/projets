package popschool.bankroot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.bankroot.dao.CompteDepotDAO;
import popschool.bankroot.dao.PersonneDAO;
import popschool.bankroot.dao.RattachementDAO;
import popschool.bankroot.model.CompteDepot;
import popschool.bankroot.model.Personne;
import popschool.bankroot.model.Rattachement;

import java.util.List;

@Service
public class ServiceCompteImpl implements ServiceCompte{
    @Autowired
    CompteDepotDAO compteDepotDAO;
    @Autowired
    PersonneDAO personneDAO;
    @Autowired
    RattachementDAO rattachementDAO;


    @Override
    public List<CompteDepot> findAllComptes() {
        return compteDepotDAO.findAll();
    }

    @Override
    public List<Personne> findAllPersonnes() {
        return personneDAO.findAll();
    }

    @Override
    public List<CompteDepot> findComptesOfPersonne(int id) {
        return rattachementDAO.findCompteByPersonne(id);
    }

    @Override
    public void crediter(Integer id, Double montant) {
       try {
           compteDepotDAO.crediter_compte(id,montant);
       }catch (Exception e){
           System.out.println(e.getMessage());
       }
    }

    @Override
    public void debiter(Integer id, Double montant) {
        try {
            compteDepotDAO.debiter_compte(id,montant);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void bloquer(Integer id) {
        compteDepotDAO.bloquer_compte(id);
    }

    @Override
    public void debloquer(Integer id) {
        compteDepotDAO.debloquer_compte(id);
    }
}
