package popschool.bankroot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Table(name = "compte_depot", schema = "public", catalog = "bankROOT")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompteDepot {
    private int numero;
    private String categorie;
    private Timestamp dateCreation;
    private double solde;
    private Boolean bloque;
    private Double decouvertAutorise;
    private String libelle;
    private Double tauxAnnuel;
    private Timestamp dateMaj;

    @Id
    @Column(name = "numero")
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "categorie")
    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Basic
    @Column(name = "date_creation")
    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

    @Basic
    @Column(name = "solde")
    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    @Basic
    @Column(name = "bloque")
    public Boolean getBloque() {
        return bloque;
    }

    public void setBloque(Boolean bloque) {
        this.bloque = bloque;
    }

    @Basic
    @Column(name = "decouvert_autorise")
    public Double getDecouvertAutorise() {
        return decouvertAutorise;
    }

    public void setDecouvertAutorise(Double decouvertAutorise) {
        this.decouvertAutorise = decouvertAutorise;
    }

    @Basic
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Basic
    @Column(name = "taux_annuel")
    public Double getTauxAnnuel() {
        return tauxAnnuel;
    }

    public void setTauxAnnuel(Double tauxAnnuel) {
        this.tauxAnnuel = tauxAnnuel;
    }

    @Basic
    @Column(name = "date_maj")
    public Timestamp getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Timestamp dateMaj) {
        this.dateMaj = dateMaj;
    }

    @Override
    public String toString() {
        return "CompteDepot{" +
                "numero=" + numero +
                ", categorie='" + categorie + '\'' +
                ", dateCreation=" + dateCreation +
                ", solde=" + solde +
                ", bloque=" + bloque +
                ", decouvertAutorise=" + decouvertAutorise +
                ", libelle='" + libelle + '\'' +
                ", tauxAnnuel=" + tauxAnnuel +
                ", dateMaj=" + dateMaj +
                '}';
    }
}
