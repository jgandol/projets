package popschool.bankroot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Table(name = "type_carte", schema = "public", catalog = "bankROOT")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeCarte {
    private int id;
    private String statut;
    private double plafondRetrait;
    private double cotisationAnnuelle;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "statut")
    public String getStatut() {
        return statut;
    }
    public void setStatut(String statut) {
        this.statut = statut;
    }

    @Basic
    @Column(name = "plafond_retrait")
    public double getPlafondRetrait() {
        return plafondRetrait;
    }
    public void setPlafondRetrait(double plafondRetrait) {
        this.plafondRetrait = plafondRetrait;
    }

    @Basic
    @Column(name = "cotisation_annuelle")
    public double getCotisationAnnuelle() {
        return cotisationAnnuelle;
    }
    public void setCotisationAnnuelle(double cotisationAnnuelle) {
        this.cotisationAnnuelle = cotisationAnnuelle;
    }


    @Override
    public String toString() {
        return "TypeCarte{" +
                "id=" + id +
                ", statut='" + statut + '\'' +
                ", plafondRetrait=" + plafondRetrait +
                ", cotisationAnnuelle=" + cotisationAnnuelle +
                '}';
    }
}
