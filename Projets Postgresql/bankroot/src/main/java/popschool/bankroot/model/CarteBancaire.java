package popschool.bankroot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Table(name = "carte_bancaire", schema = "public", catalog = "bankROOT")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarteBancaire {
    private int numero;
    private int codeSecret;
    private int nbEssais;
    private boolean bloquee;
    private Date dateExpiration;
    private Personne personne;
    private CompteDepot compteDepot;

    @Id
    @Column(name = "numero")
    public int getNumero() {
        return numero;
    }
    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "code_secret")
    public int getCodeSecret() {
        return codeSecret;
    }
    public void setCodeSecret(int codeSecret) {
        this.codeSecret = codeSecret;
    }

    @Basic
    @Column(name = "nb_essais")
    public int getNbEssais() {
        return nbEssais;
    }
    public void setNbEssais(int nbEssais) {
        this.nbEssais = nbEssais;
    }

    @Basic
    @Column(name = "bloquee")
    public boolean isBloquee() {
        return bloquee;
    }
    public void setBloquee(boolean bloquee) {
        this.bloquee = bloquee;
    }

    @Basic
    @Column(name = "date_expiration")
    public Date getDateExpiration() {
        return dateExpiration;
    }
    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    @ManyToOne
    @JoinColumn(name = "id_titulaire", referencedColumnName = "id", nullable = false, table = "carte_bancaire")
    public Personne getPersonne() {
        return personne;
    }
    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @ManyToOne
    @JoinColumn(name = "id_compte", referencedColumnName = "numero", nullable = false, table = "carte_bancaire")
    public CompteDepot getCompteDepot() {
        return compteDepot;
    }
    public void setCompteDepot(CompteDepot compteDepot) {
        this.compteDepot = compteDepot;
    }

    @Override
    public String toString() {
        return "CarteBancaire{" +
                "numero=" + numero +
                ", codeSecret=" + codeSecret +
                ", nbEssais=" + nbEssais +
                ", bloquee=" + bloquee +
                ", dateExpiration=" + dateExpiration +
                '}';
    }
}
