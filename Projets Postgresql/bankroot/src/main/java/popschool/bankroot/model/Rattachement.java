package popschool.bankroot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Table(name = "rattachement", schema = "public", catalog = "bankROOT")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rattachement {
    private int id;
    private Timestamp dateRattachement;
    private CompteDepot compteDepot;
    private Personne personne;

    @Id
    @Column(name = "id", table = "rattachement")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "date_rattachement", table = "rattachement")
    public Timestamp getDateRattachement() {
        return dateRattachement;
    }

    public void setDateRattachement(Timestamp dateRattachement) {
        this.dateRattachement = dateRattachement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rattachement that = (Rattachement) o;
        return id == that.id &&
                Objects.equals(dateRattachement, that.dateRattachement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateRattachement);
    }

    @ManyToOne
    @JoinColumn(name = "id_compte", referencedColumnName = "numero", nullable = false, table = "rattachement")
    public CompteDepot getCompteDepot() {
        return compteDepot;
    }

    public void setCompteDepot(CompteDepot compteDepot) {
        this.compteDepot = compteDepot;
    }

    @ManyToOne
    @JoinColumn(name = "id_titulaire", referencedColumnName = "id", nullable = false, table = "rattachement")
    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @Override
    public String toString() {
        return "Rattachement{" +
                "id=" + id +
                ", dateRattachement=" + dateRattachement +
                ", compteDepot=" + compteDepot.getLibelle() +
                ", personne=" + personne.getNom() + " " + personne.getPrenom() +
                '}';
    }
}
