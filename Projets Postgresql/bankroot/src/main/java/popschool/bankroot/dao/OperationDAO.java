package popschool.bankroot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import popschool.bankroot.model.Operation;
@Repository
public interface OperationDAO extends JpaRepository<Operation, Integer> {
}
