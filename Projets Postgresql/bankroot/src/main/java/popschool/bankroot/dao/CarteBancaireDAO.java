package popschool.bankroot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import popschool.bankroot.model.CarteBancaire;
@Repository
public interface CarteBancaireDAO extends JpaRepository<CarteBancaire, Integer> {
}
