package popschool.bankroot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;
import popschool.bankroot.model.Personne;

import java.util.List;

@Repository
public interface PersonneDAO extends JpaRepository<Personne, Integer> {
    List<Personne> findAll();

    @Procedure
    Integer get_nb_comptes_from_id(Integer id);

}
