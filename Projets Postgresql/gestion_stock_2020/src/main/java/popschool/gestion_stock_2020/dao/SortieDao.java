package popschool.gestion_stock_2020.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.gestion_stock_2020.model.StockSortie;

public interface SortieDao extends JpaRepository<StockSortie, Integer> {
}
