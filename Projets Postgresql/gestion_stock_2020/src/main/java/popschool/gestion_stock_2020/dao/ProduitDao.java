package popschool.gestion_stock_2020.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.gestion_stock_2020.model.StockProduit;

public interface ProduitDao extends JpaRepository<StockProduit, Integer> {

}
