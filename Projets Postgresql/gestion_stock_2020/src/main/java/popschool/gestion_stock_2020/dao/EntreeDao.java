package popschool.gestion_stock_2020.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.gestion_stock_2020.model.StockEntree;

public interface EntreeDao extends JpaRepository<StockEntree, Integer> {
}
