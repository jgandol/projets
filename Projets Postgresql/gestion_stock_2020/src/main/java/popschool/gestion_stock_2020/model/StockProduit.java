package popschool.gestion_stock_2020.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "stock_produit", schema = "public", catalog = "pg_gestion_stock")
@SequenceGenerator (name = "prod_gen", sequenceName= "stock_produit_nproduit_seq", allocationSize=1 )
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockProduit {
    private int nproduit;
    private int qteStock;
    private String libelle;
    private double valeurStock;
    public List<StockEntree> stockEntrees;
    private List<StockSortie> stockSorties;

    public StockProduit(String libelle) {
        this.libelle = libelle;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prod_gen")
    @Column(name = "nproduit")
    public int getNproduit() {
        return nproduit;
    }
    public void setNproduit(int nproduit) {
        this.nproduit = nproduit;
    }

    @Basic
    @Column(name = "qte_stock")
    public int getQteStock() {
        return qteStock;
    }
    public void setQteStock(int qteStock) {
        this.qteStock = qteStock;
    }

    @Basic
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Basic
    @Column(name = "valeur_stock")
    public double getValeurStock() {
        return valeurStock;
    }
    public void setValeurStock(double valeurStock) {
        this.valeurStock = valeurStock;
    }


    @OneToMany(mappedBy = "stockProduit")
    public List<StockEntree> getStockEntrees() {
        return stockEntrees;
    }
    public void setStockEntrees(List<StockEntree> stockEntrees) {
        this.stockEntrees = stockEntrees;
    }

    @OneToMany(mappedBy = "stockProduit")
    public List<StockSortie> getStockSorties() {
        return stockSorties;
    }
    public void setStockSorties(List<StockSortie> stockSorties) {
        this.stockSorties = stockSorties;
    }

    @Override
    public String toString() {
        return "StockProduit{" +
                "nproduit=" + nproduit +
                ", qteStock=" + qteStock +
                ", libelle='" + libelle + '\'' +
                ", valeurStock=" + valeurStock +
                '}';
    }
}

