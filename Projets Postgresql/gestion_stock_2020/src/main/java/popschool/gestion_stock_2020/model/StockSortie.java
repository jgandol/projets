package popschool.gestion_stock_2020.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "stock_sortie", schema = "public", catalog = "pg_gestion_stock")
@SequenceGenerator(name = "sortie_gen", sequenceName = "stock_sortie_nsortie_seq", allocationSize = 1)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockSortie {
    private int nsortie;
    private int qteSortie;
    private double montantSortie;
    private Date dateSortie;
    private StockProduit stockProduit;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sortie_gen")
    @Column(name = "nsortie")
    public int getNsortie() {
        return nsortie;
    }
    public void setNsortie(int nsortie) {
        this.nsortie = nsortie;
    }

    @Basic
    @Column(name = "qte_sortie")
    public int getQteSortie() {
        return qteSortie;
    }
    public void setQteSortie(int qteSortie) {
        this.qteSortie = qteSortie;
    }

    @Basic
    @Column(name = "montant_sortie")
    public double getMontantSortie() {
        return montantSortie;
    }
    public void setMontantSortie(double montantSortie) {
        this.montantSortie = montantSortie;
    }

    @Basic
    @Column(name = "date_sortie")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDateSortie() {
        return dateSortie;
    }
    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }

    @ManyToOne
    @JoinColumn(name = "nproduit", referencedColumnName = "nproduit", nullable = false, table = "stock_sortie")
    public StockProduit getStockProduit() {
        return stockProduit;
    }
    public void setStockProduit(StockProduit stockProduit) {
        this.stockProduit = stockProduit;
    }

    @Override
    public String toString() {
        return "StockSortie{" +
                "nsortie=" + nsortie +
                ", qteSortie=" + qteSortie +
                ", montantSortie=" + montantSortie +
                ", dateSortie=" + dateSortie +
                (stockProduit != null ? ", stockProduit=" + stockProduit : "") +
                '}';
    }
}
