package popschool.gestion_stock_2020.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "stock_entree", schema = "public", catalog = "pg_gestion_stock")
@SequenceGenerator(name = "entree_gen", sequenceName = "stock_entree_nentree_seq", allocationSize = 1)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockEntree {
    private int nentree;
    private int qteEntree;
    private int qteUtilise;
    private double prixUnitaire;
    private Date dateEntree;
    private StockProduit stockProduit;

    public StockEntree(int qteEntree, double prixUnitaire, StockProduit stockProduit) {
        this.qteEntree = qteEntree;
        this.prixUnitaire = prixUnitaire;
        this.stockProduit = stockProduit;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entree_gen")
    @Column(name = "nentree")
    public int getNentree() {
        return nentree;
    }
    public void setNentree(int nentree) {
        this.nentree = nentree;
    }

    @Basic
    @Column(name = "qte_entree")
    public int getQteEntree() {
        return qteEntree;
    }
    public void setQteEntree(int qteEntree) {
        this.qteEntree = qteEntree;
    }

    @Basic
    @Column(name = "qte_utilise")
    public int getQteUtilise() {
        return qteUtilise;
    }
    public void setQteUtilise(int qteUtilise) {
        this.qteUtilise = qteUtilise;
    }

    @Basic
    @Column(name = "prix_unitaire")
    public double getPrixUnitaire() {
        return prixUnitaire;
    }
    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    @Basic
    @Column(name = "date_entree")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDateEntree() {
        return dateEntree;
    }
    public void setDateEntree(Date dateEntree) {
        this.dateEntree = dateEntree;
    }

    @ManyToOne
    @JoinColumn(name = "nproduit", referencedColumnName = "nproduit", table = "stock_entree")
    public StockProduit getStockProduit() {
        return stockProduit;
    }
    public void setStockProduit(StockProduit stockProduit) {
        this.stockProduit = stockProduit;
    }

    @Override
    public String toString() {
        return "StockEntree{" +
                "nentree=" + nentree +
                ", qteEntree=" + qteEntree +
                ", prixUnitaire=" + prixUnitaire +
                ", dateEntree=" + dateEntree +
                (stockProduit != null ? ", stockProduit=" + stockProduit : "") +
                '}';
    }
}
