package popschool.gestion_stock_2020;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.gestion_stock_2020.dao.EntreeDao;
import popschool.gestion_stock_2020.dao.ProduitDao;
import popschool.gestion_stock_2020.dao.SortieDao;
import popschool.gestion_stock_2020.model.StockEntree;
import popschool.gestion_stock_2020.model.StockProduit;

@Component
public class TEST implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(GestionStock2020Application.class);

    private void information(StockProduit p) {
        log.info(p.toString());
    }

    @Autowired
    ProduitDao produitDao;
    @Autowired
    EntreeDao entreeDao;
    @Autowired
    SortieDao sortieDao;

    @Override
    public void run(String... args) throws Exception {
//        StockProduit p =produitDao.findById(2).get();
//        StockEntree e = new StockEntree(3,100.0,p);
//        e.setStockProduit(p);
//        entreeDao.save(e);
//        System.out.println("--------------------------------------\n");
//        entreeDao.findAll().forEach(System.out::println);
//        System.out.println("--------------------------------------\n");
////        sortieDao.findAll().forEach(System.out::println);
//        System.out.println("--------------------------------------\n");
//        produitDao.findAll().forEach(System.out::println);
    }
    //TODO:
    // modifier produit (lib)
    // Afficher les entrees et sorties du produit
    // Creer entree et sortie
}
