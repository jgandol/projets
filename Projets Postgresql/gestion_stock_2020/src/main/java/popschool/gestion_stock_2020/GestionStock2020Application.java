package popschool.gestion_stock_2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionStock2020Application {

    public static void main(String[] args) {
        SpringApplication.run(GestionStock2020Application.class, args);
    }

}
