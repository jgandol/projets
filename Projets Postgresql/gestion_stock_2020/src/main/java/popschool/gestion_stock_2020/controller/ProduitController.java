package popschool.gestion_stock_2020.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import popschool.gestion_stock_2020.dao.ProduitDao;
import popschool.gestion_stock_2020.model.StockProduit;

import javax.websocket.server.PathParam;

@Controller
public class ProduitController {
    @Autowired
    ProduitDao produitDao;

    @GetMapping("/")
    public String getAllProduits(Model model){
        model.addAttribute("produits",produitDao.findAll());
        return "ensProduits";
    }

    @GetMapping("/createProduit")
    public String redirectCreateProduit(Model model){
        StockProduit p = new StockProduit();
        model.addAttribute("produit",p);
        model.addAttribute("produits",produitDao.findAll());
        return "createProduit";
    }

    @GetMapping("/deleteProduit")
    public String deleteCreateProduit(Model model, @PathParam("id")int id){
        StockProduit p = produitDao.findById(id).get();
        produitDao.delete(p);

        model.addAttribute("produits",produitDao.findAll());
        return "ensProduits";
    }

    @PostMapping("/createProduit")
    public String createProduits(Model model, @RequestParam("lib")String lib){
        StockProduit p = new StockProduit(lib);
        produitDao.save(p);
        model.addAttribute("produits",produitDao.findAll());
        return "ensProduits";
    }

    @GetMapping("/updateProduit")
    public String redirectUpdateProduits(Model model, @PathParam("id")int id){
        StockProduit p = produitDao.findById(id).get();
        model.addAttribute("produit",p);
        return "updateProduit";
    }

    @PostMapping("/updateProduit")
    public String updateProduits(Model model, @RequestParam("libelle")String lib,@RequestParam("id")int id){
        StockProduit p = produitDao.findById(id).get();
        p.setLibelle(lib);
        produitDao.save(p);
        model.addAttribute("produits",produitDao.findAll());
        return "ensProduits";
    }
}
