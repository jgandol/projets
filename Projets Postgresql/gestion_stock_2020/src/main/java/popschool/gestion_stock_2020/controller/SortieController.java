package popschool.gestion_stock_2020.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import popschool.gestion_stock_2020.dao.EntreeDao;
import popschool.gestion_stock_2020.dao.ProduitDao;
import popschool.gestion_stock_2020.dao.SortieDao;
import popschool.gestion_stock_2020.model.StockEntree;
import popschool.gestion_stock_2020.model.StockProduit;
import popschool.gestion_stock_2020.model.StockSortie;

import javax.websocket.server.PathParam;

@Controller
public class SortieController {
    @Autowired
    ProduitDao produitDao;
    @Autowired
    SortieDao sortieDao;

    @GetMapping("/showOutputs")
    public String redirectShowOutPuts(Model model, @PathParam("id")int id){
        StockProduit p = produitDao.findById(id).get();
        model.addAttribute("produit",p);
        return "showOutputs";
    }

    @GetMapping("/createOutput")
    public String redirectCreateOutputs(Model model, @PathParam("id") int id){
        model.addAttribute("output",new StockEntree());
        model.addAttribute("idprod",id);
        return "createOutput";
    }

    @PostMapping("/createOutput")
    public String updateProduits(Model model, @RequestParam("qte")int qte,
                                 @RequestParam("pu")double pu, @RequestParam("id")int id){
        StockProduit p = produitDao.findById(id).get();
        System.out.println(p.getNproduit());
        StockSortie e = new StockSortie();
        e.setQteSortie(qte);
        e.setMontantSortie(pu);
        e.setStockProduit(p);
        p.getStockSorties().add(e);
        produitDao.save(p);
        System.out.println("Produit :" + p);
        System.out.println("Entree :" + e);

        sortieDao.save(e);
        model.addAttribute("produit",p);
        return "showOutputs";
    }
}
