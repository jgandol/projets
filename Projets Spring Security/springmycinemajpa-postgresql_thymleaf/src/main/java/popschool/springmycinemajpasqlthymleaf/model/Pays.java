package popschool.springmycinemajpasqlthymleaf.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="pays")
public class Pays {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long npays;

    private String nom;

    @OneToMany(mappedBy = "pays")
    private List<Acteur> acteurs = new ArrayList<>();

    @OneToMany(mappedBy = "pays")
    private List<Film> films = new ArrayList<>();

    @Override
    public String toString() {
        return "Pays{" +
                ", nom='" + nom + '\'' +
                '}';
    }
}
