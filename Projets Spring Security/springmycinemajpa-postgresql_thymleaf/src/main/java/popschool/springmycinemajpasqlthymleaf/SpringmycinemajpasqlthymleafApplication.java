package popschool.springmycinemajpasqlthymleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringmycinemajpasqlthymleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringmycinemajpasqlthymleafApplication.class, args);
    }

}
