package popschool.springmycinemajpasqlthymleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.springmycinemajpasqlthymleaf.model.Client;
import popschool.springmycinemajpasqlthymleaf.model.Film;
import popschool.springmycinemajpasqlthymleaf.service.VideoService;


import java.util.Iterator;
import java.util.List;

@Controller
@SessionAttributes(value="clientSession", types = {Client.class})
public class MainController {

    @Autowired
    private VideoService videoService;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                      CREATION DE LA COMMANDE EN SESSION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @ModelAttribute("clientSession")
    public Client addMyBean1ToSessionScope() {
        return new Client();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          INITIALISATION DE LA PAGE INDEX
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        return "index";
    }

    @GetMapping(value = {"/pageGestionClients"})
    public String clientList(Model model) {
        List<Client> ensClients = videoService.ensClient();
        Client newClient = new Client();
        model.addAttribute("ensClients", ensClients);
        model.addAttribute("newClient", newClient);

        return "gestionClients";
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          GESTION DES CLIENTS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @PostMapping(value = {"/ajouterClient"})
    public String joutClient(@ModelAttribute("newClient") Client newClient, Model model){
        if (newClient.getNom()!=null && newClient.getPrenom()!=null && newClient.getAdresse()!=null && newClient.getAnciennete()!=null){
            videoService.ajouterUnClient(newClient);
            return "redirect:/pageGestionClients";
        }else {
            return "redirect:/pageGestionClients";
        }
    }

    @GetMapping(value = {"/deleteClient/{clientDelete}"})
    public String deleteClient(@PathVariable("clientDelete") Long id, Model model) {
        Client cl = videoService.trouverParId(id);
        videoService.supprimerClient(cl);
        return "redirect:/pageGestionClients";
    }

    @GetMapping(value = {"/updateClient/{clientUpdate}"})
    public String updateClient(@PathVariable("clientUpdate") Long id, Model model){
        Client cl = videoService.trouverParId(id);
        model.addAttribute("clientUpdate", cl);
        return "pageModifClients";
    }

    @PostMapping(value = {"/modifierClient"})
    public String modifierClient(@ModelAttribute("clientUpdate") Client clientUpdate, Model model){
        if (clientUpdate.getNom()!=null && clientUpdate.getPrenom()!=null && clientUpdate.getAdresse()!=null && clientUpdate.getAnciennete()!=null){
            //la methode ajouter client est identique a une methode update client donc j'utilise ajouter
            videoService.ajouterUnClient(clientUpdate);
            return "redirect:/pageGestionClients";
        }else {
            return "redirect:/pageGestionClients";
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          GESTION DES FILMS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @GetMapping(value = {"/pageGestionFilms"})
    public String filmList(Model model){
        List<Film> ensFilms = videoService.ensFilm();
        Film newFilm = new Film();
        model.addAttribute("ensFilms", ensFilms);
        model.addAttribute("newFilm", newFilm);
        return "gestionFilms";
    }

    @GetMapping(value = {"/deleteFilm/{filmDelete}"})
    public String deleteFilm(@PathVariable("filmDelete") Long id, Model model) {
        Film film = videoService.trouverFilmParId(id);
        videoService.supprimerFilm(film);
        return "redirect:/pageGestionFilms";
    }

}