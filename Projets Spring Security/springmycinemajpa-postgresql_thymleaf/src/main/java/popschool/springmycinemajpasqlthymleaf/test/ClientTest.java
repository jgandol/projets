package popschool.springmycinemajpasqlthymleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.springmycinemajpasqlthymleaf.SpringmycinemajpasqlthymleafApplication;
import popschool.springmycinemajpasqlthymleaf.dao.ClientRepository;
import popschool.springmycinemajpasqlthymleaf.model.Client;

@Component
public class ClientTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(SpringmycinemajpasqlthymleafApplication.class);

    @Autowired
    private ClientRepository clientRepository;

    private void information(Client c){
        log.info(c.toString());
    }

    @Override
    public void run(String... args) throws Exception {

        //Creation d'un client
        /*clientRepository.save(new Client("Bouchez","Marc",
               "Valenciennes 59300", 30));*/

        //Update d'un client
        /*log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                    Update un client                                               ");
        log.info("---------------------------------------------------------------------------------------------------");
        Optional<Client>cl=clientRepository.findById(3L);
        Client c = cl.get();
        c.setNom("TINTIN");
        log.info(c.toString());
        clientRepository.save(c);*/

        //Afficher tous les clients
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                    afficher tous les clients                                      ");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Client c : clientRepository.findAllByOrderByNom()){
            log.info(c.toString());
        }



    }
}
