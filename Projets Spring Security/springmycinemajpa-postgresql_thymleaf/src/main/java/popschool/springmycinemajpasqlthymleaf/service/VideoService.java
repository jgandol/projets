package popschool.springmycinemajpasqlthymleaf.service;

import popschool.springmycinemajpasqlthymleaf.model.Client;
import popschool.springmycinemajpasqlthymleaf.model.Film;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;


public interface VideoService{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          EMPRUNT                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void emprunter(String nom, String titre);
    void retourEmprunt(String nom, String titre);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                           CLIENT                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Client> ensClient();
    void ajouterUnClient(Client client);
    Client trouverParId(Long id);
    void supprimerClient(Client client);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                         FILM                                                               //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Film> ensFilmEmpruntable();
    List<Film> ensFilm();
    List<Film> ensFilmGenre(String genre);
    List<Tuple>infoRealisateurActeur(String titre);
    int nbrFilmDuGenre(String genre);
    List<Film> ensFilmEmpruntesByClient(Optional<Client> client);
    Film trouverFilmParId(Long id);
    void supprimerFilm(Film film);

}
