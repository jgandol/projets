package popschool.springmycinemajpasqlthymleaf.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.springmycinemajpasqlthymleaf.model.Emprunt;

import java.util.Optional;

public interface EmpruntRepository extends CrudRepository<Emprunt, Long> {


    Optional<Emprunt> findByClient_NomAndFilm_TitreAndRetour(String nom, String titre, String non);
}
