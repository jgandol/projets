package popschool.rest_crimeportal_client.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import uk.co.blackpepper.bowman.InlineAssociationDeserializer;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Data
@RemoteResource("/organisations")
public class Organisation {
    private URI id;
    private String orgName;
    private String description;

    private Gangster boss;


    @ResourceId
    public URI getId() {
        return id;
    }

    public void setId(URI id) {
        this.id = id;
    }


    @LinkedResource
    @JsonDeserialize(contentUsing = InlineAssociationDeserializer.class)
    public Gangster getBoss() {
        return boss;
    }
    public void setBoss(Gangster theboss) {
        this.boss = theboss;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
