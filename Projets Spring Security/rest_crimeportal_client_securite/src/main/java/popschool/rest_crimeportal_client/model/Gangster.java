package popschool.rest_crimeportal_client.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import uk.co.blackpepper.bowman.InlineAssociationDeserializer;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;

@RemoteResource("/gangsters")
public class Gangster {
    private URI id;

    private String gname;
    private String nickname;
    private Double salary = 0.0;

    private Organisation organisation;

    @ResourceId
    public URI getId() {
        return id;
    }

    @LinkedResource
    @JsonDeserialize(contentUsing = InlineAssociationDeserializer.class)
    public Organisation getOrganisation() {
        return organisation;
    }

    public void setId(URI id) {
        this.id = id;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    @Override
    public String toString() {
        return "Gangster{" +
                "id=" + getId() +
                ", gname='" + getGname() + '\'' +
                ", nickname='" + getNickname() + '\'' +
                '}';
    }
}
