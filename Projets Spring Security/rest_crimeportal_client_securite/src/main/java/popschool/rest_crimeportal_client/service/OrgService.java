package popschool.rest_crimeportal_client.service;

import com.sun.org.apache.xpath.internal.operations.Or;
import org.springframework.stereotype.Service;
import popschool.rest_crimeportal_client.model.Gangster;
import popschool.rest_crimeportal_client.model.Organisation;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import javax.transaction.Transactional;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OrgService {
    ClientFactory clientFactory =
            Configuration.builder().setBaseUri("http://localhost:8080/").build()
                    .buildClientFactory();
    Client<Organisation> clientOrg = clientFactory.create(Organisation.class);

    public List<Organisation> findAll(){

        Iterable<Organisation> organisations = clientOrg.getAll(URI.create(
                "http://localhost:8080/organisations"));

        List<Organisation> list = new ArrayList<>();
        organisations.forEach(list::add);
        return list;
    }

    public Organisation save (Organisation organisation){


        URI id = clientOrg.post(organisation);

        organisation.setId(id);
        return organisation;
    }

    public void update (Organisation organisation){

        System.out.println(organisation);
        clientOrg.patch(organisation.getId(), organisation);


    }

    public void delete (String id){
        Organisation organisation = findById(id).get();
        organisation.setBoss(null);
        clientOrg.delete(URI.create(id));
    }

    public Optional<Organisation> findByOrgname(String orgname){

            Organisation organisation = clientOrg.get(URI.create(
                    "http://localhost:8080/organisations/search/findByOrgname?orgname="+orgname));
            return Optional.ofNullable(organisation);
    }

    public Optional<Organisation> findById(String id){

        Organisation organisation = clientOrg.get(URI.create(id));
        return Optional.ofNullable(organisation);
    }
}
