//package popschool.rest_crimeportal_client.service;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//@Service
//public class ServiceSecurity implements UserDetailsService {
//    AppRoleDao roleDao;
//    AppUserDao userDao;
//
//    @Autowired
//    public ServiceSecurity(AppRoleDao roleDao, AppUserDao userDao) {
//        this.roleDao = roleDao;
//        this.userDao = userDao;
//    }
//    @Override
//    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
//        Optional<AppUser> appUser = this.userDao.findByUserName(userName);
//        if (! appUser.isPresent()) {
//            System.out.println("User not found! " + userName);
//            throw new UsernameNotFoundException("User " + userName + " was not found in the database");
//        }
//        System.out.println("Found User: " + appUser.get());
//        // [ROLE_USER, ROLE_ADMIN,..]
//        List<AppRole> roles = appUser.get().getRoles();
//
//        System.out.println("roles : " + roles);
//
//
//        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
//        if (roles.size()>0) {
//            for (AppRole role : roles) {
//                // ROLE_USER, ROLE_ADMIN,..
//                GrantedAuthority authority = new SimpleGrantedAuthority(role.getRoleName());
//                grantList.add(authority);
//            }
//        }
//        UserDetails userDetails = (UserDetails) new org.springframework.security.core.userdetails.User(appUser.get().getUserName(), //
//                appUser.get().getEncrytedPassword(), grantList);
//        return userDetails;
//    }
//}
