package popschool.rest_crimeportal_client.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import popschool.rest_crimeportal_client.model.Organisation;
import popschool.rest_crimeportal_client.service.GangsterService;
import popschool.rest_crimeportal_client.service.OrgService;

import java.net.URI;

@Controller
public class OrgController {
    private final OrgService orgService;
    private final GangsterService gangsterService;


    @Autowired
    public OrgController(OrgService orgService, GangsterService gangsterService ) {
        this.orgService = orgService;
        this.gangsterService=gangsterService;
    }

    @ModelAttribute(name = "organisation")
    public Organisation design() {
        return new Organisation();
    }

    @GetMapping({"index"})
    public String index(Model model){
        model.addAttribute("orgs", orgService.findAll());
        return "index-org";
    }

    @GetMapping("/signup")
    public String showSignUpForm(Model model) {
        return "add-org";
    }

    @PostMapping("/adduser")
    public String addUser(@Valid Organisation organisation, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-org";
        }

        orgService.save(organisation);
        model.addAttribute("organisations", orgService.findAll());
        return "redirect:/index";
    }

    @GetMapping("/edit")
    public String showUpdateForm(@RequestParam("id") String id, Model model) {
       Organisation organisation = orgService.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
       organisation.setId(URI.create(id));
       model.addAttribute("organisation", organisation);
        return "update-org";
    }

    @PostMapping("/update")
    public String updateUser(
                             @Valid Organisation organisation, BindingResult result,
                             @RequestParam("gname") String gname, Model model) {
        if (result.hasErrors()) {
            return "update-org";
        }

        if(gname!=null){
            gangsterService.findByGname(gname).ifPresent(organisation::setBoss);

        }
        orgService.update(organisation);
        model.addAttribute("organisations", orgService.findAll());
        return "redirect:/index";
    }

    @GetMapping("/delete")
    public String deleteUser(@RequestParam("id") String id, Model model) {
/*        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));*/

        orgService.delete(id);
        model.addAttribute("orgs", orgService.findAll());
        return "index-org";
    }
}
