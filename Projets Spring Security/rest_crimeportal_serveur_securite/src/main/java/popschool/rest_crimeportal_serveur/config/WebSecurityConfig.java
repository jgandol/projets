package popschool.rest_crimeportal_serveur.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import popschool.rest_crimeportal_serveur.service.ServiceSecurity;


import javax.sql.DataSource;
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private ServiceSecurity userDetailsService;
    @Autowired
    private DataSource dataSource;
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // Setting Service to find User in the database.
        // And Setting PassswordEncoder
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());

    }
    // Secure the endpoins with HTTP Basic authentication
    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests().antMatchers("/gangsters").permitAll();
        http
                //HTTP Basic authentication
                .httpBasic()
                .and()
                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/gangsters").hasAnyRole
//                ("USER","ADMIN")
                .antMatchers(HttpMethod.GET, "/organisations","/gangsters").hasAnyRole
                ("USER","ADMIN")
                .antMatchers(HttpMethod.POST, "/gangsters").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/gangsters").hasRole
                ("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/gangsters").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/gangsters").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/organisations").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/organisations").hasRole
                ("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/organisations").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/organisations").hasRole("ADMIN")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }
    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
        db.setDataSource(dataSource);
        return db;
    }
}
