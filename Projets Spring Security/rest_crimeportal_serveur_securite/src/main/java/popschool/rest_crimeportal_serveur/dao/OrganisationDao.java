package popschool.rest_crimeportal_serveur.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.rest_crimeportal_serveur.model.Organisation;

import javax.transaction.Transactional;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "organisations", path = "organisations")
public interface OrganisationDao extends JpaRepository<Organisation, Integer> {
    Optional<Organisation> findByOrgName(String orgname);


}
