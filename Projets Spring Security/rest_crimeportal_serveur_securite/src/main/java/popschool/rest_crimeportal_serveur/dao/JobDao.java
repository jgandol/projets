package popschool.rest_crimeportal_serveur.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.rest_crimeportal_serveur.model.Job;

@RepositoryRestResource(collectionResourceRel = "jobs", path = "jobs")
public interface JobDao extends JpaRepository<Job, Integer> {
}
