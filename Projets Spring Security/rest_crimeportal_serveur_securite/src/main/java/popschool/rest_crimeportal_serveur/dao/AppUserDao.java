package popschool.rest_crimeportal_serveur.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.rest_crimeportal_serveur.model.AppUser;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface AppUserDao extends JpaRepository<AppUser, Integer> {
    Optional<AppUser> findByUserName(String name);
}
