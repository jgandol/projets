package popschool.rest_crimeportal_serveur.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.rest_crimeportal_serveur.model.AppRole;

@RepositoryRestResource(collectionResourceRel = "roles", path = "roles")
public interface AppRoleDao extends JpaRepository<AppRole, Integer> {
}
