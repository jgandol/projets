package popschool.rest_crimeportal_serveur;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.*;

@SpringBootApplication
public class RestCrimeportalServeurApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestCrimeportalServeurApplication.class, args);
    }

}

