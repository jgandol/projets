package popschool.rest_crimeportal_serveur.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "app_role", schema = "public", catalog = "pg_my_mafia")
public class AppRole {
    private long roleId;
    private String roleName;
    private List<AppUser> users;

    @Id
    @Column(name = "role_id")
    public long getRoleId() {
        return roleId;
    }
    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "role_name")
    public String getRoleName() {
        return roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @ManyToMany(mappedBy = "roles")
    public List<AppUser> getUsers() {
        return users;
    }
    public void setUsers(List<AppUser> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "AppRole{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}
