package popschool.rest_crimeportal_serveur.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@SequenceGenerator (name = "org_gen", sequenceName= "organisation_org_id_seq", allocationSize=1 )
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Organisation {
    private int orgId;
    private String orgName;
    private String description;

    private List<Gangster> gangsters;
    private Gangster boss;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "org_gen")
    @Column(name = "org_id", table = "organisation")
    public int getOrgId() {
        return orgId;
    }
    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    @Basic
    @Column(name = "org_name", table = "organisation")
    public String getOrgName() {
        return orgName;
    }
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @Basic
    @Column(name = "description", table = "organisation")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy = "organisation")
    public List<Gangster> getGangsters() {
        return gangsters;
    }
    public void setGangsters(List<Gangster> gangsters) {
        this.gangsters = gangsters;
    }

    @ManyToOne
    @JoinColumn(name = "theboss", referencedColumnName = "gangster_id", table = "organisation")
    public Gangster getBoss() {
        return boss;
    }
    public void setBoss(Gangster boss) {
        this.boss = boss;
    }
}
