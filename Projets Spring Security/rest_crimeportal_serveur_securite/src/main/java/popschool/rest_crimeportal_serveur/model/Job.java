package popschool.rest_crimeportal_serveur.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@Entity
@SequenceGenerator(name = "job_gen", sequenceName= "job_job_id_seq", allocationSize=1 )
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Job {
    private int jobId;
    private String jobName;
    private Integer score;
    private Double setupcost;

    private List<Gangster> gangsters = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "job_gen")
    @Column(name = "job_id", table = "job")
    public int getJobId() {
        return jobId;
    }
    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "job_name", table = "job")
    public String getJobName() {
        return jobName;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Basic
    @Column(name = "score", table = "job")
    public Integer getScore() {
        return score;
    }
    public void setScore(Integer score) {
        this.score = score;
    }

    @Basic
    @Column(name = "setupcost", table = "job")
    public Double getSetupcost() {
        return setupcost;
    }
    public void setSetupcost(Double setupcost) {
        this.setupcost = setupcost;
    }

    @ManyToMany
    public List<Gangster> getGangsters() {
        return gangsters;
    }
    public void setGangsters(List<Gangster> gangsters) {
        this.gangsters = gangsters;
    }
}
