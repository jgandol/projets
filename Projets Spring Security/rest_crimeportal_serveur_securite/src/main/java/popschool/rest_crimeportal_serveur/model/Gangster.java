package popschool.rest_crimeportal_serveur.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@SequenceGenerator(name = "gang_gen", sequenceName= "gangster_gangster_id_seq", allocationSize=1 )
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Gangster {
    private int gangsterId;
    private String gname;
    private String nickname;
    private Integer badness;
    private BigDecimal salary;
    private Organisation organisation;

    private List<Job> jobs = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "org_gen")
    @Column(name = "gangster_id", table = "gangster")
    public int getGangsterId() {
        return gangsterId;
    }
    public void setGangsterId(int gangsterId) {
        this.gangsterId = gangsterId;
    }

    @Basic
    @Column(name = "gname", table = "gangster")
    public String getGname() {
        return gname;
    }
    public void setGname(String gname) {
        this.gname = gname;
    }

    @Basic
    @Column(name = "nickname", table = "gangster")
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Basic
    @Column(name = "badness", table = "gangster")
    public Integer getBadness() {
        return badness;
    }
    public void setBadness(Integer badness) {
        this.badness = badness;
    }

    @Basic
    @Column(name = "salary", table = "gangster")
    public BigDecimal getSalary() {
        return salary;
    }
    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @ManyToOne
    @JoinColumn(name = "org_id", referencedColumnName = "org_id", table = "gangster")
    public Organisation getOrganisation() {
        return organisation;
    }
    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    @ManyToMany
    @JoinTable(name = "affectation",
            joinColumns = @JoinColumn(name = "gangster_id"),
            inverseJoinColumns = @JoinColumn(name = "job_id"))
    public List<Job> getJobs() {
        return jobs;
    }
    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }
}
