package popschool.bankroot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Table(name = "operation", schema = "public", catalog = "bankROOT")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Operation {
    private int numero;
    private String categorie;
    private Timestamp dateOperation;
    private String operation;
    private BigDecimal montant;
    private Boolean effectuee;
    private String causeRefus;
    private Integer numeroCheque;
    private Integer codeSecretSaisi;
    private CompteDepot compteDepot;
    private CarteBancaire carteBancaire;

    @Id
    @Column(name = "numero", table = "operation")
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "categorie", table = "operation")
    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Basic
    @Column(name = "date_operation", table = "operation")
    public Timestamp getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(Timestamp dateOperation) {
        this.dateOperation = dateOperation;
    }

    @Basic
    @Column(name = "operation", table = "operation")
    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    @Basic
    @Column(name = "montant", table = "operation")
    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "effectuee", table = "operation")
    public Boolean getEffectuee() {
        return effectuee;
    }

    public void setEffectuee(Boolean effectuee) {
        this.effectuee = effectuee;
    }

    @Basic
    @Column(name = "cause_refus", table = "operation")
    public String getCauseRefus() {
        return causeRefus;
    }

    public void setCauseRefus(String causeRefus) {
        this.causeRefus = causeRefus;
    }

    @Basic
    @Column(name = "numero_cheque", table = "operation")
    public Integer getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(Integer numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    @Basic
    @Column(name = "code_secret_saisi", table = "operation")
    public Integer getCodeSecretSaisi() {
        return codeSecretSaisi;
    }

    public void setCodeSecretSaisi(Integer codeSecretSaisi) {
        this.codeSecretSaisi = codeSecretSaisi;
    }


    @ManyToOne
    @JoinColumn(name = "id_compte", referencedColumnName = "numero", nullable = false, table = "operation")
    public CompteDepot getCompteDepot() {
        return compteDepot;
    }

    public void setCompteDepot(CompteDepot compteDepot) {
        this.compteDepot = compteDepot;
    }

    @ManyToOne
    @JoinColumn(name = "id_carte_bancaire", referencedColumnName = "numero", table = "operation")
    public CarteBancaire getCarteBancaire() {
        return carteBancaire;
    }

    public void setCarteBancaire(CarteBancaire carteBancaire) {
        this.carteBancaire = carteBancaire;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "numero=" + numero +
                ", categorie='" + categorie + '\'' +
                ", dateOperation=" + dateOperation +
                ", operation='" + operation + '\'' +
                ", montant=" + montant +
                ", effectuee=" + effectuee +
                ", causeRefus='" + causeRefus + '\'' +
                ", numeroCheque=" + numeroCheque +
                ", codeSecretSaisi=" + codeSecretSaisi +
                '}';
    }
}
