package popschool.bankroot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Table(name = "personne", schema = "public", catalog = "bankROOT")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Personne {
    private int id;
    private String categorie;
    private String nom;
    private String adresse;
    private String prenom;
    private String civilite;
    private Integer siren;
    private Personne personne;

    @Id
    @Column(name = "id", table = "personne")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "categorie", table = "personne")
    public String getCategorie() {
        return categorie;
    }
    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Basic
    @Column(name = "nom", table = "personne")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "adresse", table = "personne")
    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "prenom", table = "personne")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "civilite", table = "personne")
    public String getCivilite() {
        return civilite;
    }
    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    @Basic
    @Column(name = "siren", table = "personne")
    public Integer getSiren() {
        return siren;
    }
    public void setSiren(Integer siren) {
        this.siren = siren;
    }


    @ManyToOne
    @JoinColumn(name = "dirigeant", referencedColumnName = "id", table = "personne")
    public Personne getPersonne() {
        return personne;
    }
    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @Override
    public String toString() {
        return "Personne{" +
                "id=" + id +
                ", categorie='" + categorie + '\'' +
                ", nom='" + nom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", prenom='" + prenom + '\'' +
                ", civilite='" + civilite + '\'' +
                (siren != null?", siren=" + siren:"") +
                (categorie == "Particulier"?", personne=" + personne:"") +
                '}';
    }
}
