package popschool.bankroot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import popschool.bankroot.model.UserRole;


import java.util.List;

public interface UserRoleDao extends JpaRepository<UserRole,Long> {
    @Query("select r.role.name from UserRole r  where r.user.id = :id")
    List<String> findByUser(Long id);
}
