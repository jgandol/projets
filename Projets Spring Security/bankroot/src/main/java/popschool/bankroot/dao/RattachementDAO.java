package popschool.bankroot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import popschool.bankroot.model.CompteDepot;
import popschool.bankroot.model.Rattachement;

import java.util.List;

@Repository
public interface RattachementDAO extends JpaRepository<Rattachement, Integer> {
    @Query("select r.compteDepot from Rattachement r where r.personne.id = :id")
    List<CompteDepot> findCompteByPersonne(int id);
}
