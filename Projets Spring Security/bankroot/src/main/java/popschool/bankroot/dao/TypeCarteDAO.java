package popschool.bankroot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import popschool.bankroot.model.TypeCarte;
@Repository
public interface TypeCarteDAO extends JpaRepository<TypeCarte, Integer> {
}
