package popschool.bankroot.dao;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;
import popschool.bankroot.model.CompteDepot;

import java.util.List;

@Repository
public interface CompteDepotDAO  extends JpaRepository<CompteDepot, Integer> {

    @Procedure
    Integer bloquer_compte(Integer id);

    @Procedure
    Integer debloquer_compte(Integer id);

    @Procedure
    Integer crediter_compte(Integer id, Double montant);

    @Procedure
    Integer debiter_compte(Integer id, Double montant);
}
