package popschool.bankroot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.bankroot.model.AppUser;


import java.util.Optional;

public interface AppUserDao extends JpaRepository<AppUser,Long> {
    Optional<AppUser> findByName(String name);
}
