package popschool.bankroot.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import popschool.bankroot.service.ServiceCompte;
import popschool.bankroot.utils.WebUtils;

import javax.websocket.server.PathParam;
import java.security.Principal;

@Controller
public class PersonneController {
    @Autowired
    ServiceCompte serviceCompte;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public String getClients(Model model) {

//        // After user login successfully.
//        String userName = principal.getName();
//
////        System.out.println("User Name: " + userName);
//
//        User loginedUser = (User) ((Authentication) principal).getPrincipal();
//
//        String userInfo = WebUtils.toString(loginedUser);
////        model.addAttribute("userInfo", userInfo);
//        System.out.println(userInfo);
        model.addAttribute("personnes", serviceCompte.findAllPersonnes());

        return "/bank/ensPersonnes";
    }


    @RequestMapping(value = "/comptes", method = RequestMethod.GET)
    public String getComptesOfClient(Model model, @PathParam("id")Integer id) {
        model.addAttribute("comptes", serviceCompte.findComptesOfPersonne(id));

        return "/bank/ensComptes";
    }
}
