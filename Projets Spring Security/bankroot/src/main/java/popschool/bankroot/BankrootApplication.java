package popschool.bankroot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankrootApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankrootApplication.class, args);
    }

}
