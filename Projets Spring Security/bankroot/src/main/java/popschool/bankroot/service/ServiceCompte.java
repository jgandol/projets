package popschool.bankroot.service;

import popschool.bankroot.model.CompteDepot;
import popschool.bankroot.model.Personne;

import java.util.List;

public interface ServiceCompte {

    List<CompteDepot> findAllComptes();
    List<Personne> findAllPersonnes();

    List<CompteDepot> findComptesOfPersonne(int id);

    void crediter(Integer id, Double montant);
    void debiter(Integer id, Double montant);
    void bloquer(Integer id);
    void debloquer(Integer id);
}
