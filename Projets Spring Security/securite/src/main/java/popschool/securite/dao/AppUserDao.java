package popschool.securite.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.securite.modele.AppUser;

import java.util.Optional;

public interface AppUserDao extends JpaRepository<AppUser,Long> {
    Optional<AppUser> findByName(String name);
}
