package popschool.mycinamespringsqlthymleaf.service;

import popschool.mycinamespringsqlthymleaf.model.Client;
import popschool.mycinamespringsqlthymleaf.model.Emprunt;
import popschool.mycinamespringsqlthymleaf.model.Film;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;


public interface VideoService{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          EMPRUNT                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void emprunter(String nom, String titre);
    void retourEmprunt(String nom, String titre);
    List<Emprunt> findAllEmprunt();
    List<Emprunt> empruntNonRendu();
    Emprunt empruntById(Long id);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                           CLIENT                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Client> ensClient();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                         FILM                                                               //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Film> ensFilmEmpruntable();
    List<Film> ensFilm();
    List<Film> ensFilmGenre(String genre);
    List<Tuple>infoRealisateurActeur(String titre);
    int nbrFilmDuGenre(String genre);
    List<Film> ensFilmEmpruntesByClient(Optional<Client> client);

}
