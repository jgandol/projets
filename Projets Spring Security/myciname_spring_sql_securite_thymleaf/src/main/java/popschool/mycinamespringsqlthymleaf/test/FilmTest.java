package popschool.mycinamespringsqlthymleaf.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.mycinamespringsqlthymleaf.MycinamespringsqlthymleafApplication;
import popschool.mycinamespringsqlthymleaf.dao.ClientRepository;
import popschool.mycinamespringsqlthymleaf.dao.FilmRepository;
import popschool.mycinamespringsqlthymleaf.model.Client;
import popschool.mycinamespringsqlthymleaf.model.Film;

import java.util.Optional;

@Component
public class FilmTest implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(MycinamespringsqlthymleafApplication.class);

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ClientRepository clientRepository;

    private void information(Film f){
        log.info(f.toString());
    }

    @Override
    public void run(String... args) throws Exception {
       /* //fetch all films
        log.info("TROUVER TOUS LES FILMS findAll() :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Film film : filmRepository.findAll()){
            log.info(film.toString());
        }
        log.info("");

        //liste de films d'un genre
        log.info("TROUVER la liste des films d'un genre donné -Drame- :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Film film : filmRepository.findByGenre_Nature("Drame")){
            log.info(film.toString());
        }
        log.info("");

        //liste de films d'un acteur en fonction de son nom
        log.info("TROUVER la liste des films d'un acteur donné en fonction de son nom -Depardieu- :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Film film : filmRepository.findByActeur_Nom("Depardieu")){
            log.info(film.toString());
        }
        log.info("");

        //liste de films d'un pays en fonction de son nom
        log.info("TROUVER la liste des films d'un pays donné en fonction de son nom -France- :");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Film film : filmRepository.findByPays_Nom("France")){
            log.info(film.toString());
        }
        log.info("");

        //nombre de film par genre traitement des tuple
        log.info("TROUVER le nombre de films par genre :");
        log.info("---------------------------------------------------------------------------------------------------");
        filmRepository.findNbreFilmsByGenre().forEach(f->{
            log.info("genre : " + f.get("nature", String.class)+
                    " : "+
                    f.get("nombre",Long.class));
        });
*/
        //List fill empruntable
        /*log.info("---------------------------------------------------------------------------------------------------");
        log.info("                                Liste de film empruntable :                                        ");
        log.info("---------------------------------------------------------------------------------------------------");
        for (Film film : filmRepository.findAllFilmEmpruntable()){
            log.info(film.toString());
        }
        log.info("");*/

        //Afficher le nombre de film d'un genre
        /*log.info("---------------------------------------------------------------------------------------------------");
        log.info("                              afficher nombre de film pour 1 genre                                 ");
        log.info("---------------------------------------------------------------------------------------------------");
        int nbre = filmRepository.findNbreFilmsByOneGenre("Guerre");
        System.out.println(nbre);
        log.info("");*/


        //Afficher le nombre de film d'un genre
        log.info("---------------------------------------------------------------------------------------------------");
        log.info("                       afficher la liste de film emprunte pour un client                           ");
        log.info("---------------------------------------------------------------------------------------------------");
        Optional<Client> client = clientRepository.findById(4L);

        for (Film film : filmRepository.findFilmEmprunteByClient(client)){
            log.info(film.toString());
        }
        log.info("");
//
//        //info realisateur et acteur - traitement des tuples
//        log.info("TROUVER les infos du realisateur et acteur d'un film demandé film :");
//        log.info("---------------------------------------------------------------------------------------------------");
//        filmRepository.findinfoRealisateurAndActeur("Le Parrain").forEach(f->{
//            log.info("realisateur : " + f.get("realisateur", String.class)+
//                    " acteur : "+ f.get("acteurnom",String.class));
//        });
    }
}
