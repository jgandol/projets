package popschool.mycinamespringsqlthymleaf.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.mycinamespringsqlthymleaf.model.AppUser;


import java.util.Optional;

public interface AppUserDao extends JpaRepository<AppUser,Long> {
    Optional<AppUser> findByName(String name);
}
