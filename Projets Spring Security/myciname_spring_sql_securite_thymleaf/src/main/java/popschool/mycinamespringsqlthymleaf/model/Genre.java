package popschool.mycinamespringsqlthymleaf.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="GENRE")
public class Genre {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long ngenre;

    private String nature;

    @OneToMany(mappedBy = "genre")
    private List<Film> films = new ArrayList<>();

    @Override
    public String toString() {
        return
                "{ nature='" + nature + '\'' +
                '}';
    }
}
