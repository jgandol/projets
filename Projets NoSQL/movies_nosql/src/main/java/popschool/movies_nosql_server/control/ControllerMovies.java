package popschool.movies_nosql_server.control;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.movies_nosql_server.dao.MovieDao;

import static org.springframework.data.domain.Sort.Direction.ASC;

@Controller
public class ControllerMovies {
    private final MovieDao movieDao;

    /*
    Utilisation du contrôleur MVC et de Thymeleaf et whyn’t Bootstrapp ?
    L’application permet d’interroger la base de données :
    • Par années
    • Par acteur
    A chaque fois afficher dans un tableau les films et le moyen de consulter le réalisateur, les acteurs du film
     */


    @Autowired
    public ControllerMovies(MovieDao movieDao ) {
        this.movieDao = movieDao;
    }

    @GetMapping("movies")
    public String index(Model model){
        model.addAttribute("movies", movieDao.findAll());
        return "movies";
    }

//    @GetMapping("/signupgangster")
//    public String showSignUpForm(Model model) {
//        return "add-gangster";
//    }
//
//    @PostMapping("/addgangster")
//    public String addGangster(@Valid Gangster gangster, BindingResult result, Model model) {
//        if (result.hasErrors()) {
//            return "add-gangster";
//        }
//
//        gangsterService.save(gangster);
//        model.addAttribute("gangsters", gangsterService.findAll());
//        return "redirect:/indexgangster";
//    }
//
//    @GetMapping("/editgangster")
//    public String showUpdateForm(@RequestParam("id") String id, Model model) {
//        Gangster gangster = gangsterService.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid gangster Id:" + id));
//        gangster.setId(URI.create(id));
//        model.addAttribute("gangster", gangster);
//        return "update-gangster";
//    }
//
//    @PostMapping("/updategangster")
//    public String updateGangster(
//            @Valid Gangster gangster, BindingResult result,
//            @RequestParam("orgname") String orgname, Model model) {
//        if (result.hasErrors()) {
//            return "update-gangster";
//        }
//
//        if(orgname!=null){
//            orgService.findByOrgname(orgname).ifPresent(gangster::setOrganisation);
//
//        }
//        gangsterService.update(gangster);
//        model.addAttribute("gangsters", gangsterService.findAll());
//        return "redirect:/index-gangster";
//    }
//
//    @GetMapping("/deletegangster")
//    public String deleteUser(@RequestParam("id") String id, Model model) {
//        /*        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));*/
//
//        gangsterService.delete(id);
//        model.addAttribute("gangsters", gangsterService.findAll());
//        return "index-gangster";
//    }
}
