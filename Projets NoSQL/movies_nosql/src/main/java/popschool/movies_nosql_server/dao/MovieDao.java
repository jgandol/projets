package popschool.movies_nosql_server.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import popschool.movies_nosql_server.helper.GenreAndTitle;
import popschool.movies_nosql_server.helper.MovieAggregate;
import popschool.movies_nosql_server.model.Director;
import popschool.movies_nosql_server.model.Movie;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Repository
public interface MovieDao extends MongoRepository<Movie, String> {

    List<Movie> findByDirectorLastnameOrderByYear(String lastname);


    List<GenreAndTitle> findByYear(int year);

    @Query("{'director.last_name':?0}")
    List<Movie> findByDirector_Lastname(String lastname);

    //    a. Retrouver la liste des genres
    @Aggregation("{ $group: { _id : $genre}}")
    List<String> ensGenres(Sort sort);


    //    b. Retrouver la liste des genres et les titres des films du genre
    @Aggregation("{ $group: { _id : $genre, titles : { $addToSet : $title }, count : {$sum :1} } }")
    List<MovieAggregate> groupByGenreAndTitles();

    //    c. Retrouver les films d’un genre triés par titre
    //    @Query("{'genre':?0}")
    List<Movie> findByGenreOrderByTitleAsc(String genre);

    //    d. Retrouver le nombre de films d’un genre
    Integer countMovieByGenre(String genre);

    //    e. Retrouver les films sortis entre 2 années triés par année et titre
    List<Movie> findByYearBetweenOrderByYearAscTitleAsc(int debut, int fin);

    //    f. Retrouver le nombre de films sortis entre 2 années
    Integer countMovieByYearBetweenOrderByYearAscTitleAsc(int debut, int fin);

    //    g. Retrouver le réalisateur d’un film défini par son id
    //    h. Retrouver les acteurs d’un film défini par son id
    @Query(value = "{'_id' : ?0}")
    Optional<Movie> findByIdFilm(String id);

    //    i. Retrouver la carrière d’un réalisateur
    List<Movie> findByDirector_Id(String id);

    //    j. Retrouver la carrière d’un acteur
    List<Movie> findByActors_Id(String id);

    List<Movie> findByIdNotNull();
}
