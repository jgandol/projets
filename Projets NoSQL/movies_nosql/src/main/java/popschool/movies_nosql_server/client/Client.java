package popschool.movies_nosql_server.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import popschool.movies_nosql_server.dao.MovieDao;
import popschool.movies_nosql_server.helper.GenreAndTitle;
import popschool.movies_nosql_server.model.Actor;
import popschool.movies_nosql_server.model.Director;
import popschool.movies_nosql_server.model.Movie;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.data.domain.Sort.Direction.ASC;

@Component
public class Client implements CommandLineRunner {
    @Autowired
    MovieDao movieDao;

    public Map<String, Director> setDirectors = new HashMap<String, Director>();
    public Map<String, Actor> setActors = new HashMap<String, Actor>();

//    @Autowired
//    public Client(MovieDao movieDao) {
//        this.movieDao = movieDao;
//    }

    @Override
    public void run(String... args) throws Exception {
        List<Movie> list = movieDao.findByDirectorLastnameOrderByYear("Eastwood");
        System.out.println(list);

        System.out.println("----------------------");

        List<Movie> list2 = movieDao.findByYearBetweenOrderByYearAscTitleAsc(1999, 2006);
        System.out.println(list2);

        System.out.println("----------------------");

        List<GenreAndTitle> list3 = movieDao.findByYear(2003);
        list3.forEach(e -> System.out.println(e.getGenre() + " : " + e.getTitle() + " dirige par "
                + e.getDirector().getFullname()));

        System.out.println("----------------------");
        movieDao.ensGenres(Sort.by(ASC, "_id")).forEach(g -> System.out.println(g));

        System.out.println("----------------------");
        movieDao.groupByGenreAndTitles().forEach(e -> System.out.println(e.getGenre() + ": " + e.getTitles() + " nb: " + e.getCount()));

        System.out.println("----------------------");
        movieDao.findByGenreOrderByTitleAsc("Action").forEach(e -> System.out.println(e.getTitle()));

        System.out.println("----------------------");
        Integer nombre = movieDao.countMovieByGenre("Action");

        System.out.println("----------------------");
        System.out.println("nb de films entre 2 année " + movieDao.countMovieByYearBetweenOrderByYearAscTitleAsc(1999, 2006));

        System.out.println("----------------------");
        System.out.println("Director : " + movieDao.findByIdFilm("movie:4").get().getDirector());

        System.out.println("----------------------");
        movieDao.findByIdFilm("movie:4").get().getActors().forEach(e -> System.out.println(e.getLastname()));

        System.out.println("----------------------");
        movieDao.findByDirector_Id("artist:4").forEach(e -> System.out.println(e));

        System.out.println("----------------------");
        movieDao.findByActors_Id("artist:24").forEach(e -> System.out.println(e));

        System.out.println("----------------------");


        movieDao.findAll().forEach(m-> System.out.println(m.getId()));

        System.out.println("----------------------");
        movieDao.findAll(Sort.by(ASC, "title")).forEach(m -> setDirectors.put(m.getDirector().getId(), m.getDirector()));
        System.out.println(setDirectors);
        movieDao.findAll().forEach(m -> {m.getActors().forEach(a -> setActors.put(a.getId(),a));});
        System.out.println(setActors);
//        List<Movie> movieList = movieDao.findByDirectorLastnameOrderByYear("Eastwood");
    }
}
