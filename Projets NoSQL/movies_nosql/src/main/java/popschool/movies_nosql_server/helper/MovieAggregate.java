package popschool.movies_nosql_server.helper;

import lombok.Data;
import lombok.Value;
import org.springframework.data.annotation.Id;

import java.util.List;

@Value
public class MovieAggregate {
    private @Id
    String genre;
    private List<String> titles;
    int count;

}
