package popschool.movies_nosql_server.helper;

import popschool.movies_nosql_server.model.Director;

public interface GenreAndTitle {
    String getGenre();
    String getTitle();
    DirectorInfo getDirector();

    interface DirectorInfo{
        String getFirstname();
        String getLastname();

        default String getFullname(){
            return getFirstname().concat(" ").concat(getLastname());
        }
    }
}
