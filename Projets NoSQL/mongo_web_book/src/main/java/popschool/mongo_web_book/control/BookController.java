package popschool.mongo_web_book.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import popschool.mongo_web_book.model.Book;
import popschool.mongo_web_book.service.BookService;

import java.util.List;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public List<Book> getAllBooks(){
        return bookService.getAllBooks();
    }

    @GetMapping("/books/{id}")
    public Book getBookById(@PathVariable("id")String id){
        return bookService.findByBookId(id).get();
    }

    @PostMapping("/books")
    public void createBook(@RequestBody Book book){
        bookService.createBook(book);
    }

    @DeleteMapping("/books/{id}")
    public void deleteBook(@PathVariable("id")String id){
        bookService.deleteBookById(id);
    }

    @PutMapping("/books")
    public void updateBook(@RequestBody Book book){
        bookService.updateBook(book);
    }

}
