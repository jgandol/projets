package popschool.rest_crimeportal_client.service;

import org.springframework.stereotype.Service;
import popschool.rest_crimeportal_client.model.Gangster;
import popschool.rest_crimeportal_client.model.Organisation;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import javax.transaction.Transactional;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GangsterService {
    ClientFactory clientFactory =
            Configuration.builder().setBaseUri("http://localhost:8080/").build()
                    .buildClientFactory();
    Client<Gangster> clientGangster = clientFactory.create(Gangster.class);

    public List<Gangster> findAll(){
        Iterable<Gangster> gangsters = clientGangster.getAll(URI.create(
                "http://localhost:8080/gangsters"));

        List<Gangster> list = new ArrayList<>();
        gangsters.forEach(list::add);
        return list;
    }

    public Optional<Gangster> findByGname(String gname){
        Gangster gangster = clientGangster.get(URI.create(
                "http://localhost:8080/gangsters/search/findByGname?gname="+gname));
        return Optional.ofNullable(gangster);
    }

    public Optional<Gangster> findById(String id){
        Gangster gangster = clientGangster.get(URI.create(id));
        return Optional.ofNullable(gangster);
    }

    public Gangster save (Gangster gangster){
        URI id = clientGangster.post(gangster);

        gangster.setId(id);
        return gangster;
    }

    public void update (Gangster gangster){
        clientGangster.patch(gangster.getId(), gangster);
    }

    public void delete (String id){
        Gangster gangster = findById(id).get();
        gangster.setOrganisation(null);
        clientGangster.delete(URI.create(id));
    }
}
