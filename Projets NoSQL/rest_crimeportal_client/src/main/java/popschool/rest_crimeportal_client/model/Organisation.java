package popschool.rest_crimeportal_client.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import uk.co.blackpepper.bowman.InlineAssociationDeserializer;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RemoteResource("/organisations")
public class Organisation {
    private URI id;
    private String orgname;
    private String description;

    private Gangster theboss;


    @ResourceId
    public URI getId() {
        return id;
    }

    public void setId(URI id) {
        this.id = id;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @LinkedResource
    @JsonDeserialize(contentUsing = InlineAssociationDeserializer.class)
    public Gangster getTheboss() {
        return theboss;
    }

    public void setTheboss(Gangster theboss) {
        this.theboss = theboss;
    }



    @Override
    public String toString() {
        return "Organisation{" +
                "id=" + getId() +
                ", orgname='" + getOrgname() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", theboss=" + getTheboss() +
                '}';
    }
}
