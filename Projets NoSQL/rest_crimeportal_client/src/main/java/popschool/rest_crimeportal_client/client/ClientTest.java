package popschool.rest_crimeportal_client.client;

import popschool.rest_crimeportal_client.model.Gangster;
import popschool.rest_crimeportal_client.model.Organisation;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientTest {
    public Iterable<Organisation> findAllOrganisation(){
        ClientFactory clientFactory =
                Configuration.build().buildClientFactory();

        Client<Organisation> clientOrg = clientFactory.create(Organisation.class);
        Iterable<Organisation> organisations = clientOrg.getAll(URI.create(
                "http://localhost:8080/organisations"));
        return organisations;
    }

    public Iterable<Gangster> findAllGangster(){
        ClientFactory clientFactory =
                Configuration.build().buildClientFactory();

        Client<Gangster> clientGangster = clientFactory.create(Gangster.class);
        Iterable<Gangster> gangsters = clientGangster.getAll(URI.create(
                "http://localhost:8080/gangsters"));

        return gangsters;
    }

    public Optional<Gangster> findGangsterByGname(String gname){
        ClientFactory clientFactory =
                Configuration.build().buildClientFactory();

        Client<Gangster> clientGangster = clientFactory.create(Gangster.class);
        Gangster gangster = clientGangster.get(URI.create(
                "http://localhost:8080/gangsters/search/findByGname?gname="+gname));

        return Optional.ofNullable(gangster);
    }

    public Optional<Organisation> findOrganisationByOrgname(String name){
        ClientFactory clientFactory =
                Configuration.build().buildClientFactory();

        Client<Organisation> client = clientFactory.create(Organisation.class);
        Organisation organisation = client.get(URI.create(
                "http://localhost:8080/organisations/search/findByOrgname?orgname="+name));

        return Optional.ofNullable(organisation);
    }
    public static void main(String[] args) {
        /*        ClientFactory clientFactory = Configuration.builder().setBaseUri("http://localhost:8080").build()
                .buildClientFactory();
        System.out.println(clientFactory);*/
        /*
        ClientFactory clientFactory =
                Configuration.build().buildClientFactory();

        Client<Gangster> clientGangster = clientFactory.create(Gangster.class);
        Iterable<Gangster> gangsters = clientGangster.getAll(URI.create(
                "http://localhost:8080/gangsters"));

        System.out.println(gangsters);
*/
/*
        ClientFactory clientFactory =
                Configuration.builder().setBaseUri("http://localhost:8080/").build()
                        .buildClientFactory();

        Client<Organisation> clientOrg = clientFactory.create(Organisation.class);
        Organisation org = new Organisation();
                     org.setOrgname("Mafia russe");
                     org.setTheboss(null);
        URI id = clientOrg.post(org);
      //  clientOrg.delete(URI.create("http://localhost:8080/organisations/4"));

        System.out.println( clientOrg.get(URI.create("http://localhost:8080/organisations/3")));
     //   System.out.println(id);  / http://localhost:8080/organisations/4
*/
        ClientTest clientTest = new ClientTest();

        Optional<Organisation>  organisation= clientTest.findOrganisationByOrgname("russe");
        System.out.println(organisation.orElse(null));



        System.out.println("-------------------------------------------------");
        Optional<Gangster> gangster = clientTest.findGangsterByGname("Toni");
        System.out.println(gangster.orElse(null));

        organisation.get().setTheboss(gangster.get());

       ClientFactory clientFactory =
                Configuration.builder().setBaseUri("http://localhost:8080/").build()
                        .buildClientFactory();

        Client<Organisation> clientOrg = clientFactory.create(Organisation.class);
        clientOrg.put(organisation.get());


        Optional<Organisation> autre =  clientTest.findOrganisationByOrgname("russe");
        System.out.println(autre.orElse(null));

    }
}
