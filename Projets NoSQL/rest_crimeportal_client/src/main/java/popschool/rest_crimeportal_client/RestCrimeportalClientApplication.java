package popschool.rest_crimeportal_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestCrimeportalClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestCrimeportalClientApplication.class, args);
    }

}
