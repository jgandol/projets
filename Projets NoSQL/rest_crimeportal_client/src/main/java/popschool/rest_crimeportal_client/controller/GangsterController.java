package popschool.rest_crimeportal_client.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import popschool.rest_crimeportal_client.model.Gangster;
import popschool.rest_crimeportal_client.model.Organisation;
import popschool.rest_crimeportal_client.service.GangsterService;
import popschool.rest_crimeportal_client.service.OrgService;

import java.net.URI;

@Controller
public class GangsterController {
    private final OrgService orgService;
    private final GangsterService gangsterService;


    @Autowired
    public GangsterController(OrgService orgService, GangsterService gangsterService ) {
        this.orgService = orgService;
        this.gangsterService=gangsterService;
    }

    @ModelAttribute(name = "gangster")
    public Gangster design() {
        return new Gangster();
    }

    @GetMapping({"indexgangster"})
    public String index(Model model){
        model.addAttribute("gangsters", gangsterService.findAll());
        return "index-gangster";
    }

    @GetMapping("/signupgangster")
    public String showSignUpForm(Model model) {
        return "add-gangster";
    }

    @PostMapping("/addgangster")
    public String addGangster(@Valid Gangster gangster, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-gangster";
        }

        gangsterService.save(gangster);
        model.addAttribute("gangsters", gangsterService.findAll());
        return "redirect:/indexgangster";
    }

    @GetMapping("/editgangster")
    public String showUpdateForm(@RequestParam("id") String id, Model model) {
        Gangster gangster = gangsterService.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid gangster Id:" + id));
        gangster.setId(URI.create(id));
        model.addAttribute("gangster", gangster);
        return "update-gangster";
    }

    @PostMapping("/updategangster")
    public String updateGangster(
            @Valid Gangster gangster, BindingResult result,
            @RequestParam("orgname") String orgname, Model model) {
        if (result.hasErrors()) {
            return "update-gangster";
        }

        if(orgname!=null){
            orgService.findByOrgname(orgname).ifPresent(gangster::setOrganisation);

        }
        gangsterService.update(gangster);
        model.addAttribute("gangsters", gangsterService.findAll());
        return "redirect:/index-gangster";
    }

    @GetMapping("/deletegangster")
    public String deleteUser(@RequestParam("id") String id, Model model) {
        /*        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));*/

        gangsterService.delete(id);
        model.addAttribute("gangsters", gangsterService.findAll());
        return "index-gangster";
    }
}