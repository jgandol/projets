package popschool.demo_reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Mono;
import reactor.core.publisher.ParallelFlux;

@SpringBootApplication
public class DemoReactiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoReactiveApplication.class, args);

        Mono.just("Craig").map(n -> n.toUpperCase())
                .map(cn-> "Hello, " + cn + " !").subscribe(System.out::println);
    }

}
