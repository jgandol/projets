package popschool.demo_reactive;

import lombok.Data;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

public class FluxTransformingTests {

    @Test
    public void skipAFew() {
        Flux<String> countFlux = Flux.just("one", "two", "skip a few", "ninety nine", "one hundred").skip(3);

        StepVerifier.create(countFlux).expectNext("ninety nine", "one hundred").verifyComplete();
    }

    @Test
    public void take() {
        Flux<String> nationalParkFlux = Flux.just("Yellowstone", "Yosemite", "Grand Canyon", "Zion", "Grand teton")
                .take(3);

        StepVerifier.create(nationalParkFlux).expectNext("Yellowstone", "Yosemite", "Grand Canyon").verifyComplete();
    }

    @Test
    public void takeForAWhile() {
        Flux<String> nationalParkFlux = Flux.just("Yellowstone", "Yosemite", "Grand Canyon", "Zion", "Grand teton")
                .delayElements(Duration.ofSeconds(1)).take(Duration.ofMillis(3500));

        StepVerifier.create(nationalParkFlux).expectNext("Yellowstone", "Yosemite", "Grand Canyon").verifyComplete();
    }

    @Test
    public void filter() {
        Flux<String> nationalParkFlux = Flux.just("Yellowstone", "Yosemite", "Grand Canyon", "Zion", "Grand teton")
                .filter(np -> !np.contains(" "));

        StepVerifier.create(nationalParkFlux).expectNext("Yellowstone", "Yosemite", "Zion").verifyComplete();
    }

    @Test
    public void distinct() {
        Flux<String> animalFlux = Flux.just("dog", "cat", "bird", "dog", "bird", "anteater")
                .distinct();

        StepVerifier.create(animalFlux).expectNext("dog", "cat", "bird", "anteater").verifyComplete();
    }



//    @Test
//    public void map(){
//        Flux<Player> playerFlux = Flux.just("Michael Jordan","Scottie Pippen","Steve Kerr")
//                .map(n -> {String[]});
//
//        StepVerifier.create(animalFlux).expectNext("dog","cat","bird", "anteater").verifyComplete();
//    }

    @Test
    public void flatmap() {
        Flux<Player> playerFlux = Flux.just("Michael Jordan", "Scottie Pippen", "Steve Kerr")
                .flatMap(n -> Mono.just(n)).map(p -> {
                            String[] split = p.split("\\s");
                            return new Player(split[0], split[1]);
                        }
                ).subscribeOn(Schedulers.parallel());
        List<Player> playerList = Arrays.asList(
                new Player("Michael","Jordan"),
                new Player("Scottie","Pippen"),
                new Player("Steve","Kerr"));

    StepVerifier.create(playerFlux).expectNextMatches(p ->playerList.contains(p))
            .expectNextMatches(p ->playerList.contains(p)).expectNextMatches(p ->playerList.contains(p))
            .verifyComplete();
    }

    @Data
    private static class Player {
        private final String firstName;
        private final String lastName;

        private Player(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }


}

