package popschool.demo_reactive;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class FluxBufferingTests {
    @Test
    public void buffer() {
        Flux<String> fruitFlux = Flux.just("apple", "orange", "banana", "kiwi", "strawberry");
        Flux<List<String>> buffuredFlux = fruitFlux.buffer(3);
        StepVerifier.create(buffuredFlux).expectNext(Arrays.asList("apple", "orange", "banana"))
                .expectNext(Arrays.asList("kiwi", "strawberry"))
                .verifyComplete();
    }

    @Test
    public void bufferAndFlatMap() {
        Flux.just("apple", "orange", "banana", "kiwi", "strawberry")
                .buffer(3).flatMap(x -> Flux.fromIterable(x).map(y -> y.toUpperCase())
                .subscribeOn(Schedulers.parallel()).log()).subscribe();
    }

    @Test
    public void collectList() {
        Flux<String> fruitFlux = Flux.just("apple", "orange", "banana", "kiwi", "strawberry");
        Mono<List<String>> fruitListMono = fruitFlux.collectList();

        StepVerifier.create(fruitListMono).expectNext(Arrays.asList("apple","orange","banana","kiwi","strawberry"))
                .verifyComplete();
    }

    @Test
    public void collectMap() {
        Flux<String> fruitFlux = Flux.just("apple", "orange", "banana", "kiwi", "strawberry");
        Mono<Map<Character, String>> fruitListMono = fruitFlux.collectMap(a->a.charAt(0));

//        StepVerifier.create(fruitListMono).expectNextMatches(
//                map -> { map.size() == 3 && map.get('a').equals("apple")
//                        && map.get('o').equals("orange") && map.get('b').equals("banana");}
//        ).verifyComplete();
    }

    @Test
    public void all() {
        Flux<String> animalFlux = Flux.just("aardvark", "elephant", "koala", "eagle", "kangaroo");
        Mono<Boolean> hasAMono = animalFlux.all(a->a.contains("a"));

        StepVerifier.create(hasAMono).expectNext(true).verifyComplete();
    }

    @Test
    public void any() {

        Flux<String> animalFlux = Flux.just(
                "aardvark","elephant","koala","eagle","kangourou");

        Mono<Boolean> hasKMono =
                animalFlux.any(a-> a.contains("k"));
    }

}
