package popschool.demo_reactive;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FluxCreationTests {

    @Test
    public void createAFlux_array(){
        List<String> listeFruits = new ArrayList<>();
        listeFruits.add("Apple");
        listeFruits.add("Orange");
        listeFruits.add("Grape");
        listeFruits.add("Banana");
        listeFruits.add("Strawberry");

        Flux<String> fruitFlux = Flux.fromIterable(listeFruits);

        StepVerifier.create(fruitFlux).expectNext("Apple").expectNext("Orange")
                .expectNext("Grape").expectNext("Banana").expectNext("Strawberry").verifyComplete();
    }

    @Test
    public void createAFlux_parallel(){
        Stream<String> listeFruits = Stream.of("Apple","Orange","Grape","Banana","Strawberry").parallel();
        Flux<String> fruitFlux = Flux.fromStream(listeFruits);
        StepVerifier.create(fruitFlux).expectNext("Apple").expectNext("Orange")
                .expectNext("Grape").expectNext("Banana").expectNext("Strawberry").verifyComplete();
    }

    @Test
    public void createAFlux_just(){
        Flux<String> fruitFlux = Flux.just("Apple","Orange","Grape","Banana","Strawberry");

        StepVerifier.create(fruitFlux).expectNext("Apple").expectNext("Orange")
                .expectNext("Grape").expectNext("Banana").expectNext("Strawberry").verifyComplete();
    }

    @Test
    public void createAFlux_interval(){
        Flux<Long> intervalFlux = Flux.interval(Duration.ofSeconds(1)).take(5);

        StepVerifier.create(intervalFlux).expectNext(0L).expectNext(1L).expectNext(2L).expectNext(3L).expectNext(4L).expectNext(5L).verifyComplete();
    }


    @Test
    public void createAFlux_range(){
        Flux<Integer> intervalFlux = Flux.range(1,5);

        StepVerifier.create(intervalFlux).expectNext(1).expectNext(2).expectNext(3).expectNext(4).expectNext(5).verifyComplete();
    }


}

