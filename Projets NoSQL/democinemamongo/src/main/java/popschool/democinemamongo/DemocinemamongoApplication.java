package popschool.democinemamongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocinemamongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemocinemamongoApplication.class, args);
    }

}
