package popschool.democinemamongo.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import popschool.democinemamongo.dto.GenreAndTitle;
import popschool.democinemamongo.dto.MovieAggregate;
import popschool.democinemamongo.model.Movie;

import java.util.List;
import java.util.Optional;

public interface MovieDAO extends MongoRepository<Movie, String> {

    List<Movie> findByDirectorLastnameLikeOrderByYear(String lastname);

    List<Movie> findByYearBetweenOrderByYearDesc(int max, int min);

    List<GenreAndTitle> findByYear(int year);

    @Aggregation("{ $group : { _id : $genre }}")
    List<String> findAllGenre(Sort sort);

    @Aggregation("{$group : {_id : $genre, movies : {$addToSet : $title}, count : {$sum:1} } }")
    List<MovieAggregate> moviesGenre();

    List<Movie> findByGenreOrderByTitleAsc(String genre);

    int countByGenre(String genre);

    Optional<Movie> findById(String id);

    List<Movie> findByActorsLastnameOrderByYear(int year);









}
