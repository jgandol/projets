package popschool.democinemamongo.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import popschool.democinemamongo.dao.MovieDAO;
import popschool.democinemamongo.dto.GenreAndTitle;
import popschool.democinemamongo.model.Movie;

import java.util.List;

import static org.springframework.data.domain.Sort.Direction.ASC;

@Component
public class Client implements CommandLineRunner {
    private final MovieDAO movieDAO;

    @Autowired
    public Client(MovieDAO movieDAO){
        this.movieDAO=movieDAO;
    }


    @Override
    public void run(String... args) throws Exception {

        List<Movie> list = movieDAO.findByDirectorLastnameLikeOrderByYear("Eastwood");
        System.out.println(list);

        List<Movie> list2 = movieDAO.findByYearBetweenOrderByYearDesc(1995,2000);
        System.out.println(list2);

        List<GenreAndTitle> list3=movieDAO.findByYear(1996);
        list3.forEach(g -> System.out.println(g.getGenre()+ " : "+g.getTitle()+" dirige par "+g.getDirector().getFullName()));


        Sort sort = Sort.by(ASC,"_id");
        System.out.println(movieDAO.findAllGenre(sort));

        System.out.println(movieDAO.moviesGenre());
        System.out.println(movieDAO.findByGenreOrderByTitleAsc("Action"));
        System.out.println(movieDAO.countByGenre("Action"));
    }
}
