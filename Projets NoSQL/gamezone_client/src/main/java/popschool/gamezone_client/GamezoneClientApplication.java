package popschool.gamezone_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamezoneClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(GamezoneClientApplication.class, args);
    }

}
