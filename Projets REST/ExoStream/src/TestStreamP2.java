import java.util.Arrays;
import java.util.List;

public class TestStreamP2 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("hi", "hello", "hola", "bye", "goodbye", "adios");
//1.    Produce a single String that is the result of concatenating the uppercase versions of all of the
//    Strings. E.g., the result should be "HIHELLO...". Use a single reduce operation, without using map.

        String upperCaseWord = words.stream().parallel().reduce("",(s1,s2) -> s1 + s2.toUpperCase());
        System.out.printf("Single uppercase String: %s %n",upperCaseWord);

//2. Produce the same String as above, but this time via a map operation that turns the words into upper
//case, followed by a reduce operation that concatenates them.

        String upperCaseWordAlt= words.stream().parallel().reduce("",String::concat).toUpperCase();
        System.out.printf("Single uppercase String %s.%n",upperCaseWordAlt);

//3. Produce a String that is all the words concatenated together, but with commas in between. E.g., the
//    result should be "hi,hello,...". Note that there is no comma at the beginning, before “hi”, and also no
//    comma at the end, after the last word. Major hint: there are two versions of reduce discussed in the
//    notes.

        String upperCaseWordComma= words.stream().parallel().reduce((s1, s2) -> s1 + "," + s2 ).orElse("need at least 2 strings");
        System.out.printf("Single uppercase String %s.%n",upperCaseWordComma);



//4. Find the total number of characters (i.e., sum of the lengths) of the strings in the List.
        int numbersOfChar = words.stream().parallel().mapToInt(String::length).sum();
        System.out.printf("total numbers of char: %s.%n",numbersOfChar);

//            5. Find the number of words that contain an “h”.
//    Customized Java EE training at your location: JSF 2, PrimeFaces, general Java programming, Java 8 lambdas/stre

        long numbersOfCharWithH = words.stream().parallel().filter(s -> s.contains("h")).count();
        System.out.printf("total numbers of char with H : %s.%n",numbersOfCharWithH);
    }
}

