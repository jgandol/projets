import com.sun.xml.internal.ws.addressing.WsaActionUtil;
import com.sun.xml.internal.ws.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestStream {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("hi", "hello", "hola", "bye", "goodbye", "adios");

        //Ex 1
        System.out.println("Word (with spaces):");
        words.stream().parallel().forEach(s -> System.out.println("  " + s));

        //Ex 2
        System.out.println("Word (no spaces):");
        words.stream().parallel().forEach(System.out::println);

        //Ex 3
        List<String> excitingWords = words.stream().parallel()
                .map( s -> s + "!")
                .collect(Collectors.toList());
        System.out.printf("Exciting words: %s %n", excitingWords);

        List<String> eyeWords = words.stream().parallel()
                .map( s -> s.replace("i", "eye"))
                .collect(Collectors.toList());
        System.out.printf("Eye words: %s %n", eyeWords);

        List<String> upperCaseWords = words.stream().parallel()
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.printf("Eye words: %s %n", upperCaseWords);

        //Ex 4
        List<String> shortWords = words.stream().parallel().filter(s -> s.length()<4).collect(Collectors.toList());
        System.out.println("Mots <4 : " + shortWords);

        List<String> wordsWithB = words.stream().parallel().filter( s -> s.contains("b")).collect(Collectors.toList());
        System.out.println("mots avec B :" + wordsWithB);

        List<String> evenLengthWords = words.stream().parallel().filter(s -> (s.length() % 2) == 0).collect(Collectors.toList());
        System.out.println("mots de taille pair: " + evenLengthWords);

        //Ex 5
        List<String> listeDeLaMort = words.stream().parallel().map(String::toUpperCase).filter(s -> s.length() <4).filter(s -> s.contains("E"))
                .collect(Collectors.toList());
        List<String> listeDeLaMort2 = words.stream().parallel().map(String::toUpperCase).filter(s -> s.length() <4).filter(s -> s.contains("Q"))
                .collect(Collectors.toList());
        System.out.println("Q5 avec E: " + listeDeLaMort);
        System.out.println("Q5 avec Q: " + listeDeLaMort2);

        //Ex 6
        Function<String, String> toUpper = s->{ System.out.println("Uppercasing " + s); return (s.toUpperCase()); };
        String listeDeLaMortOpti = words.stream().parallel().map(toUpper).filter(s -> s.length() <4).filter(s -> s.contains("E")).findFirst()
                .orElse("No match");
        System.out.println("Q6 avec E: " + listeDeLaMortOpti);

        //Ex 7
        String[] array = words.stream().parallel().filter( s -> s.contains("b")).toArray(String[]::new);
        System.out.println("mots avec B :" + array.toString());
        String result = firstFunnyString(words,"Q");
        System.out.println("Uppercase short with 'Q' :" + result);

    }

    public static String firstFunnyString(List<String> words, String containedTest){
        String result = words.stream().map(String::toUpperCase).filter(s -> s.length() < 4).
                filter(s -> s.contains(containedTest)).findFirst().orElse("no match");
        return result;
    }
}