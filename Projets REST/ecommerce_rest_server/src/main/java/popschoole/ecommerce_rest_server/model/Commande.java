package popschoole.ecommerce_rest_server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "COMMANDE")
public class Commande {
    private Integer ncommande;
    private Date datecde;
    private Client client;


    private List<Detailcde> detailcdes;

    public Commande() {
        this.datecde = new Date(System.currentTimeMillis());
    }

    public Commande(int i) {
        this.ncommande = i;
        this.datecde = new Date(System.currentTimeMillis());
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NCOMMANDE")
    public Integer getNcommande() {
        return ncommande;
    }
    public void setNcommande(Integer ncommande) {
        this.ncommande = ncommande;
    }

    @Basic
    @Column(name = "DATECDE")
    public Date getDatecde() {
        return datecde;
    }
    public void setDatecde(Date datecde) {
        this.datecde = datecde;
    }


    @ManyToOne
    @JoinColumn(name = "NOMCLIENT", foreignKey = @ForeignKey(name = "FK_CDE_NCLIENT"))
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }

    @OneToMany(mappedBy = "commande",cascade=CascadeType.PERSIST)
    @JsonIgnore
    public List<Detailcde> getDetailcdes() {
        return detailcdes;
    }
    public void setDetailcdes(List<Detailcde> detailcdes) {
        this.detailcdes = detailcdes;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "ncommande=" + ncommande +
                ", datecde=" + datecde +
                ", client=" + client.getNom() +
                '}';
    }
}
