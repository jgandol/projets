package popschoole.ecommerce_rest_server.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "DETAILCDE")
public class Detailcde {
    private Integer ndetail;
    private int quantite;
    private Commande commande;
    private Produit produit;

    public Detailcde() {
    }

    public Detailcde(int quantite, Commande commande, Produit produit) {
        this.quantite = quantite;
        this.commande = commande;
        this.produit = produit;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NDETAIL")
    public Integer getNdetail() {
        return ndetail;
    }
    public void setNdetail(Integer ndetail) {
        this.ndetail = ndetail;
    }

    @Basic
    @Column(name = "QUANTITE")
    public int getQuantite() {
        return quantite;
    }
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name = "NCOMMANDE", foreignKey = @ForeignKey(name = "FK_DETAIL_NCOMMANDE"))
    public Commande getCommande() {
        return commande;
    }
    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    @ManyToOne
    @JoinColumn(name = "NPRODUIT", foreignKey = @ForeignKey(name = "FK_DETAIL_NPRODUIT"))
    public Produit getProduit() {
        return produit;
    }
    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    @Override
    public String toString() {
        return "Detailcde{" +
                "ndetail=" + ndetail +
                ", quantite=" + quantite +
                ", produit=" + produit.getDescriptif() +
                '}';
    }
}
