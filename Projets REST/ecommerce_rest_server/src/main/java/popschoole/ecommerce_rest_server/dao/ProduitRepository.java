package popschoole.ecommerce_rest_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import popschoole.ecommerce_rest_server.model.Produit;

import java.util.List;
import java.util.Optional;

public interface ProduitRepository extends JpaRepository<Produit,Integer> {
    List<Produit> findAll();
    Optional<Produit> findByNproduit(int id);
}
