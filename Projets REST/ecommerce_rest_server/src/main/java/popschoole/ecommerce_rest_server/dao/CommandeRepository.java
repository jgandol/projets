package popschoole.ecommerce_rest_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import popschoole.ecommerce_rest_server.model.Commande;

import java.util.List;
import java.util.Optional;

public interface CommandeRepository extends JpaRepository<Commande,Integer> {
    List<Commande> findAll();
    Optional<Commande> findByNcommande(int id);
}

