package popschoole.ecommerce_rest_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import popschoole.ecommerce_rest_server.model.Client;


import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client,String> {
    List<Client> findAll();
    Optional<Client> findByNom(String nom);
}
