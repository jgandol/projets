package popschoole.ecommerce_rest_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import popschoole.ecommerce_rest_server.model.Detailcde;

import java.util.List;
import java.util.Optional;

public interface DetailcdeRepository extends JpaRepository<Detailcde,Integer> {
    List<Detailcde> findAll();
    Optional<Detailcde> findByNdetail(int id);
}

