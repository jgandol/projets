package popschoole.ecommerce_rest_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschoole.ecommerce_rest_server.dao.ClientRepository;
import popschoole.ecommerce_rest_server.dao.CommandeRepository;
import popschoole.ecommerce_rest_server.dao.DetailcdeRepository;
import popschoole.ecommerce_rest_server.dao.ProduitRepository;
import popschoole.ecommerce_rest_server.exceptions.ExceptionNotFound;
import popschoole.ecommerce_rest_server.model.Client;
import popschoole.ecommerce_rest_server.model.Commande;
import popschoole.ecommerce_rest_server.model.Detailcde;
import popschoole.ecommerce_rest_server.model.Produit;


import java.util.List;
import java.util.Optional;

@Service
public class ServiceECommerceImpl implements ServiceECommerce{
    @Autowired
    ProduitRepository produitRepository;
    @Autowired
    CommandeRepository commandeRepository;
    @Autowired
    DetailcdeRepository detailcdeRepository;
    @Autowired
    ClientRepository clientRepository;

    @Override
    public List<Client> findAllClient() {
        return clientRepository.findAll();
    }

    @Override
    public Client findClientByNom(String nom) {
        if(nom.isEmpty()) throw new ExceptionNotFound();
        Client c = clientRepository.findByNom(nom).orElseThrow(ExceptionNotFound::new);
        return c;
    }

    @Override
    public List<Commande> findAllCommande() {
        return commandeRepository.findAll();
    }
    @Override
    public List<Detailcde> findAllDetailCde() {
        return detailcdeRepository.findAll();
    }
    @Override
    public List<Produit> findAllProduit() {
        return produitRepository.findAll();
    }

    @Override
    public Produit findProduitById(int id) throws ExceptionNotFound{
        if(id<=0) throw new ExceptionNotFound();
        Produit p = produitRepository.findByNproduit(id).orElseThrow(ExceptionNotFound::new);
        return p;
    }

    @Override
    public Boolean UserExist(String nom, String mdp) {
        Optional<Client> c = clientRepository.findByNom(nom);
        if(c.isPresent() && c.get().getMotdepasse().equals(mdp))
            return true;
        else return false;
    }

    @Override
    public Commande validerCommande(Commande c) {

        Commande commande = commandeRepository.save(c);
        return commande;
    }


}
