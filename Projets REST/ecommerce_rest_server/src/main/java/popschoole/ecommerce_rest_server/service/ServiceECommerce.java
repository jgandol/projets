package popschoole.ecommerce_rest_server.service;



import popschoole.ecommerce_rest_server.model.Client;
import popschoole.ecommerce_rest_server.model.Commande;
import popschoole.ecommerce_rest_server.model.Detailcde;
import popschoole.ecommerce_rest_server.model.Produit;

import java.util.List;
import java.util.Optional;

public interface ServiceECommerce {
    List<Client> findAllClient();
    Client findClientByNom(String nom);
    List<Commande> findAllCommande();
    List<Detailcde> findAllDetailCde();
    List<Produit> findAllProduit();
    Produit findProduitById(int id);
    Boolean UserExist(String nom, String mdp);
    Commande validerCommande(Commande c);
}
