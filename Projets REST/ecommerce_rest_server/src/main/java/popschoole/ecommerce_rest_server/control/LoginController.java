package popschoole.ecommerce_rest_server.control;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import popschoole.ecommerce_rest_server.exceptions.ExceptionNotFound;
import popschoole.ecommerce_rest_server.model.Client;
import popschoole.ecommerce_rest_server.model.Commande;
import popschoole.ecommerce_rest_server.model.Detailcde;
import popschoole.ecommerce_rest_server.model.Produit;
import popschoole.ecommerce_rest_server.service.ServiceECommerce;

import java.util.List;

@RestController
public class LoginController {
    @Autowired
    ServiceECommerce serviceECommerce;
//    void validerCommande(Commande c);
//    Client findClientByNom(String login);

    @GetMapping(value = "/clients")
    public ResponseEntity<List<Client>> getEnsClients() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(serviceECommerce.findAllClient());
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "clients/{nomClient}")
    public ResponseEntity<Client> getClientById(@PathVariable("nomClient")String nomClient){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(serviceECommerce.findClientByNom(nomClient));
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/produits")
    public ResponseEntity<List<Produit>> getEnsProduits() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(serviceECommerce.findAllProduit());
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/produits/{idProduit}")
    public ResponseEntity<Produit> getEnsProduits(@PathVariable("idProduit")Integer idProduit) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(serviceECommerce.findProduitById(idProduit));
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/commandes")
    public ResponseEntity<List<Commande>> getEnsCommandes() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(serviceECommerce.findAllCommande());
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/commandes/details")
    public ResponseEntity<List<Detailcde>> getEnsDetailsCommande(){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(serviceECommerce.findAllDetailCde());
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/clients/existe")
    public ResponseEntity<Boolean> isClientExist(@RequestBody Client client){
        try {
             Boolean b = serviceECommerce.UserExist(client.getNom(),client.getMotdepasse());
//            System.out.println(b);
            return ResponseEntity.status(HttpStatus.OK).body(b);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/commandes/validate")
    public ResponseEntity<?> doValidateCommand(@RequestBody Commande commande){
        try {
            Commande c = serviceECommerce.validerCommande(commande);
            System.out.println(c);
            return ResponseEntity.status(HttpStatus.OK).body(commande);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
