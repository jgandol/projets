package popschoole.ecommerce_rest_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceRestServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcommerceRestServerApplication.class, args);
    }

}
