package popschool.rest_2020_client.controler;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import popschool.rest_2020_client.model.Employee;
import popschool.rest_2020_client.service.EmployeeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class MainController {
    private static List<Employee> employees = new ArrayList<Employee>();
    static final String URL_EMPLOYEES = "http://localhost:8080/employees";

    @Autowired
    EmployeeService empServ;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }

    @RequestMapping(value = {"/employeeList"}, method = RequestMethod.GET)
    public String employeeList(Model model) {
        model.addAttribute("employees", empServ.getEmployees());
        return "employeeList";
    }

    @GetMapping(value = {"/addEmployee"})
    public String showAddEmployeePage(Model model) {
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "addEmployee";
    }

    @PostMapping(value = {"/addEmployee"})
    public String saveEmployee(Model model, //
                               @ModelAttribute("employee") Employee employee) {

        Employee newEmployee = employee;
        empServ.createEmployee(employee);

        model.addAttribute("employees", empServ.getEmployees());
        return "employeeList";
    }

}
