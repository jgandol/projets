package popschool.rest_2020_client.test;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import popschool.rest_2020_client.model.Employee;

public class PutSimpleExample {
    static final String URL_UPDATE_EMPLOYEE = "http://localhost:8080/employees";
    static final String URL_EMPLOYEE_PREFIX = "http://localhost:8080/employees";

    public static void main(String[] args) {
        String empNo = "E04";
        Employee updateInfo = new Employee("E04", "Tom", "Cleck");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);

        RestTemplate restTemplate = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Employee> requestBody = new HttpEntity<>(updateInfo, headers);


        // Send request with PUT method.
        restTemplate.put(URL_UPDATE_EMPLOYEE + "/" + empNo, requestBody,
                new Object[]{});

        String resourceUrl = URL_EMPLOYEE_PREFIX + "/" + empNo;

        Employee e = restTemplate.getForObject(resourceUrl, Employee.class);

        if (e != null) {
            System.out.println("(Client side) Employee after update: ");
            System.out.println("Employee: " + e.getEmpno() + " - " + e.getEmpname());
        }
    }
}

