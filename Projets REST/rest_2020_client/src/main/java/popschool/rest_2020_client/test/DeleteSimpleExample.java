package popschool.rest_2020_client.test;

import org.springframework.web.client.RestTemplate;
import popschool.rest_2020_client.model.Employee;

public class DeleteSimpleExample {
    public static void main(String[] args) {
        String id = "E01";
        String resourceUrl = "http://localhost:8080/employees" + "/" + id;

        // Send request with DELETE method.
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(resourceUrl);

        // Get
        Employee e = restTemplate.getForObject(resourceUrl, Employee.class);

        if (e != null) {
            System.out.println("(Client side) Employee after delete: ");
            System.out.println("Employee: " + e.getEmpno() + " - " + e.getEmpname());
        } else {
            System.out.println("Employee not found!");
        }
    }
}
