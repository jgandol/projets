package popschool.rest_2020_client.test;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import popschool.rest_2020_client.model.Employee;

public class Post_postForObject_Example {
    static final String URL_CREATE_EMPLOYEE = "http://localhost:8080/employees";

    public static void main(String[] args) {
        Employee newEmployee = new Employee("E04", "Tom", "Cleck");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Employee> requestBody = new HttpEntity<>(newEmployee, headers);

        // Send request with POST method.
        Employee e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, Employee.class);

        if (e != null && e.getEmpno() != null) {
            System.out.println("Employee created: " + e.getEmpno());
        } else {
            System.out.println("Something error!");
        }
    }
}