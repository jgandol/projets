package popschool.rest_2020_client.test;

import org.springframework.web.client.RestTemplate;
import popschool.rest_2020_client.model.Employee;

public class SimplestGetPOJOExample {
    static final String URL_EMPLOYEES = "http://localhost:8080/employees";

    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method and default Headers.
        Employee[] list = restTemplate.getForObject(URL_EMPLOYEES, Employee[].class);

        if (list != null) {
            for (Employee e : list) {
                System.out.println("Employee: " + e.getEmpno() + " - " + e.getEmpname()
                        + " - " + e.getPosition());
            }
        }
    }
}