package popschool.rest_2020_client.model;


import lombok.Data;

@Data
public class Employee {

    private String empno;
    private String empname;
    private String position;

    public Employee() {
    }

    public Employee(String empno, String empname, String position) {
        this.empno = empno;
        this.empname = empname;
        this.position = position;
    }

    public Employee(String name, String role) {
        this.empname = name;
        this.position = role;
    }
}