package popschool.rest_2020_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Rest2020ClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(Rest2020ClientApplication.class, args);
    }

}
