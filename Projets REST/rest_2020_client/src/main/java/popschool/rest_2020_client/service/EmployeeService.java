package popschool.rest_2020_client.service;

import popschool.rest_2020_client.model.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getEmployees();

    void createEmployee(Employee emp);

    void deleteEmployee(String id);
}
