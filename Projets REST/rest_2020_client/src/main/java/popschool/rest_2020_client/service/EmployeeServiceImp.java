package popschool.rest_2020_client.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import popschool.rest_2020_client.model.Employee;

import java.util.Arrays;
import java.util.List;

@Service
public class EmployeeServiceImp implements EmployeeService {
    static final String URL_EMPLOYEES = "http://localhost:8080/employees";
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<Employee> getEmployees() {
        RestTemplate restTemplate = new RestTemplate();

        // Send request with GET method and default Headers.
        Employee[] list = restTemplate.getForObject(URL_EMPLOYEES, Employee[].class);

        return Arrays.asList(list);
    }

    @Override
    public void createEmployee(Employee emp) {
        System.out.println(emp);
        Employee newEmployee = emp;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Data attached to the request.
        HttpEntity<Employee> requestBody = new HttpEntity<>(newEmployee, headers);

        // Send request with POST method.
        Employee e = restTemplate.postForObject(URL_EMPLOYEES, requestBody, Employee.class);

        if (e != null && e.getEmpno() != null) {
            System.out.println("Employee created: " + e.getEmpno());
            // Send request with GET method and default Headers.
        } else {
            System.out.println("Something error!");
        }
    }

    @Override
    public void deleteEmployee(String id) {
        String resourceUrl = URL_EMPLOYEES + "/" + id;

        // Send request with DELETE method.
        restTemplate.delete(resourceUrl);

        // Get
        Employee e = restTemplate.getForObject(resourceUrl, Employee.class);

        if (e != null) {
            System.out.println("(Client side) Employee after delete: ");
            System.out.println("Employee: " + e.getEmpno() + " - " + e.getEmpname());
        } else {
            System.out.println("Employee not found!");
        }
    }
}
