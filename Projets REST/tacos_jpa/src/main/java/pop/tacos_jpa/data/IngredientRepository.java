package pop.tacos_jpa.data;

import org.springframework.data.repository.CrudRepository;

import pop.tacos_jpa.Ingredient;

public interface IngredientRepository 
         extends CrudRepository<Ingredient, String> {

}
