package pop.tacos_jpa.data;

import org.springframework.data.repository.CrudRepository;

import pop.tacos_jpa.Taco;

public interface TacoRepository 
         extends CrudRepository<Taco, Long> {

}
