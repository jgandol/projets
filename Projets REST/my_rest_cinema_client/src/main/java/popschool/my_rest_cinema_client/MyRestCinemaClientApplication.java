package popschool.my_rest_cinema_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyRestCinemaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyRestCinemaClientApplication.class, args);
    }

}
