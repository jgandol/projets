package popschool.my_rest_cinema_client.modele;

import lombok.Data;


import java.sql.Date;
import java.util.List;

@Data
public class Film {
    private Long nfilm;
    private String titre;
    private Date sortie;
    private String realisateur;
    private Integer entrees;
    private String oscar;
    private List<Emprunt> emprunts;
    private Genre genre;
    private Nationalite nationalite;
    private Acteur acteur;

    public Acteur getActeur() {
        System.out.println(acteur);
        return acteur;
    }

    public Film() {
    }

    @Override
    public String toString() {
        return "Film{" +
                "nfilm=" + nfilm +
                ", titre='" + titre + '\'' +
                ", sortie=" + sortie +
                ", realisateur='" + realisateur + '\'' +
                ", entrees=" + entrees +
                ", oscar='" + oscar + '\'' +
                ", genre=" + genre +
                ", nationalite=" + nationalite +
                ", acteur=" + acteur +
                '}';
    }
}
