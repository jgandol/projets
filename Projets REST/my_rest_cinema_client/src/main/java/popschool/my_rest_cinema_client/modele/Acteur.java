package popschool.my_rest_cinema_client.modele;

import lombok.Data;

import java.sql.Date;
import java.util.List;

@Data
public class Acteur {
    private Long nacteur;
    private String nom;
    private String prenom;
    private Date naissance;
    private Integer nbrefilms;
    private Nationalite nationalite;
    private List<Film> films;

    public Acteur() {
    }


    @Override
    public String toString() {
        return "Acteur{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", nbrefilms=" + nbrefilms +
                ", nationalite=" + nationalite +
                '}';
    }
}
