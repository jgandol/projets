package popschool.my_rest_cinema_client.modele;

import lombok.Data;

import java.sql.Date;
import java.util.Objects;

@Data

public class Emprunt {
    private Long nemprunt;
    private String retour;
    private Date dateemprunt;
    private Client client;
    private Film film;

    public Emprunt() {
    }

    public Emprunt(Client client, Film film,String retour, Date dateemprunt) {
        this.retour = retour;
        this.dateemprunt =dateemprunt;
        this.client = client;
        this.film = film;
    }

    public Emprunt(Client client, Film film) {
        this.retour = "NON";
        this.dateemprunt = new Date(System.currentTimeMillis());
        this.client = client;
        this.film = film;
    }


    @Override
    public String toString() {
        return "Emprunt{" +
                "nemprunt=" + nemprunt +
                ", retour='" + retour + '\'' +
                ", dateemprunt=" + dateemprunt +
//                ", client=" + client.getNom() +
//                ", film=" + film.getTitre() +
                '}';
    }
}
