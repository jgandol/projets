package popschool.my_rest_cinema_client.modele;

import lombok.Data;


import java.util.List;
import java.util.Objects;

@Data
public class Client {
    private Long nclient;
    private String nom;
    private String prenom;
    private String adresse;
    private Integer anciennete;
    private List<Emprunt> emprunts;

    public Client() {
    }

    public Client(String nom, String prenom, String adresse, Integer anciennete) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.anciennete = anciennete;
    }


    @Override
    public String toString() {
        return "Client{" +
                "nclient=" + nclient +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", anciennete=" + anciennete +
                '}';
    }
}
