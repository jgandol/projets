package popschool.my_rest_cinema_client.modele;

import lombok.Data;


import java.util.List;
import java.util.Objects;

@Data
public class Genre {
    private Long ngenre;
    private String nature;
    private List<Film> films;

    public Genre() {
    }

    @Override
    public String toString() {
        return "Genre{" +
                "ngenre=" + ngenre +
                ", nature='" + nature + '\'' +
                '}';
    }
}
