package popschool.my_rest_cinema_client.modele;

import lombok.Data;


import java.util.List;
import java.util.Objects;

@Data
public class Nationalite {
    private Long npays;
    private String nom;
    private List<Acteur> acteurs;
    private List<Film> films;

    public Nationalite() {
    }


    @Override
    public String toString() {
        return "Nationalite{" +
                "npays=" + npays +
                ", nom='" + nom + '\'' +
                '}';
    }
}
