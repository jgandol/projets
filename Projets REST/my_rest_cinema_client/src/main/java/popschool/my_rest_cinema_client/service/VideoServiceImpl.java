package popschool.my_rest_cinema_client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import popschool.my_rest_cinema_client.modele.*;

import java.sql.Date;
import java.util.Arrays;
import java.util.Optional;
import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {
    static final String URL_EMPRUNTS = "http://localhost:8080/emprunts";
    static final String URL_FILMS = "http://localhost:8080/films";
    static final String URL_CLIENTS = "http://localhost:8080/clients";
    static final String URL_ACTEURS = "http://localhost:8080/acteurs";
    static final String URL_GENRES = "http://localhost:8080/genres";
    private RestTemplate restTemplate = new RestTemplate();
    private HttpHeaders headers = new HttpHeaders();

    public void emprunter(Long idClient, Long idFilm){
        Client c = restTemplate.getForObject(URL_CLIENTS + "/" + idClient, Client.class);
        Film f = restTemplate.getForObject(URL_FILMS + "/" + idClient, Film.class);
        Emprunt e = new Emprunt(c,f);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Data attached to the request.
        HttpEntity<Emprunt> requestBody = new HttpEntity<>(e, headers);
        // Send request with POST method.
        // BUG
        restTemplate.postForObject(URL_EMPRUNTS, requestBody, Emprunt.class);


    }

    public List<Film> ensFilm(){
        return Arrays.asList(restTemplate.getForObject(URL_FILMS, Film[].class));
    }
    public List<Acteur> ensActeur(){
        return Arrays.asList(restTemplate.getForObject(URL_ACTEURS, Acteur[].class));
    }

    public List<Film> findFilmsByActeur(String nomActeur){
        return Arrays.asList(restTemplate.getForObject(URL_FILMS + "by/acteur/" + nomActeur, Film[].class));
    }

    @Override
    public List<Genre> ensGenres() {
        return Arrays.asList(restTemplate.getForObject(URL_GENRES, Genre[].class));
    }

    @Override
    public List<Film> ensFilmDuGenre(String nature) {
    return Arrays.asList(restTemplate.getForObject(URL_FILMS + "/by/genre/" + nature,  Film[].class ));
    }

    @Override
    public List<Film> findFilmsByActeur(long id) {
        return Arrays.asList(restTemplate.getForObject(URL_FILMS + "/by/acteur/" + id,  Film[].class ));
    }

    @Override
    public List<Client> ensClients() {
        return Arrays.asList(restTemplate.getForObject(URL_CLIENTS, Client[].class));
    }

    @Override
    public List<Film> ensFilmsEmpruntables() {
        return Arrays.asList(restTemplate.getForObject(URL_EMPRUNTS + "/dispo", Film[].class));
    }

    @Override
    public void retourEmprunt(String nomClient, String titreFilm) {

    }

    @Override
    public List<Emprunt> ensEmprunts() {
        return Arrays.asList(restTemplate.getForObject(URL_EMPRUNTS, Emprunt[].class));
    }

    @Override
    public Emprunt findEmpruntByNemprunt(Long id) {
        return restTemplate.getForObject(URL_EMPRUNTS+"/"+id, Emprunt.class);
    }


}
