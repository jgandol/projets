package popschool.my_rest_cinema_client.service;

import popschool.my_rest_cinema_client.modele.*;


import java.util.List;

public interface VideoService {

    void emprunter(Long idClient, Long idFilm);

//    public List<Client> ensClient();

    List<Film> ensFilm();

//    @Query("select e.film from Emprunt e where e.retour = \"NON\" order by e.film.titre")
//    List<Client> findAllByOrderByNom();

    List<Genre> ensGenres();
    List<Acteur> ensActeur();
    List<Film> ensFilmDuGenre(String nature);
    List<Film> findFilmsByActeur(long id);
//    List<Tuple>infoRealisateurActeur(String titre);
//    int nombreFilmDuGenre(String nature);
    List<Client> ensClients();
    List<Film> ensFilmsEmpruntables();
//    List<Film> ensFilmsEmpruntes();
//    int emprunter2(String nomClient, String titreFilm);
    void retourEmprunt(String nomClient, String titreFilm);

    List<Emprunt> ensEmprunts();

    Emprunt findEmpruntByNemprunt(Long id);
}
