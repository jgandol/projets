package popschool.my_rest_cinema_server.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.my_rest_cinema_server.model.Acteur;
import popschool.my_rest_cinema_server.model.Nationalite;


import java.util.List;

public interface NationaliteRepositry extends CrudRepository<Nationalite, Long> {
    List<Nationalite> findAll();

}
