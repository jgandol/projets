package popschool.my_rest_cinema_server.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.my_rest_cinema_server.model.Genre;


import java.util.List;

public interface GenreRepository extends CrudRepository<Genre, Long> {

    List<Genre> findAll();

}
