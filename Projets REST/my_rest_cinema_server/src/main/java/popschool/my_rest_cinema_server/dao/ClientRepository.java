package popschool.my_rest_cinema_server.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.my_rest_cinema_server.model.Client;


import java.util.List;
import java.util.Optional;


public interface ClientRepository extends CrudRepository<Client, Long> {

    //        Gestion des Clients
//                Creation,Modification
//                Suppression
//                Finder ... divers
//
    @Query("delete from Emprunt e where e.client.nom = :nom")
    @Modifying
    int supprimerEmpruntsClient(String nom);

    Optional<Client> findByNom(String nom);

    List<Client> findAll();

    List<Client> findAllByOrderByNom();


}
