package popschool.my_rest_cinema_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;
import popschool.my_rest_cinema_server.dao.*;
import popschool.my_rest_cinema_server.model.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CinemaServiceImp implements CinemaService{
    @Autowired
    ActeurRepository acteurRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    EmpruntRepository empruntRepository;
    @Autowired
    FilmRepository filmRepository;
    @Autowired
    GenreRepository genreRepository;
    @Autowired
    NationaliteRepositry nationaliteRepository;

    @Override
    public List<Acteur> findAllActeurs() {
        return acteurRepository.findAll();
    }
    @Override
    public List<Client> findAllClients() {
        return clientRepository.findAll();
    }
    @Override
    public List<Emprunt> findAllEmprunts() {
        return empruntRepository.findAll();
    }
    @Override
    public List<Film> findAllFilms() {
        return filmRepository.findAll();
    }
    @Override
    public List<Nationalite> findAllNationalite() {
        return nationaliteRepository.findAll();
    }
    @Override
    public List<Genre> findAllGenre() {
    return genreRepository.findAll();
    }

    @Override
    public Acteur findActeurById(Long id) throws NotFoundException {
        return acteurRepository.findById(id).orElseThrow(NotFoundException::new);
    }
    @Override
    public Client findClientById(Long id) throws NotFoundException {
        return clientRepository.findById(id).orElseThrow(NotFoundException::new);
    }
    @Override
    public Emprunt findEmpruntById(Long id) throws NotFoundException {
        return empruntRepository.findById(id).orElseThrow(NotFoundException::new);
    }
    @Override
    public Film findFilmById(Long id) throws NotFoundException {
        return filmRepository.findById(id).orElseThrow(NotFoundException::new);
    }
    @Override
    public Nationalite findNationaliteById(Long id) throws NotFoundException {
        return nationaliteRepository.findById(id).orElseThrow(NotFoundException::new);
    }
    @Override
    public Genre findGenreById(Long id) throws NotFoundException {
        return genreRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public void deleteEmpruntById(Long id) throws NotFoundException {
        Emprunt e = empruntRepository.findById(id).orElseThrow(NotFoundException::new);
        empruntRepository.delete(e);
    }

    @Override
    public Emprunt createEmprunt(Emprunt emprunt) {
        if(emprunt == null) throw new NullPointerException();
        return empruntRepository.save(emprunt);
    }

    @Override
    public void deleteClientById(Long id) throws NotFoundException {
        Client c = clientRepository.findById(id).orElseThrow(NotFoundException::new);
        clientRepository.delete(c);
    }

    @Override
    public Client createClient(Client client) {
        if(client == null) throw new NullPointerException();
        return clientRepository.save(client);
    }

    @Override
    public List<Film> ensFilmsEmpruntables() {
        return empruntRepository.findAllFilmsEmpruntables();
    }

    @Override
    public List<Film> ensFilmsEmpruntes() {
        return empruntRepository.findAllFilmsEmpruntes();
    }

    @Override
    public Emprunt emprunter(String nom, String titre){
        Optional<Client> opCl = clientRepository.findByNom(nom);
        Optional<Film> filmCl = filmRepository.findByTitre(titre);
        java.util.Date utilDate=new java.util.Date();
        return empruntRepository.save(new Emprunt(opCl.get(),filmCl.get(),"NON",new Date(utilDate.getTime())));
    }

    @Override
    public Emprunt retourEmpruntById(Long id) throws NotFoundException {
        Emprunt e = empruntRepository.findById(id).orElseThrow(NotFoundException::new);
        e.setRetour("OUI");
        return empruntRepository.save(e);
    }


    @Override
    public Emprunt retourEmpruntByNomTitre(String nomClient, String titreFilm) throws NotFoundException {
        Film f = filmRepository.findByTitre(titreFilm).orElseThrow(NotFoundException::new);
        Client c = clientRepository.findByNom(nomClient).orElseThrow(NotFoundException::new);
        Emprunt e = empruntRepository.findByFilmAndClient(f,c).orElseThrow(NotFoundException::new);
        e.setRetour("OUI");
        return empruntRepository.save(e);
    }

    @Override
    public Emprunt retourEmpruntByEmprunt(Emprunt e) throws NullPointerException {
        if(e == null)throw new NullPointerException();
        e.setRetour("OUI");
        return empruntRepository.save(e);
    }

    @Override
    public List<Film> findFilmsByActeur(String nomActeur) {
        return filmRepository.findFilmByActeur(nomActeur);
    }

    @Override
    public List<Film> findFilmsByGenre(String nature) {
        return filmRepository.findEnsFilmByNature(nature);
    }

    @Override
    public List<Film> findFilmsByActeurId(Long id) throws NotFoundException {
        Acteur a = acteurRepository.findById(id).orElseThrow(NotFoundException::new);
        return filmRepository.findFilmByActeur(a.getNom());
    }

    @Override
    public Acteur findActeurByNom(String nom) throws NotFoundException {
        return acteurRepository.findActeurByNom(nom).orElseThrow(NotFoundException::new);
    }
}
