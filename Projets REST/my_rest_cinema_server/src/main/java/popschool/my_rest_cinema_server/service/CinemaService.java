package popschool.my_rest_cinema_server.service;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import popschool.my_rest_cinema_server.model.*;

import java.util.List;

public interface CinemaService {

    List<Acteur> findAllActeurs();
    List<Client> findAllClients();
    List<Emprunt> findAllEmprunts();
    List<Film> findAllFilms();
    List<Nationalite> findAllNationalite();
    List<Genre> findAllGenre();

    Acteur findActeurById(Long id) throws NotFoundException;
    Client findClientById(Long id) throws NotFoundException;
    Emprunt findEmpruntById(Long id) throws NotFoundException;
    Film findFilmById(Long id) throws NotFoundException;
    Nationalite findNationaliteById(Long id) throws NotFoundException;
    Genre findGenreById(Long id) throws NotFoundException;

    void deleteEmpruntById(Long id) throws NotFoundException;
    Emprunt createEmprunt(Emprunt emprunt);

    void deleteClientById(Long id) throws NotFoundException;
    Client createClient(Client client);

    List<Film> ensFilmsEmpruntables();
    List<Film> ensFilmsEmpruntes();
    Emprunt emprunter(String nomClient, String titreFilm);
    Emprunt retourEmpruntById(Long id) throws NotFoundException;

    Emprunt retourEmpruntByNomTitre(String nomClient, String titreFilm) throws NotFoundException;

    Emprunt retourEmpruntByEmprunt(Emprunt e) throws NullPointerException;

    List<Film> findFilmsByActeur(String nomActeur);
    List<Film> findFilmsByGenre(String nature);

    List<Film> findFilmsByActeurId(Long id) throws NotFoundException;

    Acteur findActeurByNom(String nom) throws NotFoundException;
//    List<Film> findFilmsBy
}
