package popschool.my_rest_cinema_server.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.my_rest_cinema_server.dao.ActeurRepository;
import popschool.my_rest_cinema_server.model.Acteur;
import popschool.my_rest_cinema_server.service.CinemaService;

import java.util.List;

@RestController
@RequestMapping("/acteurs")
public class ActeurController {
    @Autowired
    CinemaService cinemaService;

    @GetMapping
    public ResponseEntity<List<Acteur>> all(){
        return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findAllActeurs());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Acteur> byId(@PathVariable("id")Long id){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findActeurById(id));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(null);
        }
    }

    @GetMapping("/nom/{nomActeur}")
    public ResponseEntity<Acteur> byNom(@PathVariable("nomActeur")String nom){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findActeurByNom(nom));
        } catch (NotFoundException e) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(null);
        }
    }
}
