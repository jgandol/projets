package popschool.my_rest_cinema_server.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import popschool.my_rest_cinema_server.model.Emprunt;
import popschool.my_rest_cinema_server.model.Genre;
import popschool.my_rest_cinema_server.service.CinemaService;

import java.util.List;

@RestController
@RequestMapping("/genres")
public class GenreController {
    @Autowired
    CinemaService cinemaService;

    @GetMapping
    public ResponseEntity<List<Genre>> all(){
        return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findAllGenre());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Genre> byId(@PathVariable("id")Long id){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findGenreById(id));
        } catch (ChangeSetPersister.NotFoundException e) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(null);
        }
    }
}
