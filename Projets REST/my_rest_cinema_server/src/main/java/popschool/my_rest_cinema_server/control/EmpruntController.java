package popschool.my_rest_cinema_server.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.my_rest_cinema_server.model.Client;
import popschool.my_rest_cinema_server.model.Emprunt;
import popschool.my_rest_cinema_server.model.Film;
import popschool.my_rest_cinema_server.service.CinemaService;

import java.util.List;

@RestController
@RequestMapping("/emprunts")
public class EmpruntController {
    @Autowired
    CinemaService cinemaService;

    @GetMapping
    public ResponseEntity<List<Emprunt>> all(){
        return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findAllEmprunts());
    }

    @GetMapping("dispo")
    public ResponseEntity<List<Film>> allDispo(){
        return ResponseEntity.status(HttpStatus.OK).body(cinemaService.ensFilmsEmpruntables());
    }

    @GetMapping("nondispo")
    public ResponseEntity<List<Film>> allNonDispo(){
        return ResponseEntity.status(HttpStatus.OK).body(cinemaService.ensFilmsEmpruntes());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Emprunt> byId(@PathVariable("id")Long id){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findEmpruntById(id));
        } catch (ChangeSetPersister.NotFoundException e) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(null);
        }
    }

    @PostMapping
    public ResponseEntity<Emprunt> emprunter(@RequestBody Emprunt emprunt){
        try {
            Emprunt e = cinemaService.emprunter(emprunt.getClient().getNom(),emprunt.getFilm().getTitre());
            return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findEmpruntById(e.getNemprunt()));
        } catch (ChangeSetPersister.NotFoundException e) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(null);
        }
    }

    @PutMapping("/retour/{id}")
    public ResponseEntity<Emprunt> retourEmprunt(@PathVariable("id")Long id){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(cinemaService.retourEmpruntById(id));
        } catch (ChangeSetPersister.NotFoundException e) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(null);
        }
    }
}
