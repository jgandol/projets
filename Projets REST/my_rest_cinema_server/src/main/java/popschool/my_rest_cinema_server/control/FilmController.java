package popschool.my_rest_cinema_server.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import popschool.my_rest_cinema_server.model.Emprunt;
import popschool.my_rest_cinema_server.model.Film;
import popschool.my_rest_cinema_server.service.CinemaService;

import java.util.List;

@RestController
@RequestMapping("/films")
public class FilmController {
    @Autowired
    CinemaService cinemaService;

    @GetMapping
    public ResponseEntity<List<Film>> all(){
        return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findAllFilms());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Film> byId(@PathVariable("id")Long id){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findFilmById(id));
        } catch (ChangeSetPersister.NotFoundException e) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(null);
        }
    }

    @GetMapping("/by/acteurprincipal/{nomActeur}")
    public ResponseEntity<List<Film>> byActeurName(@PathVariable("nomActeur")String nomActeur){
        return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findFilmsByActeur(nomActeur));
    }

    @GetMapping("/by/acteur/{idActeur}")
    public ResponseEntity<List<Film>> byActeurId(@PathVariable("idActeur")Long id){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findFilmsByActeurId(id));
        } catch (ChangeSetPersister.NotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/by/genre/{nature}")
    public ResponseEntity<List<Film>> byGenre(@PathVariable("nature")String nature){
        return ResponseEntity.status(HttpStatus.OK).body(cinemaService.findFilmsByGenre(nature));
    }
}
