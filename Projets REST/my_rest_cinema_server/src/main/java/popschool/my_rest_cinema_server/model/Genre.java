package popschool.my_rest_cinema_server.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "GENRE")
@Data
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ngenre")
    Long ngenre;
    String nature;

    @OneToMany
    @JoinColumn(name = "ngenre")
    @JsonIgnore
    List<Film> filmsbygenre;
}
