package popschool.my_rest_cinema_server.model;

public class InfoGenre{
    private String nature;
    private Long nb;

    public InfoGenre(String nature, Long nb) {
        this.nature = nature;
        this.nb = nb;
    }
}
