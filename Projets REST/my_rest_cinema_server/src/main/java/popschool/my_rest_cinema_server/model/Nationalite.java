package popschool.my_rest_cinema_server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PAYS")
@Data
public class Nationalite {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "npays")
    Long npays;
    String nom;

    @OneToMany(mappedBy = "nationalite")
    @JsonIgnore
    List<Acteur> acteurs ;

    @OneToMany
    @JoinColumn(referencedColumnName = "npays",name = "npays")
    @JsonIgnore
    List<Film> filmsbynationalite;

}
