package popschool.my_rest_cinema_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyRestCinemaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyRestCinemaServerApplication.class, args);
    }

}
