package pop.tacosrest.repositories;

import org.springframework.data.repository.CrudRepository;
import pop.tacosrest.entities.Taco;

public interface TacoRepository
        extends CrudRepository<Taco, Long> {

}
