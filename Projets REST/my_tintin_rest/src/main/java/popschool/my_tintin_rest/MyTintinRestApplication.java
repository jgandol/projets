package popschool.my_tintin_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyTintinRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTintinRestApplication.class, args);
    }

}
