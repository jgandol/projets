package popschool.my_tintin_rest.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.my_tintin_rest.dao.AlbumRepository;
import popschool.my_tintin_rest.dao.PersonnageRepository;
import popschool.my_tintin_rest.model.Album;
import popschool.my_tintin_rest.model.Personnage;


import java.util.List;

@RestController
public class AlbumController {
    /*

liste de personnages
personnages d'un id donné
Rechercher la liste des personnages d'un genre donné
Rechercher les personnages d'un ablum donné


 */
    @Autowired
    AlbumRepository albumDao;

    @Autowired
    PersonnageRepository persDao;

    //    Rechercher liste des albums
    @GetMapping(value = "/albums")
    public List<Album> getAlbums() {
        return albumDao.findAll();
    }

    //    albums d'un id donné
    @GetMapping(value = "/albums/{id}")
    public Album getAlbumsById(@PathVariable("id") int id) {
        return albumDao.findById(id).get();
    }

    //    liste des albums entre 2 dates
    @GetMapping(value = "/albums/between/{an1}/and/{an2}")
    public List<Album> getAlbumsBetween(@PathVariable("an1") int an1, @PathVariable("an2") int an2) {
        return albumDao.findByAnneeBetween(an1, an2);
    }


    //    Rechercher les personnages d'un album donné
    @GetMapping(value = "/albums/{id}/personnages")
    public List<Personnage> getPersonnagesOfAlbums(@PathVariable("id") int id) {
        return persDao.findByApparaitsAlbumId(id);
    }

    //    Rechercher les personnages d'un album donné desc
    @GetMapping(value = "/albums/{id}/personnages/desc")
    public List<Personnage> getPersonnagesOfAlbumsDesc(@PathVariable("id") int id) {
        return persDao.findByApparaitsAlbumIdDesc(id);
    }

//
//    @PostMapping(value = "/albums")
//    public ResponseEntity<Employee> addEmployee(@RequestBody Employee emp) {
//        Employee e = employeeDao.save(emp);
//        return ResponseEntity.status(HttpStatus.CREATED).body(e);
//    }
//
//    //     URI:/contextPath/servletPath/employees
//    @GetMapping(value = "/employees")
//    public List<Employee> getEmployees() {
//        List<Employee> list = employeeDao.findAll();
//        return list;
//    }
//
//    @GetMapping(value = "/depts")
//    public List<Dept> getDepts() {
//        List<Dept> list = deptDao.findAll();
//        return list;
//    }
//
//    //     URI:/contextPath/servletPath/employees/{empNo}
//    @GetMapping(value = "/employees/{empNo}")
//    public Employee getEmployee(
//            @PathVariable("empNo") String empNo) {
//        return employeeDao.findById(empNo).get();
//    }
//
//    //     URI:/contextPath/servletPath/employees/{empNo}
//    @PutMapping(value = "/employees/{empNo}")
//    public Employee updateEmployee(
//            @RequestBody Employee emp,
//            @PathVariable("empNo") String empNo) {
//        return employeeDao.save(emp);
//    }
//
//    //     URI:/contextPath/servletPath/employees/{empNo}
//    @DeleteMapping(value = "/employees/{empNo}")
//    public void deleteEmployee(
//            @PathVariable("empNo") String empNo) {
//        employeeDao.deleteById(empNo);
//    }
//
//    @PutMapping(value = "/employees/{empNo}/affecte/{deptNo}")
//    public Employee affecterDept(@PathVariable("empNo") String empNo, @PathVariable("deptNo") String deptNo) {
//        Employee e = employeeDao.findById(empNo).get();
//        e.setDept(deptDao.findByDeptno(deptNo).get());
//        return employeeDao.save(e);
////        return ResponseEntity.status(HttpStatus.CREATED).body(e);
//    }
//
//    //    Rechercher tous les employés d'un departement donné
//    @GetMapping(value = "/employees/de/{deptNo}")
//    public List<Employee> getEmployeeDeDept(@PathVariable("deptNo") String deptNo) {
//        Dept d = deptDao.findByDeptno(deptNo).get();
//        List<Employee> list = employeeDao.findAllByDept(d);
//        return list;
//    }
//
//    //    Afficher la masse salariale d'un departement donné
//    @GetMapping(value = "/departement/salaire/{deptNo}")
//    public Double masseSalariale(@PathVariable("deptNo") String deptNo) {
//        Double d = deptDao.salaryWeight(deptNo);
//        return d;
//    }


}
