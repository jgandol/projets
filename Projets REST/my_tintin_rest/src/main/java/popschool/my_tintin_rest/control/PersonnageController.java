package popschool.my_tintin_rest.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import popschool.my_tintin_rest.dao.AlbumRepository;
import popschool.my_tintin_rest.dao.PersonnageRepository;
import popschool.my_tintin_rest.model.Album;
import popschool.my_tintin_rest.model.Personnage;

import java.util.List;

@RestController
public class PersonnageController {

    @Autowired
    AlbumRepository albumDao;

    @Autowired
    PersonnageRepository persDao;

    //    Liste de personnages
    @GetMapping(value = "/personnages")
    public List<Personnage> getPersonnages() {
        return persDao.findAll();
    }

    //    personnages d'un id donné
    @GetMapping(value = "/personnages/{id}")
    public Personnage getAlbumsById(@PathVariable("id") int id) {
        return persDao.findById(id).get();
    }

    //    Rechercher la liste des personnages d'un genre donné
    @GetMapping(value = "/personnages/genre/{genre}")
    public List<Personnage> getAlbumsById(@PathVariable("genre") String genre) {
        return persDao.findByGenre(genre);
    }

}
