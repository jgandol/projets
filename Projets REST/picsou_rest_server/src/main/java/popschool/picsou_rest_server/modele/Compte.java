package popschool.picsou_rest_server.modele;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.sql.Timestamp;

import org.hibernate.annotations.Cascade.*;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Compte {
    private int idCompte;
    private String num;
    private Timestamp dateOpen;
    private Double plafond;
    private String bloque;
    private double solde;
    private Client client;
    private List<Mouvement> mouvements;

    public Compte() {
    }

    public Compte(String num) {
        this.num = num;
        this.client = client;
        this.dateOpen = new Timestamp(System.currentTimeMillis());
        this.bloque = "non";
        this.solde = 0.0;
        this.plafond = 0.0;
        this.client = null;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_compte")
    @JsonGetter
    public int getIdCompte() {
        return idCompte;
    }

    @JsonIgnore
    public void setIdCompte(int idCompte) {
        this.idCompte = idCompte;
    }

    @Basic
    @Column(name = "num")
    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    @Basic
    @Column(name = "date_open")
    @JsonGetter
    public Timestamp getDateOpen() {
        return dateOpen;
    }

    @JsonIgnore
    public void setDateOpen(Timestamp dateOpen) {
        this.dateOpen = dateOpen;
    }

    @Basic
    @Column(name = "plafond")
    public Double getPlafond() {
        return plafond;
    }

    public void setPlafond(Double plafond) {
        this.plafond = plafond;
    }

    @Basic
    @Column(name = "bloque")
    public String getBloque() {
        return bloque;
    }

    public void setBloque(String bloque) {
        this.bloque = bloque;
    }

    @Basic
    @Column(name = "solde")
    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    @ManyToOne
    @JoinColumn(name = "titulaire", referencedColumnName = "id_client", nullable = false)
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }


    @OneToMany(mappedBy = "compte", cascade = CascadeType.ALL)
    @JsonIgnore
    public List<Mouvement> getMouvements() {
        return mouvements;
    }

    public void setMouvements(List<Mouvement> mouvements) {
        this.mouvements = mouvements;
    }


    @Override
    public String toString() {
        return "Compte{" +
                "idCompte=" + idCompte +
                ", num='" + num + '\'' +
                ", dateOpen=" + dateOpen +
                ", plafond=" + plafond +
                ", bloque='" + bloque + '\'' +
                ", solde=" + solde +
                ", client=" + client.getNom() + " " + client.getPrenom() +
                '}';
    }
}
