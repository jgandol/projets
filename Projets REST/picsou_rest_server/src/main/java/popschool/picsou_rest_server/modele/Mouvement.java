package popschool.picsou_rest_server.modele;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
public class Mouvement {
    private int idMouv;
    private Timestamp dateOperation;
    private double montant;
    private String nature;
    private Compte compte;

    public Mouvement(double montant, String nature) {
        this.montant = montant;
        this.nature = nature;
        this.dateOperation = new Timestamp(System.currentTimeMillis());
    }

    public Mouvement() {
    }

    @Id
    @Column(name = "id_mouv")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonGetter
    public int getIdMouv() {
        return idMouv;
    }

    @JsonIgnore
    public void setIdMouv(int idMouv) {
        this.idMouv = idMouv;
    }

    @Basic
    @Column(name = "date_operation")
    @JsonGetter
    public Timestamp getDateOperation() {
        return dateOperation;
    }

    @JsonIgnore
    public void setDateOperation(Timestamp dateOperation) {
        this.dateOperation = dateOperation;
    }

    @Basic
    @Column(name = "montant")
    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "nature")
    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_compte")
    public Compte getCompte() {
        return compte;
    }

    @JsonIgnore
    public void setCompte(Compte compte) {
        this.compte = compte;
    }
}
