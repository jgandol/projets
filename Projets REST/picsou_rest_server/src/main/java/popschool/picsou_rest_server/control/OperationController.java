package popschool.picsou_rest_server.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.picsou_rest_server.dao.ClientRepository;
import popschool.picsou_rest_server.dao.CompteRepository;
import popschool.picsou_rest_server.modele.Client;
import popschool.picsou_rest_server.modele.Compte;
import popschool.picsou_rest_server.modele.Mouvement;
import popschool.picsou_rest_server.service.ExceptionClientExistant;
import popschool.picsou_rest_server.service.ExceptionNotFound;
import popschool.picsou_rest_server.service.PicsouService;

import java.util.List;
import java.util.Optional;

@RestController
public class OperationController {
    @Autowired
    PicsouService picsouService;

    @GetMapping(value = "/operations")
    public ResponseEntity<List<Mouvement>> getEnsOperations() {
        try {
            List<Mouvement> liste = picsouService.listOperations();
            return ResponseEntity.status(HttpStatus.OK).body(liste);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/comptes/{idCompte}/crediter/{montant}")
    public ResponseEntity<Mouvement> crediterCompte(@PathVariable("idCompte") int idCompte, @PathVariable("montant") Double montant) {
        try {
            Mouvement m = picsouService.crediter(idCompte, montant);
            return ResponseEntity.status(HttpStatus.CREATED).body(m);
        } catch (ExceptionNotFound e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/comptes/{idCompte}/debiter/{montant}")
    public ResponseEntity<Mouvement> debiterCompte(@PathVariable("idCompte") int idCompte, @PathVariable("montant") Double montant) {
        try {
            Mouvement m = picsouService.debiter(idCompte, montant);
            return ResponseEntity.status(HttpStatus.CREATED).body(m);
        } catch (ExceptionNotFound e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/comptes/{idCompteOri}/vers/{idCompteDest}/montant/{montant}")
    public ResponseEntity<Mouvement[]> transfertCompte(@PathVariable("idCompteOri") int idCompteOri, @PathVariable("idCompteDest") int idCompteDest, @PathVariable("montant") Double montant) {
        try {
            Mouvement[] m = picsouService.transfert(idCompteOri, idCompteDest, montant);
            return ResponseEntity.status(HttpStatus.CREATED).body(m);
        } catch (ExceptionNotFound e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
