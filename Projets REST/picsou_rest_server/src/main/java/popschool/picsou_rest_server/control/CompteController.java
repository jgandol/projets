package popschool.picsou_rest_server.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.picsou_rest_server.modele.Compte;
import popschool.picsou_rest_server.service.ExceptionNotFound;
import popschool.picsou_rest_server.service.PicsouService;

import java.util.List;

@RestController
public class CompteController {
    @Autowired
    PicsouService picsouService;


    @GetMapping(value = "/comptes/{idCompte}")
    public ResponseEntity<Compte> getCompteById(@PathVariable("idCompte") Integer idCompte) {
        try {
            Compte compte = picsouService.consulterCompte(idCompte);
            return ResponseEntity.status(HttpStatus.OK).body(compte);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/comptes/of/{idClient}")
    public ResponseEntity<List<Compte>> getCompteByIdClient(@PathVariable("idClient") Integer idClient) {
        try {
            List<Compte> liste = picsouService.listComptesOfClients(idClient);
            return ResponseEntity.status(HttpStatus.OK).body(liste);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/comptes")
    public ResponseEntity<List<Compte>> getEnsComptes() {
        try {
            if (picsouService.listComptes().size() > 0)
                return ResponseEntity.status(HttpStatus.OK).body(picsouService.listComptes());
            return null;
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

    }

    @PostMapping(value = "/comptes/owner/{idClient}")
    public ResponseEntity<Compte> createComptes(@RequestBody Compte compte, @PathVariable("idClient") int idClient) throws ExceptionNotFound {
        try {
            Compte c = picsouService.creerCompte(idClient, compte);
            return ResponseEntity.status(HttpStatus.CREATED).body(c);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @DeleteMapping(value = "/comptes/{idClient}")
    public ResponseEntity<Void> deleteCompte(@PathVariable("idClient") int idClient) throws ExceptionNotFound {
        try {
            picsouService.deleteCompte(idClient);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
