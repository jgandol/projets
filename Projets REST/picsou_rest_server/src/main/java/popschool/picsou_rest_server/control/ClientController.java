package popschool.picsou_rest_server.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.picsou_rest_server.modele.Client;
import popschool.picsou_rest_server.service.ExceptionClientExistant;
import popschool.picsou_rest_server.service.ExceptionNotFound;
import popschool.picsou_rest_server.service.PicsouService;

import java.util.List;

@RestController
public class ClientController {
    @Autowired
    PicsouService picsouService;


    @PutMapping(value = "/clients/{id}")
    public ResponseEntity<Client> modifyClient(@RequestBody Client client, @PathVariable("id") Integer idClient) throws ExceptionNotFound {
        try {
            Client c = picsouService.modifyClient(idClient, client);
            return ResponseEntity.status(HttpStatus.CREATED).body(c);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/clients")
    public ResponseEntity<List<Client>> getEnsClients() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(picsouService.listClients());
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/clients/{id}")
    public ResponseEntity<Client> getClientById(@PathVariable("id") int id) {
        try {
            Client client = picsouService.findClientById(id);
            return ResponseEntity.status(HttpStatus.OK).body(client);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping(value = "/clients")
    public ResponseEntity<Client> createClient(@RequestBody Client client) throws ExceptionNotFound {
        try {
            Client c = picsouService.createClient(client);
            return ResponseEntity.status(HttpStatus.CREATED).body(c);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (ExceptionClientExistant e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @DeleteMapping(value = "/clients/{idClient}")
    public ResponseEntity<Void> deleteClient(@PathVariable("idClient") int idClient) throws ExceptionNotFound {
        try {
            picsouService.deleteClient(idClient);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } catch (ExceptionNotFound ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
