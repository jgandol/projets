package popschool.picsou_rest_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import popschool.picsou_rest_server.modele.Client;
import popschool.picsou_rest_server.modele.Compte;


import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Integer> {

    List<Client> findAll();

    Optional<Client> findById(int id);

    Optional<Client> findByNom(String nom);

    @Query("select c.comptes from Client c where c.idClient = :id")
    List<Compte> findComptesByClient(int id);


}
