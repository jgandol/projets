package popschool.picsou_rest_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import popschool.picsou_rest_server.modele.Mouvement;

import java.util.List;
import java.util.Optional;

public interface MouvementRepository extends JpaRepository<Mouvement, Integer> {

    List<Mouvement> findAll();

    Optional<Mouvement> findById(int id);

}
