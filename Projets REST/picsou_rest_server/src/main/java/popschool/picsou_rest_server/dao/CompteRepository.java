package popschool.picsou_rest_server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import popschool.picsou_rest_server.modele.Compte;


import java.awt.*;
import java.util.List;
import java.util.Optional;

public interface CompteRepository extends JpaRepository<Compte, Integer> {

    List<Compte> findAll();

    Optional<Compte> findById(int id);
}
