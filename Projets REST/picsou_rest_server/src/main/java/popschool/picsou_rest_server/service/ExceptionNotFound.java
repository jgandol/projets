package popschool.picsou_rest_server.service;

import lombok.Data;
import net.bytebuddy.implementation.bind.annotation.Super;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;
import popschool.picsou_rest_server.modele.Client;
import popschool.picsou_rest_server.modele.Compte;

@Data
public class ExceptionNotFound extends RuntimeException {
    private Integer i;
    private Compte c;
    private Client client;

    public ExceptionNotFound(Integer i) {
        this.i = i;
    }

    public ExceptionNotFound(Compte c) {
        this.c = c;
    }

    public ExceptionNotFound() {
    }
}
