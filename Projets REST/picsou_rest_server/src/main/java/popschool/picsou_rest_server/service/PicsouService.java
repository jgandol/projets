package popschool.picsou_rest_server.service;

import popschool.picsou_rest_server.modele.Client;
import popschool.picsou_rest_server.modele.Compte;
import popschool.picsou_rest_server.modele.Mouvement;

import java.util.List;

public interface PicsouService {
    List<Compte> listComptes();

    Compte consulterCompte(Integer idCompte);

    Compte creerCompte(int idClient, Compte compte);

    void deleteCompte(int idClient);

    List<Client> listClients();

    List<Compte> listComptesOfClients(int idClient);

    Client findClientById(int id);

    Client createClient(Client client);

    Client modifyClient(Integer idClient, Client nouv) throws ExceptionNotFound;

    void deleteClient(int idClient);

    List<Mouvement> listOperations();

    Mouvement crediter(int id, Double montant);


    Mouvement debiter(int id, Double montant);

    Mouvement[] transfert(int idCompteDepart, int idCompteArrivé, Double montant);
}
