package popschool.picsou_rest_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.picsou_rest_server.dao.ClientRepository;
import popschool.picsou_rest_server.dao.CompteRepository;
import popschool.picsou_rest_server.dao.MouvementRepository;
import popschool.picsou_rest_server.modele.Client;
import popschool.picsou_rest_server.modele.Compte;
import popschool.picsou_rest_server.modele.Mouvement;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class PicsouServiceImp implements PicsouService {
    @Autowired
    CompteRepository compteDao;
    @Autowired
    ClientRepository clientDao;
    @Autowired
    MouvementRepository mvtDao;

    // ------------  OPERATIONS COMPTES -------------- //
    @Override
    public List<Compte> listComptes() {
        return compteDao.findAll();
    }

    @Override
    public Compte consulterCompte(Integer idCompte) throws ExceptionNotFound {
        Optional<Compte> opt_c = compteDao.findById(idCompte);
        if (!opt_c.isPresent()) throw new ExceptionNotFound(idCompte);
        Compte c = opt_c.get();
        return c;
    }

    @Override
    public Compte creerCompte(int idClient, Compte compte) throws ExceptionNotFound {
        if (compte == null) throw new ExceptionNotFound(compte);
        Optional<Client> client = clientDao.findById(idClient);
        if (!client.isPresent()) {
            throw new ExceptionNotFound(idClient);
        }
        compte.setClient(client.get());
        return compteDao.save(compte);
    }

    @Override
    public void deleteCompte(int idCompte) throws ExceptionNotFound {
        if (idCompte <= 0) throw new ExceptionNotFound(idCompte);
        Optional<Compte> compte = compteDao.findById(idCompte);
        if (!compte.isPresent()) throw new ExceptionNotFound(idCompte);
        compteDao.delete(compte.get());
    }

    // ------------  OPERATIONS CLIENTS -------------- //

    @Override
    public List<Client> listClients() {
        return clientDao.findAll();
    }

    @Override
    public List<Compte> listComptesOfClients(int idClient) throws ExceptionNotFound{
        if(idClient <= 0 ) throw new ExceptionNotFound(idClient);
        Client c = clientDao.findById(idClient).orElseThrow(ExceptionNotFound::new);
        return c.getComptes();
    }

    @Override
    public Client findClientById(int id) {
        Optional<Client> opt_c = clientDao.findById(id);
        if (!opt_c.isPresent()) throw new ExceptionNotFound(id);
        Client c = opt_c.get();
        return c;
    }

    @Override
    public Client createClient(Client client) throws ExceptionNotFound, ExceptionClientExistant {
        if (client == null) throw new ExceptionNotFound();
        try {
            return clientDao.save(client);

        } catch (Exception ex) {
            throw new ExceptionClientExistant();
        }

    }

    @Override
    public Client modifyClient(Integer idClient, Client nouv) throws ExceptionNotFound {
        Client client = clientDao.findById(idClient).orElseThrow(ExceptionNotFound::new);
        nouv.setIdClient(client.getIdClient());
        return clientDao.save(nouv);
    }

    @Override
    public void deleteClient(int idClient) {
        if (idClient <= 0) throw new ExceptionNotFound(idClient);
        Optional<Client> client = clientDao.findById(idClient);
        if (!client.isPresent()) throw new ExceptionNotFound(idClient);
        clientDao.delete(client.get());
    }

    // ------------  OPERATIONS OPERATIONS -------------- //

    @Override
    public List<Mouvement> listOperations() {
        return mvtDao.findAll();
    }

    @Override
    public Mouvement crediter(int id, Double montant) {
        Compte compte = compteDao.findById(id).orElseThrow(ExceptionNotFound::new);
        compte.setSolde(compte.getSolde() + montant);
        Mouvement m = new Mouvement();
        m.setCompte(compte);
        m.setMontant(montant);
        m.setNature("CR");
        m.setDateOperation(new Timestamp(System.currentTimeMillis()));
        return mvtDao.save(m);
    }

    @Override
    public Mouvement debiter(int id, Double montant) throws ExceptionNotFound {
        Compte compte = compteDao.findById(id).orElseThrow(ExceptionClientExistant::new);
        if (montant < (compte.getSolde() + compte.getPlafond())) {
            compte.setSolde(compte.getSolde() - montant);
            compteDao.save(compte);

            Mouvement mvt = new Mouvement();
            mvt.setCompte(compte);
            mvt.setMontant(montant);
            mvt.setNature("DB");
            mvt.setDateOperation(new Timestamp(System.currentTimeMillis()));
            return mvtDao.save(mvt);
        }
        throw new ExceptionNotFound();
    }

    @Override
    public Mouvement[] transfert(int idorigine, int iddestination, Double montant) throws ExceptionNotFound {
        //Compte existe
        Compte compteOrigine = compteDao.findById(idorigine).orElseThrow(ExceptionClientExistant::new);
        Compte compteDest = compteDao.findById(iddestination).orElseThrow(ExceptionClientExistant::new);

        //Transfert vers compte bloqué
        if (compteOrigine.getBloque().equals("oui")) return null;

        //Depasse pas le plafond
        if (montant > (compteOrigine.getSolde() + compteOrigine.getPlafond())) throw new ExceptionNotFound();

        //Pas vers le meme compte
        if (compteOrigine == compteDest) throw new ExceptionNotFound();

        compteOrigine.setSolde(compteOrigine.getSolde() - montant);
        compteDest.setSolde(compteDest.getSolde() + montant);

        Mouvement mvt2 = new Mouvement();
        mvt2.setCompte(compteDest);
        mvt2.setMontant(montant);
        mvt2.setNature("DB");
        mvt2.setDateOperation(new Timestamp(System.currentTimeMillis()));
        mvtDao.save(mvt2);

        Mouvement mvt = new Mouvement();
        mvt.setCompte(compteOrigine);
        mvt.setMontant(montant);
        mvt.setNature("CR");
        mvt.setDateOperation(new Timestamp(System.currentTimeMillis()));
//        mvt.setIdMouv(mvt2.getIdMouv()+1);
        mvtDao.save(mvt);
        compteDao.save(compteOrigine);
        compteDao.save(compteDest);

        Mouvement[] tabmouv = new Mouvement[2];
        tabmouv[0] = mvt;
        tabmouv[1] = mvt2;
        return tabmouv;
    }

}
