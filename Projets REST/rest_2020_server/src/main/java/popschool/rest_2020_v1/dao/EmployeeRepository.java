package popschool.rest_2020_v1.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import popschool.rest_2020_v1.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, String> {

    List<Employee> findAll();

    Optional<Employee> findById(String empno);

}
