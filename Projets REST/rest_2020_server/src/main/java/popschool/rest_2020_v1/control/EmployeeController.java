package popschool.rest_2020_v1.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import popschool.rest_2020_v1.dao.EmployeeRepository;
import popschool.rest_2020_v1.model.Employee;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeDao;

    //     URI:/contextPath/servletPath/employees
    @PostMapping(value = "/employees")
    public Employee addEmployee(@RequestBody Employee emp) {
        if (emp != null) {
            return employeeDao.save(emp);
        } else return new Employee();

    }

    //     URI:/contextPath/servletPath/employees
    @GetMapping(value = "/employees")
    public List<Employee> getEmployees() {
        List<Employee> list = employeeDao.findAll();
        return list;
    }

    //     URI:/contextPath/servletPath/employees/{empNo}
    @GetMapping(value = "/employees/{empNo}")
    public Employee getEmployee(
            @PathVariable("empNo") String empNo) {
        return employeeDao.findById(empNo).get();
    }

    //     URI:/contextPath/servletPath/employees/{empNo}
    @PutMapping(value = "/employees/{empNo}")
    public Employee updateEmployee(
            @RequestBody Employee emp,
            @PathVariable("empNo") String empNo) {
        return employeeDao.save(emp);
    }

    //     URI:/contextPath/servletPath/employees/{empNo}
    @DeleteMapping(value = "/employees/{empNo}")
    public void deleteEmployee(
            @PathVariable("empNo") String empNo) {
        employeeDao.deleteById(empNo);
    }

}
