package popschool.rest_2020_v1.model;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Employee {
    @Id
    @Column(name = "empno")
    private String empno;

    @Column(name = "empname")
    private String empname;

    private String position;

    public Employee() {
    }

    public Employee(String empno, String empname, String position) {
        this.empno = empno;
        this.empname = empname;
        this.position = position;
    }
}