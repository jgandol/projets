package pop.tacosweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TacoswebApplication {

    public static void main(String[] args) {
        SpringApplication.run(TacoswebApplication.class, args);
    }

}
