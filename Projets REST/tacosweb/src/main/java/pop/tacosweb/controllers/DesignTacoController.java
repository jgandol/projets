package pop.tacosweb.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import pop.tacosweb.model.Ingredient;
import pop.tacosweb.model.Order;
import pop.tacosweb.model.Taco;


import javax.validation.Valid;



@Controller
@RequestMapping("/design")
@SessionAttributes("order")
public class DesignTacoController {

  @Autowired
  public DesignTacoController() {

  }

  @ModelAttribute(name = "order")
  public Order order() {
    return new Order();
  }

  @ModelAttribute(name = "design")
  public Taco design() {
    return new Taco();
  }



  @GetMapping
  public String showDesignForm(Model model) {
    String URL_INGREDIENTS_BY_TYPE = "http://localhost:8080/ingredients/type/";

    Ingredient.Type[] types = Ingredient.Type.values();
    for (Ingredient.Type type : types){
      RestTemplate restTemplate = new RestTemplate();
      Ingredient[] ingredientsByType = restTemplate
              .getForObject(URL_INGREDIENTS_BY_TYPE+type.toString(),Ingredient[].class);
      model.addAttribute(type.toString().toLowerCase(),ingredientsByType);
    }

    return "design";
  }


  @PostMapping()
  public String processDesign(
      @Valid Taco taco,
      Errors errors,
      @ModelAttribute Order order)
  {


    if (errors.hasErrors()) {
      return "design";
    }

//    Taco saved = tacoRepo.save(taco);

    String URL_CREATE_TACO = "http://localhost:8080/design";
    RestTemplate restTemplate = new RestTemplate();
    HttpEntity<Taco> requestBody = new HttpEntity<>(taco);
    ResponseEntity<Taco> saved = restTemplate.postForEntity(URL_CREATE_TACO,requestBody,Taco.class);
    order.addDesign(saved.getBody());
    System.out.println(taco);
    System.out.println(order);

    return "redirect:/orders/current";
  }

}

