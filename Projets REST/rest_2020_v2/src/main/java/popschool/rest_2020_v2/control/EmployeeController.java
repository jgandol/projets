package popschool.rest_2020_v2.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.rest_2020_v2.dao.DeptRepository;
import popschool.rest_2020_v2.dao.EmployeeRepository;
import popschool.rest_2020_v2.model.Dept;
import popschool.rest_2020_v2.model.Employee;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    EmployeeRepository employeeDao;

    @Autowired
    DeptRepository deptDao;

    //     URI:/contextPath/servletPath/employees
    @PostMapping(value = "/employees")
    public ResponseEntity<Employee> addEmployee(@RequestBody Employee emp) {
        Employee e = employeeDao.save(emp);
        return ResponseEntity.status(HttpStatus.CREATED).body(e);
    }

    //     URI:/contextPath/servletPath/employees
    @GetMapping(value = "/employees")
    public List<Employee> getEmployees() {
        List<Employee> list = employeeDao.findAll();
        return list;
    }

    @GetMapping(value = "/depts")
    public List<Dept> getDepts() {
        List<Dept> list = deptDao.findAll();
        return list;
    }

    //     URI:/contextPath/servletPath/employees/{empNo}
    @GetMapping(value = "/employees/{empNo}")
    public Employee getEmployee(
            @PathVariable("empNo") String empNo) {
        return employeeDao.findById(empNo).get();
    }

    //     URI:/contextPath/servletPath/employees/{empNo}
    @PutMapping(value = "/employees/{empNo}")
    public Employee updateEmployee(
            @RequestBody Employee emp,
            @PathVariable("empNo") String empNo) {
        return employeeDao.save(emp);
    }

    //     URI:/contextPath/servletPath/employees/{empNo}
    @DeleteMapping(value = "/employees/{empNo}")
    public void deleteEmployee(
            @PathVariable("empNo") String empNo) {
        employeeDao.deleteById(empNo);
    }

    @PutMapping(value = "/employees/{empNo}/affecte/{deptNo}")
    public Employee affecterDept(@PathVariable("empNo") String empNo, @PathVariable("deptNo") String deptNo) {
        Employee e = employeeDao.findById(empNo).get();
        e.setDept(deptDao.findByDeptno(deptNo).get());
        return employeeDao.save(e);
//        return ResponseEntity.status(HttpStatus.CREATED).body(e);
    }

    //    Rechercher tous les employés d'un departement donné
    @GetMapping(value = "/employees/de/{deptNo}")
    public List<Employee> getEmployeeDeDept(@PathVariable("deptNo") String deptNo) {
        Dept d = deptDao.findByDeptno(deptNo).get();
        List<Employee> list = employeeDao.findAllByDept(d);
        return list;
    }

    //    Afficher la masse salariale d'un departement donné
    @GetMapping(value = "/departement/salaire/{deptNo}")
    public Double masseSalariale(@PathVariable("deptNo") String deptNo) {
        Double d = deptDao.salaryWeight(deptNo);
        return d;
    }

    //
}
