package popschool.rest_2020_v2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Rest2020V2Application {

    public static void main(String[] args) {
        SpringApplication.run(Rest2020V2Application.class, args);
    }

}
