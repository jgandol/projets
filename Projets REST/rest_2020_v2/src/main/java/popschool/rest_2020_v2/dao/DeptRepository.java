package popschool.rest_2020_v2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import popschool.rest_2020_v2.model.Dept;
import popschool.rest_2020_v2.model.Employee;

import java.util.List;
import java.util.Optional;

public interface DeptRepository extends JpaRepository<Dept, String> {
    List<Dept> findAll();

    Optional<Dept> findByDeptno(String deptno);

    @Query("select sum(e.salary) from Employee e where e.dept.deptno = :deptno ")
    Double salaryWeight(String deptno);
}
