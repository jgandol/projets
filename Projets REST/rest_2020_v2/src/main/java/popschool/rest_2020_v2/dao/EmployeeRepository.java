package popschool.rest_2020_v2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.rest_2020_v2.model.Dept;
import popschool.rest_2020_v2.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, String> {

    List<Employee> findAll();

    Optional<Employee> findById(String empno);

    List<Employee> findAllByDept(Dept dept);

}
