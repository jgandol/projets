package popschool.rest_2020_v2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Dept {
    @Id
    @Column(name = "deptno")
    private String deptno;

    @Column(name = "deptname")
    private String deptname;

    private String localisation;

    @OneToMany(mappedBy = "dept")
    @JsonIgnore
    private List<Employee> employees = new ArrayList<>();

    public Dept(String deptno, String deptname, String localisation) {
        this.deptno = deptno;
        this.deptname = deptname;
        this.localisation = localisation;
    }

    public Dept() {
    }


}
