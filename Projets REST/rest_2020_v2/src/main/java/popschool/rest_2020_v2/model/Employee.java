package popschool.rest_2020_v2.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Employee {
    @Id
    @Column(name = "empno")
    private String empno;

    @Column(name = "empname")
    private String empname;

    private String position;
    private Double salary = 0.0;

    @ManyToOne
    @JoinColumn(name = "deptno")
    private Dept dept;

    public Employee() {
    }

    public Employee(String empno, String empname, String position) {
        this.empno = empno;
        this.empname = empname;
        this.position = position;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "empno='" + empno + '\'' +
                ", empname='" + empname + '\'' +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                ", dept=" + dept.getDeptname() +
                '}';
    }
}