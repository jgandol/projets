create table if not exists dept (
        deptno varchar(4) not null,
        deptname varchar(25) not null,
        localisation varchar(20) not null,
        constraint pk_dept primary key (deptno)
);
create table if not exists employee (
        empno varchar(4) not null,
        empname varchar(25) not null,
        position varchar(15) not null,
        salary double ,
        deptno varchar(4),
        constraint pk_employee primary key (empno)
);

alter table employee
  add constraint fk_employee_deptno foreign key (deptno) references dept(deptno);