package popschool.tacos_jpa_client.web;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import popschool.tacos_jpa_client.model.Ingredient;
import popschool.tacos_jpa_client.service.ServiceTacosClient;


@Component
public class IngredientByIdConverter implements Converter<String, Ingredient> {

    private ServiceTacosClient serviceTacosClient;

    @Autowired
    public IngredientByIdConverter(ServiceTacosClient serviceTacosClient) {
        this.serviceTacosClient = serviceTacosClient;
    }

    @Override
    public Ingredient convert(String id) {
        Optional<Ingredient> optionalIngredient = serviceTacosClient.findIngredientById(id);
        return optionalIngredient.isPresent() ?
                optionalIngredient.get() : null;
    }

}
