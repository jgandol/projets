package popschool.tacos_jpa_client.web;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;
import popschool.tacos_jpa_client.model.Ingredient;
import popschool.tacos_jpa_client.model.Ingredient.Type;
import popschool.tacos_jpa_client.model.Order;
import popschool.tacos_jpa_client.model.Taco;
import popschool.tacos_jpa_client.service.ServiceTacosClient;


@Controller
@RequestMapping("/design")
@SessionAttributes("order")
public class DesignTacoController {
  private ServiceTacosClient serviceTacosClient;

  /*
  public DesignTacoController(IngredientRepository ingredientRepo) {
    this.ingredientRepo = ingredientRepo;
  }
   */

  @Autowired
  public DesignTacoController(
          ServiceTacosClient serviceTacosClient)
  {
    this.serviceTacosClient = serviceTacosClient;
  }

  @ModelAttribute(name = "order")
  public Order order() {
    return new Order();
  }

  @ModelAttribute(name = "design")
  public Taco design() {
    return new Taco();
  }

//  @GetMapping
//  public String showDesignForm(Model model){
//    String URL_INGREDIENTS_BY_TYPE = "http://localhost:8080/ingredients/type/";
//
//    Type[] types = Type.values();
//    for(Type type:types){
//
//      RestTemplate restTemplate = new RestTemplate();
//      Ingredient[] ingredientsByType = restTemplate.getForObject(
//              URL_INGREDIENTS_BY_TYPE + type.toString(), Ingredient[].class);
//      model.addAttribute(type.toString().toLowerCase(),ingredientsByType);
////      System.out.println(ingredientsByType);
//    }
//    return "design";
//  }

  @GetMapping
  public String showDesignForm(Model model) {
    List<Ingredient> ingredients = new ArrayList<>();
    serviceTacosClient.findAllIngredients().forEach(i -> ingredients.add(i));

    Type[] types = Ingredient.Type.values();
    for (Type type : types) {
      model.addAttribute(type.toString().toLowerCase(), filterByType(ingredients, type));
    }

    return "design";
  }

  @PostMapping
  public String processDesign(
      @Valid Taco taco, Errors errors,
      @ModelAttribute Order order) {

    if (errors.hasErrors()) {
      return "redirect:/design";
    }

    Taco saved = serviceTacosClient.saveTaco(taco);
//    saved.setIngredients(taco.getIngredients());
//    System.out.println(saved);
//    List<Taco> tacos = new ArrayList<>();
//    tacos.add(saved);
    order.getTacos().add(saved);
    System.out.println(order);

    return "redirect:/orders/current";
  }


  private List<Ingredient> filterByType(
      List<Ingredient> ingredients, Type type)
  {
    return ingredients
              .stream()
              .filter(x -> x.getType().equals(type))
              .collect(Collectors.toList());
  }



}

