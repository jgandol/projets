package popschool.tacos_jpa_client.web;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.client.RestTemplate;
import popschool.tacos_jpa_client.model.Ingredient;
import popschool.tacos_jpa_client.model.Ingredient.Type;
import popschool.tacos_jpa_client.model.Order;
import popschool.tacos_jpa_client.service.ServiceTacosClient;


@Controller
@RequestMapping("/orders")
@SessionAttributes("order")
public class OrderController {
  
  private ServiceTacosClient serviceTacosClient;

  public OrderController(ServiceTacosClient serviceTacosClient) {
    this.serviceTacosClient = serviceTacosClient;
  }
  
  @GetMapping("/current")
  public String orderForm() {
    return "orderForm";
  }

  @PostMapping
  public String processOrder(@Valid@ModelAttribute Order order, Errors errors, SessionStatus sessionStatus) {
    if (errors.hasErrors()) {
      return "orderForm";
    }
//    System.out.println(order);
    serviceTacosClient.saveOrder(order);
    sessionStatus.setComplete();
    
    return "redirect:/";
  }

}
