package popschool.tacos_jpa_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TacosJpaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(TacosJpaClientApplication.class, args);
    }

}
