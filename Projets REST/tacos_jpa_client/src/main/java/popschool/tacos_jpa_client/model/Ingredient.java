package popschool.tacos_jpa_client.model;


import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Data
@RequiredArgsConstructor
@NoArgsConstructor(access=AccessLevel.PRIVATE, force=true)
// erreur normale car une entite ne peut avoir un constructeur sans arg. private MAIS spring l'autorise!!!
public class Ingredient implements Serializable {

  private final String id;
  private final String name;
  private final Type type;
  
  public  enum Type {
    WRAP, PROTEIN, VEGGIES, CHEESE, SAUCE
  }

}
