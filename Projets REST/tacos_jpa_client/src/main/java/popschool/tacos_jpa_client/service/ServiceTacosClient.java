package popschool.tacos_jpa_client.service;

import org.springframework.http.ResponseEntity;
import popschool.tacos_jpa_client.model.Ingredient;
import popschool.tacos_jpa_client.model.Order;
import popschool.tacos_jpa_client.model.Taco;

import java.util.List;
import java.util.Optional;

public interface ServiceTacosClient {
    List<Ingredient> findAllIngredients();

    Taco saveTaco(Taco taco);

    Order saveOrder(Order order);

    Optional<Ingredient> findIngredientById(String id);
}
