package popschool.tacos_jpa_client.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import popschool.tacos_jpa_client.model.Ingredient;
import popschool.tacos_jpa_client.model.Order;
import popschool.tacos_jpa_client.model.Taco;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class ServiceTacosClientImp implements ServiceTacosClient{
    static final String URL_TACOS = "http://localhost:8080/tacos";
    static final String URL_ORDERS = "http://localhost:8080/orders";
    static final String URL_INGREDIENTS = "http://localhost:8080/ingredients";
    private RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();


    @Override
    public List<Ingredient> findAllIngredients() {
        List<Ingredient> ingredients = Arrays.asList(restTemplate.getForObject(URL_INGREDIENTS, Ingredient[].class));
        return ingredients;
    }


//    @Override
//    public void validerCommande(Commande c) {
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
//        headers.setContentType(MediaType.APPLICATION_JSON);
//
//        // Data attached to the request.
//        HttpEntity<Commande> requestBody = new HttpEntity<>(c, headers);
//        // Send request with POST method.
//        // BUG
//        restTemplate.postForObject(URL_COMMANDES + "/validate", requestBody, Commande.class);
//
//        // Send request with POST method.
////        Commande commande = restTemplate.postForObject(URL_COMMANDES + "/validate", requestBody, Commande.class);
////        if (result.getStatusCode() == HttpStatus.OK) {
////            Commande commande = result.getBody();
////            System.out.println("(Client Side) Commande sauvegardée: "+ commande.getNcommande());
////        }
//    }

    @Override
    public Taco saveTaco(Taco taco) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Taco> requestBody = new HttpEntity<>(taco, headers);
        Taco tako = restTemplate.postForObject(URL_TACOS, requestBody, Taco.class);
        return tako;
    }

    @Override
    public Order saveOrder(Order order) {
        System.out.println("CLIENT: " + order);

        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Order> requestBody = new HttpEntity<>(order,headers);
        Order ord = restTemplate.postForObject(URL_ORDERS, requestBody, Order.class);
        return ord;
    }

    @Override
    public Optional<Ingredient> findIngredientById(String id) {
        Ingredient ingredient = restTemplate.getForObject(URL_INGREDIENTS + "/"+id, Ingredient.class);
        return Optional.of(ingredient);
    }
}
