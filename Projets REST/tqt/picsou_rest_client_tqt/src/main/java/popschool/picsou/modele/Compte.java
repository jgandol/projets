package popschool.picsou.modele;


import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class Compte {
    private int idCompte;
    private String num;
    private Timestamp dateOpen;
    private Double plafond;
    private String bloque;
    private double solde;
    private Client client;
    private List<Mouvement> mouvements;

    public Compte() {
    }

    public Compte(String num) {
        this.num = num;
        this.client = client;
        this.dateOpen = new Timestamp(System.currentTimeMillis());
        this.bloque = "non";
        this.solde = 0.0;
        this.plafond = 0.0;
        this.client = null;
    }

    @Override
    public String toString() {
        return "Compte{" +
                "idCompte=" + idCompte +
                ", num='" + num + '\'' +
                ", dateOpen=" + dateOpen +
                ", plafond=" + plafond +
                ", bloque='" + bloque + '\'' +
                ", solde=" + solde +
                ", client=" + client.getNom() + " " + client.getPrenom() +
                '}';
    }
}
