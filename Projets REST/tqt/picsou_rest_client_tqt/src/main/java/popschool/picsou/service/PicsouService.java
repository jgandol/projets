package popschool.picsou.service;

import org.springframework.stereotype.Service;
import popschool.picsou.modele.Client;
import popschool.picsou.modele.Compte;

import java.util.List;

@Service
public interface PicsouService {
    List<Client> findAllClients();

    List<Compte> findCompteByClient(int id);

    void crediter(int id, double montant);

    void debiter(int id, double montant);

    List<Compte> findAllComptes();

    void transfert(Integer id_origin, Integer id_dest, Double montant);
}
