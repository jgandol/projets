package popschool.picsou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PicsouApplication {

    public static void main(String[] args) {
        SpringApplication.run(PicsouApplication.class, args);
    }

}
