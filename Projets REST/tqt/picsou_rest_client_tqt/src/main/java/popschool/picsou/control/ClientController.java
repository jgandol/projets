package popschool.picsou.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.picsou.service.PicsouService;

@RestController
public class ClientController {

    @Autowired
    private PicsouService picsouService;


    //    Injectez (inject) via application.properties.
    @Value("${welcome.message}")
    private String message;
    @Value("${error.message}")
    private String errorMessage;


    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        return "index";
    }

    @GetMapping(value = {"/clientsList"})
    public String clientsList(Model model) {
        model.addAttribute("listClient", picsouService.findAllClients());
        return "clientsList";
    }

    @GetMapping("/comptesClient/{id}")
    public String comptesClient(@PathVariable("id") int id, Model model) {
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
        model.addAttribute("id", id);
        return "comptesClient";
    }

    @GetMapping(value = {"/comptesClient/{id}/credit"})
    public String creditByID(@PathVariable("id") int id, Model model) {
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));

        return "credit";
    }

    @PostMapping(value = {"credit"})
    public String credit(@RequestParam("comptechoisi") int id, @RequestParam("montant") double montant, Model model) {
        picsouService.crediter(id, montant);
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
//        model.addAttribute("id",id);
        id = picsouService.findCompteByClient(id).get(1).getClient().getIdClient();
        return "redirect:/comptesClient/{" + id + "}/credit";
    }

    @GetMapping(value = {"/comptesClient/{id}/debit"})
    public String debitById(@PathVariable("id") int id, Model model) {
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
        return "debit";
    }

    @PostMapping(value = {"/debit"})
    public String debit(@RequestParam("comptechoisideb") int id, @RequestParam("montant") double montant, Model model) {
        picsouService.debiter(id, montant);
//        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
        return "index";
    }

    @GetMapping(value = {"/comptesClient/{id}/transfert"})
    public String transfertById(@PathVariable("id") int id, Model model) {
        model.addAttribute("ensCompteClient", picsouService.findCompteByClient(id));
        model.addAttribute("ensComptes", picsouService.findAllComptes());
        return "transfert";
    }

    @PostMapping(value = {"/transfert"})
    public String transfert(@RequestParam("compteorigin") Integer id_origin, @RequestParam("comptedest") Integer id_dest,
                            @RequestParam("montant") Double montant, Model model) {
        System.out.println(id_origin + " | " + id_dest + " | " + montant);
        picsouService.transfert(id_origin, id_dest, montant);
        model.addAttribute("ensComptes", picsouService.findAllComptes());
        return "index";
    }

}
