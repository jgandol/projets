package popschool.picsou.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import popschool.picsou.modele.Client;
import popschool.picsou.modele.Compte;

import java.util.Arrays;
import java.util.List;

@Service
public class PicsouServiceImp implements PicsouService {
    static final String URL_CLIENT = "http://localhost:8080/clients";
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<Client> findAllClients() {
        List<Client> list = Arrays.asList(restTemplate.getForObject(URL_CLIENT, Client[].class));
        System.out.println(list);
        return list;
    }

    @Override
    public List<Compte> findCompteByClient(int id) {
        return null;
    }

    @Override
    public void crediter(int id, double montant) {

    }

    @Override
    public void debiter(int id, double montant) {

    }

    @Override
    public List<Compte> findAllComptes() {
        return null;
    }

    @Override
    public void transfert(Integer id_origin, Integer id_dest, Double montant) {

    }
}
