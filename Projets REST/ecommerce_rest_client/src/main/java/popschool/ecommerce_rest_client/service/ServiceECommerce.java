package popschool.ecommerce_rest_client.service;



import popschool.ecommerce_rest_client.model.Client;
import popschool.ecommerce_rest_client.model.Commande;
import popschool.ecommerce_rest_client.model.Produit;

import java.util.List;

public interface ServiceECommerce {
    List<Client> findAllClient();
    List<Commande> findAllCommande();
//    List<Detailcde> findAllDetailCde();
    List<Produit> findAllProduit();
    Boolean UserExist(String nom, String mdp);
    void validerCommande(Commande c);

    Produit findByNproduit(int i);

    Client findClientByNom(String login);
}
