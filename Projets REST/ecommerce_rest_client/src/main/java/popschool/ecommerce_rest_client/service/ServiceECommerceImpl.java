package popschool.ecommerce_rest_client.service;

import org.springframework.boot.CommandLineRunner;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import popschool.ecommerce_rest_client.model.Client;
import popschool.ecommerce_rest_client.model.Commande;
import popschool.ecommerce_rest_client.model.Produit;

import java.util.Arrays;
import java.util.List;

@Service
public class ServiceECommerceImpl implements ServiceECommerce{
    static final String URL_CLIENTS = "http://localhost:8080/clients";
    static final String URL_COMMANDES = "http://localhost:8080/commandes";
    static final String URL_PRODUITS = "http://localhost:8080/produits";
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<Client> findAllClient() {
        List<Client> clients = Arrays.asList(restTemplate.getForObject(URL_CLIENTS, Client[].class));
        return clients;
    }

    @Override
    public List<Commande> findAllCommande() {
        List<Commande> commandes = Arrays.asList(restTemplate.getForObject(URL_COMMANDES, Commande[].class));
        return commandes;
    }

    @Override
    public List<Produit> findAllProduit() {
        List<Produit> produits = Arrays.asList(restTemplate.getForObject(URL_PRODUITS, Produit[].class));
        return produits;
    }

    @Override
    public Boolean UserExist(String nom, String mdp) {
        Client client = new Client(nom,mdp);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Data attached to the request.
        HttpEntity<Client> requestBody = new HttpEntity<>(client, headers);

        // Send request with POST method.
        Boolean b = restTemplate.postForObject(URL_CLIENTS + "/existe", requestBody, Boolean.class);

        if (b) {
            System.out.println("Le client existe");
            // Send request with GET method and default Headers.
            return true;
        } else {
            System.out.println("Client inexistant");
            return false;
        }
    }

    @Override
    public void validerCommande(Commande c) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Data attached to the request.
        HttpEntity<Commande> requestBody = new HttpEntity<>(c, headers);
        // Send request with POST method.
        // BUG
        restTemplate.postForObject(URL_COMMANDES + "/validate", requestBody, Commande.class);

        // Send request with POST method.
//        Commande commande = restTemplate.postForObject(URL_COMMANDES + "/validate", requestBody, Commande.class);
//        if (result.getStatusCode() == HttpStatus.OK) {
//            Commande commande = result.getBody();
//            System.out.println("(Client Side) Commande sauvegardée: "+ commande.getNcommande());
//        }
    }

    @Override
    public Produit findByNproduit(int i) {
        Produit produit = restTemplate.getForObject(URL_PRODUITS + "/" + i,Produit.class);
        return produit;
    }

    @Override
    public Client findClientByNom(String login) {
        Client client = restTemplate.getForObject(URL_CLIENTS + "/" + login,Client.class);
        return client;
    }
}
