package popschool.ecommerce_rest_client.model;

import lombok.Data;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class Commande {
    private Integer ncommande;
    private Date datecde;
    private Client client;
    private List<Detailcde> detailcdes = new ArrayList<>();

    public Commande() {
        this.datecde = new Date(System.currentTimeMillis());
    }

    public Commande(int i) {
        this.ncommande = i;
        this.datecde = new Date(System.currentTimeMillis());
    }


    @Override
    public String toString() {
        return "Commande{" +
                "ncommande=" + ncommande +
                ", datecde=" + datecde +
                ", client=" + client.getNom() +
                '}';
    }
}
