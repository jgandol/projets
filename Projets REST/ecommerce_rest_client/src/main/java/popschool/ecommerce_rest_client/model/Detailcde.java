package popschool.ecommerce_rest_client.model;

import lombok.Data;

@Data
public class Detailcde {
    private Integer ndetail;
    private int quantite;
    private Commande commande;
    private Produit produit;

    public Detailcde() {
    }

    public Detailcde(int quantite, Commande commande, Produit produit) {
        this.quantite = quantite;
        this.commande = commande;
        this.produit = produit;
    }


    @Override
    public String toString() {
        return "Detailcde{" +
                "ndetail=" + ndetail +
                ", quantite=" + quantite +
                '}';
    }
}
