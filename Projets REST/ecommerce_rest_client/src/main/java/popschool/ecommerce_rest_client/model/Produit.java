package popschool.ecommerce_rest_client.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Produit {
    private Integer nproduit;
    private String descriptif;
    private double prix;
    private String disponible;
    private int qteenstock;
    private List<Detailcde> detailcdes = new ArrayList<>();

    public Produit() {
    }


    @Override
    public String toString() {
        return "Produit{" +
                "nproduit=" + nproduit +
                ", descriptif='" + descriptif + '\'' +
                ", prix=" + prix +
                ", disponible='" + disponible + '\'' +
                ", qteenstock=" + qteenstock +
                '}';
    }
}
