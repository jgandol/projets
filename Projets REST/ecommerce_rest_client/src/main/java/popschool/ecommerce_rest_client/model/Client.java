package popschool.ecommerce_rest_client.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class Client {
    private String nom;
    private String motdepasse;
    private List<Commande> commandes = new ArrayList<>();

    public Client() {
    }

    public Client(String nom, String mdp) {
        this.nom = nom;
        this.motdepasse = mdp;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nom='" + nom + '\'' +
                ", motdepasse='" + motdepasse + '\'' +
                '}';
    }
}
