package popschool.ecommerce_rest_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceRestClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EcommerceRestClientApplication.class, args);
    }

}
