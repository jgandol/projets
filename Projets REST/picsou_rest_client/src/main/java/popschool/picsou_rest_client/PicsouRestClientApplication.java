package popschool.picsou_rest_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PicsouRestClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(PicsouRestClientApplication.class, args);
    }

}
