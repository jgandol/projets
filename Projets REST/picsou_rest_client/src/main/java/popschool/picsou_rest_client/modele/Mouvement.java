package popschool.picsou_rest_client.modele;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class Mouvement {
    private int idMouv;
    private Timestamp dateOperation;
    private double montant;
    private String nature;
    private Compte compte;

    public Mouvement(double montant, String nature) {
        this.montant = montant;
        this.nature = nature;
        this.dateOperation = new Timestamp(System.currentTimeMillis());
    }

    public Mouvement() {
    }

    @Override
    public String toString() {
        return "Mouvement{" +
                "idMouv=" + idMouv +
                ", dateOperation=" + dateOperation +
                ", montant=" + montant +
                ", nature='" + nature + '\'' +
                ", compte=" + compte +
                '}';
    }
}
