package popschool.picsou_rest_client.service;

import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import popschool.picsou_rest_client.modele.Client;
import popschool.picsou_rest_client.modele.Compte;
import popschool.picsou_rest_client.modele.Mouvement;

import java.util.Arrays;
import java.util.List;

@Service
public class PicsouServiceImp implements PicsouService {
    static final String URL_CLIENT = "http://localhost:8080/clients";
    static final String URL_COMPTE = "http://localhost:8080/comptes";
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<Client> findAllClients() {
        List<Client> list = Arrays.asList(restTemplate.getForObject(URL_CLIENT, Client[].class));
//        System.out.println(list);
        return list;
    }

    @Override
    public List<Compte> findCompteByClient(int id) {
        List<Compte> liste = Arrays.asList(restTemplate.getForObject(URL_COMPTE + "/of/" + id,Compte[].class));
//        System.out.println(liste);
        return liste;
    }

    @Override
    public void crediter(int id, double montant) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        restTemplate.postForObject(URL_COMPTE+"/"+id+"/crediter/" + montant,null, Mouvement.class);

//        Mouvement m = restTemplate.getForObject(URL_COMPTE + "/" + id + "/crediter/" + montant,Mouvement.class);
//        System.out.println("Status code:" + result.getStatusCode());// Code = 200.
//         if (result.getStatusCode() == HttpStatus.OK) {
//             Mouvement m = result.getBody();
//             System.out.println("(Client Side) Operation Created: "+ m.getIdMouv());
//         }

        return;
    }

    @Override
    public void debiter(int id, double montant) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        restTemplate.postForObject(URL_COMPTE+"/"+id+"/debiter/" + montant,null, Mouvement.class);
        return;
    }

    @Override
    public List<Compte> findAllComptes() {
        List<Compte> list = Arrays.asList(restTemplate.getForObject(URL_COMPTE, Compte[].class));
//        System.out.println(list);
        return list;
    }

    @Override
    public void transfert(Integer id_origin, Integer id_dest, Double montant) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        restTemplate.postForObject(URL_COMPTE+"/"+id_origin+"/vers/" + id_dest + "/montant/" + montant,null, Mouvement[].class);
        return;
    }
}
