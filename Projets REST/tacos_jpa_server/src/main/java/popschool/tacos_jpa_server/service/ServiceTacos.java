package popschool.tacos_jpa_server.service;

import popschool.tacos_jpa_server.model.Ingredient;
import popschool.tacos_jpa_server.model.Order;
import popschool.tacos_jpa_server.model.Taco;

import java.util.List;
import java.util.Optional;

public interface ServiceTacos {

    List<Ingredient> findAllIngredients();

    Taco saveTaco(Taco taco);

    Order saveOrder(Order order);

    List<Taco> findAllTacos();

    List<Order> findAllOrders();

    Optional<Ingredient> findIngredientById(String id);
}
