package popschool.tacos_jpa_server.service;

import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.tacos_jpa_server.data.IngredientRepository;
import popschool.tacos_jpa_server.data.OrderRepository;
import popschool.tacos_jpa_server.data.TacoRepository;
import popschool.tacos_jpa_server.model.Ingredient;
import popschool.tacos_jpa_server.model.Order;
import popschool.tacos_jpa_server.model.Taco;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceTacosImp implements ServiceTacos {
    @Autowired
    IngredientRepository ingredientRepository;
    @Autowired
    TacoRepository tacoRepository;
    @Autowired
    OrderRepository orderRepository;

    @Override
    public List<Ingredient> findAllIngredients() {
        return ingredientRepository.findAll();
    }

    @Override
    public Taco saveTaco(Taco taco){
//        if(taco == null) throw new NullPointerException();
        return tacoRepository.save(taco);
    }

    @Override
    public Order saveOrder(Order order)  throws IllegalArgumentException{
        try{
            System.out.println("saveOrder 1 :" + order);
            System.out.println("Save: " + orderRepository.save(order));
            return new Order();
        }catch (Exception e){
            throw new IllegalArgumentException();
        }

    }

    @Override
    public List<Taco> findAllTacos() {
        return tacoRepository.findAll();
    }

    @Override
    public List<Order> findAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Optional<Ingredient> findIngredientById(String id) {
        return ingredientRepository.findById(id);
    }
}
