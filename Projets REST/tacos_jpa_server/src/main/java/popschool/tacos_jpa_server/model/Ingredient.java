package popschool.tacos_jpa_server.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@Entity
public class Ingredient {

  public Ingredient(String id, String name, Type type) {
    this.id = id;
    this.name = name;
    this.type = type;
  }

  public Ingredient() {
  }

  @Id
  private String id;
  private String name;
  private Type type;
  
  public  enum Type {
    WRAP, PROTEIN, VEGGIES, CHEESE, SAUCE
  }

}
