// tag::allButDetailProperties[]
package popschool.tacos_jpa_server.model;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name="Taco_order")
public class Order implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
  
  private Date placedAt;
  
//end::allButDetailProperties[]
  private String deliveryName;

  private String deliveryStreet;

  private String deliveryCity;

  private String deliveryState;

  private String deliveryZip;

  private String ccNumber;

  private String ccExpiration;

  private String ccCVV;

  /*
  //tag::allButDetailProperties[]
  ...
  
  //end::allButDetailProperties[]
   */
  
//tag::allButDetailProperties[]
  @ManyToMany(cascade = CascadeType.PERSIST, targetEntity=Taco.class)
  private List<Taco> tacos = new ArrayList<>();
  public void addDesign(Taco design) {
    this.tacos.add(design);
  }
  
//  @PrePersist
  void placedAt() {
    this.placedAt = new Date();
  }
  
}
//end::allButDetailProperties[]
