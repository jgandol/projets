package popschool.tacos_jpa_server.control;

import org.hibernate.tool.schema.internal.exec.ScriptTargetOutputToFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import popschool.tacos_jpa_server.model.Order;
import popschool.tacos_jpa_server.service.ServiceTacos;

import java.util.List;

@RestController
public class OrderControl {
    @Autowired
    ServiceTacos serviceTacos;

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrders(){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(serviceTacos.findAllOrders());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping("/orders")
    public Order saveOrder(@RequestBody Order order){
            System.out.println("SERVER : " + order);
            Order order2 = serviceTacos.saveOrder(order);
            return order2;
    }
}
