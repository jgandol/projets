package popschool.tacos_jpa_server.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import popschool.tacos_jpa_server.data.IngredientRepository;
import popschool.tacos_jpa_server.model.Ingredient;
import popschool.tacos_jpa_server.service.ServiceTacos;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class IngredientControl {

    @Autowired
    ServiceTacos serviceTacos;
    @Autowired
    IngredientRepository ingredientRepository;

    @GetMapping("/ingredients")
    public ResponseEntity<List<Ingredient>> getAllIngredients(){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(serviceTacos.findAllIngredients());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/ingredients/{id}")
    public ResponseEntity<Ingredient> getIngredientsById(@PathVariable("id")String id){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(serviceTacos.findIngredientById(id).get());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }



    @GetMapping("/ingredients/type/{type}")
    public Iterable<Ingredient> allIngredientsByType(@PathVariable Ingredient.Type type){
        List<Ingredient> ingredients = new ArrayList<>();
        ingredientRepository.findAll().forEach(ingredients::add);
        return ingredients.stream().filter(x -> x.getType().equals(type)).collect(Collectors.toList());
    }
}
