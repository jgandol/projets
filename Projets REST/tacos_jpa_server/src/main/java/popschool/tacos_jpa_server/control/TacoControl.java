package popschool.tacos_jpa_server.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import popschool.tacos_jpa_server.model.Ingredient;
import popschool.tacos_jpa_server.model.Order;
import popschool.tacos_jpa_server.model.Taco;
import popschool.tacos_jpa_server.service.ServiceTacos;

import java.util.List;

@RestController
public class TacoControl {
    @Autowired
    ServiceTacos serviceTacos;

    @GetMapping("/tacos")
    public ResponseEntity<List<Taco>> getAllTaco(){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(serviceTacos.findAllTacos());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping("/tacos")
    public ResponseEntity<Taco> saveTacos(@RequestBody Taco taco){
        try{
            System.out.println(taco);
            return ResponseEntity.status(HttpStatus.CREATED).body(serviceTacos.saveTaco(taco));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }


}
