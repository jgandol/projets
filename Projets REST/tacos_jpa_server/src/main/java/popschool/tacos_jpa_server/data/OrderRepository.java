package popschool.tacos_jpa_server.data;

import org.springframework.data.repository.CrudRepository;
import popschool.tacos_jpa_server.model.Order;

import java.util.List;


public interface OrderRepository 
         extends CrudRepository<Order, Long> {
    List<Order> findAll();

}
