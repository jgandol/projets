package popschool.tacos_jpa_server.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import popschool.tacos_jpa_server.model.Ingredient;

import java.util.List;

public interface IngredientRepository 
         extends CrudRepository<Ingredient, String> {
    List<Ingredient> findAll();
}
