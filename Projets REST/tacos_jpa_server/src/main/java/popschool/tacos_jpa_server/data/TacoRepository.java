package popschool.tacos_jpa_server.data;

import org.springframework.data.repository.CrudRepository;
import popschool.tacos_jpa_server.model.Taco;

import java.util.List;


public interface TacoRepository 
         extends CrudRepository<Taco, Long> {
    List<Taco> findAll();

}
