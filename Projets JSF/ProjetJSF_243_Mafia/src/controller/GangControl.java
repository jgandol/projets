package controller;

import modele.Gangster;
import modele.Organisation;
import service.Service;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "gangCtrl")
@ViewScoped
public class GangControl implements Serializable{

    private Gangster newGang = new Gangster();
    private int idGangsterChoisi = 0;

    public Gangster getNewGang() {
        return newGang;
    }
    public void setNewGang(Gangster newGang) {
        this.newGang = newGang;
    }

    public int getIdGangsterChoisi() {
        return idGangsterChoisi;
    }
    public void setIdGangsterChoisi(int idGangsterChoisi) {
        this.idGangsterChoisi = idGangsterChoisi;
    }

    public String addGangster(){
        Service.getSingleton().createGangster(newGang);
        return "index";
    }

    public List<Gangster> getListeGangsters(){
        return Service.getSingleton().findAllGangsters();
    }

    public String deleteGangster(){
//        System.out.println(gangsterChoisi);
//        System.out.println(idGangsterChoisi);
        Service.getSingleton().removeGangster(idGangsterChoisi);
        return "gangSuppression";
    }

    //-------------------- Ajout dans organisation

    private String orgNameChoisi;
    private String gangNameChoisi;

    public String getOrgNameChoisi() {
        return orgNameChoisi;
    }
    public void setOrgNameChoisi(String orgNameChoisi) {
        this.orgNameChoisi = orgNameChoisi;
    }
    public String getGangNameChoisi() {
        return gangNameChoisi;
    }
    public void setGangNameChoisi(String gangNameChoisi) {
        this.gangNameChoisi = gangNameChoisi;
    }

    public List<Gangster> getGangsters(){
        return Service.getSingleton().findAllGangsters();
    }

    public List<Organisation> getOrgs(){
        return Service.getSingleton().findAllOrganisation();
    }

    public String doAffecterGangToOrg(){
//        System.out.println(gangNameChoisi);
//        System.out.println(orgNameChoisi);
            Service.getSingleton().addGangsterToOrganisation(gangNameChoisi, orgNameChoisi);
        this.orgNameChoisi=null;
        this.gangNameChoisi=null;
            return "index";
    }

    //------------------------------- Choix du boss
    public List<Gangster> getGangsterOfOrg(){
        if(this.orgNameChoisi == null) return null;
        return Service.getSingleton().findGangstersOfOrg(this.orgNameChoisi);
    }

    public String doChoixDuBoss(){
        System.out.println(gangNameChoisi);
        System.out.println(orgNameChoisi);
        Service.getSingleton().addTheBossToOrganisation(gangNameChoisi,orgNameChoisi);
        this.orgNameChoisi=null;
        this.gangNameChoisi=null;
        return "index";
    }

}
