package controller;

import modele.Gangster;
import modele.Organisation;
import service.Service;

import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "orgCtrl")
public class OrgControl implements Serializable {

    //------------------------ Ajout Organisation
    private Organisation newOrg = new Organisation();

    public Organisation getNewOrg() {
        return newOrg;
    }
    public void setNewOrg(Organisation newOrg) {
        this.newOrg = newOrg;
    }

    public String addOrganisation(){
        Service.getSingleton().createOrganisation(newOrg);
        return "index";
    }

    public List<Organisation> getListeOrganisations(){
        return Service.getSingleton().findAllOrganisation();
    }

    public String deleteOrganisation(Organisation org){
        Service.getSingleton().removeOrganisation(org);
        return "orgSuppression";
    }

    // ---------------------------- Gangsters de l'org
    private String nameChoisi = null;

    public String getNameChoisi(){
        return nameChoisi;
    }
    public void setNameChoisi(String nameChoisi) {
        this.nameChoisi = nameChoisi;
    }

    public List<Gangster> gangstersOfOrg = null;

    public List<Gangster> getGangstersOfOrg(){
        return Service.getSingleton().findGangstersOfOrg(nameChoisi);
    }

    public String doShowGangstersOfOrg(){
        return null;
    }


}
