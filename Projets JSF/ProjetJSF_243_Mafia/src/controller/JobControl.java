package controller;

import modele.Job;
import service.Service;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.xml.soap.Name;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "jobctrl")
@ViewScoped
public class JobControl implements Serializable {
    //------------------ Ajout
    private Job newJob = new Job();

    public Job getNewJob() {
        return newJob;
    }
    public void setNewJob(Job newJob) {
        this.newJob = newJob;
    }

    public String doAjoutJob(){
        Service.getSingleton().createJob(newJob);
        return "index";
    }

    ///-----------------------------------update job
    public List<Job> jobs=null;

    public List<Job> getJobs(){
        return Service.getSingleton().findAllJob();
    }
    public Job jobAModifier = null;
    public Job getJobAModifier(){
        System.out.println(jobChoisi);
        if(jobChoisi == 0) return null;
        jobAModifier = Service.getSingleton().findJobById(jobChoisi);
        return jobAModifier;
    }
    public int jobChoisi = 0;
    public int getJobChoisi() {
        return jobChoisi;
    }
    public void setJobChoisi(int jobChoisi) {
        this.jobChoisi = jobChoisi;
        System.out.println(jobChoisi);
    }
    public String doJobUpdate(){
        System.out.println(jobAModifier);
        if (jobAModifier == null)return null;
        Service.getSingleton().updateJob(jobAModifier);
        return "index";
    }
    ///-------------------do page
    public String doJobU(){
        return "jobUpdate";
    }
    public String doJobAjout(){
        return "jobAjout";
    }
}
