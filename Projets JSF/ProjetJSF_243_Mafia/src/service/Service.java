package service;

import modele.Gangster;
import modele.Job;
import modele.Organisation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

public class Service {
    private static Service service = new Service();

    public static Service getSingleton() {
        return service;
    }

    private EntityManagerFactory emf = JPAUtil.getEmf();

    //    a. Afficher l'ensemble des Organisations
    public List<Organisation> findAllOrganisation() {
        List<Organisation> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Organisation.findAll", Organisation.class).getResultList();
        em.getTransaction().commit();
        return liste;
    }

    //    b. creer organisation
    public void createOrganisation(Organisation org) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(org);
        em.getTransaction().commit();
        em.close();
    }

    public void createOrganisation(String orgName, String description) {
        //Petit bras
        Organisation org = new Organisation(orgName, description);
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(org);
        em.getTransaction().commit();
        em.close();
    }

    //    c. supprimer une organisation
    public void removeOrganisation(Organisation org) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        //Ligne de la mort qui tue : em.remove(em.merge(org))
        em.remove(em.merge(org));
        em.getTransaction().commit();
        em.close();
    }

    //    d. Modifier une organisation
    public void updateOrganisation(Organisation org) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        org = em.merge(org);
        em.getTransaction().commit();
        em.close();
    }


    //    e. Afficher l'ensemble des Gangsters
    public List<Gangster> findAllGangsters() {
        List<Gangster> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Gangster.findAll", Gangster.class).getResultList();
        em.getTransaction().commit();
        return liste;
    }

    //    f. creer gangster
    public void createGangster(Gangster g) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(g);
        em.getTransaction().commit();
        em.close();
    }

    public void createJob(Job job) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(job);
        em.getTransaction().commit();
        em.close();
    }

    public List<Job> findAllJob(){
        return emf.createEntityManager().createNamedQuery("Job.findAll",Job.class).getResultList();
    }

    //    g. supprimer une organisation
    public void removeGangster(Gangster g) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        //Ligne de la mort qui tue : em.remove(em.merge(org))
        em.remove(em.merge(g));
        em.getTransaction().commit();
        em.close();
    }

    public void removeGangster(int gangsterId) {
        EntityManager em = emf.createEntityManager();
        Gangster g = em.createNamedQuery("Gangster.findById", Gangster.class).setParameter(1, gangsterId).getSingleResult();
        removeGangster(g);
    }

    //    h. Modifier une organisation
    public void updateGangster(Gangster g) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        g = em.merge(g);
        em.getTransaction().commit();
        em.close();
    }

    //    i.ajouter gangster a organisation
    public void addGangsterToOrganisation(String gname, String orgName) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster g = em.createNamedQuery("Gangster.findByGname", Gangster.class).setParameter("gname",
                gname).getSingleResult();
        Organisation org = em.createNamedQuery("Organisation.findByOrgName", Organisation.class).setParameter(
                "orgName", orgName.toUpperCase()).getSingleResult();
        if (g != null && org != null) {
            org.addGangster(g);
        }
        em.getTransaction().commit();
        em.close();
    }

    public void removeGangsterFromOrganisation(String gname) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster g = em.createNamedQuery("Gangster.findByGname", Gangster.class).setParameter("gname",
                gname).getSingleResult();
        g.removeOrganisation();
        em.getTransaction().commit();
        em.close();
    }

    public void removeGangsterFromOrganisation(int gangsterId) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster g = em.find(Gangster.class, gangsterId);
        g.removeOrganisation();
        em.getTransaction().commit();
        em.close();
    }

    public void addTheBossToOrganisation(String gname, String orgName) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster g = em.createNamedQuery("Gangster.findByGname", Gangster.class).setParameter("gname",
                gname).getSingleResult();
        Organisation o = em.createNamedQuery("Organisation.findByOrgName", Organisation.class).setParameter("orgName",
                orgName).getSingleResult();
        o.setBoss(g);
        em.getTransaction().commit();
        em.close();
    }

    public void addTheBossToOrganisation(int gangsterId, String orgName) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster g = em.find(Gangster.class, gangsterId);
        Organisation o = em.createNamedQuery("Organisation.findByOrgName", Organisation.class).setParameter("orgName",
                orgName).getSingleResult();
        o.setBoss(g);
        em.getTransaction().commit();
        em.close();
    }


    public void addJobToGangster(String gname, String jobName) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster g = em.createNamedQuery("Gangster.findByGname", Gangster.class).setParameter("gname",
                gname).getSingleResult();
        Job j = em.createNamedQuery("Job.findByJobName", Job.class).setParameter("jobName",
                jobName).getSingleResult();
        if (g.getJobs().contains(j)) return;
        g.getJobs().add(j);
        j.getGangsters().add(g);
        em.getTransaction().commit();
        em.close();
    }

    public void addJobToGangster(int gangsterId, int jobId) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Gangster g = em.find(Gangster.class, gangsterId);
        Job j = em.find(Job.class, jobId);
        if (g.getJobs().contains(j)) return;
        g.getJobs().add(j);
        j.getGangsters().add(g);
        em.getTransaction().commit();
        em.close();
    }

    public List<Gangster> findGangstersOfOrg(String nameChoisi) {
        return emf.createEntityManager().createNamedQuery("Gangster.findGangsterOfOrg", Gangster.class)
                .setParameter("orgName", nameChoisi).getResultList();
    }

    public Job findJobByName(String name) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Job j = em.createNamedQuery("Job.findByJobName", Job.class).setParameter("jobName",name).getSingleResult();
        em.close();
        return j;
    }

    public void updateJob(Job job) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
            job = em.merge(job);
            em.getTransaction().commit();
            em.close();
    }

    public Job findJobById(int id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Job j = em.createNamedQuery("Job.findByJobId", Job.class).setParameter("jobId",id).getSingleResult();
        em.close();
        return j;
    }
}
