package service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

// Creation de la fabrique d'entity manager
public class JPAUtil {
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("243MAFIA");

    public static EntityManagerFactory getEmf(){
        return emf;
    }
}
