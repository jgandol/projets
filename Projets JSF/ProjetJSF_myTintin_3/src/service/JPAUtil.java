package service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
    // Gestion d'un SINGLETON
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintinPU");

    public static EntityManagerFactory getEmf() {return emf;}
}
