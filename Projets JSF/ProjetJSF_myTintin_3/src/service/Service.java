package service;

import control.Client;
import modele.Album;
import modele.Personnage;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

public class Service {
    private static Service service = new Service();

    public static Service getSingleton() {
        return service;
    }

    private EntityManagerFactory emf = JPAUtil.getEmf();

    public List<Album> findAllAlbums() {
        List<Album> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Album.findAll", Album.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }

    public List<Personnage> findAllPersonnages() {
        List<Personnage> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Personnage.findAll", Personnage.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }

    public List<Personnage> findPersonnagesOfAlbum(Album al) {
        List<Personnage> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Personnage.findByAlbum", Personnage.class).setParameter("album",al).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }

    public List<Album> findAlbumsOfPersonnage(int id) {
        List<Album> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Album.findByPersonnage", Album.class).setParameter("id",id).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }

    public Personnage findPersonnageById(Integer id) {
        EntityManager em = emf.createEntityManager();
        Personnage pers = em.find(Personnage.class,id);
        em.close();
        return pers;
    }

    public boolean authentification(Client client){
       return client.getNom().equals("scott")&&client.getMotDePasse().equals("tiger");
    }
}
