package control;

import modele.Album;
import modele.Personnage;
import service.Service;

import javax.faces.bean.ManagedBean;
import java.util.List;

@ManagedBean(name = "ctrl")
public class Control {
    private String nom = "jean";
    private Album albumChoisi = null;


    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Album> getAlbums(){
        return Service.getSingleton().findAllAlbums();
    }
    public Album getAlbumChoisi() {
        return albumChoisi;
    }
    public List<Personnage> getPersonnagesChoisis(){
        return Service.getSingleton().findPersonnagesOfAlbum(albumChoisi);
    }

    //---------- Gestion des personnages
    private Integer idPersonnageChoisi = null;

    public Integer getIdPersonnageChoisi() {
        return idPersonnageChoisi;
    }
    public void setIdPersonnageChoisi(Integer idPersonnageChoisi) {
        this.idPersonnageChoisi = idPersonnageChoisi;
    }

    public List<Personnage> getEnsPersonnages(){
        return Service.getSingleton().findAllPersonnages();
    }


    private Personnage personnageChoisi = null;

    public Personnage getPersonnageChoisi() {
        return personnageChoisi;
    }

    public void setPersonnageChoisi(Personnage personnageChoisi) {
        this.personnageChoisi = personnageChoisi;
    }

    public String doShowAlbumsDuPersonnageChoisi(){
        this.personnageChoisi = Service.getSingleton().findPersonnageById(this.idPersonnageChoisi);
        return "albums";
    }
    public List<Album> getAlbumsChoisis() {
        return Service.getSingleton().findAlbumsOfPersonnage(this.idPersonnageChoisi);
    }

    //---------------------------------------- A c t i o n s
    public String doShowPersonnages(Album album){
        this.albumChoisi=album;
        return "personnages";
    }
    public String doHome(){
        return "index";
    }

    //---------------------------------------- Login
    private Client client = new Client();
    public String doAuthentification(){
        if(client.getNom().equals("scott")&&client.getMotDePasse().equals("tiger")) return "index";
        return null;
    }

    public void setAlbumChoisi(Album albumChoisi) {
        this.albumChoisi = albumChoisi;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
