package modele;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "album")
@NamedQueries({
        @NamedQuery(name = "Album.findAll", query = "select a from Album a"),
        @NamedQuery(name = "Album.findByTitre", query = "select a from Album a where upper(a.titre) = upper(:titre)"),
        @NamedQuery(name = "Album.findById", query = "select a from Album a where a.id = :id "),
        @NamedQuery(name = "Album.findByPersonnage", query = "select a.album from Apparait a where a.personnage.id = :id "),
})
public class Album {
    private int id;
    private String titre;
    private int annee;
    private Album albumBySuivant;
    private Set<Apparait> apparaits = new HashSet<>();

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "annee")
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @ManyToOne
    @JoinColumn(name = "suivant", referencedColumnName = "id")
    public Album getAlbumBySuivant() {
        return albumBySuivant;
    }
    public void setAlbumBySuivant(Album albumBySuivant) {
        this.albumBySuivant = albumBySuivant;
    }

    @OneToMany(mappedBy = "album")
    public Set<Apparait> getApparaits() {
        return apparaits;
    }
    public void setApparaits(Set<Apparait> apparaits) {
        this.apparaits = apparaits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Album)) return false;
        Album album = (Album) o;
        return titre.equals(album.titre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titre);
    }

    @Override
    public String toString() {
        return "Album{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", annee=" + annee +
                '}';
    }
}
