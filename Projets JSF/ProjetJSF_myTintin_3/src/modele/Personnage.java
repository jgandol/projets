package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "personnage")
@NamedQueries({
        @NamedQuery(name = "Personnage.findAll", query = "select p from Personnage p order by p.nom,p.prenom"),
        @NamedQuery(name = "Personnage.findById", query = "select p from Personnage p where p.id = :id "),
        @NamedQuery(name = "Personnage.findByAlbum", query = "select a.personnage from Apparait a where a.album = :album "),
})

public class Personnage implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String nom;
    private String prenom;
    private String profession;
    private String sexe;
    private String genre;
    private Set<Apparait> apparaits = new HashSet<>();

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "profession")
    public String getProfession() {
        return profession;
    }
    public void setProfession(String profession) {
        this.profession = profession;
    }

    @Basic
    @Column(name = "sexe")
    public String getSexe() {
        return sexe;
    }
    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    @Basic
    @Column(name = "genre")
    public String getGenre() {
        return genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }

    @OneToMany(mappedBy = "personnage")
    public Set<Apparait> getApparaits() {
        return apparaits;
    }
    public void setApparaits(Set<Apparait> apparaits) {
        this.apparaits = apparaits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Personnage)) return false;
        Personnage that = (Personnage) o;
        return nom.equals(that.nom) &&
                prenom.equals(that.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }

    @Override
    public String toString() {
        return "Personnage{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", profession='" + profession + '\'' +
                ", sexe='" + sexe + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}
