package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "apparait")
public class Apparait {
    private int id;
    private Album album;
    private Personnage personnage;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Apparait apparaits = (Apparait) o;
        return id == apparaits.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @ManyToOne
    @JoinColumn(name = "album_id", referencedColumnName = "id", nullable = false)
    public Album getAlbum() {
        return album;
    }
    public void setAlbum(Album album) {
        this.album = album;
    }

    @ManyToOne
    @JoinColumn(name = "personnage_id", referencedColumnName = "id", nullable = false)
    public Personnage getPersonnage() {
        return personnage;
    }
    public void setPersonnage(Personnage personnage) {
        this.personnage = personnage;
    }
}
