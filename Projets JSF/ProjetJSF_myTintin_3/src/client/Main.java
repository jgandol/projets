package client;

import modele.Album;
import modele.Apparait;
import service.Service;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Service service = Service.getSingleton();

        List<Album> listeA = service.findAllAlbums();
        for(Album a : listeA){
            System.out.println(a);
            for(Apparait ap : a.getApparaits()){
                System.out.println("  --> " + ap.getPersonnage());
            }
            System.out.println();
        }
    }
}
