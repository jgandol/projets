package service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
    // Gestion d'un Singleton

    private static EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("POPSchool_JSF_JPA_ECommPU");

    public static EntityManagerFactory getEmf() {
        return emf;
    }
}