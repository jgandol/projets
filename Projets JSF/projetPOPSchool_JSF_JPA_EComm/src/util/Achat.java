package util;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author didier
 */
public class Achat implements Serializable{
    private int code;
    private String titre;
    private Double prix;
    private int quantite=1;

    public Achat(int code, int quantite){
        this.setCode(code);
        this.setQuantite(quantite);
    }

    public Achat(int code,String  titre, Double prix,  int quantite){
        this.setCode(code);
        this.setQuantite(quantite);
        this.setTitre(titre);
        this.setPrix(prix);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Achat)) return false;
        Achat achat = (Achat) o;
        return code == achat.code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
