package Control;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "ctrl")
public class Control {
    private String nom = "jean";

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
