package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Albums {
    private int id;
    private String titre;
    private int annee;
    private Albums albumsBySuivant;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "annee")
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Albums albums = (Albums) o;
        return id == albums.id &&
                annee == albums.annee &&
                Objects.equals(titre, albums.titre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titre, annee);
    }

    @ManyToOne
    @JoinColumn(name = "suivant", referencedColumnName = "id")
    public Albums getAlbumsBySuivant() {
        return albumsBySuivant;
    }

    public void setAlbumsBySuivant(Albums albumsBySuivant) {
        this.albumsBySuivant = albumsBySuivant;
    }
}
