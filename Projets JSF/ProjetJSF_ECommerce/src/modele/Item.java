package modele;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "item")
@NamedQueries({
        @NamedQuery(name = "Item.findAll", query = "select i from Item i")
})
public class Item {
    private int id;
    private String categorie;
    private String titre;
    private Double prix;
    private int codeBarre;
    private List<DetailCde> detailCdes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "categorie")
    public String getCategorie() {
        return categorie;
    }
    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "prix")
    public Double getPrix() {
        return prix;
    }
    public void setPrix(Double prix) {
        this.prix = prix;
    }

    @Basic
    @Column(name = "code_barre")
    public int getCodeBarre() {
        return codeBarre;
    }
    public void setCodeBarre(int codeBarre) {
        this.codeBarre = codeBarre;
    }

    public Item(String categorie,String titre,Double prix,int codeBarre) {
        this.setCategorie(categorie);
        this.setTitre(titre);
        this.setPrix(prix);
        this.setCodeBarre(codeBarre);
    }

    public Item() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return id == item.id &&
                codeBarre == item.codeBarre &&
                Objects.equals(categorie, item.categorie) &&
                Objects.equals(titre, item.titre) &&
                Objects.equals(prix, item.prix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categorie, titre, prix, codeBarre);
    }

    @OneToMany(mappedBy = "item")
    public List<DetailCde> getDetailCdes() {
        return detailCdes;
    }
    public void setDetailCdes(List<DetailCde> detailCdes) {
        this.detailCdes = detailCdes;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", categorie='" + categorie + '\'' +
                ", titre='" + titre + '\'' +
                ", prix=" + prix +
                ", codeBarre=" + codeBarre +
                '}' + "\n";
    }
}
