package controller;

import modele.Client;
import modele.Item;
import service.ServiceCart;
import service.ServiceItem;
import util.Achat;
import util.CardExpiredException;
import util.InfoCB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.util.*;

@ManagedBean(name = "cartCtrl")
@SessionScoped
public class CartController implements Serializable {
    private Client client;
    private List<Achat> panier = null;
    private InfoCB infoCB;
    private Boolean authentifie;
    private String numeroCB;
    private Date expirationDate;

    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }
    public List<Achat> getPanier() {
        return panier;
    }
    public void setPanier(List<Achat> panier) {
        this.panier = panier;
    }
    public InfoCB getInfoCB() {
        return infoCB;
    }
    public void setInfoCB(InfoCB infoCB) {
        this.infoCB = infoCB;
    }
    public Boolean getAuthentifie() {
        return authentifie;
    }
    public void setAuthentifie(Boolean authentifie) {
        this.authentifie = authentifie;
    }
    public String getNumeroCB() {
        return numeroCB;
    }

    public void setNumeroCB(String numeroCB) {
        this.numeroCB = numeroCB;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    //Login
    private String nom;
    private String prenom;

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String doLogin() {
        this.client = ServiceCart.getSingleton().findOrCreate(this.nom, this.prenom);
        this.authentifie = true;
        this.panier = new ArrayList<>();
        this.infoCB = new InfoCB();
        return "showItems";
    }

    public String doHome() {
        this.client = null;
        this.authentifie = false;

        return "index";
    }

    //---------------------- ShowItems
    public List<Item> getEnsItems() {
        return ServiceItem.getSingleton().findAllItem();
    }

    private Item itemSelectionne;

    public String doSelectItem(Item item) {
        this.itemSelectionne = item;
        this.panier.add(new Achat(item.getId(), 1, item.getPrix(), item.getTitre()));
        return "showPanier";
    }

    //--------------------- ShowPanier
    private Integer nouvQte;
    private Achat achatSelectionne;


    public Double getTotalPanier() {
        return ServiceCart.getSingleton().valeurDuPanier(panier);
    }

    public void updateQte(ValueChangeEvent event) {
        nouvQte = (Integer) event.getNewValue();
    }

    public String doModifierAchat(Achat achat) {
        this.achatSelectionne = achat;
        achatSelectionne.setQuantite(nouvQte);
        this.panier.remove(achat);
        this.panier.add(achatSelectionne);
        return "showPanier";
    }

    public String doSupprimerAchat(Achat a) {
        this.achatSelectionne = a;
        this.panier.remove(a);

        if (this.panier.isEmpty()) return "showItems";
        return "showPanier";
    }

    public String doListItem(){
        return "showItems";
    }

    //----------------------- Validation
    public String doValidation(){
        this.infoCB = new InfoCB(numeroCB,expirationDate);
        try {
            ServiceCart.getSingleton().validerPanier(client,panier,infoCB);
        } catch (CardExpiredException e) {
            e.printStackTrace();
        }
        return "logout";
    }
}
