package popschool.my_mafia_hateos_client.model;

import lombok.Data;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Data
@RemoteResource("/organisations")
public class Organisation {
    URI id;

    private String orgName;
    private String description;
    private List<Gangster> gangsters = new ArrayList<>();
    private Gangster boss;

    @ResourceId
    public URI getId() {
        return id;
    }

}
