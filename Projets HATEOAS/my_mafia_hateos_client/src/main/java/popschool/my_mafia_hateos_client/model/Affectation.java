package popschool.my_mafia_hateos_client.model;

import lombok.Data;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import javax.annotation.Resource;
import java.net.URI;


@Data
@RemoteResource("/affectations")
public class Affectation {
    URI id;

    private Gangster gangster;
    private Job job;

    @LinkedResource
    public Gangster getGangster() {
        return gangster;
    }
    @LinkedResource
    public Job getJob() {
        return job;
    }
    @ResourceId
    public URI getId() {
        return id;
    }
}
