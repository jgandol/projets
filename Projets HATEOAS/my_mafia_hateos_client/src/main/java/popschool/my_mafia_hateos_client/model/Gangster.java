package popschool.my_mafia_hateos_client.model;

import lombok.Data;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Data
@RemoteResource("/gangsters")
public class Gangster {

    URI id;
    private String gname;
    private String nickname;
    private Integer badness;

    private List<Affectation> affectations = new ArrayList<>();
    private Organisation organisation;
    private Organisation organisations_boss;

    @LinkedResource
    public Organisation getOrganisations_boss() {
        return organisations_boss;
    }
    @LinkedResource
    public Organisation getOrganisation() {
        return organisation;
    }

    @ResourceId
    public URI getId() {
        return id;
    }
}
