package popschool.my_mafia_hateos_client.model;


import lombok.Data;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@RemoteResource("/jobs")
public class Job {
    URI id;
    private String jobName;
    private Integer score;
    private Double setupcost;
    private List<Affectation> affectations = new ArrayList<>();

    @ResourceId
    public URI getId() {
        return id;
    }
}
