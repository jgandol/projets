package popschool.my_mafia_hateos_client;

import popschool.my_mafia_hateos_client.model.Gangster;
import popschool.my_mafia_hateos_client.model.Job;
import popschool.my_mafia_hateos_client.model.Organisation;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import java.net.URI;

public class MainTest {


    public static void main(String args[]) {
//        ClientFactory clientFactory = Configuration.build().buildClientFactory();
//        Client<Pays> client = clientFactory.create(Pays.class);
//        Iterable<Pays> pays = client.getAll(URI.create("http://localhost:8080/pays"));
//        System.out.println(pays);

//        Traverson client = new Traverson(URI.create("http://localhost:8080/"), MediaTypes.HAL_JSON);
//        CollectionModel<EntityModel<Acteur>> acteurs = client.follow("acteurs")
//                .toObject(new TypeReferences.CollectionModelType<EntityModel<Acteur>>() {
//                });
//        acteurs.forEach(r -> {
//            System.out.println(r.getContent());
//        });


        //POST
//        ClientFactory clientFactory = Configuration.build().buildClientFactory();
//        Client<Pays> client = clientFactory.create(Pays.class);
//        client.post(new Pays("Italy"));

//        POST
//        client.post();
        String BASE_URL = "http://localhost:8080/";

        ClientFactory clientFactory = Configuration.build().buildClientFactory();
        Client<Gangster> clientGangster = clientFactory.create(Gangster.class);
        Client<Job> clientJob = clientFactory.create(Job.class);
        Client<Organisation> clientOrganisation = clientFactory.create(Organisation.class);
        Iterable<Gangster> gangsters = clientGangster.getAll(URI.create(BASE_URL + "gangsters"));
        Iterable<Job> jobs = clientJob.getAll(URI.create(BASE_URL + "jobs"));
        Iterable<Organisation> organisations = clientOrganisation.getAll(URI.create(BASE_URL + "organisations"));
        System.out.println(gangsters);
        System.out.println(jobs);
        System.out.println(organisations);

        getGangsterNom("Yojimbo");

    }

    public static void getGangsterNom(String nom){
        ClientFactory clientFactory = Configuration.build().buildClientFactory();
        Client<Gangster> clientGangster = clientFactory.create(Gangster.class);
        Gangster g = clientGangster.get(URI.create("http://localhost:8080/gangsters/search/findByNom?nom="+nom));
        System.out.println(g);
    }

//    public static void PostActeur(String nom, String prenom, int idPays){
//        String URL_LOCAL = "http://localhost:8080/";
//        ClientFactory clientFactory = Configuration.builder().setBaseUri(URL_LOCAL).build().buildClientFactory();
//        Client<Gangster> clientGangster = clientFactory.create(Gangster.class);
////        Client<Acteur> clientActeur = clientFactory.create(Acteur.class);
////        Pays p = clientPays.get(URI.create("pays/"+idPays));
//        Gangster g = clientGangster.get(URI.create("gangsters/1"));

//        Acteur acteur = new Acteur(nom,prenom);
//        acteur.setPays(p);
//        clientActeur.post(acteur);

    }

