package popschool.my_mafia_hateos_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyMafiaHateosClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyMafiaHateosClientApplication.class, args);
    }

}
