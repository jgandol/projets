package popschool.payrool_v4;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.payrool_v4.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
