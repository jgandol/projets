package popschool.payrool_v4;

public enum Status {

    IN_PROGRESS, //
    COMPLETED, //
    CANCELLED
}
