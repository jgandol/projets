package popschool.payrool_v4;

import org.springframework.data.jpa.repository.JpaRepository;
import popschool.payrool_v4.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
