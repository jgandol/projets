package popschool.payrool_v4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PayroolV4Application {

    public static void main(String[] args) {
        SpringApplication.run(PayroolV4Application.class, args);
    }

}
