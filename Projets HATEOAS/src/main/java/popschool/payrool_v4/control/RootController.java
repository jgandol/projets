package popschool.payrool_v4.control;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import popschool.payrool_v4.control.EmployeeController;
import popschool.payrool_v4.control.OrderController;

@RestController
class RootController {

    @GetMapping
    RepresentationModel<?> index() {

        RepresentationModel<?> rootModel = new RepresentationModel<>();
        rootModel.add(linkTo(methodOn(EmployeeController.class).all()).withRel("employees"));
        rootModel.add(linkTo(methodOn(OrderController.class).all()).withRel("orders"));
        return rootModel;
    }

}
