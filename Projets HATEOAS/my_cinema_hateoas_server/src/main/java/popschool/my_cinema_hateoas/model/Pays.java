package popschool.my_cinema_hateoas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PAYS")
@Data
public class Pays {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "npays")
    Long npays;
    String nom;

    @OneToMany(mappedBy = "pays",fetch = FetchType.EAGER)
    @JsonIgnore
    List<Acteur> acteurs ;

    @OneToMany(mappedBy = "pays")
    List<Film> films;

    @Override
    public String toString() {
        return "Pays{" +
                "nom='" + nom + '\'' +
                '}';
    }
}
