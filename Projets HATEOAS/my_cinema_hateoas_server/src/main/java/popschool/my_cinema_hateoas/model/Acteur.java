package popschool.my_cinema_hateoas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ACTEUR")
@Data
public class Acteur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "nacteur")
    Long nacteur;

    @Basic
    @Column(name = "nom")
    String nom;

    @Basic
    @Column(name = "prenom")
    String prenom;

    @Basic
    @Column(name = "naissance")
    Date naissance;

    @Basic
    @Column(name = "nbreFilms")
    Integer nbrefilms;


    @ManyToOne
    @JoinColumn(name = "nationalite")
    Pays pays;

    @OneToMany(mappedBy = "nacteurprincipal")

    List<Film> filmsbyacteur = new ArrayList<>();

    public Acteur() {
    }



}
