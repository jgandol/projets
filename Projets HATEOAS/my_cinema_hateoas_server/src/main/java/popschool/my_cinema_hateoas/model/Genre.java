package popschool.my_cinema_hateoas.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "GENRE")
@Data
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ngenre")
    Long ngenre;
    String nature;

    @OneToMany
    @JoinColumn(name = "ngenre")
    @JsonIgnore
    List<Film> filmsbygenre;
}
