package popschool.my_cinema_hateoas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
@Data
@Entity
@Table(name = "CLIENT")
public class Client {
    private Long nclient;
    private String nom;
    private String prenom;
    private String adresse;
    private int anciennete;
    private List<Emprunt> emprunts;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "nclient")
    public Long getNclient() {
        return nclient;
    }
    public void setNclient(Long nclient) {
        this.nclient = nclient;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "adresse")
    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "anciennete")
    public int getAnciennete() {
        return anciennete;
    }
    public void setAnciennete(int anciennete) {
        this.anciennete = anciennete;
    }

    @OneToMany(mappedBy = "client")
    @JsonIgnore
    public List<Emprunt> getEmprunts() {
        return emprunts;
    }
    public void setEmprunts(List<Emprunt> emprunts) {
        this.emprunts = emprunts;
    }
}
