package popschool.my_cinema_hateoas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyCinemaHateoasApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyCinemaHateoasApplication.class, args);
    }

}
