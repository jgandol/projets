package popschool.my_cinema_hateoas.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.my_cinema_hateoas.model.Acteur;
import popschool.my_cinema_hateoas.model.Genre;


import java.sql.Date;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "acteurs", path = "acteurs")
public interface ActeurRepository extends PagingAndSortingRepository<Acteur,Long> {





}
