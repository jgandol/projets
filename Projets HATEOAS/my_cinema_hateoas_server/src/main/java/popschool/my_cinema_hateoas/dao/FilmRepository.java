package popschool.my_cinema_hateoas.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.my_cinema_hateoas.model.Film;
import popschool.my_cinema_hateoas.model.Genre;


import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "films", path = "films")
public interface FilmRepository extends PagingAndSortingRepository<Film,Long> {



}
