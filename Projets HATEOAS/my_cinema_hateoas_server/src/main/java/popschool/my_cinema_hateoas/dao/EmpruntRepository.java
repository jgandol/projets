package popschool.my_cinema_hateoas.dao;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.my_cinema_hateoas.model.Client;
import popschool.my_cinema_hateoas.model.Emprunt;
import popschool.my_cinema_hateoas.model.Film;
import popschool.my_cinema_hateoas.model.Genre;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "emprunt", path = "emprunt")
public interface EmpruntRepository extends PagingAndSortingRepository<Emprunt,Long> {



}
