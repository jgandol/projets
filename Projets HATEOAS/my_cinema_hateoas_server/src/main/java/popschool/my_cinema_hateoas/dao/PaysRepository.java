package popschool.my_cinema_hateoas.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.my_cinema_hateoas.model.Genre;
import popschool.my_cinema_hateoas.model.Pays;


import java.util.List;

@RepositoryRestResource(collectionResourceRel = "pays", path = "pays")
public interface PaysRepository extends PagingAndSortingRepository<Pays,Long> {
    List<Pays> findAll();

}
