package popschool.my_cinema_hateoas.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.my_cinema_hateoas.model.Client;
import popschool.my_cinema_hateoas.model.Genre;


import java.util.List;
import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "clients", path = "clients")
public interface ClientRepository extends PagingAndSortingRepository<Client,Long> {




}
