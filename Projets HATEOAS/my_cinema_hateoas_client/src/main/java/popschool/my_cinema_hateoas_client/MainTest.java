package popschool.my_cinema_hateoas_client;

import net.minidev.json.JSONUtil;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.hateoas.server.core.TypeReferences;
import org.springframework.http.MediaType;
import popschool.my_cinema_hateoas_client.model.Acteur;
import popschool.my_cinema_hateoas_client.model.Pays;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import java.net.URI;
import java.sql.Date;

public class MainTest {

    public static void main(String args[]) {
//        ClientFactory clientFactory = Configuration.build().buildClientFactory();
//        Client<Pays> client = clientFactory.create(Pays.class);
//        Iterable<Pays> pays = client.getAll(URI.create("http://localhost:8080/pays"));
//        System.out.println(pays);

//        ClientFactory clientFactory = Configuration.build().buildClientFactory();
//        Client<Pays> client = clientFactory.create(Pays.class);
//        Iterable<Pays> pays = client.getAll(URI.create("http://localhost:8080/pays/1"));
//        System.out.println(pays);

//        Traverson client = new Traverson(URI.create("http://localhost:8080/"), MediaTypes.HAL_JSON);
//        CollectionModel<EntityModel<Acteur>> acteurs = client.follow("acteurs")
//                .toObject(new TypeReferences.CollectionModelType<EntityModel<Acteur>>() {
//                });
//        acteurs.forEach(r -> {
//            System.out.println(r.getContent());
//        });


        //POST
//        ClientFactory clientFactory = Configuration.build().buildClientFactory();
//        Client<Pays> client = clientFactory.create(Pays.class);
//        client.post(new Pays("Italy"));

//        POST
//        client.post();
    }

    public void PostActeur(String nom, String prenom, int idPays){
        String URL_LOCAL = "http://localhost:8080/";
        ClientFactory clientFactory = Configuration.builder().setBaseUri(URL_LOCAL).build().buildClientFactory();
        Client<Pays> clientPays = clientFactory.create(Pays.class);
        Client<Acteur> clientActeur = clientFactory.create(Acteur.class);
        Pays p = clientPays.get(URI.create("pays/"+idPays));


        Acteur acteur = new Acteur(nom,prenom);
        acteur.setPays(p);
        clientActeur.post(acteur);

    }


}
