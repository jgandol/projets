package popschool.my_cinema_hateoas_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyCinemaHateoasClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyCinemaHateoasClientApplication.class, args);
    }

}
