package popschool.my_cinema_hateoas_client.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

@Data
@RemoteResource("/acteurs")
public class Acteur {
    URI id;
    String nom;
    String prenom;
    Pays pays;

    public Acteur(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public Acteur() {
    }

//    @LinkedResource
    public Pays getPays() {
        return pays;
    }
    @ResourceId
    public URI getId() {
        return id;
    }
}
