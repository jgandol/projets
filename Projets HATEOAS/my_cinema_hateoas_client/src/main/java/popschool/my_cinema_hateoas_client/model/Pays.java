package popschool.my_cinema_hateoas_client.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sun.org.glassfish.external.statistics.annotations.Reset;
import lombok.Data;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;
import uk.co.blackpepper.bowman.InlineAssociationDeserializer;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import javax.annotation.Resource;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Data
@RemoteResource("/pays")
public class Pays {

    URI id;
    private String nom;
    private List<Acteur> acteurs = new ArrayList<>();

    public Pays(String nom) {
        this.nom = nom;
    }

    public Pays() {
    }

    @JsonDeserialize(contentUsing = InlineAssociationDeserializer.class)
    @LinkedResource
    public List<Acteur> getActeurs(){ return acteurs;}
    @ResourceId
    public URI getId() {
        return id;
    }
}
