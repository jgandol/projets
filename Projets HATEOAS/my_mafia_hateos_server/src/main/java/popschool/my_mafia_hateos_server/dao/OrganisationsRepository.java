package popschool.my_mafia_hateos_server.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.my_mafia_hateos_server.model.Affectation;
import popschool.my_mafia_hateos_server.model.Organisation;

@RepositoryRestResource(collectionResourceRel = "orgnisations", path = "organisations")
public interface OrganisationsRepository extends PagingAndSortingRepository<Organisation,String> {

}
