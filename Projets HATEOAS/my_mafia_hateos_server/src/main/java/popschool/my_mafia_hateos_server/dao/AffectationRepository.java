package popschool.my_mafia_hateos_server.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.my_mafia_hateos_server.model.Affectation;
import popschool.my_mafia_hateos_server.model.Job;

@RepositoryRestResource(collectionResourceRel = "affectations", path = "affectations")
public interface AffectationRepository extends PagingAndSortingRepository<Affectation,Integer> {

}