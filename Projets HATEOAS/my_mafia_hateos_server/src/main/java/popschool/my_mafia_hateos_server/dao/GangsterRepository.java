package popschool.my_mafia_hateos_server.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.my_mafia_hateos_server.model.Affectation;
import popschool.my_mafia_hateos_server.model.Gangster;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "gangsters", path = "gangsters")
public interface GangsterRepository extends PagingAndSortingRepository<Gangster,Integer> {
    @Query("select g from Gangster g where g.gname = :nom")
    Optional<Gangster> findByNom(String nom);
}