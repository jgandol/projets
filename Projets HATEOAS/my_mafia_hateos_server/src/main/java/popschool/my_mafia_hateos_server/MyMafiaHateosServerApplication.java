package popschool.my_mafia_hateos_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyMafiaHateosServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyMafiaHateosServerApplication.class, args);
    }

}
