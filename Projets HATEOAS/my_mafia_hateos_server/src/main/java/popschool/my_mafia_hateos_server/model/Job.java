package popschool.my_mafia_hateos_server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "JOB")
public class Job {
    private int jobId;
    private String jobName;
    private Integer score;
    private Double setupcost;
    private List<Affectation> affectations = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "JOB_ID", table = "JOB")
    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "JOB_NAME", table = "JOB")
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Basic
    @Column(name = "SCORE", table = "JOB")
    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Basic
    @Column(name = "SETUPCOST", table = "JOB")
    public Double getSetupcost() {
        return setupcost;
    }
    public void setSetupcost(Double setupcost) {
        this.setupcost = setupcost;
    }

    @OneToMany(mappedBy = "job")
    public List<Affectation> getAffectations() {
        return affectations;
    }
    public void setAffectations(List<Affectation> affectations) {
        this.affectations = affectations;
    }
}
