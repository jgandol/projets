package popschool.my_mafia_hateos_server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "ORGANISATION")
public class Organisation {
    private String orgName;
    private String description;
    private List<Gangster> gangsters = new ArrayList<>();
    private Gangster boss;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORG_NAME", table = "ORGANISATION")
    public String getOrgName() {
        return orgName;
    }
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @Basic
    @Column(name = "DESCRIPTION", table = "ORGANISATION")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @OneToMany(mappedBy = "organisation")
    public List<Gangster> getGangsters() {
        return gangsters;
    }
    public void setGangsters(List<Gangster> gangsters) {
        this.gangsters = gangsters;
    }

    @OneToOne
    @JoinColumn(name = "THEBOSS", referencedColumnName = "GANGSTER_ID", table = "ORGANISATION")
    public Gangster getBoss() {
        return boss;
    }
    public void setBoss(Gangster boss) {
        this.boss = boss;
    }
}
