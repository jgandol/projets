package popschool.my_mafia_hateos_server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "GANGSTER")
public class Gangster {
    private int gangsterId;
    private String gname;
    private String nickname;
    private Integer badness;
    private List<Affectation> affectations = new ArrayList<>();
    private Organisation organisation;
    private Organisation organisations_boss;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GANGSTER_ID", table = "GANGSTER")
    public int getGangsterId() {
        return gangsterId;
    }
    public void setGangsterId(int gangsterId) {
        this.gangsterId = gangsterId;
    }

    @Basic
    @Column(name = "GNAME", table = "GANGSTER")
    public String getGname() {
        return gname;
    }
    public void setGname(String gname) {
        this.gname = gname;
    }

    @Basic
    @Column(name = "NICKNAME", table = "GANGSTER")
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Basic
    @Column(name = "BADNESS", table = "GANGSTER")
    public Integer getBadness() {
        return badness;
    }
    public void setBadness(Integer badness) {
        this.badness = badness;
    }

    @OneToMany(mappedBy = "gangster")
    public List<Affectation> getAffectations() {
        return affectations;
    }
    public void setAffectations(List<Affectation> affectations) {
        this.affectations = affectations;
    }

    @ManyToOne
    @JoinColumn(name = "ORG_NAME", referencedColumnName = "ORG_NAME", table = "GANGSTER")
    public Organisation getOrganisation() {
        return organisation;
    }
    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    @OneToOne(mappedBy = "boss")
    public Organisation getOrganisations_boss() {
        return organisations_boss;
    }
    public void setOrganisations_boss(Organisation organisations_boss) {
        this.organisations_boss = organisations_boss;
    }
}
