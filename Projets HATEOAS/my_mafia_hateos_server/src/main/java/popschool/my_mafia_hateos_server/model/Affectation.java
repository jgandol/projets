package popschool.my_mafia_hateos_server.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "AFFECTATION")
public class Affectation {

    private int id;
    private Gangster gangster;
    private Job job;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", table = "AFFECTATION")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "GANGSTER_ID", referencedColumnName = "GANGSTER_ID", nullable = false, table = "AFFECTATION")
    public Gangster getGangster() {
        return gangster;
    }
    public void setGangster(Gangster gangster) {
        this.gangster = gangster;
    }

    @ManyToOne
    @JoinColumn(name = "JOB_ID", referencedColumnName = "JOB_ID", nullable = false, table = "AFFECTATION")
    public Job getJob() {
        return job;
    }
    public void setJob(Job job) {
        this.job = job;
    }
}
