package control;

import modele.Item;
import service.ItemSS;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import java.util.List;

@ManagedBean(name = "itemCtrl")
public class ItemControl {
    public String titre;
    public Double prix;
    public String categorie;

    @EJB
    private ItemSS facadeItem;

    public List<Item> getEnsembleItems(){
        return facadeItem.findAllItems();
    }
}
