package control;


import modele.Client;
import service.ClientSS;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLOutput;
import java.util.List;

@Named("loginCtrl")
@SessionScoped
public class LoginCtrl implements Serializable {

    private Client client = new Client();
    private String nom;
    private String prenom;

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }

    public List<Client> getEnsClient(){
        return facadeClient.findAllClient();
    }

    @EJB
    private ClientSS facadeClient;

    public String doAuthenticate(){
        System.out.println(facadeClient.findClientByNomPrenom(nom,prenom));
        if(facadeClient.findClientByNomPrenom(nom, prenom) == null){
            return null;
        } return "menu";

    }

    public void doDeconnexion() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.invalidateSession();
        ec.redirect(ec.getRequestContextPath() + "/index.xhtml");
    }

    //-------- CREATE CLIENT
    private String newNom;
    private String newPrenom;

    public String getNewNom() {
        return newNom;
    }
    public void setNewNom(String newNom) {
        this.newNom = newNom;
    }
    public String getNewPrenom() {
        return newPrenom;
    }
    public void setNewPrenom(String newPrenom) {
        this.newPrenom = newPrenom;
    }

    public String doCreateClient(){
        System.out.println("oui");
        if(this.newNom.isEmpty() && this.newPrenom.isEmpty()){
            System.out.println("oui");
            return null;
        }
        Client c = new Client(this.newNom.toUpperCase(),this.newPrenom.toUpperCase());
        facadeClient.createClient(c);
        return "index";
    }

}