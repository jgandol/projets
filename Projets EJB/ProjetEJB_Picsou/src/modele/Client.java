package modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Client")
@NamedQueries({
        @NamedQuery(name = "Client.findAll", query = "select c from Client c order by c.nom, c.prenom"),
        @NamedQuery(name = "Client.findByNomPrenom", query = "select c from Client c where c.nom = :nom and c.prenom =:prenom")
})
public class Client {
    private int idClient;
    private String nom;
    private String prenom;
    private String adresse;
    private List<Compte> comptes;

    public Client(String nom, String prenom) {
        this.setNom(nom);
        this.setPrenom(prenom);
    }

    public Client() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_client")
    public int getIdClient() {
        return idClient;
    }
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "adresse")
    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }


    @OneToMany(mappedBy = "titulaire")
    public List<Compte> getComptes() {
        return comptes;
    }
    public void setComptes(List<Compte> comptes) {
        this.comptes = comptes;
    }


}
