package modele;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Carte")
public class Carte {
    private int idCarte;
    private String numero;
    private Date dateExpiration;
    private String codeSecret;
    private String bloque;
    private Integer nbEssai;
    private Compte compte;
    private List<Retrait> retraits;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_carte")
    public int getIdCarte() {
        return idCarte;
    }
    public void setIdCarte(int idCarte) {
        this.idCarte = idCarte;
    }

    @Basic
    @Column(name = "numero")
    public String getNumero() {
        return numero;
    }
    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "date_expiration")
    public Date getDateExpiration() {
        return dateExpiration;
    }
    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    @Basic
    @Column(name = "codeSecret")
    public String getCodeSecret() {
        return codeSecret;
    }
    public void setCodeSecret(String codeSecret) {
        this.codeSecret = codeSecret;
    }

    @Basic
    @Column(name = "bloque")
    public String getBloque() {
        return bloque;
    }
    public void setBloque(String bloque) {
        this.bloque = bloque;
    }

    @Basic
    @Column(name = "nbEssai")
    public Integer getNbEssai() {
        return nbEssai;
    }
    public void setNbEssai(Integer nbEssai) {
        this.nbEssai = nbEssai;
    }

    @ManyToOne
    @JoinColumn(name = "id_compte", referencedColumnName = "id_compte", nullable = false)
    public Compte getCompte() {
        return compte;
    }
    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    @OneToMany(mappedBy = "carte")
    public List<Retrait> getRetraits() {
        return retraits;
    }
    public void setRetraits(List<Retrait> retraits) {
        this.retraits = retraits;
    }


}
