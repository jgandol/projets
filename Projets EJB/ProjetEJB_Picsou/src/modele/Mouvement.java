package modele;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "Mouvement")
public class Mouvement {
    private int idMouv;
    private Timestamp dateOperation;
    private double montant;
    private String nature;
    private Compte compte;
    private Retrait retrait;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_mouv")
    public int getIdMouv() {
        return idMouv;
    }
    public void setIdMouv(int idMouv) {
        this.idMouv = idMouv;
    }

    @Basic
    @Column(name = "date_operation")
    public Timestamp getDateOperation() {
        return dateOperation;
    }
    public void setDateOperation(Timestamp dateOperation) {
        this.dateOperation = dateOperation;
    }

    @Basic
    @Column(name = "montant")
    public double getMontant() {
        return montant;
    }
    public void setMontant(double montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "nature")
    public String getNature() {
        return nature;
    }
    public void setNature(String nature) {
        this.nature = nature;
    }

    @ManyToOne
    @JoinColumn(name = "id_compte", referencedColumnName = "id_compte", nullable = false)
    public Compte getCompte() {
        return compte;
    }
    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    @OneToOne(mappedBy = "mouvement")
    public Retrait getRetrait() {
        return retrait;
    }
    public void setRetrait(Retrait retrait) {
        this.retrait = retrait;
    }
}
