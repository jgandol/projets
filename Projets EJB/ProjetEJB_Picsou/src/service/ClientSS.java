package service;

import modele.Client;
import modele.Compte;
import modele.Mouvement;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.List;

/*   TODO:
            - Créer les vues
            - Affichage des comptes client
            - Choix d'une opération
            - Créditer
            - Débiter
            - Transfert

 */

@Singleton
public class ClientSS {
     //service CRUD : entite Client

    @PersistenceContext(unitName = "PicsouPU")
    public EntityManager em;

    public List<Client> findAllClient(){
        return em.createNamedQuery("Client.findAll",Client.class).getResultList();
    }

    public Client findClientByNomPrenom(String nom, String prenom){
        Client client = null;
        try {
            client = em.createNamedQuery("Client.findByNomPrenom",Client.class).setParameter("nom",nom.toUpperCase())
                    .setParameter("prenom",prenom.toUpperCase()).getSingleResult();
        } catch (Exception e) {
        }
        return client;
    }

    public void createClient(Client c){
        em.persist(c);
    }

    public void updateClient(Client c){
        c = em.merge(c);
    }

    public void deleteClient(Client c){
//        item = em.merge(item);
//        em.remove(item);
        em.remove(em.merge(c));
    }

    public void crediter(int id, Double montant){
        Compte cpt = em.find(Compte.class, id);
        if(cpt != null){
            cpt.setSolde(cpt.getSolde()+montant);

            Mouvement mvt = new Mouvement();
            mvt.setCompte(cpt);
            mvt.setMontant(montant);
            mvt.setNature("CR");
            mvt.setDateOperation(new Timestamp(System.currentTimeMillis()));

            em.persist(mvt);
        }
    }
}
