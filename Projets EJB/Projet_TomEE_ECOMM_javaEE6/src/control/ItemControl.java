package control;

import modele.Item;
import service.ItemSS;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import java.util.List;

@ManagedBean(name = "itemCtrl")
public class ItemControl {

    private Item newitem = new Item();

    public Item getNewitem() {
        return newitem;
    }
    public void setNewitem(Item newitem) {
        this.newitem = newitem;
    }

    @EJB
    private ItemSS facadeItem;

    public List<Item> getEnsembleItems(){
        return facadeItem.findAllItems();
    }

    public String doAddItem(){
        facadeItem.createItem(newitem);
        return "showCatalogue";
    }

    public String doRemoveItem(Item item){
        facadeItem.delete(item);
        return "showCatalogue";
    }
}
