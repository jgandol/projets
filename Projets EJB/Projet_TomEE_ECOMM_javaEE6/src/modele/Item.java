package modele;

import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "Item.findAll", query = "select i from Item i order by i.categorie, i.titre"),
        @NamedQuery(name = "Item.findById", query = "select i from Item i where i.id=:id"),
        @NamedQuery(name = "Item.findAllOfCategorie", query="SELECT item FROM Item item WHERE item.categorie=:categorie ORDER BY item.titre"),
        @NamedQuery(name = "Item.findAllCategorie", query="SELECT DISTINCT item.categorie FROM Item item WHERE item.categorie=:categorie ORDER BY item.categorie")
})
@Table(name = "item")
public class Item {
    private int id;
    private String categorie;
    private String titre;
    private Double prix;
    private int codeBarre;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "categorie")
    public String getCategorie() {
        return categorie;
    }
    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "prix")
    public Double getPrix() {
        return prix;
    }
    public void setPrix(Double prix) {
        this.prix = prix;
    }

    @Basic
    @Column(name = "code_barre")
    public int getCodeBarre() {
        return codeBarre;
    }
    public void setCodeBarre(int codeBarre) {
        this.codeBarre = codeBarre;
    }


}
