package service;

import modele.Item;

import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Singleton
public class ItemSS {
     //service CRUD : entite Item

    @PersistenceContext(unitName = "ECommPU")
    public EntityManager em;

    public List<Item> findAllItems(){
        return em.createNamedQuery("Item.findAll",Item.class).getResultList();
    }

    public void createItem(Item item){
        em.persist(item);
    }

    public void update(Item item){
        item = em.merge(item);
    }

    public void delete(Item item){
//        item = em.merge(item);
//        em.remove(item);
        em.remove(em.merge(item));
    }
}
