
import java.io.*;
import java.util.Scanner;

public class Colle {

    public static void colle(String nomf, String nomdest){

            try {
                FileOutputStream out = new FileOutputStream(nomdest);
                String nomFichier = nomf;
                int p = nomf.lastIndexOf('.');
                if (p >= 0) {
                    nomFichier = nomf.substring(0, p);
                }

                int num = 1;
                boolean encore = false;

                do {
                    //declaration de l'un des fichiers contenant une tranche
                    File f = new File(nomFichier + num + ".ttt");
                    encore = f.exists();

                    if(encore){
                        //ouverture du fichier tranche
                        FileInputStream in = new FileInputStream(f);
                        int taille = in.available(); // available -> nombre d'octets d'un fichier
                        byte octets[] = new byte[taille];
                        //lecture de la tranche
                        in.read(octets);
                        in.close();
                        System.out.println("" + taille + " octets lus.");
                        //ecriture de la tranche
                        out.write(octets);
                        System.out.println("" + taille + " octets ajoutes dans " + nomf);
                        num++;
                    }
                } while(encore);
                out.close();

            }catch (IOException e) {
                e.printStackTrace();
            }



    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Nom du fichier tranche : ");
        String nomFichier = sc.nextLine();

        System.out.print("Nom du fichier destination : ");
        String nomDest = sc.nextLine();

        colle(nomFichier, nomDest);
    }
}
