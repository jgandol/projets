import java.io.*;
import java.util.Scanner;

public class Coupe {

    public static void decoupe(String nomf, int taille) throws IOException {


        try {
            //declaration
            FileInputStream in = new FileInputStream(nomf);
            byte[] octets = new byte[taille];

            //Recupere le nom du fichier sans extension
            String nomfichier = nomf;
            int p = nomf.lastIndexOf('.');
            if(p>0){
                nomfichier = nomf.substring(0,p);
            }


            //Traitement
            int num =1,nbLus=0;

            do{
                //lecture de la tranche
                nbLus = in.read(octets);
                System.out.println(""+nbLus+" octets lus.");

                //ecriture de la tranche
                if(nbLus>0) {

                    FileOutputStream out = new FileOutputStream("liste_fr/" + nomfichier + num + ".ttt");
                    out.write(octets,0,nbLus);
                    out.close();
                    //System.out.println(""+nbLus+" octets écrits dans " + nomfichier + num + ".ttt");
                    num++;

                }

            } while(nbLus == taille);
            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }




    }

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Entrez le nom du fichier : ");
        String nomf = sc.nextLine();

        System.out.print("-> taille : ");
        byte taille = sc.nextByte();


        decoupe(nomf, taille*1024);

    }

}
