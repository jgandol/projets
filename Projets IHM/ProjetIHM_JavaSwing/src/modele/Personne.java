package modele;

import java.io.Serializable;
import java.util.Objects;

public class Personne implements Comparable<Personne>, Serializable {
/////////////////////////// A T T R I B U T E S \\\\\\\\\\\\\\\\\\\\\\\\\\\
// Class Attributes

// Instance Attributes

    private String nom;
    private String prenom;
    private String region;

/////////////////// G E T T E R S    &    S E T T E R S \\\\\\\\\\\\\\\\\\\\

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = (nom==null?"GATOR":nom.toUpperCase());
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = (prenom==null?"NATHALIE":prenom.toUpperCase());
    }
    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = (region==null?"NORD":region.toUpperCase());
    }

///////////////////////// C O N S T R U C T O R S \\\\\\\\\\\\\\\\\\\\\\\\\\

    public Personne(String nom, String prenom, String region) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setRegion(region);
    }

////////////////////// B U S I N E S S    R U L E S \\\\\\\\\\\\\\\\\\\\\\\\


//////////////////// O V E R R I D E D    M E T H O D S \\\\\\\\\\\\\\\\\\\\\\
    @Override
    public int compareTo(Personne personne) {
        return nom.compareTo(personne.nom);
    }
    // toString


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return nom.equals(personne.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

    @Override
    public String toString() {
        return nom;
    }
}
