package vue;

import com.sun.tools.javac.Main;
import modele.Personne;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;


public class Agenda {
    private JPanel rootPanel;
    private JList<Personne> listePersonne;
    private JButton buttonAjouter;
    private JButton buttonModifier;
    private JButton buttonMailing;
    private JTextArea textAreaInfoPersonne;

    private DefaultListModel<Personne> modele = new DefaultListModel<>();

    private DefaultListModel<Personne> modelBis = new DefaultListModel<>();
    private MailingDlg mailing = new MailingDlg();


    private AjoutDlg ajoutDlg = null;

    public void initModele(){

        try {
            JFileChooser fileChooser = new JFileChooser("."); // choix du repertoire
            int status = fileChooser.showOpenDialog(null); // recuperer le fichier
            if (status == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                FileInputStream f = new FileInputStream(selectedFile);
                ObjectInputStream out = new ObjectInputStream(f);

                this.modele = (DefaultListModel) out.readObject();
                out.close();
                return;
            }
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }

        this.modele = new DefaultListModel<>();
        modele.addElement(new Personne("gator", "nathalie", "nord"));
        modele.addElement(new Personne("gator", "magalie", "picardie"));
        modele.addElement(new Personne("pelouse", "thierry", "nord"));
    }


    public Agenda() {
        //initModele();
        this.listePersonne.setModel(modele);

        buttonAjouter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(ajoutDlg == null){
                    ajoutDlg = new AjoutDlg();
                    ajoutDlg.pack();
                }
                Personne pers = new Personne(null,null,null);
                if(ajoutDlg.ouvrirDlg(pers,true)){
                    if(!modele.contains(pers)) // refus des doublons
                        modele.addElement(pers);
                }
                textAreaInfoPersonne.setText("");
                buttonModifier.setEnabled(false);
            }
        });

        buttonModifier.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(ajoutDlg == null){
                    ajoutDlg = new AjoutDlg();
                    ajoutDlg.pack();
                }
                Personne pers = listePersonne.getSelectedValue();
                Personne nouv = new Personne(pers.getNom(),pers.getPrenom(),pers.getRegion());
                if(ajoutDlg.ouvrirDlg(nouv,false)){
                    modele.removeElement(pers);
                    modele.addElement(nouv);
                }
                textAreaInfoPersonne.setText("");
                buttonModifier.setEnabled(false);
            }
        });

        listePersonne.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                Personne pers = listePersonne.getSelectedValue();

                if(pers==null)return;

                textAreaInfoPersonne.setText("");
                textAreaInfoPersonne.setText("nom : " + pers.getNom());
                textAreaInfoPersonne.append("\nprenom : "+pers.getPrenom());
                textAreaInfoPersonne.append("\nregion : "+pers.getRegion());
            }
        });
        listePersonne.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                buttonModifier.setEnabled(true);
                Personne pers = listePersonne.getSelectedValue();

                if(pers==null)return;

                textAreaInfoPersonne.setText("\nnom : " + pers.getNom());
                textAreaInfoPersonne.append("\nprenom : " + pers.getPrenom());
                textAreaInfoPersonne.append("\nregion : " + pers.getRegion());

                listePersonne.setEnabled(true);


            }
        });

        buttonMailing.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(mailing == null){
                    mailing = new MailingDlg();
                    mailing.pack();
                    mailing.setSize(200,200);
                }

                mailing.ouvrir(modele);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Agenda");
        frame.setContentPane(new Agenda().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
