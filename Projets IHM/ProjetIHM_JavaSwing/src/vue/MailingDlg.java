package vue;

import modele.Personne;

import javax.management.modelmbean.InvalidTargetObjectTypeException;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;
import java.util.List;


public class MailingDlg extends JDialog {
    private JPanel mailPanel;
    private JButton buttonOK;
    private JTextArea textAreaInfo;
    private JRadioButton radioButtonRegion;
    private JRadioButton radioButtonNom;
    private JCheckBox ascendantCheckBox;
    private JButton buttonApercu;
    private JButton buttonFichier;
    private JButton buttonSerialization;

    private List<Personne> liste = new LinkedList<>();
    private DefaultListModel<Personne> modele;

    //CONSTRUCTEUR
    public MailingDlg() {
        setContentPane(mailPanel);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        //BOUTON APERCU
        buttonApercu.addActionListener(e -> {
            trier();
            textAreaInfo.setText("");
            String format;

            if (radioButtonNom.isSelected()) {
                format = "Nom    : %s\nPrénom : %s\nRégion : %s\n";
                for (Personne p : liste)
                    textAreaInfo.append(String.format(format, p.getNom(), p.getPrenom(), p.getRegion() + "\n"));
            } else if (radioButtonRegion.isSelected()) {
                format = "Région : %s\nNom    : %s\nPrénom : %s\n";
                for (Personne p : liste)
                    textAreaInfo.append(String.format(format, p.getRegion(), p.getNom(), p.getPrenom() + "\n"));
            }

            textAreaInfo.select(0, 0);

        });


        buttonFichier.addActionListener(actionEvent -> {
            PrintWriter sortie; // fichier d'impression
            textAreaInfo.setText("");

            try {
                JFileChooser fileChooser = new JFileChooser("."); // choix du repertoire
                int status = fileChooser.showSaveDialog(null); // recuperer le fichier
                if (status == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    sortie = new PrintWriter(new FileWriter(selectedFile)); // on structure le fichier
                    trier();
                    String format;

                    if (radioButtonNom.isSelected()) {
                        format = "Nom    : %s\nPrénom : %s\nRégion : %s\n";
                        for (Personne p : liste)
                            sortie.println(String.format(format, p.getNom(), p.getPrenom(), p.getRegion() + "\n"));
                    } else if (radioButtonRegion.isSelected()) {
                        format = "Région : %s\nNom    : %s\nPrénom : %s\n";
                        for (Personne p : liste)
                            sortie.println(String.format(format, p.getRegion(), p.getNom(), p.getPrenom() + "\n"));
                    }
                    sortie.close();

                }
            } catch (Exception ioe) {
                ioe.printStackTrace();
            }

        });


        buttonSerialization.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    JFileChooser fileChooser = new JFileChooser("."); // choix du repertoire
                    int status = fileChooser.showSaveDialog(null); // recuperer le fichier
                    if (status == JFileChooser.APPROVE_OPTION) {
                        File selectedFile = fileChooser.getSelectedFile();
                        FileOutputStream f = new FileOutputStream(selectedFile);
                        ObjectOutputStream out = new ObjectOutputStream(f);
                        out.writeObject(modele);
                        out.close();
                    }
                } catch (Exception ioe) {
                    ioe.printStackTrace();
                }
            }
        });
    }

    //METIER

        public void ouvrir(DefaultListModel<Personne> modele){
            this.modele = modele;
            this.setVisible(true);
        }

        private void onOK(){this.setVisible(false); //rootPane.setVisible(true);
    }

        private void trier(){

            class ComparaisonRegionCroissante implements Comparator<Personne> {

                @Override
                public int compare(Personne p1, Personne p2) {
                    int etat = p1.getRegion().compareTo(p2.getRegion());
                    if(etat != 0){
                        return etat;
                    } else return p1.getNom().compareTo(p2.getNom());
                }
            }

            class ComparaisonRegionDecroissante implements Comparator<Personne> {

                @Override
                public int compare(Personne p1, Personne p2) {
                    int etat = p1.getRegion().compareTo(p2.getRegion());
                    if(etat != 0){
                        return -etat;
                    } else return -p1.getNom().compareTo(p2.getNom());
                }
            }

            liste = new LinkedList<>();
                Enumeration<Personne> iter = modele.elements();
                while(iter.hasMoreElements()){
                    liste.add(iter.nextElement());
                }

                if(this.radioButtonNom.isSelected()) {
                    if(this.ascendantCheckBox.isSelected()){
                        Collections.sort(liste);
                    } else {
                        liste.sort(Collections.reverseOrder());
                    }
                } else if(this.ascendantCheckBox.isSelected()){
                    liste.sort(new ComparaisonRegionCroissante());
                } else {
                    liste.sort(new ComparaisonRegionDecroissante());
                }
        }




    public static void main(String[] args) {
        MailingDlg dialog = new MailingDlg();
        //dialog.setContentPane(new MailingDlg().mailPanel);
        dialog.pack();

        DefaultListModel<Personne> modele = new DefaultListModel<>();
        modele.addElement(new Personne("gator","nathalie","nord"));
        modele.addElement(new Personne("gator","magalie","picardie"));
        modele.addElement(new Personne("pelouse","thierry","nord"));


        dialog.ouvrir(modele);
        System.exit(0);
    }

}
