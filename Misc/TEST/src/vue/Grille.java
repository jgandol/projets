package vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Grille extends JFrame{
    private JPanel rootPanel;
    private JButton Personnage;
    private JPanel panel;
    private JTextArea textArea1;
    Color color = new Color(0xff0000);

    // HAUT = 38
    // BAS = 40
    // GAUCHE = 37
    //DROITE = 39

    public Grille() {
        //rootPanel.setSize(200,200);
        this.setSize(200,200);
        textArea1.setLocation(180,180);

        textArea1.setBackground(color);

        Personnage.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                System.out.println(panel.getLocation());
                if(e.getKeyCode() == 38){
                    //System.out.println("HAUT");
                    panel.setLocation(panel.getX(),panel.getY()-20);
                    setVisible(false);
                    new Grille();
                }
                if(e.getKeyCode() == 40){
                    //System.out.println("BAS");
                    panel.setLocation(panel.getX(),panel.getY()+20);
                    setVisible(false);
                    new Grille();
                }
                if(e.getKeyCode() == 39){
                    //System.out.println("DROITE");
                    panel.setLocation(panel.getX()+20,panel.getY());
                    setVisible(false);
                    new Grille();
                }
                if(e.getKeyCode() == 37){
                    //System.out.println("GAUCHE");
                    panel.setLocation(panel.getX()-20,panel.getY());
                    setVisible(false);
                    new Grille();
                }

                if (panel.getX() == 180 && panel.getY() == 180) {
                    System.out.println("BRAVO");
                    setVisible(false);
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Grille");
        frame.setContentPane(new Grille().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
