package beans;

import java.util.Objects;

public class Player {
    private String ip;
    private Boolean isTurn;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Boolean getTurn() {
        return isTurn;
    }

    public void setTurn(Boolean turn) {
        isTurn = turn;
    }

    public Player(String ip) {
        this.ip = ip;
        this.isTurn = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return ip.equals(player.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip);
    }

    @Override
    public String toString() {
        return "Player{" +
                "ip='" + ip + '\'' +
                ", isTurn=" + isTurn +
                '}';
    }
}
