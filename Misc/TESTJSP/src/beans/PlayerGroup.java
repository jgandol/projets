package beans;

import beans.Player;

import java.util.HashSet;
import java.util.Set;

public class PlayerGroup {

    private Set<Player> players = new HashSet<>();

    public void addPlayer(String ip){
        players.add(new Player(ip));
    }

    public Set<Player> getPlayers() {
        return players;
    }

    @Override
    public String toString() {
        return "PlayerGroup{" +
                "players=" + players +
                '}';
    }


}
