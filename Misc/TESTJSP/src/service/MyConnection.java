package db_utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {
    // identifications de la BdD
    private final static String URL = "jdbc:mysql://localhost:3306/tech_support";
    private final static String USER = "scott";
    private final static String PW = "tiger";

    // Singleton : declaration
    private static Connection INSTANCE = null;

    // Singleton : initialisation
    static {
        try {
            // ouvrir la connection a la BD
            INSTANCE = DriverManager.getConnection(URL,USER, PW);
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Singleton : accesseur
    public static Connection getINSTANCE() {
        return INSTANCE;
    }
}
