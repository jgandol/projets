<%--
  Created by IntelliJ IDEA.
  User: jeremiepop
  Date: 21/02/2020
  Time: 13:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  <jsp:useBean id="pg"
               scope="application"
               class="beans.PlayerGroup">
    <jsp:setProperty name="pg"
                     property="*"/>
  </jsp:useBean>
  <% String ip = request.getRemoteAddr();
    pg.addPlayer(ip);
  %>
  <%= pg %>
  </body>
</html>
