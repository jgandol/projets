DROP TABLE IF EXISTS Reservation_Emplacement CASCADE;
DROP TABLE IF EXISTS Activite_Participant CASCADE;
DROP TABLE IF EXISTS Participant CASCADE;
DROP TABLE IF EXISTS Activite CASCADE;
DROP TABLE IF EXISTS Restauration CASCADE;
DROP TABLE IF EXISTS Emplacement CASCADE;
DROP TABLE IF EXISTS Reservation CASCADE;
DROP TABLE IF EXISTS Utilisateur CASCADE;

CREATE TABLE Activite (
  id_activ int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  libelle varchar(20) NOT NULL,
  prix double
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE Restauration (
  id_rest int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  type varchar(30) NOT NULL,
  prix double
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE Emplacement (
  id_emp int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  type varchar(30) NOT NULL,
  nb_pers_type int NOT NULL,
  num_localisation int NOT NULL,
  adresse varchar(50) NOT NULL,
  prix double
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE Utilisateur (
  id_util int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  nom varchar(20) NOT NULL,
  prenom varchar(20) NOT NULL,
  fonction varchar(20) NOT NULL DEFAULT 'USER'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE Reservation (
  id_res int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  id_util int not null,
  semaines int not null,
  date_debut date NOT NULL,
  date_fin date NOT NULL,
  accompte varchar(4) NOT NULL DEFAULT 'FAUX',
  solde varchar(4) NOT NULL DEFAULT 'FAUX',
  total double,
  CONSTRAINT FK_RESERVATION_USER FOREIGN KEY (id_util) REFERENCES Utilisateur(id_util)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE Participant (
  id_part int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  id_rest int not null,
  id_res int not null,
  nom varchar(30) NOT NULL,
  prenom varchar(30) NOT NULL,
  nSemaine int NOT NULL,
  nJour int NOT NULL,
  depenses double,
  CONSTRAINT PARTICIPANT_FK_RESTAURATION FOREIGN KEY (id_rest) REFERENCES Restauration(id_rest),
  CONSTRAINT PARTICIPANT_FK_RESERVATION FOREIGN KEY (id_res) REFERENCES Reservation(id_res)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE Activite_Participant (
  id_activ_part int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  id_part int NOT NULL,
  id_activ int NOT NULL,
  CONSTRAINT FK_ACTIV_PART_PART FOREIGN KEY (id_part ) REFERENCES Participant(id_part),
  CONSTRAINT FK_ACTIV_PART_ACTIV FOREIGN KEY (id_activ ) REFERENCES Activite(id_activ)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE Reservation_Emplacement (
  id_res_emp int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  id_res int NOT NULL,
  id_emp int NOT NULL,
  CONSTRAINT FK_RES_EMP_RES FOREIGN KEY (id_res ) REFERENCES Reservation(id_res),
  CONSTRAINT FK_RES_EMP_EMP FOREIGN KEY (id_emp ) REFERENCES Emplacement(id_emp)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO Utilisateur(prenom, nom, fonction ) VALUES
("Jeremie","Gandolfo","ADMIN"), -- 1
("Thierry","Pelouse","USER"), -- 2
("Nathanael","Boudoir","USER"); -- 3

INSERT INTO Reservation(id_util ,semaines, date_debut, date_fin) VALUES
(2,3,'2020-03-09','20-03-30'),
(3,1,'2020-03-18','20-03-25');




