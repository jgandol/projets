package modele;

import java.util.Objects;

public class User {
    private String login;
    private String choix = null;
    private Boolean vivant = true;

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getChoix() {
        return choix;
    }
    public void setChoix(String choix) {
        this.choix = choix;
    }
    public Boolean getVivant() {
        return vivant;
    }
    public void setVivant(Boolean vivant) {
        this.vivant = vivant;
    }

    public User(String login, String choix) {
        this.login = login;
        this.choix = choix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                "choix = " + choix +
                '}';
    }

}
