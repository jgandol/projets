package control;

import modele.User;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "appCtrl")
@ApplicationScoped
public class AppControler {
    public List<User> appUsers = new ArrayList<>();
    public Boolean partieEnCours = false;
    public List<User> joueursVivants = new ArrayList<>();
    public int compteur;

    public Boolean getPartieEnCours() {
        return partieEnCours;
    }
    public void setPartieEnCours(Boolean partieEnCours) {
        this.partieEnCours = partieEnCours;
    }

    public List<User> getAppUsers() {
        if(partieEnCours){
            updateChoix();
        }
        getLogin();
        return appUsers;
    }
    public void setAppUsers(List<User> appUsers) {
        this.appUsers = appUsers;
    }
    public void addAppUser(User user){
        this.appUsers.add(user);
    }

    public User getUserInstance(){
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) ctx.getExternalContext().getSession(true);
        return ((User) session.getAttribute("user"));
    }

    public void getLogin() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) ctx.getExternalContext().getSession(true);
        User u = (User) session.getAttribute("user");
        if(!appUsers.contains(u)){
            appUsers.add(u);
        } else {
            appUsers.remove(u);
            appUsers.add(u);
        }
//        System.out.println(appUsers);
    }

    public String goNextRound(){
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) ctx.getExternalContext().getSession(true);
        User u = (User) session.getAttribute("user");
        if(!partieEnCours){
            partieEnCours = true;
            compteur = appUsers.size();
        }
        if(u.getVivant()){
            return "arene";
        } else return "main";
    }

    public List<User> remplirListeJoueursVivants(){
        for(User u : appUsers){
            if(u.getVivant()){
                joueursVivants.add(u);
            }
        }
        return joueursVivants;
    }

    public String updateChoix(){
        getLogin();
        System.out.println("ping");
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) ctx.getExternalContext().getSession(true);
        session.setAttribute("user",getUserInstance());
        if()
        compteur -= 1;
        if(compteur == 0)
//
//
//        this.joueur.setChoix(this.choix);
        return "main";
    }
}
