package control;

import modele.User;
import org.omg.CORBA.TIMEOUT;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.Duration;
import java.util.*;

@ManagedBean(name = "ctrl")
@SessionScoped
public class Controler extends HttpServlet {
    private String login;
    private List<User> users = new ArrayList<>();
    private User joueur;
    private String choix = null;
    private Boolean isJoue = false;
    private Boolean isVivant = true;

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public List<User> getUsers() {
        System.out.println(users);
        return users;
    }
    public void setUsers(List<User> users) {
        this.users = users;
    }
    public User getJoueur() {
        return joueur;
    }
    public void setJoueur(User joueur) {
        this.joueur = joueur;
    }
    public String getChoix() {
        return choix;
    }
    public void setChoix(String choix) {
        this.choix = choix;
    }
    public Boolean getVivant() {
        return isVivant;
    }
    public void setVivant(Boolean vivant) {
        isVivant = vivant;
    }

    public String doLogin(){

        FacesContext ctx = FacesContext.getCurrentInstance();

        System.out.println();
        User user = new User(this.login, this.choix);
        setJoueur(user);

        HttpSession session = (HttpSession) ctx.getExternalContext().getSession(true);
        session.setAttribute("user", this.joueur);

        return "main";
    }

    public String updateChoix(){
        FacesContext ctx = FacesContext.getCurrentInstance();
        User user = new User(this.login, this.choix);
        this.setJoueur(user);
        HttpSession session = (HttpSession) ctx.getExternalContext().getSession(true);
        session.setAttribute("user", this.joueur);
        this.isJoue = true;
        return "main";
    }
}
