//ClientEcho.java
import java.io.*;
import java.net.*;
class ClientEcho {
    public static void main (String argv[]) {
        try {
            // On crée un objet InetAdress sur  l'interface de loopback
            InetAddress adr = InetAddress.getByName("127.0.0.1");
            // On crée une socket
            Socket s = new Socket (adr, Integer.valueOf (argv[0]).intValue());
            System.out.println("Socket crée");
            // On crée 2 flots d'entrée et un de sortie
            DataInputStream saisie = new DataInputStream(new BufferedInputStream (System.in));
            DataInputStream entree = new DataInputStream (new BufferedInputStream (s.getInputStream()));
            PrintStream sortie = new PrintStream ( new BufferedOutputStream(s.getOutputStream()));
            // On envoie du texte au serveur et on
            // affiche l'echo reçu
            while (true) {
                // Saisie du texte à envoyer au serveur
                System.out.println("Texte ? ");
                String buff = saisie.readLine();
                // Si on entre "FIN", on quitte
                if (buff.equals("FIN")) break;
                // On envoie le texte saisi au serveur
                sortie.println(buff);
                sortie.flush();
                // On affiche l'écho du serveur
                String buff2 = entree.readLine();
                System.out.println(buff2);
            }
            // On ferme la socket
            s.close();
        }
        catch (Exception e) {
            return;
        };
    }
}
