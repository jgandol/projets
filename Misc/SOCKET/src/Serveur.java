import java.io.*;
import java.net.*;

public class Serveur {
    public static int PORT=5000;
    public static String MACHINE="vlaminck.prism.uvsq.fr";

    public static void main(String [] a) {
        ServerSocket serverSocket=null;
        // Création de la socket de connexion
        try {
            serverSocket=new ServerSocket(Serveur.PORT);
        } catch(IOException e) {
            // échec de la création
            System.exit(-1);
        }

        try {
            System.out.println("En attente...");
            // En attente de connexion
            Socket clientSocket=serverSocket.accept();
            // Un client s'est connecté...
            System.out.println("Connexion d'un client");
            try {
                // Création des flots d'entrée/sortie
                PrintWriter out=new PrintWriter(clientSocket.getOutputStream(),
                        true);
                BufferedReader in=
                        new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String entree,sortie;
                // Envoi d'un premier message lors de la connexion
                out.println("Bienvenue !");
                while (true) {
                    // Lecture de ce qu'envoie le client
                    entree=in.readLine();
                    if(entree!=null) System.out.println("Client>"+entree);
                    // Si c'est bye, alors on renvoie et on quitte
                    if(entree.equals("bye")) {
                        sortie="bye";
                        out.println(sortie);
                        break;
                    } else {
                        // Sinon on renvoie
                        sortie="Je répète:"+entree;
                        System.out.println("Serveur>"+sortie);
                        out.println(sortie);
                    }
                }
                in.close() ;
                out.close();
            } catch (IOException e) {
            } finally { clientSocket.close(); }
        } catch (IOException e) {
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {}
        }
    }
}
