import java.io.*;
import java.net.*;
public class Client {
    public static void main(String [] a) {
        InetAddress adr ;
        Socket socket=null;
        String entree,sortie;
        try {
            // Connexion au serveur
            adr=InetAddress.getByName(Serveur.MACHINE);
            socket = new Socket(adr,Serveur.PORT);
        } catch (Exception e) {
            // echec de la connexion au serveur
            e.printStackTrace();
            System.exit(-1);
        }

        try {
            // Création des flots d'entrée/sortie
            PrintWriter out=new PrintWriter(socket.getOutputStream(),true);
            BufferedReader in=new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            BufferedReader stdin=new BufferedReader(
                    new InputStreamReader(System.in));
            // Communication entre client/serveur
            while (true) {
                // on lit ce qu'envoie le serveur
                entree=in.readLine();
                if(entree!=null) System.out.println("Serveur>"+entree);
                // si c'est bye, alors c'est fini
                if(entree.equals("bye")) break;
                // on lit ce que tape l'utilisateur
                sortie=stdin.readLine();
                if(sortie!=null) {
                    // et on l'envoie au serveur
                    System.out.println("Client>"+sortie);
                    out.println(sortie);
                }
            }
            // Fermeture des flots d'entrée sortie
            in.close();
            out.close();
        }  catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Dans tous les cas, fermeture de la socket de communication.
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
