import java.io.*;
import java.net.*;
class ServeurEcho {
    public static void main(String [] argv) {
        try {
            // On crée une socket serveur ( le port est passé en argument)
            ServerSocket s_ecoute = new ServerSocket(Integer.valueOf (argv[0]).intValue());
            System.out.println("Serveur démarré sur la socket d'écoute " + s_ecoute);
            // Attente d'une connexion
            Socket s_service = s_ecoute.accept();
            // Une connexion a été ouverte
            System.out.println("Ouverture de la connexion sur la socket de service " + s_service);
            // On crée un flot d'entrée et un de sortie
            BufferedReader entree = new BufferedReader (new InputStreamReader ( s_service.getInputStream()));
            PrintWriter sortie = new PrintWriter ( new OutputStreamWriter ( s_service.getOutputStream()));
            // On boucle sur l'echo
            while (true){
                // On lit une ligne en entrée
                String buff = entree.readLine();
                // On quitte si c'est égal à "FIN"
                if (buff.equals("FIN")) break;
                // On renvoie l'echo au client
                sortie.println(buff);
                sortie.flush();
            }
            // Le client s'est déconnecté
            System.out.println("Fermeture de la connexion...");
            // On ferme la socket de service
            s_service.close();
        }
        catch (Exception e) {
            return;
        }
    }
}