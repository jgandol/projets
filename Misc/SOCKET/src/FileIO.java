import java.io.File;
import java.io.Serializable;

public class FileIO implements Serializable {
    /**
     * Comment for serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private File[] file;
    private int lenght;
    /**
     * @param files
     */
    public FileIO(File[] file) {
        super();
        this.file = file;
        this.lenght=file.length;
    }
    public int lenght() {
        return lenght;
    }
    /**
     * @return Returns the files.
     */
    public File getFile(int i) {
        return file[i];
    }
}