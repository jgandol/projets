package modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Reservation")
public class Reservation {
    private int idRes;
    private int semaines;
    private Date dateDebut;
    private Date dateFin;
    private String accompte;
    private String solde;
    private Double total;
    private List<Participant> participants;
    private Utilisateur utilisateur;
    private List<ReservationEmplacement> reservationEmplacements;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_res")
    public int getIdRes() {
        return idRes;
    }
    public void setIdRes(int idRes) {
        this.idRes = idRes;
    }

    @Basic
    @Column(name = "semaines")
    public int getSemaines() {
        return semaines;
    }
    public void setSemaines(int semaines) {
        this.semaines = semaines;
    }

    @Basic
    @Column(name = "date_debut")
    public Date getDateDebut() {
        return dateDebut;
    }
    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    @Basic
    @Column(name = "date_fin")
    public Date getDateFin() {
        return dateFin;
    }
    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    @Basic
    @Column(name = "accompte")
    public String getAccompte() {
        return accompte;
    }
    public void setAccompte(String accompte) {
        this.accompte = accompte;
    }

    @Basic
    @Column(name = "solde")
    public String getSolde() {
        return solde;
    }
    public void setSolde(String solde) {
        this.solde = solde;
    }

    @Basic
    @Column(name = "total")
    public Double getTotal() {
        return total;
    }
    public void setTotal(Double total) {
        this.total = total;
    }


    @OneToMany(mappedBy = "reservation")
    public List<Participant> getParticipants() {
        return participants;
    }
    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    @ManyToOne
    @JoinColumn(name = "id_util", referencedColumnName = "id_util", nullable = false)
    public Utilisateur getUtilisateur() {
        return utilisateur;
    }
    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @OneToMany(mappedBy = "reservation")
    public List<ReservationEmplacement> getReservationEmplacements() {
        return reservationEmplacements;
    }
    public void setReservationEmplacements(List<ReservationEmplacement> reservationEmplacements) {
        this.reservationEmplacements = reservationEmplacements;
    }
}
