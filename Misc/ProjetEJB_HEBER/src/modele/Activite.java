package modele;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Activite")
public class Activite {
    private int idActiv;
    private String libelle;
    private Double prix;
    private ActiviteParticipant activiteParticipants;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_activ")
    public int getIdActiv() {
        return idActiv;
    }
    public void setIdActiv(int idActiv) {
        this.idActiv = idActiv;
    }

    @Basic
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Basic
    @Column(name = "prix")
    public Double getPrix() {
        return prix;
    }
    public void setPrix(Double prix) {
        this.prix = prix;
    }

    @OneToOne(mappedBy = "activite")
    public ActiviteParticipant getActiviteParticipants() {
        return activiteParticipants;
    }
    public void setActiviteParticipants(ActiviteParticipant activiteParticipants) {
        this.activiteParticipants = activiteParticipants;
    }
}
