package service;

import modele.Utilisateur;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Singleton
public class HeberFacade {
    //service CRUD

    @PersistenceContext(unitName = "HEBER_PU")
    public EntityManager em;

    public List<Utilisateur> findAllUser(){
        System.out.println("oui");
        return em.createNamedQuery("Utilisateur.findAll",Utilisateur.class).getResultList();
    }

    public Utilisateur findUserByNomPrenom(String nom, String prenom){
        Utilisateur user = null;
        try {
            user = em.createNamedQuery("Utilisateur.findByNomPrenom",Utilisateur.class).setParameter("nom",nom)
                    .setParameter("prenom",prenom).getSingleResult();
        } catch (Exception e) {
        }
        return user;
    }

    public void createUser(Utilisateur u){
        em.persist(u);
    }

    public void updateUser(Utilisateur u){
        u = em.merge(u);
    }

    public void deleteUser(Utilisateur u){
        em.remove(em.merge(u));
    }

}
