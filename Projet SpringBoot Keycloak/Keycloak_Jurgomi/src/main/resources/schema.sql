drop table  if exists CUSTOMER;

CREATE TABLE CUSTOMER (
                      ID  INT   NOT NULL AUTO_INCREMENT ,
                      NAME   VARCHAR(20),
                      SERVICERENDERED     VARCHAR(20),
                      ADDRESS     VARCHAR(20),
                      CONSTRAINT PK_CUSTOMER
                          PRIMARY KEY ( ID ) );
