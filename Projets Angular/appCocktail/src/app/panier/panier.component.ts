import { Component, OnInit } from '@angular/core';
import {Panier} from '../Panier';
import {ServiceCartService} from '../service-cart.service';
import {applySourceSpanToExpressionIfNeeded} from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {
  panier: Panier;
  cartServ: ServiceCartService;

  constructor(cartServ: ServiceCartService) {
    this.cartServ = cartServ;
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  OuaisOuais($event: MouseEvent) {
    this.panier = this.cartServ.panier;
    // console.log(this.panier);
    this.panier.total = this.panier.setTotale();
    console.log(this.panier);
  }
}
