import { Cocktail } from './cocktail';

export const COCKTAILS: Cocktail[] = [
  {
    id: 1,
    nom: 'Sex on the Beach',
    prix: 12,
    ingredients:[
      {
        nom: 'Vodka',
        quantite: '3 cl'
      },
      {
        nom: 'Liqueur de pêche',
        quantite: '3 cl'
      },
      {
        nom: 'Jus d\'ananas',
        quantite: '6 cl'
      },
      {
        nom: 'Jus de Cranberry',
        quantite: '10 cl'
      }
    ]
  },{
  id: 2,
  nom: 'Mojito',
  prix: 9,
  ingredients:[
      {
        nom: 'Rhum blanc',
        quantite: '5 cl'
      },
      {
        nom: 'Menthe',
        quantite: '7 feuilles'
      },
      {
        nom: 'Jus de citron',
        quantite: '0.5'
      },
      {
        nom: 'Sucre',
        quantite: '2 c à c.'
      },
      {
        nom: 'Eau gazeuse',
        quantite: '6 cl'
      },
      {
        nom: 'Glaçons pilés',
        quantite: '4'
      }
    ]
  }
];
