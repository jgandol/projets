import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CocktailsListComponent } from './cocktails-list/cocktails-list.component';
import { IngredientsListComponent } from './ingredients-list/ingredients-list.component';
import {FormsModule} from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { PanierComponent } from './panier/panier.component';

@NgModule({
  declarations: [
    AppComponent,
    CocktailsListComponent,
    IngredientsListComponent,
    HeaderComponent,
    PanierComponent,
  ],
    imports: [
        BrowserModule,
        NgbModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
