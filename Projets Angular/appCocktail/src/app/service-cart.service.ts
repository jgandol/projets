import { Injectable } from '@angular/core';
import {Panier} from './Panier';

@Injectable({
  providedIn: 'root'
})
export class ServiceCartService {

  // tslint:disable-next-line:variable-name
  panier: Panier;


  // tslint:disable-next-line:no-shadowed-variable
  constructor() {
    this.panier = new Panier();
  }

  // tslint:disable-next-line:typedef
  setData(panier){
    this.panier = panier;
  }

  // tslint:disable-next-line:typedef
  getData(){
    const temp = this.panier;
    this.clearData();
    return temp;
  }

  // tslint:disable-next-line:typedef
  clearData(){
    this.panier = undefined;
  }
}
