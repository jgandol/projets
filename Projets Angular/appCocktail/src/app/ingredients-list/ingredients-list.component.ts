import { Component, OnInit, Input } from '@angular/core';
import {Cocktail} from '../cocktail';
import {Ingredient} from '../ingredient';


@Component({
  selector: 'app-ingredients-list',
  templateUrl: './ingredients-list.component.html',
  styleUrls: ['./ingredients-list.component.css']
})
export class IngredientsListComponent implements OnInit {
  @Input() cocktail: Cocktail;
  newIngr: Ingredient ;
  selectedIngredient: Ingredient;

  constructor() { }

  // tslint:disable-next-line:typedef
  public setSelelectIngredient(event, ingredient) {
    if (this.selectedIngredient === ingredient){
      this.selectedIngredient = new Ingredient();
    } else { this.selectedIngredient = ingredient; }
  }

  ngOnInit(): void {
    // console.log(this.cocktail.ingredients);
    // this.selectedIngredient = this.cocktail.ingredients[1];
    this.selectedIngredient = new Ingredient();
    this.newIngr = new Ingredient();
  }

  // tslint:disable-next-line:typedef
  addIngredient($event: MouseEvent, newIngr: Ingredient) {
    // console.log(newIngr);
    this.cocktail.ingredients.push(newIngr);
    this.newIngr = new Ingredient();
  }

  // tslint:disable-next-line:typedef
  deleteIngredient($event: MouseEvent) {
    this.cocktail.ingredients.splice(this.cocktail.ingredients.indexOf(this.selectedIngredient), 1);
  }
}
