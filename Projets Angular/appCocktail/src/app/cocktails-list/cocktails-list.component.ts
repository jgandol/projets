import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { COCKTAILS } from '../mock-cocktails';
import {Cocktail} from '../cocktail';
import {Ingredient} from '../ingredient';
import {Panier} from '../Panier';
import {ServiceCartService} from '../service-cart.service';

@Component({
  selector: 'app-cocktails-list',
  templateUrl: './cocktails-list.component.html',
  styleUrls: ['./cocktails-list.component.css']
})
export class CocktailsListComponent implements OnInit {
  @Output() showIngredient = new EventEmitter<any>();
  @Output() showPanier = new EventEmitter<any>();
  cardServ: ServiceCartService;
  cocktails = COCKTAILS;
  selectedCocktail: Cocktail;
  newCocktail = new Cocktail();
  ingredientNewCock = new Ingredient();
  qte: number;
  private i = 0;
  panier: Panier;

  constructor(cardServ: ServiceCartService) {
    this.cardServ = cardServ;
  }

  // tslint:disable-next-line:typedef
  public setSelelectCocktail(event, cocktail) {
    this.selectedCocktail = cocktail;
    this.showIngredient.emit(this.selectedCocktail);
    // if(this.cocktailToAdd != null){
    //   this.cocktails.push(this.cocktailToAdd);
    // }
  }
  ngOnInit(): void {
    this.selectedCocktail = this.cocktails[0];
    this.qte = 0;
    this.panier = new Panier();
  }

  // tslint:disable-next-line:typedef
  createCocktail($event: MouseEvent) {
    let c = null;
    c = this.newCocktail;
    // c.ingredients.push(this.ingredientNewCock);
    this.cocktails.push(c);
    this.newCocktail = new Cocktail();
  }



  // tslint:disable-next-line:typedef
  deleteCocktail($event: MouseEvent) {
    this.i = this.cocktails.indexOf(this.selectedCocktail);
    this.cocktails.splice(this.i, 1);
    this.i = 0;
  }

  // tslint:disable-next-line:typedef
  AjoutPanier($event: MouseEvent) {
    this.cardServ.panier.qteCock.set( this.selectedCocktail, this.qte);
    // console.log(this.panier);
    this.showPanier.emit(this.panier);
  }
}
