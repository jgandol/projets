import { Ingredient } from './ingredient';

export class Cocktail {
  id: number;
  nom: string;
  prix: number;
  ingredients: Ingredient[];


  constructor(id?: number, nom?: string, prix?: number, ingredients?: Ingredient[]) {
    this.id = id;
    this.nom = nom;
    this.prix = prix;
    if(ingredients == null){
      this.ingredients = new Array<Ingredient>();
    } else this.ingredients = ingredients;

  }


}
