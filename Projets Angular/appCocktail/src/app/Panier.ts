import { Cocktail} from './cocktail';
import {Ingredient} from './ingredient';
import set = Reflect.set;

export class Panier {
  public id: number;
  public qteCock = new Map();
  // tslint:disable-next-line:variable-name
  private _total = 0;

  // tslint:disable-next-line:typedef
  public setTotale(): number {
    let tot = this._total;
    if (this.qteCock) {
      // tslint:disable-next-line:typedef only-arrow-functions
      this.qteCock.forEach(function( clef, valeur ) {
        tot += clef * valeur.prix;
        // console.log(clef + ',' + valeur.prix);
        // console.log(tot);
      });
      // this._total = tot;
      return tot;
    } else { return 0; }
  }


  set total(value: number) {
    this._total = value;
  }
}
