import { Component, OnInit } from '@angular/core';
import { Cocktail } from './cocktail';
import {Panier} from './Panier';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  selectedCocktail: Cocktail;
  constructor(){
  }

  // tslint:disable-next-line:typedef
  setDetailIngredient(cocktail){
    this.selectedCocktail = cocktail;
  }
  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit(): void {
  }


}
