import {Component, HostListener} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bonjour';
  monType = 'text';
  bgColor = 'red';
  left = -50;
  down = 0;

  constructor() {
    setTimeout(() => {
      this.monType = 'date';
    }, 3000);
  }

  public changeColor(color: string): void {
    if (this.bgColor === color) {
      this.bgColor = 'red';
    } else {
      this.bgColor = color;
    }
    this.left = this.left + 10;
  }

  @HostListener('window:keydown', ['$event'])
  @HostListener('window:right', ['$event'])
  public doKeydown(event: KeyboardEvent): void {
    console.log(event);
    if (event.key === 'ArrowLeft') {
      if (this.left >= -490) {
        this.left = this.left - 10;
      }
    }
    if (event.key === 'ArrowUp') {
      if (this.down <= 420) {
        this.down = this.down + 10;
      }
    }
    if (event.key === 'ArrowRight') {
      if (this.left <= -50) {
        this.left = this.left + 10;
      }
    }
    if (event.key === 'ArrowDown') {
      if ( this.down >= 10) {
        this.down = this.down - 10;
      }
    }
  }
}
