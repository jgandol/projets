
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="login" class="metier.Login" scope="request">
    <jsp:setProperty property="*" name="login" />
</jsp:useBean>

<%
    if(login.isConnu()) {
%>
<jsp:forward page="afficheView.jsp"/>
<%
    }
%>
<jsp:forward page="login.jsp"/>


