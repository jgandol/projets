<%--
  Created by IntelliJ IDEA.
  User: jeremiepop
  Date: 19/02/2020
  Time: 16:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>index.jsp</title>
    <link href="css/styles.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="contenu">
        <form method="POST" action="doCheckLogin.jsp">
            <p class="double">
                <label for = "f1-nom">Nom :</label>
                <input type="text" id="f1-nom" name="login" value="${login.nom}"/>
                <br />
            </p>

            <p class="double">
                <label for = "f1-pw">Password :</label>

                <input type="text" id="f1-pw" name="password"> <span class="error">${login.msgErreur}
                    </span>
                <br />
            </p>

            <input type="submit" value="login">
        </form>
    </div>
</body>

</html>
