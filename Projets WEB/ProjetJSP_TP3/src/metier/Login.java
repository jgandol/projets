package metier;

public class Login {
    // Proprietes
    private String nom = "";
    private String password = "";
    //private String msgErreur="";
    //private Boolean connu;

    public String getNom() {
        return nom;
    }
    public void setNom(String login) {
        this.nom = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    // Regle metier ( elle est un GETTER )
    public String getMsgErreur(){
        return (this.isConnu() ? "" : "Inconnu");
    }

    public boolean isConnu(){
        return nom.equals("scott") && password.equals("tiger");
    }

    // Constructeur par defaut
    public Login() {}

}
