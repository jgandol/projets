<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: jeremiepop
  Date: 20/02/2020
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Description d'un produit</title>
</head>
<body>

<!-- recuperer le produit du catalogue -->
<c:set var="produit" value="${ens.catalogue[param.id]}" scope="page" />
<h1>${produit.nom}</h1>

${produit.descr} <br />
${produit.prix}
<p />


<c:url var="ajoutAuPanierURL" value="ajoutAuPanier.jsp" >
    <c:param name="id" value="${produit.id}" />
</c:url>

<a href="${ajoutAuPanierURL}"> Ajouter ce produit au panier </a>


</body>
</html>
