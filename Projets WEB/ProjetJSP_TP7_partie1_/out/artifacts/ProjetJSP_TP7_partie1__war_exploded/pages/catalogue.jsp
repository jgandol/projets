<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: jeremiepop
  Date: 20/02/2020
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Catalogue des Produits</title>
    </head>
    <body>
        <h1>Catalogue des produits</h1>
        Veuillez choisir un livre dans le catalogue afin d'obtenir des infos sur ce dernier et d'acheter une copie


    </body>
</html>


<!-- Genere une liste de livres -->
<jsp:useBean id="ens" scope="application" class="beans.CatalogueBean"/>
<jsp:useBean id="panier" class="beans.PanierBean"/>
<ul>
    <c:forEach items="${ens.catalogue}" var="association">
        <c:url var="produitURL" value="produit.jsp">
            <c:param name="id" value="${association.key}"/>
        </c:url>
        <li>
            <a href="${produitURL}">${association.value.nom}</a>
        </li>
    </c:forEach>
</ul>

<!-- Affiche le contenu du panier, si il existe -->
<c:if test="${!empty panier.listeProduits}">
    Votre panier contient les livres suivants :
    <p />
    <table>
    <c:forEach items="${panier.listeProduits}" var="produit">
        <tr>
            <td>${produit.nom}</td>
            <td>
                <fmt:formatNumber value="${produit.prix}" type="currency" />
            </td>
        </tr>
    </c:forEach>
        <tr><td colspan="2"><hr /></td> </tr>
        <tr>
            <td><b>Total :</b></td>
            <td>
                <fmt:formatNumber value="${panier.total}" type="currency" />
            </td>
        </tr>


    </table>
</c:if>
