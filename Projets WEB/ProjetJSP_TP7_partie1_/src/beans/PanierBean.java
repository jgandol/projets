package beans;

import java.io.*;
import java.util.*;


public class PanierBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 2L;
    // attribut d'instance
    private Set<ProduitBean> listeProduits = new HashSet<>();

    // attribut expose via un GETTER
    public Set<ProduitBean> getListeProduits() {
        return listeProduits;
    }

    // setter necessaire pour l'ajoit d'un produit
    public void setArticle(ProduitBean livre){
        System.out.println("ajout");
        if(livre!= null)
            this.listeProduits.add(livre);
    }

    public PanierBean(){
        ProduitBean prod = new ProduitBean();
        prod.setId("1000");
        prod.setNom("JavaServer Pages");
        prod.setDescr("Learn how to develop a JSP based web application.");
        prod.setPrix(32.95f);

        listeProduits.add(prod);
    }

    // Regle metier (utilisée dans catalogue.jsp)
    public float getTotal() {
        float total = 0;
        for(ProduitBean produit : listeProduits) {
            float price = produit.getPrix();
            total = total + price;
        }
        return total;
    }


    @Override
    public String toString() {
        return "PanierBean [listeProduits=" + listeProduits + "]";
    }

    public static void main(String[] args) {
        System.out.println(new PanierBean());
    }

}