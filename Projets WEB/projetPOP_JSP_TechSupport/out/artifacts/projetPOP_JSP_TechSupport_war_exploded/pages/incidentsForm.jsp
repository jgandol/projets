<%-- 
    Document   : index
    Created on : 11 déc. 2009, 09:28:59
    Author     : didier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
      Insert title here
    </title>
    <link href="css/formulaire.css" rel="stylesheet" type="text/css" >
  </head>
  <body>
    <form method="post" action="techSupport.jsp">
      <p class="titre">
        Coordonnées
      </p>
      <fieldset id="coordonnees">
        <legend></legend>
        <p id="email">
          <label>
            Email:
          </label>
            <input type="email" name="email" size="30" required="true"/>
          <br />
        </p>
        <p id="software">
          <label>
            Logiciel :
          </label>

          <select name="software">
            <option value="Word">
              Microsoft Word
            </option>
            <option value="Excel">
              Microsoft Excel
            </option>
            <option value="Access">
              Microsoft Access
            </option>
          </select>
        </p>
        <p id="systeme">
          <label>
            Système :
          </label>
          <select name="os">
            <option value="95">
              Windows 95
            </option>
            <option value="98">
              Windows 98
            </option>
            <option value="NT">
              Windows NT
            </option>
          </select>
        </p>
      </fieldset>

      <p class="titre">
        Description du problème
      </p>
      <fieldset id="message">
          <legend></legend>
        <textarea name="problem" cols="50" rows="4" >
        </textarea>
      </fieldset>

      <div style="margin-left: 120px;">
                <input type="submit" value="Submit" /> 
                <input type="reset" value="reset" />
      </div>
    </form>
  </body>
</html>

