<%--
    Document   : regForm
    Created on : 22 oct. 2008, 10:45:49
    Author     : didier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Enregistrement client</title>
        <link href="css/styles.css" rel="stylesheet" type="text/css" >
    </head>
    <body>
    <h1> Enregistrement client</h1>

    <form id="registerform"
          class="cssform"
          action="register.jsp"
          method="POST">
        <p>
            <label for="prenom">Prénom</label>
            <input type="text" 
                   id="prenom" 
                   name="firstName" 
                   value="" 
                   required="true"/>
        </p>
        
        <p>
            <label for="nom">Nom</label>
            <input type="text" 
                   id="nom" 
                   name="lastName" 
                   value="" 
                   required="true"/>
        </p>
        
        <p>
            <label for="telephone">Téléphone</label>
            <input type="tel"
                   id="telephone" 
                   name="phoneNumber"
                   value=""
                   required="true"/>
        </p>
        <br />
        <div style="margin-left: 150px;">
            <input type="submit" value="Submit" /> 
            <input type="reset" value="reset" />
        </div>
    </form>
</body>
</html>
