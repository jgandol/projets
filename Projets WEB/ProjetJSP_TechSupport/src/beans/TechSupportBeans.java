package beans;

import db_utils.MyConnection;

import javax.xml.transform.Result;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TechSupportBeans implements Serializable {
    //Conseillé pour la gestion de version de la sérialisation
    private static final long serialVersionUID = 1L;

    //Correspond aux aprametres des deux formulaires html
    //formulaire enregistrement incident
    private String email;
    private String software;
    private String os;
    private String problem;

    //formulaire enregistrement client
    private String firstName;
    private String lastName;
    private String phoneNumber;

    //requetes
    private static String insertStatementStr =
            "insert into CUSTOMERS values(?, ?, ?, ?)";
    private static String selectCustomerStr =
            "select fname, lname, phone " +
                    " from CUSTOMERS " +
                    " where LOWER(email) = ? ";

    private static String insertSupp_request_Str =
            "insert into SUPP_REQUESTS(email,software,os,problem) " +
                    " values (?, ?, ?, ?)";

    public TechSupportBeans() {
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }
    public void setSoftware(String software) {
        this.software = software;
    }
    public void setOs(String os) {
        this.os = os;
    }
    public void setProblem(String problem) {
        this.problem = problem;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public static void setInsertStatementStr(String insertStatementStr) {
        TechSupportBeans.insertStatementStr = insertStatementStr;
    }
    public static void setSelectCustomerStr(String selectCustomerStr) {
        TechSupportBeans.selectCustomerStr = selectCustomerStr;
    }
    public static void setInsertSupp_request_Str(String insertSupp_request_Str) {
        TechSupportBeans.insertSupp_request_Str = insertSupp_request_Str;
    }

    public String getEmail() {
        return email;
    }
    public String getSoftware() {
        return software;
    }
    public String getOs() {
        return os;
    }
    public String getProblem() {
        return problem;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public static String getInsertStatementStr() {
        return insertStatementStr;
    }
    public static String getSelectCustomerStr() {
        return selectCustomerStr;
    }
    public static String getInsertSupp_request_Str() {
        return insertSupp_request_Str;
    }

    public void registerSupportRequest() throws SQLException {
        PreparedStatement insertStatement = MyConnection.getInstance().prepareStatement(insertSupp_request_Str);

        insertStatement.setString(1, email.toLowerCase());
        insertStatement.setString(2, software);
        insertStatement.setString(3, os);
        insertStatement.setString(4, problem);

        insertStatement.executeUpdate();

        //On verifie si le client est enregistre ou non
        PreparedStatement selectStatement = MyConnection.getInstance().prepareStatement(selectCustomerStr);
        selectStatement.setString(1,this.email.toLowerCase());

        ResultSet rs =selectStatement.executeQuery();

        if(rs.next()){
            System.out.println("Le client est enregistré");
            this.setFirstName("fname");
            this.setLastName("lname");
            this.setPhoneNumber("phone");

            // Le client est enregistré - On va a la page "response.jsp
            this.registered = true;
        } else {
            this.registered = false;
        }
    }

    private boolean registered;

    public boolean isRegistered(){return registered; }

    public void registerCustomer() throws SQLException{
        PreparedStatement insertStatement = null;

        try{
            MyConnection.getInstance().setAutoCommit(false);
            insertStatement = MyConnection.getInstance().prepareStatement(insertStatementStr);

            insertStatement.setString(1, email.toLowerCase());
            insertStatement.setString(2, software);
            insertStatement.setString(3, os);
            insertStatement.setString(4, problem);

            insertStatement.executeUpdate();
            MyConnection.getInstance().commit();
        } catch (SQLException ex) {
            MyConnection.getInstance().rollback();
            throw ex;
        }

    }

    public static void main(String[] args) {
        TechSupportBeans tc = new TechSupportBeans();

        tc.setEmail("ngator@gmail.com");
        tc.setFirstName("Nathalie");
        tc.setLastName("Gator");
        tc.setOs("W95");
    }
}
