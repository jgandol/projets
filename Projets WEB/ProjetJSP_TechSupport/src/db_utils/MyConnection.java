package db_utils;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {
    private final static String URL ="jdbc:mysql://localhost:3306/TechSupport";
    private final static String USER = "jeremie";
    private final static String PW = "admin";

    //singleton
    private static Connection INSTANCE = null;

    //init singleton
    static {
        try{
            //open connection bdd
            INSTANCE = DriverManager.getConnection(URL,USER,PW);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    //Singleton accesseur
    public static Connection getInstance(){
        return INSTANCE;
    }
}
