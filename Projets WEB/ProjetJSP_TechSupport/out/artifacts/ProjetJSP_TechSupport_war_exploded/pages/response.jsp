<%--
  Created by IntelliJ IDEA.
  User: jeremiepop
  Date: 20/02/2020
  Time: 14:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Réponse du Service Client</title>
</head>
<body>
    <h1> Demande de support technique reçue</h1>

    <p>Merci de votre demande</p>
    <p>Celle-ci a été enregistrée. Une réponse sera apportée dans les trois jours ouvrables.</p>

    <jsp:include page="banner.jsp"/>
</body>
</html>
