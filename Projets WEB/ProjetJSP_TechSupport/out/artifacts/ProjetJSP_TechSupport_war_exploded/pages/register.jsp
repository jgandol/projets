<%--
  Created by IntelliJ IDEA.
  User: jeremiepop
  Date: 20/02/2020
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="beans.*" %>
<jsp:useBean id="techSupportBean" scope="session" class="beans.TechSupportBeans">
</jsp:useBean>

<jsp:setProperty name="techSupportBean" property="*"/>

<-- Regle metier : enregistrement client -->
<% techSupportBean.registerCustomer(); %>

<jsp:forward page="response.jsp"></jsp:forward>