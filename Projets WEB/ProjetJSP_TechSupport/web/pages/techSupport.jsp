<%--
  Created by IntelliJ IDEA.
  User: jeremiepop
  Date: 20/02/2020
  Time: 13:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" errorPage="error.jsp" import="beans.*" %>
<-- Creation et init du composant Metier -->
<jsp:useBean id="techSupportBean" scope="session" class="beans.TechSupportBeans">
    <jsp:setProperty name="techSupportBean" property="*" />
</jsp:useBean>

<-- Regle metier : enregistrement incident Helas encore du Java -->
<% techSupportBean.registerSupportRequest(); %>

<-- Regle metier : le client existe t il dans la bdd? -->
<c if test="${techSupportBean.registered}">
    <jsp:forward page="response.jsp"></jsp:forward>
</c>

<jsp:forward page="regForm.jsp"/>