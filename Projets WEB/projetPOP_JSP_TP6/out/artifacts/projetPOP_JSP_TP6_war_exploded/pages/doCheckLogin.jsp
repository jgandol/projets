<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

  <jsp:useBean id="login" class="metier.Login" scope="request">
       <jsp:setProperty property="*" name="login"/>
  </jsp:useBean>

    <%
        if(login.isConnu()) {
    %>
<c:redirect url="afficheView.jsp">
<c:param name="nom" value="${login.nom}"/>
</c:redirect>
    <%
        }
    %>

    <jsp:forward page="login.jsp"/>



	<c:if test="${login.connu }">
     <jsp:forward page="afficheView.jsp"/>
    </c:if>

    <jsp:forward page="login.jsp"/>