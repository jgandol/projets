<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link href="css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div id="contenu">
		<form method="POST" action="doCheckLogin.jsp">
			<fieldset>
				<legend>Veuillez vous authentifier.</legend>
                <hr />

				<p class="double">
					<label for="f1-nom">Nom : </label>
					<input type="text" 
					       id="f1-nom"
						   name="nom"
						   value="${login.nom}" >
				    <br />
				</p>
				
				<p class="double">
					<label for="f1-pw">Password : </label> 
					<input type="text" 
					       id="f1-pw"
						   name="password" > 
				    <span class="erreur"> 
				         ${login.msgErreur }
				    </span>
					<br />
				</p>
				
				<p class="simple">
					<input type="submit" value="Login">
				</p>
			</fieldset>
		</form>
	</div>
</body>
</html>