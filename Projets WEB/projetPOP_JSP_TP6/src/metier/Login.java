package metier;

public class Login {
	private String nom="";
	private String password="";
	//private String msgErreur="";
	//private boolean connu;


	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = (nom==null?"":nom);
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = (password==null?"":password);
	}

	public String getMsgErreur() {
		return (this.isConnu() ?"":"Inconnu");
	}

	public boolean isConnu() {
		return (nom.equals("scott") && password.equals("tiger"));
	}

	public Login() {
		super();
	}

}
