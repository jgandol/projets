package modele;

import java.io.IOException;
import java.sql.*;

public class Test3 {

    private final static String SQLselectEmployeesofDept =
                    " SELECT ename, sal, comm, deptno FROM employee "
                    + " WHERE deptno = ?"
                    + " order by ename ";

    private final static String SQLupdateEmployeesofDept =
            "UPDATE employee "
                    + " set sal = sal + (sal* ?)/100 "
                    + " where deptno = ? ";

    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {

        // Lien BDD
        String URL = "jdbc:mysql://localhost:3306/Employees";
        String USER = "jeremie";
        String PW = "admin";


        Connection conn = null;
        PreparedStatement ps = null;
        PreparedStatement psUp = null;

        try {
            //Connection à la BDD
            conn = DriverManager.getConnection(URL, USER, PW);
            //conn = MyConnection.getInstance();

            conn.setAutoCommit(false);

            ps = conn.prepareStatement(SQLselectEmployeesofDept);
            psUp = conn.prepareStatement(SQLupdateEmployeesofDept);

            //AVANT
            ps.setInt(1,30);
            ResultSet rset = ps.executeQuery();

            while(rset.next()){
                String nom = rset.getString("ename");
                Double salaire = rset.getDouble("sal");
                Double comm = rset.getDouble("comm");

                String commentaire = (rset.wasNull()? " / pas de commission ": " / commission de : " + comm);
                System.out.println(nom + " gagne : " + salaire + "€" + commentaire);

            }


            //MISE A JOUR

            psUp.setDouble(1,15);
            psUp.setInt(2,10);

            int nb = psUp.executeUpdate();
            rset = ps.executeQuery();



            conn.commit();
        }catch (SQLException ex){
            ex.printStackTrace();
            conn.rollback();

        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
