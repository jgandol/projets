package modele;

import java.io.IOException;
import java.sql.*;

public class Test31 {


    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {

        // Lien BDD
        String URL = "jdbc:mysql://localhost:3306/Employees";
        String USER = "jeremie";
        String PW = "admin";


        Connection conn = null;
        PreparedStatement ps = null;

        String SQLrecupererNumDep =
                " select deptno, sum(sal) somme " +
                        " from employee " +
                        " group by deptno" +
                        " order by somme desc" +
                        " limit 1";

        String SQLlisteEmployesDep =
                " select ename, dept.deptno, sal, comm " +
                        " from employee join dept on dept.deptno = employee.deptno" +
                " where dept.deptno = ?";

        String SQLselectDeOUf =
                  "Select ename, deptno, sal, comm "
                + "from employee "
                + "where deptno in ( "
                +       " select d "
                +       " from (select deptno as d, sum(sal) as somme "
                +        " from employee "
                +       "  group by deptno "
                +       ") as aux "
                +       " where somme >= all "
                +               "(select sum(sal) "
                +               "from employee "
                +               "group by deptno "
                +       ")"
                + ")";

        try {
            //Connection à la BDD
            conn = DriverManager.getConnection(URL, USER, PW);
            //conn = MyConnection.getInstance();
            conn.setAutoCommit(false);

            ps = conn.prepareStatement(SQLselectDeOUf);
            /*ps = conn.prepareStatement(SQLrecupererNumDep);
            ResultSet rset = ps.executeQuery();
            rset.next();
            int deptnum = rset.getInt("deptno");

            ps = conn.prepareStatement(SQLlisteEmployesDep);
            ps.setInt(1,deptnum);
            rset = ps.executeQuery();
           */
            ResultSet rset = ps.executeQuery();
            while(rset.next()){
                String nom = rset.getString("ename");
                Double salaire = rset.getDouble("sal");
                Double comm = rset.getDouble("comm");

                String commentaire = (rset.wasNull()? " / pas de commission ": " / commission de : " + comm);
                System.out.println(nom + " gagne : " + salaire + "€" + commentaire);
            }


            conn.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
            conn.rollback();

        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    private static ResultSet getRset(ResultSet rset) {
        return rset;
    }
}
