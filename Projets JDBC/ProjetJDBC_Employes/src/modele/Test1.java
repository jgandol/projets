package modele;

import java.io.IOException;
import java.sql.*;

public class Test1 {
    public void test1() throws SQLException, ClassNotFoundException, java.io.IOException {

        Connection conn = null;
        Statement stmt = null;


        try {
            //Chargement du pilote
            //Class.forName(DRIVER);
            //Connection à la BDD
            //conn = DriverManager.getConnection(URL, USER, PW);
            //conn = MyConnection.getInstance();
            conn = MyConnection.getInstance();
            stmt = conn.createStatement();
            ResultSet rset = stmt.executeQuery(
                    " select dname, count(*) as nbre "
                            + " from     employee e "
                            + " inner join dept d "
                            + " using(deptno) "
                            + " group by dname "
                            + " order by dname"
            );

            while(rset.next()){
                System.out.println(rset.getString("dname") + " a " + rset.getInt("nbre") + " employes");

            }

        } finally {
            if(stmt != null){
                stmt.close();
            }

        }
    }//main

     public static void main(String[] args) {
        Test1 ts = new Test1();
        try {
            ts.test1();
        }catch (ClassNotFoundException | SQLException | IOException e) {
            e.printStackTrace();
        }
    }
}
