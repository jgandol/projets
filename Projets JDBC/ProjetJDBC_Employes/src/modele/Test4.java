package modele;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Test4{

    private final static String SQLselectEmployeesofDept =
            " SELECT ename, sal, comm, deptno FROM employee "
                    + " WHERE deptno = ?"
                    + " order by ename ";

    private final static String SQLupdateEmployeesofDept =
            "UPDATE employee "
                    + " set sal = sal + (sal* ?)/100 "
                    + " where deptno = ? ";



    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {
        Connection conn = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;



        try{
            conn =MyConnection.getInstance();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement(SQLselectEmployeesofDept);
            ps2 = conn.prepareStatement(SQLupdateEmployeesofDept);

            ResultSet rset;

            //Augmentation des employés de certains departements
            // Les données sont préalablement enregistrées dans un tableau Java
            // le dept 20 est augmenté de 5%, le dept 30 est augmenté de 8%
            // Le dept 10 est augmenté de 15%

            int[][] augmentations = { { 20,5}, {30,8},{10,15} };

            // AVANT AUGMENTATION
            for(int i = 0; i< augmentations.length; i++){
                ps.setInt(1,augmentations[i][0]);
                rset = ps.executeQuery();
                System.out.println("*** Dept " + augmentations[i][0] + " avant augmentation :");

                //recuperer les données
                while(rset.next()){
                    System.out.println(rset.getString(1) + " gagne "+rset.getFloat(2));
                }
                System.out.println();
            }

            // APRES AUGMENTATION




            conn.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
            conn.rollback();

        } finally {
            if (ps != null) {
                ps.close();
            }
        }

    }

}
