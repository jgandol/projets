package modele;

import java.io.IOException;
import java.sql.*;

public class Test2 {
    private static String SQLajoutEmploye = " insert into employee (empno, ename, sal, deptno) "
            + " values(6789, 'BELIVIER', 500, 10) ";
    private static String SQLajoutEmploye2 = " insert into employee (empno, ename, sal, deptno) "
            + " values(6790, 'TOLIVIER', 500, 10)";
    private static String SQLajoutEmploye3 = "insert into employee (empno, ename, sal, deptno) "
            + " values(6791, 'MARCOLIV', 500, 10)";

    /*private static String SQLmodificationEmploye =
                    " update employee e1"
                    + " set sal = sal * 1.1 "
                    + " where sal is not null "
                    + "     and 2>= (select count(distinct sal) from employee where sal < e1.sal ) ";   */
    private static String SQLmodificationEmploye =
             "UPDATE employee e,\n" +
                     "(select e2.EMPNO as id,\n" +
                     "(select count(distinct sal) from employee e3 where e3.sal < e2.sal)as num FROM employee e2) as test\n" +
                     "set sal = sal *1.1\n" +
                     "where 2>= test.num\n" +
                     "and test.id=e.EMPNO;\n";


    // e1 est une solution ssi on ne put trouver quer 0, 1 ou 2 salaires.
    // inférieurs au sien (e1.sal)

    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {

        // Lien BDD
        String DRIVER = "com.mysql.jdbc.driver";
        String URL = "jdbc:mysql://localhost:3306/Employees";
        String USER = "jeremie";
        String PW = "admin";

        //Class.forName(DRIVER);
        Connection conn = null;
        Statement stmt = null;

        try {
            //Connection à la BDD
            conn = DriverManager.getConnection(URL, USER, PW);
            //conn = MyConnection.getInstance();
            conn.setAutoCommit(false);

            stmt = conn.createStatement();

            //INSERTION BD
            int nbLignes = stmt.executeUpdate(SQLajoutEmploye);
            System.out.println(nbLignes);
            //INSERTION BD
            int nbLignes2 = stmt.executeUpdate(SQLajoutEmploye2);
            System.out.println(nbLignes);
            //INSERTION BD
            int nbLignes3 = stmt.executeUpdate(SQLajoutEmploye3);
            System.out.println(nbLignes);

            // MODIFICATION BD
            nbLignes = stmt.executeUpdate(SQLmodificationEmploye);

            // VALIDATION DE LA TRANSACTION
            conn.commit();
            System.out.println(nbLignes);


        }catch (SQLException ex){
            ex.printStackTrace();
            conn.rollback();

        } finally{
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
